<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    <xsl:template match = "/MAGE-ML">
      <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="DesignElement_package">
    </xsl:template>
    
    <xsl:template match="BioAssayData_package">
      <BioAssayData_package>
      <xsl:apply-templates select="//DesignElementDimension_assnlist"/>
      <xsl:apply-templates select="//BioAssayDimension_assnlist"/> 
      <xsl:apply-templates select="//QuantitationTypeDimension_assnlist"/> 
      <xsl:apply-templates select="//BioAssayMap_assnlist"/> 
      <xsl:apply-templates select="//BioAssayData_assnlist"/> 
      </BioAssayData_package>  
    </xsl:template>
  
    <xsl:template match="DesignElementDimension_assnlist">
    </xsl:template>
    

    <xsl:template match="node()|@*">
      <xsl:copy-of select="." />
    </xsl:template>
</xsl:stylesheet>
