package RAD::MR_T::MageImport::Test::TestMain;

use strict;

use base qw(Test::Unit::TestCase);
use Carp;
use Log::Log4perl qw(get_logger :levels);
use XML::Simple;
use Data::Dumper;
use Error qw(:try);

use RAD::MR_T::MageImport::ServiceFactory;

use  GUS::ObjRelP::DbiDatabase;
use GUS::Supported::GusConfig;

use GUS::Model::Study::Study;
use GUS::Model::RAD::StudyAssay;
use GUS::Model::RAD::Assay;

use RAD::MR_T::MageImport::Service::Reader::MockReader;
use RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator;
use RAD::MR_T::MageImport::Service::Tester::SqlTester;

Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $sLogger = get_logger("RAD::MR_T::MageImport::Service");

my ($loadMageDoc, $testCount);

sub new {
  my $self = shift()->SUPER::new(@_);

  $testCount = scalar $self->list_tests();

  my @properties;

  my $configFile = "$ENV{GUS_HOME}/config/gus.config";

  unless(-e $configFile) {
    my $error = "Config file $configFile does not exist.";
    die $error;
  }

  my $config = GUS::Supported::GusConfig->new($configFile);
  
  my $login       = $config->getDatabaseLogin();
  my $password    = $config->getDatabasePassword();
  my $core        = $config->getCoreSchemaName();
  my $dbiDsn      = $config->getDbiDsn();
  my $oraDfltRbs  = $config->getOracleDefaultRollbackSegment();

  my $database;
  unless($database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    $database = GUS::ObjRelP::DbiDatabase->new($dbiDsn, $login, $password,
                                                  1,0,1,$core, $oraDfltRbs);
  }

  $self->{_dbi_database} = $database;

  return $self;
}

sub set_up {
  my ($self) = @_;
  my $database =   GUS::ObjRelP::DbiDatabase->getDefaultDatabase();
  my $val = 0;  

  $database->setCommitState(0);

  $database->setDefaultProjectId($val);
  $database->setDefaultUserId($val);
  $database->setDefaultGroupId($val);
  $database->setDefaultAlgoInvoId(-99);

  $database->setDefaultUserRead(1);
  $database->setDefaultUserWrite(1);
  $database->setDefaultGroupRead($val);
  $database->setDefaultGroupWrite($val);
  $database->setDefaultOtherRead($val);
  $database->setDefaultOtherWrite($val);
}

sub tear_down {
  my $self = shift;

  $testCount--;

  my $database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase();
  $database->{'dbh'}->rollback() if $database->{'dbh'};

  # LOG OUT AFTER ALL TESTS ARE FINISHED
  if($testCount <= 0) {
    $database->logout();
    GUS::ObjRelP::DbiDatabase->setDefaultDatabase(undef);
    print STDERR "LOGGING OUT FROM DBI DATABASE\n";
  }
}

sub test_run {
  my ($self) = @_;

  try {
    my $mLogger = get_logger("RAD::MR_T::MageImport");
    #  my $config = XMLin("config_fortestmain.xml");
    my $config = XMLin("config_testMageTab.xml", ForceArray => 1);
    my $serviceFactory = RAD::MR_T::MageImport::ServiceFactory->new($config);

    my $reader =  $serviceFactory->getServiceByName('reader');

    $mLogger->info("parse the mage file", $config->{service}->{reader}->{property}->{value});
    my $docRoot = $reader->parse();

    if(my $processor = $serviceFactory->getServiceByName('processor')) {
      $mLogger->info("process the value objects using ", ref($processor));
      $processor->process($docRoot);
    }

    if(my $validator = $serviceFactory->getServiceByName('validator')){

      $mLogger->info("validate the value objects using ", ref($validator));
      $validator->check($docRoot);
    }

    my $translator = $serviceFactory->getServiceByName('translator');
    $mLogger->info("translate the value objects to GUS objects using ", ref($translator));
    my $study = $translator->mapAll($docRoot);

    $mLogger->info("submit GUS objects");
    $self->submit($study) if $study;

    if(my $tester = $serviceFactory->getServiceByName('tester')) {
      $self->assert($tester->test());
    }

    if(my $reporter = $serviceFactory->getServiceByName('reporter')){
      $reporter->report();
    }
  } catch Error with {
    my $e = shift;
    print $e->stacktrace();
    $e->throw();
  };

}

sub submit{
  my ($self, $study) = @_;

#this will submit study, studydesign, studyfactor and assays if studyAssays are set
  if(my @studyAssay = $study->getChildren("GUS::Model::RAD::StudyAssay")){

    foreach my $studyAssay (@studyAssay){
      my $assay = $studyAssay->getParent("GUS::Model::RAD::Assay");
      $study->addToSubmitList($assay);
    }
  }

  if(my @studyBioMaterial = $study->getChildren("GUS::Model::RAD::StudyBioMaterial")){
    foreach my $studyBioMaterial (@studyBioMaterial){
      my $biomat = $studyBioMaterial->getParent("GUS::Model::Study::BioMaterialImp");
      $study->addToSubmitList($biomat);
    }
  }

  $study->submit;
}
1;
