package RAD::MR_T::MageImport::Test::TestAcquisitionVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::AcquisitionVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::AcquisitionVO->new({name=>"test",
                                                               uri=>"test", 
                                                               channels=>[RAD::MR_T::MageImport::VO::OntologyEntryVO->new()], 
                                                               quantifications=>[RAD::MR_T::MageImport::VO::QuantificationVO->new()],
                                                               parameterValues=>[RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'testName', value => 'testValue'}),
                                                                                ],
                                                              });
}


sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::AcquisitionVO'), "check object type ok");
  $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getUri {
  my $self = shift;
  my $setValue = $self->{vo}->getUri();
  $self->assert("test" eq $setValue);
}

sub test_setUri {
  my $self = shift;
  $self->{vo}->setUri("test2");
  my $setValue = $self->{vo}->getUri();
  $self->assert("test2" eq $setValue);
}

sub test_getChannels {
  my ($self) = @_;
  my $setValue = $self->{vo}->getChannels();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setChannels {
  my $self = shift;
  $self->{vo}->setChannels([RAD::MR_T::MageImport::VO::OntologyEntryVO->new()]);
  my $setValue = $self->{vo}->getChannels();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_addChannels {
  my $self = shift;
  $self->{vo}->addChannels((RAD::MR_T::MageImport::VO::OntologyEntryVO->new()));
  my $setValue = $self->{vo}->getChannels();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check added OntologyEntry object type");
}

sub test_getQuantifications {
  my ($self) = @_;
  my $setValue = $self->{vo}->getQuantifications();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::QuantificationVO'), "check object type ok");
}

sub test_setQuantifications {
  my $self = shift;
  $self->{vo}->setQuantifications([RAD::MR_T::MageImport::VO::QuantificationVO->new()]);
  my $setValue = $self->{vo}->getQuantifications();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::QuantificationVO'), "check object type ok");
}

sub test_getParameterValues {
  my ($self) = @_;
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('testValue', $setValue->[0]->getValue);
}

sub test_setParameterValues {
  my $self = shift;
  $self->{vo}->setParameterValues([RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'newValue'})]);
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('newValue', $setValue->[0]->getValue);
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
