package RAD::MR_T::MageImport::Test::TestDocRoot;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::StudyVO;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::VO::PersonVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::AffiliationVO;
use RAD::MR_T::MageImport::VO::ExternalDatabaseVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::TreatmentVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = 
    RAD::MR_T::MageImport::VO::DocRoot->new({
					     studyVO=>RAD::MR_T::MageImport::VO::StudyVO->new(),
					     assayVOs=>[RAD::MR_T::MageImport::VO::AssayVO->new()],
					     personVOs=>[RAD::MR_T::MageImport::VO::PersonVO->new()],
					     affiliationVOs=>[RAD::MR_T::MageImport::VO::AffiliationVO->new()],
					     externalDatabaseVOs=>[RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new()],
					     bioMaterialVOs=>[RAD::MR_T::MageImport::VO::BioMaterialVO->new()],
					     treatmentVOs=>[RAD::MR_T::MageImport::VO::TreatmentVO->new()],
					    });
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::DocRoot'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getStudyVO {
  my $self = shift;
  my $setValue = $self->{vo}->getStudyVO();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::StudyVO'), "check object type ok");
}

sub test_setStudyVO {
  my $self = shift;
  $self->{vo}->setStudyVO(RAD::MR_T::MageImport::VO::StudyVO->new());
  my $setValue = $self->{vo}->getStudyVO();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::StudyVO'), "check object type ok");
}

sub test_getAssayVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getAssayVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AssayVO'), "check object type again ok");
}

sub test_setAssayVOs {
  my $self = shift;
  $self->{vo}->assayVOs([RAD::MR_T::MageImport::VO::AssayVO->new()]);
  my $setValue = $self->{vo}->getAssayVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AssayVO'), "check AssayVO object type ok");
}

sub test_addAssayVOs {
  my $self = shift;
  $self->{vo}->addAssayVOs((RAD::MR_T::MageImport::VO::AssayVO->new()));
  my $setValue = $self->{vo}->getAssayVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::AssayVO'), "check added assayvo object type ok");
}


sub test_getPersonVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getPersonVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::PersonVO'), "check object type again ok");
}

sub test_setPersonVOs {
  my $self = shift;
  $self->{vo}->personVOs([RAD::MR_T::MageImport::VO::PersonVO->new()]);
  my $setValue = $self->{vo}->getPersonVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::PersonVO'), "check PersonVO object type ok");
}

sub test_addPersonVOs {
  my $self = shift;
  $self->{vo}->addPersonVOs((RAD::MR_T::MageImport::VO::PersonVO->new()));
  my $setValue = $self->{vo}->getPersonVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::PersonVO'), "check added PersonVO object type ok");
}

sub test_getAffiliationVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getAffiliationVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AffiliationVO'), "check object type again ok");
}

sub test_setAffiliationVOs {
  my $self = shift;
  $self->{vo}->affiliationVOs([RAD::MR_T::MageImport::VO::AffiliationVO->new()]);
  my $setValue = $self->{vo}->getAffiliationVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AffiliationVO'), "check AffiliationVO object type ok");
}

sub test_addAffiliationVOs {
  my $self = shift;
  $self->{vo}->addAffiliationVOs((RAD::MR_T::MageImport::VO::AffiliationVO->new()));
  my $setValue = $self->{vo}->getAffiliationVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::AffiliationVO'), "check added AffiliationVO object type ok");
}

sub test_getExternalDatabaseVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getExternalDatabaseVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ExternalDatabaseVO'), "check object type again ok");
}

sub test_setExternalDatabaseVOs {
  my $self = shift;
  $self->{vo}->externalDatabaseVOs([RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new()]);
  my $setValue = $self->{vo}->getExternalDatabaseVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ExternalDatabaseVO'), "check ExternalDatabaseVO object type ok");
}

sub test_addExternalDatabaseVOs {
  my $self = shift;
  $self->{vo}->addExternalDatabaseVOs((RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new()));
  my $setValue = $self->{vo}->getExternalDatabaseVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::ExternalDatabaseVO'), "check added ExternalDatabaseVO object type ok");
}

sub test_getBioMaterialVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getBioMaterialVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type again ok");
}

sub test_setBioMaterialVOs {
  my $self = shift;
  $self->{vo}->bioMaterialVOs([RAD::MR_T::MageImport::VO::BioMaterialVO->new()]);
  my $setValue = $self->{vo}->getBioMaterialVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check BioMaterialVO object type ok");
}

sub test_addBioMaterialVOs {
  my $self = shift;
  $self->{vo}->addBioMaterialVOs((RAD::MR_T::MageImport::VO::BioMaterialVO->new()));
  my $setValue = $self->{vo}->getBioMaterialVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check added BioMaterialVO object type ok");
}

sub test_getTreatmentVOs {
  my ($self) = @_;
  my $setValue = $self->{vo}->getTreatmentVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::TreatmentVO'), "check object type again ok");
}

sub test_setTreatmentVOs {
  my $self = shift;
  $self->{vo}->treatmentVOs([RAD::MR_T::MageImport::VO::TreatmentVO->new()]);
  my $setValue = $self->{vo}->getTreatmentVOs();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::TreatmentVO'), "check TreatmentVO object type ok");
}

sub test_addTreatmentVOs {
  my $self = shift;
  $self->{vo}->addTreatmentVOs((RAD::MR_T::MageImport::VO::TreatmentVO->new()));
  my $setValue = $self->{vo}->getTreatmentVOs();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::TreatmentVO'), "check added TreatmentVO object type ok");
}


sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
