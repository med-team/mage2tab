package RAD::MR_T::MageImport::Test::TestNamingAcqAndQuan;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan;
use RAD::MR_T::MageImport::Service::Processor;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;

 $self->{vo} = RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
}

sub test_NamingAcqAndQuan{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan->new($processor);

  $self->assert ($decoProcessor->process($self->{vo}) eq "RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan");
  my $docRoot = $self->{vo};
  foreach my $assay(@{$docRoot->getAssayVOs}){
    foreach my $acq(@{$assay->getAcquisitions}){
      my $name = $assay->getName;
      $self->assert_matches(qr($name), $acq->getName);
      foreach my $quan(@{$acq->getQuantifications}){
	$self->assert_matches(qr($name), $quan->getName);
      }
    }
  }
}


1;
