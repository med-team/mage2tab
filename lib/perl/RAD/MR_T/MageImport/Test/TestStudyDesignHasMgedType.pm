package RAD::MR_T::MageImport::Test::TestStudyDesignHasMgedType;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;


}


sub test_studyDesignHasMgedType {
  my $self = shift;

  my $reader =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("toxoTemplate.tab");
  $reader->setLogger($logger);
  $docRoot = $reader->parse();

  my $study = $docRoot->getStudyVO();

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType->new($validator);
  $decoValidator->setLogger($logger);

  try {
    $decoValidator->check($docRoot);
    $self->assert(1);
  } catch RAD::MR_T::MageImport::ValidatorException with { $self->assert(0); };

  $study->getDesigns->[0]->getTypes->[0]->setValue("badType");

  try {
    $decoValidator->check($docRoot);
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ValidatorException with { $self->assert(1); };
}

sub test_isMgedTerm {
 my $self = shift;

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 $validator->setLogger($logger);

 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType->new($validator);
 $decoValidator->setLogger($logger);

 $decoValidator->_setStudyDesignTypes ();

 my $result=$decoValidator->_isMgedTerm("test");

 $self->assert($result==0);

 $result=$decoValidator->_isMgedTerm("sex_design");

 $self->assert($result==1);

}
1;
