package RAD::MR_T::MageImport::Test::TestAssayVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::VO::AcquisitionVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;

use Error qw(:try);

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::AssayVO->
    new({name=>"test",
         studyName=>"test", 
         acquisitions=>[RAD::MR_T::MageImport::VO::AcquisitionVO->new()],
         parameterValues=>[RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'testName', value => 'testValue'}),],
         labeledExtracts=>[RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'testName', subclassView => 'LabeledExtract'})],
        }); 
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::AssayVO'), "check object type ok");
 
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getStudyName {
  my $self = shift;
  my $setValue = $self->{vo}->getStudyName();
  $self->assert("test" eq $setValue);
}

sub test_setStudyName {
  my $self = shift;
  $self->{vo}->setStudyName("test2");
  my $setValue = $self->{vo}->getStudyName();
  $self->assert("test2" eq $setValue);
}

sub test_getAcquisitions {
  my ($self) = @_;
  my $setValue = $self->{vo}->getAcquisitions();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AcquisitionVO'), "check object type ok");
}

sub test_setAcquisitions {
  my $self = shift;
  $self->{vo}->setAcquisitions([RAD::MR_T::MageImport::VO::AcquisitionVO->new()]);
  my $setValue = $self->{vo}->getAcquisitions();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::AcquisitionVO'), "check object type ok");
}

sub test_getParameterValues {
  my ($self) = @_;
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('testValue', $setValue->[0]->getValue);
}

sub test_setParameterValues {
  my $self = shift;
  $self->{vo}->setParameterValues([RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'newValue'})]);
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('newValue', $setValue->[0]->getValue);
}

sub test_getLabeledExtracts {
  my ($self) = @_;
  my $setValue = $self->{vo}->getLabeledExtracts();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
  $self->assert_equals('testName', $setValue->[0]->getName);
}

sub test_setLabeledExtracts {
  my $self = shift;
  $self->{vo}->setLabeledExtracts([RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'newName', subclassView => 'LabeledExtract'})]);
  my $setValue = $self->{vo}->getLabeledExtracts();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
  $self->assert_equals('newName', $setValue->[0]->getName);

  try {
    $self->{vo}->setLabeledExtracts([RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'newName', subclassView => 'BioSample'})]);
  } catch RAD::MR_T::MageImport::VOException::ObjectTypeError with {};

}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
