package RAD::MR_T::MageImport::Test::TestConfigChecking;

use base qw(Test::Unit::TestCase);
use XML::Simple;
use Data::Dumper;

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
  my $self = shift;

}

sub test_checkConfig{
  my $self = shift;

  my $config = XMLin("config.xml");
  my $services = $config->{service};

  my $reader = 0;
  my $translator = 0;

  foreach my $service ( keys %$services){
       $reader = 1 if $service eq 'reader';
       $translator = 1 if $service eq 'translator';
  }
  $reader = 1 if $services->{id} && $services->{id} eq 'reader';
  $self->assert($reader);
  $self->assert($translator);

}


sub test_checkConfig2{
  my $self = shift;

  my $config = XMLin("config_not_good.xml");
  my $services = $config->{service};

  my $reader = 0;
  my $translator = 0;

  foreach my $service ( keys %$services){
    $reader = 1 if $service eq 'reader';
    $translator = 1 if $service eq 'translator';
  }
  $reader = 1 if $services->{id} && $services->{id} eq 'reader';
  $self->assert_equals($reader, 1);
  $self->assert_equals($translator, 0);

}


sub test_checkConfig3{
  my $self = shift;

  my $config = XMLin("config_not_good2.xml");
  my $services = $config->{service};

  my $reader = 0;
  my $translator = 0;

  foreach my $service ( keys %$services){
    $reader = 1 if $service eq 'reader';
    $translator = 1 if $service eq 'translator';
  }
  $reader = 1 if $services->{id} && $services->{id} eq 'reader';
  $self->assert_equals($reader, 1);
  $self->assert_equals($translator, 0);

}
1;
