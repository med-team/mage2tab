package RAD::MR_T::MageImport::Test::TestStudyNameNotInGus;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;


}


sub test_studyNameNotInGus {
  my $self = shift;

  my $reader =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("toxoTemplate.tab");
  $reader->setLogger($logger);
  $docRoot = $reader->parse();

  my $study = $docRoot->getStudyVO();

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus->new($validator);
  $decoValidator->setLogger($logger);

  try {
    $decoValidator->check($docRoot);
    $self->assert(1);
  } catch RAD::MR_T::MageImport::ValidatorException with { $self->assert(0); };

  $study->setName("test");

  try {
    $decoValidator->check($docRoot);
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ValidatorException with { $self->assert(1); };

}

sub test_isGusStudy {
 my $self = shift;

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 $validator->setLogger($logger);

 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus->new($validator);

 my $result=$decoValidator->_isGusStudy("test");

 $self->assert_not_null($result);

 $result=$decoValidator->_isGusStudy("testtesttesttest");

 $self->assert_null($result);

}
1;
