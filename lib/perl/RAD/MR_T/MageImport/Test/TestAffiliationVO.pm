package RAD::MR_T::MageImport::Test::TestAffiliationVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::AffiliationVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::AffiliationVO->new({name=>"test", first=>"test", last=>"test", address=>"test", email=>"test"});
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::AffiliationVO'), "check object type ok");

}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test", $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2", $setValue);
}

sub test_getFirst {
  my $self = shift;
  my $setValue = $self->{vo}->getFirst();
  $self->assert("test", $setValue);
}

sub test_setFirst {
  my $self = shift;
  $self->{vo}->setFirst("test2");
  my $setValue = $self->{vo}->getFirst();
  $self->assert("test2", $setValue);
}

sub test_getLast {
  my $self = shift;
  my $setValue = $self->{vo}->getLast();
  $self->assert("test", $setValue);
}

sub test_setLast {
  my $self = shift;
  $self->{vo}->setLast("test2");
  my $setValue = $self->{vo}->getLast();
  $self->assert("test2", $setValue);
}

sub test_getAddress {
  my $self = shift;
  my $setValue = $self->{vo}->getAddress();
  $self->assert("test", $setValue);
}

sub test_setAddress {
  my $self = shift;
  $self->{vo}->setAddress("test2");
  my $setValue = $self->{vo}->getAddress();
  $self->assert("test2", $setValue);
}

sub test_getEmail {
  my $self = shift;
  my $setValue = $self->{vo}->getEmail();
  $self->assert("test", $setValue);
}

sub test_setEmail {
  my $self = shift;
  $self->{vo}->setEmail("test2");
  my $setValue = $self->{vo}->getEmail();
  $self->assert("test2", $setValue);
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
