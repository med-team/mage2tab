package RAD::MR_T::MageImport::Test::TestAssayHasLexAndNoHangingLex;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;

 my $reader =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("toxoTemplate.tab");
 $reader->setLogger($logger);
 $docRoot = $reader->parse();
}

sub test_AssayHasContact {
  my $self = shift;

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex->new($validator);
  $self->assert("RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex", $decoValidator->check($docRoot));

  my $assays = $docRoot->getAssayVOs();

  my $lexs = $assays->[0]->getLabeledExtracts();
  $assays->[0]->{_labeledExtracts} = undef;

  try {
    $decoValidator->check($docRoot);
  } catch RAD::MR_T::MageImport::ValidatorException with {  };

  $assays->[0]->setLabeledExtracts($lexs);
  $assays->[1]->setLabeledExtracts($lexs);

  try {
    $decoValidator->check($docRoot);
  } catch RAD::MR_T::MageImport::ValidatorException with {  };
}


1;
