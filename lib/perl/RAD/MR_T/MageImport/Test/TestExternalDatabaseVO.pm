package RAD::MR_T::MageImport::Test::TestExternalDatabaseVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::AffiliationVO;
use RAD::MR_T::MageImport::VO::PersonVO;
use RAD::MR_T::MageImport::VO::ExternalDatabaseVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new({name=>"test", version=>"test"});
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::ExternalDatabaseVO'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue, "$setValue is not equal to test");
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getVersion {
  my $self = shift;
  my $setValue = $self->{vo}->getVersion();
  $self->assert("test" eq $setValue, "$setValue is not equal to test");
}

sub test_setVersion {
  my $self = shift;
  $self->{vo}->setVersion("test2");
  my $setValue = $self->{vo}->getVersion();
  $self->assert("test2" eq $setValue);
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
