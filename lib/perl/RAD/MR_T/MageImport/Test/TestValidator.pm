package RAD::MR_T::MageImport::Test::TestValidator;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Service::Validator;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;

use Log::Log4perl qw(get_logger :levels);

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;
 $self->{vo} =  RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();

 # set up logger
 Log::Log4perl->init("$ENV{GUS_HOME}/config/log4perl.config");
 my $sLogger = get_logger("RAD::MR_T::MageImport::Service");
 $sLogger->debug("TestValidator set up");
 $self->{va} = RAD::MR_T::MageImport::Service::Validator->new;
 $self->{va}->setLogger($sLogger);
}

sub test_checkAll{
  my $self = shift;
#  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $self->{va}->check($self->{vo});
}


sub test_checkArrayElementUnique{
  my $self = shift;
  my @names = qw (test1 test2);
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $self->assert($validator->checkArrayElementUnique(\@names) == 1);

  @names = qw (test1 test1);
  $self->assert($validator->checkArrayElementUnique(\@names) == 0);
}


sub test_checkObjHasName{
  my $self = shift;
  my $doc = RAD::MR_T::MageImport::VO::DocRoot->new();
  my $p1 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"test1",
					     });
  $doc->personVOs([$p1]);
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->checkObjHasName($doc, "Person");

  my $p2 = RAD::MR_T::MageImport::VO::PersonVO->new();
  $doc->personVOs([$p2]);
  try{
    $validator->checkObjHasName($doc, "Person");
    $self->assert(0);
  }
    catch RAD::MR_T::MageImport::MageImportUnNamedVOError with {}
}

sub test_checkObjNameUnique{
  my $self = shift;
  my $doc = RAD::MR_T::MageImport::VO::DocRoot->new();
  my $p1 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"test1",
					     });

  my $p2 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"test2", 

					     });
  $doc->personVOs([$p1, $p2]);
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
#  try {
    $validator->checkObjNameUnique($doc, "Person");
    $self->assert(1);
#  }
# catch RAD::MR_T::MageImport::MageImportError with {my $ex=shift; $ex->stack_trace;};

}

sub test_checkObjNameUnique2{
  my $self = shift;
  my $doc = RAD::MR_T::MageImport::VO::DocRoot->new();
  my $p1 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"test1",
					     });

  my $p2 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"test1", 

					     });
  $doc->personVOs([$p1, $p2]);
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  try {
    $validator->checkObjNameUnique($doc, "Person");
    $self->assert(0);
  }
    catch RAD::MR_T::MageImport::MageImportVONameNotUniqueError with {   $self->assert(1);};

}


1;
