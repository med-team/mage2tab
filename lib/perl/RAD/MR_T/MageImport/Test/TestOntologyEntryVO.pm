package RAD::MR_T::MageImport::Test::TestOntologyEntryVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{oe} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{oe} = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"pooling", category=>"protocolType"});
}

sub test_construct{
  my $self = shift;

  $self->assert(UNIVERSAL::isa($self->{oe},'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getValue {
  my $self = shift;
  my $setValue = $self->{oe}->getValue();
  $self->assert("pooling" eq $setValue);
}

sub test_setValue {
  my $self = shift;
  $self->{oe}->setValue("splitting");
  my $setValue = $self->{oe}->getValue();
  $self->assert("splitting" eq $setValue);
}

sub test_getCategory {
  my ($self) = @_;
  my $setValue = $self->{oe}->getCategory();
  $self->assert("protocolType" eq $setValue);
}

sub test_setCategory {
  my $self = shift;
  $self->{oe}->setCategory("protocol");
  my $setValue = $self->{oe}->getCategory();
  $self->assert("protocol" eq $setValue);
}

sub tear_down {
  my $self = shift;
  $self->{oe} = undef;
}

1;
