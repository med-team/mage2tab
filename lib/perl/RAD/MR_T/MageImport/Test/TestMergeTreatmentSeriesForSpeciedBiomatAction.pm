package RAD::MR_T::MageImport::Test::TestMergeTreatmentSeriesForSpeciedBiomatAction;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use Data::Dumper;

use RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeriesForSpeciedBiomatAction;
use RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan;
use RAD::MR_T::MageImport::Service::Processor;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;

 $self->{vo} = RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
}

sub test_MergeTreatmentSeriesForSpeciedBiomatAction{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  $processor = RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan->new($processor);

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeriesForSpeciedBiomatAction->new($processor, "purify");
  my $docRoot = $self->{vo};
  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt->getName eq "treatment_pool"){
      $trt->setTreatmentType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({category=>"ComplexAction", 
									      value=>"specified_biomaterial_action",
									     })
			    );
    }
  }

  $self->assert ($decoProcessor->process($docRoot) eq "RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeriesForSpeciedBiomatAction");
  $self->assert_equals("purify", $decoProcessor->getTermStr);

  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt && $trt->getTreatmentType->getValue eq "purify"){
      my $outputBM = $trt->getOutputBM;
       foreach my $trt2(@{$docRoot->getTreatmentVOs}){
	if($trt2 && $trt2->getOutputBM == $outputBM && $trt2->getOrderNum != 1){
	  $self->assert(0);
	}
      }
    }

    if($trt->getOrderNum != 1){
      $self->assert(0);
    }
  }
}

1;
