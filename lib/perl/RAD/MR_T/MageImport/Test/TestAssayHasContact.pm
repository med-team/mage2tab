package RAD::MR_T::MageImport::Test::TestAssayHasContact;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::Service::Reader::MockReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;

 my $mockReader =  RAD::MR_T::MageImport::Service::Reader::MockReader->new();
 $docRoot = $mockReader->parse();
}

sub test_AssayHasContact {
  my $self = shift;

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact->new($validator);
  $self->assert("RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact", $decoValidator->check($docRoot));

  my $missingOpAssay = RAD::MR_T::MageImport::VO::AssayVO->new({name => 'test'});
  $docRoot->addAssayVOs($missingOpAssay);

  try {
    $decoValidator->check($docRoot);
  } catch RAD::MR_T::MageImport::ValidatorException with {};

}


1;
