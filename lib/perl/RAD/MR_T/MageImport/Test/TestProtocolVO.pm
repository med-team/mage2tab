package RAD::MR_T::MageImport::Test::TestProtocolVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::ProtocolParamVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = 
    RAD::MR_T::MageImport::VO::ProtocolVO->new({
					      name=>"test", 
					      protocolDescription=>"test", 
					      softwareDescription=>"test", 
					      hardwareDescription=>"test", 
					      uri=>"test", 
					      protocolType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
					      softwareType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
					      hardwareType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
					      params=>[RAD::MR_T::MageImport::VO::ProtocolParamVO->new()],
					     });
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::ProtocolVO'), "check object type ok");
 # $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getProtocolDescription {
  my $self = shift;
  my $setValue = $self->{vo}->getProtocolDescription();
  $self->assert("test" eq $setValue);
}

sub test_setProtocolDescription {
  my $self = shift;
  $self->{vo}->setProtocolDescription("test2");
  my $setValue = $self->{vo}->getProtocolDescription();
  $self->assert("test2" eq $setValue);
}

sub test_getHardwareDescription {
  my $self = shift;
  my $setValue = $self->{vo}->getHardwareDescription();
  $self->assert("test", $setValue);
}

sub test_setHardwareDescription {
  my $self = shift;
  $self->{vo}->setHardwareDescription("test2");
  my $setValue = $self->{vo}->getHardwareDescription();
  $self->assert("test2" eq $setValue);
}

sub test_getSoftwareDescription {
  my $self = shift;
  my $setValue = $self->{vo}->getSoftwareDescription();
  $self->assert("test" eq $setValue);
}

sub test_setSoftwareDescription {
  my $self = shift;
  $self->{vo}->setSoftwareDescription("test2");
  my $setValue = $self->{vo}->getSoftwareDescription();
  $self->assert("test2" eq $setValue);
}

sub test_getUri {
  my $self = shift;
  my $setValue = $self->{vo}->getUri();
  $self->assert("test" eq $setValue);
  $self->assert("test1" ne $setValue);
}

sub test_setUri {
  my $self = shift;
  $self->{vo}->setUri("test2");
  my $setValue = $self->{vo}->getUri();
  $self->assert("test2" eq $setValue);
}


sub test_getProtocolType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getProtocolType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setProtocolType {
  my $self = shift;
  $self->{vo}->setProtocolType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getProtocolType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getSoftwareType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getSoftwareType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setSoftwareType {
  my $self = shift;
  $self->{vo}->setSoftwareType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getSoftwareType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getHardwareType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getHardwareType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setHardwareType {
  my $self = shift;
  $self->{vo}->setHardwareType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getHardwareType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getParams {
  my ($self) = @_;
  my $setValue = $self->{vo}->getParams();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ProtocolParamVO'), "check object type again ok");
}

sub test_setParams {
  my $self = shift;
  $self->{vo}->setParams([RAD::MR_T::MageImport::VO::ProtocolParamVO->new()]);
  my $setValue = $self->{vo}->getParams();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ProtocolParamVO'), "check studydesign object type ok");
}

sub test_addParams {
  my $self = shift;
  $self->{vo}->addParams((RAD::MR_T::MageImport::VO::ProtocolParamVO->new()));
  my $setValue = $self->{vo}->getParams();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::ProtocolParamVO'), "check added studydesign object type ok");
}


sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
