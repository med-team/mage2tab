package RAD::MR_T::MageImport::Test::TestProtocolHasMgedType;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;


}


sub test_AssayHasContact {
  my $self = shift;

  my $reader =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("toxoTemplate.tab");
  $reader->setLogger($logger);
  $docRoot = $reader->parse();

  my $protocols = $docRoot->getProtocolVOs();

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType->new($validator);
  $self->assert("RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials", $decoValidator->check($docRoot));


  my $badType = $protocols->[0]->getProtocolType();
  $badType->setValue('nonMged');

  try {
    $decoValidator->check($docRoot);
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ValidatorException with { $self->assert(1); };
}

sub test_setExperimenatProtocolTypes {
 my $self = shift;

 # my $protocols = $docRoot->getProtocolVOs();

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 $validator->setLogger($logger);

 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType->new($validator);

 my $types=$decoValidator->_setExperimenatProtocolTypes();
 $self->assert(scalar @$types>5);
}
1;
