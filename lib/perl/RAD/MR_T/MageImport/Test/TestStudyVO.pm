package RAD::MR_T::MageImport::Test::TestStudyVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::StudyVO;
use RAD::MR_T::MageImport::VO::StudyDesignVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{study} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{study} = RAD::MR_T::MageImport::VO::StudyVO->new({name=>"test", pubMedId => 'pubMedId', designs=>[RAD::MR_T::MageImport::VO::StudyDesignVO->new()]});
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{study},'RAD::MR_T::MageImport::VO::StudyVO'), "check object type ok");

}

sub test_getPubMedId {
  my $self = shift;
  my $setValue = $self->{study}->getPubMedId();
  $self->assert("pubMedId" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{study}->setPubMedId("test");
  my $setValue = $self->{study}->getName();
  $self->assert("test" eq $setValue);
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{study}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{study}->setName("test2");
  my $setValue = $self->{study}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getDesigns {
  my ($self) = @_;
  my $setValue = $self->{study}->getDesigns();
  $self->assert(@$setValue==1, "@$setValue check number of designs");
  $self->assert(UNIVERSAL::isa($$setValue[0],'RAD::MR_T::MageImport::VO::StudyDesignVO'), "check studydesign object type");
}

sub test_setDesigns {
  my $self = shift;
  $self->{study}->setDesigns([RAD::MR_T::MageImport::VO::StudyDesignVO->new()]);
  my $setValue = $self->{study}->getDesigns();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::StudyDesignVO'), "check set studydesign object type");
}

sub test_addDesigns {
  my $self = shift;
  $self->{study}->addDesigns((RAD::MR_T::MageImport::VO::StudyDesignVO->new()));
  my $setValue = $self->{study}->getDesigns();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::StudyDesignVO'), "check added studydesign object type");
}

sub tear_down {
  my $self = shift;
  $self->{study} = undef;
}

1;
