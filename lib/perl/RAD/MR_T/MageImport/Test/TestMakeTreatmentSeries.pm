package RAD::MR_T::MageImport::Test::TestMakeTreatmentSeries;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use Data::Dumper;

use RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries;
use RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocol;
use RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan;
use RAD::MR_T::MageImport::Service::Processor;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;

 $self->{vo} = RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
}

sub t2est_SetQuanProtocol{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries->new($processor, "test,test2");
  my $docRoot = $self->{vo};
  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt->getName eq "treatment_pool"){
      $trt->setTreatmentType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"specified_biomaterial_action", 
									      category=>"ComplexAction"
									     })
			    );
    }
  }
  $self->assert ($decoProcessor->process($self->{vo}) eq "RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries");
  $self->assert_equals("test,test2", $decoProcessor->getTermStr);

}

sub test_SetQuanProtocol2{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  $processor = RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan->new($processor);
  $processor = RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocol->new($processor, "testtesttest");

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries->new($processor, "test,test2");
  my $docRoot = $self->{vo};
  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt->getName eq "treatment_pool"){
      $trt->setTreatmentType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({category=>"specified_biomaterial_action", 
									      value=>$trt->getTreatmentType->getValue
									     })
			    );
    }
  }
  $self->assert ($decoProcessor->process($self->{vo}) eq "RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries");
  $self->assert_equals("test,test2", $decoProcessor->getTermStr);

  print Dumper($docRoot);
  my $found = 0;
  foreach my $protocol(@{$docRoot->getProtocolVOs}){
    $found = 1 if $protocol->getName eq "testtesttest";
  }

  $self->assert($found);
}

1;
