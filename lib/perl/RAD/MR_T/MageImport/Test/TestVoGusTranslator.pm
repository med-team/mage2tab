package RAD::MR_T::MageImport::Test::TestVoGusTranslator;
use base qw(Test::Unit::TestCase);

#================================================================================
use RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator;
use RAD::MR_T::MageImport::Service::Reader::MockReader;

use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use GUS::ObjRelP::DbiDatabase;
use Data::Dumper;

use GUS::Supported::GusConfig;

use GUS::Model::RAD::Protocol;
use GUS::Model::RAD::Acquisition;
use GUS::Model::RAD::Assay;
use GUS::Model::Study::BioMaterial;

use Log::Log4perl qw(get_logger :levels);

use RAD::MR_T::MageImport::VO::BioMaterialVO;

use Error qw(:try);

use strict;

my $testCount;

my $docRoot;
my $voAffiliations;
my $voPersons;
my $voExternalDatabases;
my $voStudy;
my $voStudyDesigns;
my $voProtocols;
my $voAssays;
my $voBioMaterials;
my $voTreatments;

#================================================================================

sub new {
  my $self = shift()->SUPER::new(@_);

  $testCount = scalar $self->list_tests();

  return $self;
}

#--------------------------------------------------------------------------------

sub set_up {
  my $self = shift;

  my @properties;

  my $configFile = "$ENV{GUS_HOME}/config/gus.config";

  unless(-e $configFile) {
    my $error = "Config file $configFile does not exist.";
    RAD::MR_T::MageImport::MageImportError->new($error)->throw();
    exit();
  }

  my $config = GUS::Supported::GusConfig->new($configFile);
  
  my $login       = $config->getDatabaseLogin();
  my $password    = $config->getDatabasePassword();
  my $core        = $config->getCoreSchemaName();
  my $dbiDsn      = $config->getDbiDsn();
  my $oraDfltRbs  = $config->getOracleDefaultRollbackSegment();

  my $database;
  unless($database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    $database = GUS::ObjRelP::DbiDatabase->new($dbiDsn, $login, $password,
                                                  0,0,1,$core, $oraDfltRbs);
  }

  $self->{_dbi_database} = $database;

  my $reader = RAD::MR_T::MageImport::Service::Reader::MockReader->new();
  $docRoot = $reader->parse();

  $voAffiliations = $docRoot->affiliationVOs();
  $voPersons = $docRoot->personVOs();
  $voExternalDatabases = $docRoot->externalDatabaseVOs();
  $voStudy = $docRoot->getStudyVO();
  $voStudyDesigns = $voStudy->getDesigns();
  $voProtocols = $docRoot->getProtocolVOs();
  $voAssays = $docRoot->getAssayVOs();
  $voBioMaterials = $docRoot->getBioMaterialVOs();
  $voTreatments = $docRoot->getTreatmentVOs();

  # set up logger
  Log::Log4perl->init("$ENV{GUS_HOME}/config/log4perl.config");
  my $sLogger = get_logger("RAD::MR_T::MageImport::Service");

  $self->{_translator} =  RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator->new();
  $self->{_translator}->setLogger($sLogger);

  $self->{_doc_root} = $docRoot;

}

sub tear_down {
  my $self = shift;

  $testCount--;

  # LOG OUT AFTER ALL TESTS ARE FINISHED
  if($testCount <= 0) {
    GUS::ObjRelP::DbiDatabase->getDefaultDatabase()->logout();
    GUS::ObjRelP::DbiDatabase->setDefaultDatabase(undef);

    print STDERR "LOGGING OUT FROM DBI DATABASE\n";
  }

}

#--------------------------------------------------------------------------------

sub getTranslator  {$_[0]->{_translator}}
sub getDbiDatabase {$_[0]->{_dbi_database}}
sub getDocRoot     {$_[0]->{_doc_root}}

#--------------------------------------------------------------------------------

sub test_objectMapper {
  my $self = shift;
  my $t = $self->getTranslator();

  # The call to object mapper within a try loop  will come from the __ANON__ method
  # Use this to test failure when method is not in the mapper's global hash
  try {
    $t->objectMapper({last => 'Brestelli', 
                      first => 'John',
                     });
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ObjectMapperException with { };

  RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator->
      addMethodToObjectMap('test_objectMapper', 'GUS::Model::SRes::Contact');

  RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator->
      addMethodToObjectMap('__ANON__', 'GUS::Model::SRes::Contact');

  # Test that the object is being created
  my $me = $t->objectMapper({last => 'Brestelli', 
                             first => 'John',
                            });
  my $last = $me->getLast();
  $self->assert_str_equals('Brestelli', $last);

  my $first = $me->getFirst();
  $self->assert_str_equals('John', $first);

  # Test for Retrieve object from DB and Fail on no retrieve
  my $retrieveFromDbRules = {'GUS::Model::SRes::Contact' => 0 };
  $t->setRetrieveObjectsRules($retrieveFromDbRules);

  my $meFromDb =  $t->objectMapper({last => 'Brestelli', 
                                    first => 'John',
                                   });

  my $city = $meFromDb->getCity();
  $self->assert_str_equals('Philadelphia', $city);

  # Test for Unable to Retrieve
  try {
    $t->objectMapper({last => 'Breztello', 
                      first => 'John',
                     });
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ObjectMapperException with {};

  # Test for Retrieve object from DB and Create object on no retrieve
  $retrieveFromDbRules = {'GUS::Model::SRes::Contact' => 1 };
  $t->setRetrieveObjectsRules($retrieveFromDbRules);


  $meFromDb =  $t->objectMapper({last => 'Brestelli', 
                                 first => 'John',
                                });

  $city = $meFromDb->getCity();
  $self->assert_str_equals('Philadelphia', $city);

  # Test for Unable to Retrieve (SHOULD NOT THROW Error)
  my $newObj = $t->objectMapper({last => 'Breztello', 
                                 first => 'John',
                                });

  # Test for Error on Default Db
  GUS::ObjRelP::DbiDatabase->setDefaultDatabase();

  try {
    $t->objectMapper({last => 'Brestelli', 
                      first => 'John',
                     });
    $self->assert(0);
  } catch RAD::MR_T::MageImport::ObjectMapperException with {};

  GUS::ObjRelP::DbiDatabase->setDefaultDatabase($self->getDbiDatabase());

  # Test that no retrieve happens correctly
  $t->{_retrieve_objects_rules} = {};

  $me = $t->objectMapper({last => 'Brestelli', 
                          first => 'John',
                         });
  $last = $me->getLast();
  $self->assert_str_equals('Brestelli', $last);

  $first = $me->getFirst();
  $self->assert_str_equals('John', $first);

  $self->assert_null($me->getCity);


}

#--------------------------------------------------------------------------------

sub test_mapAssayBioMaterials {
  my $self = shift;

  my $t = $self->getTranslator();

  my $bioMaterials = [RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'source', subclassView => 'BioSource', bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample1', subclassView => 'BioSample', bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample2', subclassView => 'BioSample', bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample3', subclassView => 'BioSample', bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'samplePool', subclassView => 'BioSample', bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({channel=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"Cy3", category=>"LabelCompound"}),
                                                                     name => 'lex', 
                                                                     subclassView => 'LabeledExtract', 
                                                                     bioMaterialType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'total_RNA'})}),
                     ];

  my $bioMaterialsGus = $t->mapBioMaterials($bioMaterials);

  my $assay = $voAssays->[0];
  $assay->setLabeledExtracts([$bioMaterials->[5]]);

  my $affiliationsGus = $t->mapAffiliations($voAffiliations);
  my $contactsGus = $t->mapPersons($voPersons, $affiliationsGus);
  my $arrayDesignsGus = $t->mapArrayDesigns($voAssays);
  my $protocolsGus = $t->mapProtocols($voProtocols);
  my $studyGus = $t->mapStudy($voStudy, $contactsGus);
  my $studyDesignsGus = $t->mapStudyDesigns($voStudyDesigns, $studyGus);

  my $assaysGus = $t->mapAssays([$assay], $contactsGus, $arrayDesignsGus, $studyDesignsGus, $protocolsGus, $bioMaterialsGus);

  my $treatments = [RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[5], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[4]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[4], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[1]}),
                                                                               RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[2]}),
                                                                               RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[3]}),
                                                                              ],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[3], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[2], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[1], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                   ];


  my $assayBioMaterials = $t->mapAssayBioMaterials([$assay], $treatments, $assaysGus, $bioMaterialsGus);

  $self->assert_equals(6, scalar(@$assayBioMaterials));


}

#--------------------------------------------------------------------------------

sub test_mapStudyAssays {
  my $self = shift;

  my $t = $self->getTranslator();
  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);
  my $arrayDesigns = $t->mapArrayDesigns($voAssays);
  my $study = $t->mapStudy($voStudy, $contacts);
  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);
  my $protocols = $t->mapProtocols($voProtocols);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  my $assays = $t->mapAssays($voAssays, $contacts, $arrayDesigns, $studyDesigns, $protocols, $bioMaterials);

  my $studyAssays = $t->mapStudyAssays($study, $assays);

  # Test the number
  $self->assert_equals(2, scalar(@$studyAssays));
  
  # Test the Type and studyparent (will be the same for all)
  foreach my $sa (@$studyAssays) {
    $self->assert_equals('GUS::Model::RAD::StudyAssay', ref($sa));

    my $study = $sa->getParent('Study::Study');
    $self->assert_equals('study', $study->getName);
  }
}

#--------------------------------------------------------------------------------

sub test_mapStudyBioMaterials {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $protocols = $t->mapProtocols($voProtocols);

  my $study = $t->mapStudy($voStudy, $contacts);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  my $studyBioMaterials = $t->mapStudyBioMaterials($study, $bioMaterials);

  # Test the number
  $self->assert_equals(3, scalar(@$studyBioMaterials));

  # Test the Type and studyparent (will be the same for all)
  foreach my $sa (@$studyBioMaterials) {
    $self->assert_equals('GUS::Model::RAD::StudyBioMaterial', ref($sa));

    my $study = $sa->getParent('Study::Study');
    $self->assert_equals('study', $study->getName);
  }
}

#--------------------------------------------------------------------------------

sub test_mapAffiliations {
  my ($self) = @_;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);

  # Mock reader contains 2 affiliations
  $self->assert_num_equals(2, scalar(@$affiliations));

  # Test that both are SRes.Contacts
  foreach(@$affiliations) {
    $self->assert_str_equals('GUS::Model::SRes::Contact', ref($_));
  }

  my $name0 = $affiliations->[0]->getName();
  my $name1 = $affiliations->[1]->getName();

  $self->assert_str_equals('affiliation_1', $name0);
  $self->assert_str_equals('affiliation_2', $name1);

}

#--------------------------------------------------------------------------------

sub test_mapPersons {
  my ($self) = @_;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $persons = $t->mapPersons($voPersons, $affiliations);

  # Mock reader contains 2 persons
  $self->assert_num_equals(2, scalar(@$persons));

  # Test that both are SRes.Contacts
  foreach(@$persons) {
    $self->assert_str_equals('GUS::Model::SRes::Contact', ref($_));
  }

  my $name0 = $persons->[0]->getName();
  my $name1 = $persons->[1]->getName();

  my $email0 = $persons->[0]->getEmail();
  my $email1 = $persons->[1]->getEmail();

  my $affiliationNm0 = $persons->[0]->getParent('SRes::Contact')->getName();
  my $affiliationNm1 = $persons->[1]->getParent('SRes::Contact')->getName();

  $self->assert_str_equals('person_1', $name0);
  $self->assert_str_equals('person_1_email', $email0);
  $self->assert_str_equals('affiliation_1', $affiliationNm0);

  $self->assert_str_equals('person_2', $name1);
  $self->assert_str_equals('person_2_email', $email1);
  $self->assert_str_equals('affiliation_2', $affiliationNm1);
}

#--------------------------------------------------------------------------------

sub test_mapExternalDatabases {
  my ($self) = @_;

  my $t = $self->getTranslator();
  my $docRoot = $self->getDocRoot();

  my $extDbRls = $t->mapExternalDatabases($voExternalDatabases);

  # Mock reader contains 2 ext dbs
  $self->assert_num_equals(2, scalar(@$extDbRls));

  # Test that both are SRes.ExternalDatabaseReleases
  foreach(@$extDbRls) {
    $self->assert_str_equals('GUS::Model::SRes::ExternalDatabaseRelease', ref($_));
  }

  # test the version is expected
  my $version0 = $extDbRls->[0]->getVersion();
  my $version1 = $extDbRls->[1]->getVersion();

  $self->assert_str_equals('2005-10-13', $version0);
  $self->assert_str_equals('2005-10-13', $version1);

  # Test the parent is correct
  my $extDbNm0 = $extDbRls->[0]->getParent('SRes::ExternalDatabase')->getName();
  my $extDbNm1 = $extDbRls->[1]->getParent('SRes::ExternalDatabase')->getName();

  $self->assert_str_equals('Entrez gene', $extDbNm0);
  $self->assert_str_equals('NCBI RefSeq', $extDbNm1);

  # This should be retrieved from the db
  my $egIdType = $extDbRls->[0]->getIdType();
  $self->assert_str_equals('Entrez GeneId', $egIdType);
}

#--------------------------------------------------------------------------------

sub test_mapStudy {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);

  $self->assert_str_equals('GUS::Model::Study::Study', ref($study));

  # Test the simple study attributes
  my $name = $study->getName();
  my $desc = $study->getDescription();

  $self->assert_str_equals('study', $name);
  $self->assert_str_equals('studyDescription', $desc);

  # Test the parent contact is being set correctly
  my $contact = $study->getParent('SRes::Contact');
  my $contactName = $contact->getName();
  my $contactEmail = $contact->getEmail();

  $self->assert_str_equals('person_2', $contactName);
  $self->assert_str_equals('person_2_email', $contactEmail);
}

#--------------------------------------------------------------------------------


sub test_mapStudyDesigns {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);

  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);

  # Test for number of designs (mock reader has 1
  $self->assert_num_equals(1, scalar(@$studyDesigns));

  my $studyDesign = $studyDesigns->[0];

  # Test the name
  my $name = $studyDesign->getName();
  $self->assert_str_equals('studyDesign', $name);

  my @studyFactors = $studyDesign->getChildren('Study::StudyFactor');

  # The mock reader has 2 studyFactors as children
  $self->assert_num_equals(2, scalar(@studyFactors));

  foreach my $sf (@studyFactors) {
    $self->assert_str_equals('GUS::Model::Study::StudyFactor', ref($sf));

    my $name = $sf->getName();
    my $oe = $sf->getParent('Study::OntologyEntry');

    if($name eq 'studyFactor_1') {
      $self->assert_str_equals('#genetic_modification', $oe->getSourceId());
    }
    elsif($name eq 'studyFactor_2') {
      $self->assert_str_equals('#organism_part', $oe->getSourceId());
    }
    else {
      $self->assert(0, "Expected studyFactor_1 or studyFactor_2 but found $name");
    }
  }

  my @types = $studyDesign->getChildren('Study::StudyDesignType');
  $self->assert_equals(2, scalar(@types));
}

#--------------------------------------------------------------------------------

sub test_mapStudyDesignType {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);

  # get the first one
  my $studyDesign = $t->mapStudyDesigns($voStudyDesigns, $study)->[0];
  my $types = $voStudyDesigns->[0]->getTypes();

  my $studyDesignTypes = $t->mapStudyDesignType($studyDesign, $types);

  foreach(@$studyDesignTypes) {
    $self->assert_str_equals('GUS::Model::Study::StudyDesignType', ref($_));
  }

  my $oe = $studyDesignTypes->[0]->getParent('Study::OntologyEntry');
  my $parentStudyDesign = $studyDesignTypes->[0]->getParent('Study::StudyDesign');

  $self->assert_str_equals('#strain_or_line_design', $oe->getSourceId());
  $self->assert_str_equals('studyDesign', $parentStudyDesign->getName());
}

#--------------------------------------------------------------------------------

sub test_mapStudyFactors {
  my $self = shift;

  my $t = $self->getTranslator();

  my $voStudyFactors = $voStudyDesigns->[0]->getFactors();

  my $affiliations = $t->mapAffiliations($voAffiliations);

  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);

  # get the first one
  my $studyDesign = $t->mapStudyDesigns($voStudyDesigns, $study)->[0];

  my $studyFactors = $t->mapStudyFactors($studyDesign, $voStudyFactors);

  foreach(@$studyFactors) {
    $self->assert_str_equals('GUS::Model::Study::StudyFactor', ref($_));
  }

  # Test the mock reader has 2 study factors
  $self->assert_num_equals(2, scalar(@$studyFactors));

  my $name0 = $studyFactors->[0]->getName();
  my $name1 = $studyFactors->[1]->getName();

  $self->assert_str_equals('studyFactor_1', $name0);
  $self->assert_str_equals('studyFactor_2', $name1);

  my $oe0 = $studyFactors->[0]->getParent('Study::OntologyEntry');
  my $oe1 = $studyFactors->[1]->getParent('Study::OntologyEntry');

  $self->assert_str_equals('#genetic_modification', $oe0->getSourceId());
  $self->assert_str_equals('#organism_part', $oe1->getSourceId());

  my $parent = $studyFactors->[0]->getParent('Study::StudyDesign');
  $self->assert_str_equals('studyDesign', $parent->getName);
}

#--------------------------------------------------------------------------------

sub test_mapProtocols {
  my $self = shift;

  my $t = $self->getTranslator();

  my $protocols = $t->mapProtocols($voProtocols);

  # test there were 2 Rad protocols made
  $self->assert_num_equals(2, scalar(@$protocols));

  foreach(@$protocols) {
    $self->assert_str_equals('GUS::Model::RAD::Protocol', ref($_));
  }

  # Test the easy stuff
  my $name0 = $protocols->[0]->getName();
  my $name1 = $protocols->[1]->getName();

  $self->assert_str_equals('protocol_labeling', $name0);
  $self->assert_str_equals('protocol_extraction', $name1);

#  # Test the type (these should be retrieved)
#  my $type0 = $protocols->[0]->getParent('Study::OntologyEntry');
#  my $type1 = $protocols->[1]->getParent('Study::OntologyEntry');

#  $self->assert_str_equals('#labeling', $type0->getSourceId());
#  $self->assert_str_equals('#nucleic_acid_extraction', $type1->getSourceId());

  my $nullParam = $protocols->[1]->getChild('RAD::ProtocolParam');
  $self->assert_null($nullParam);

  my @params = $protocols->[0]->getChildren('RAD::ProtocolParam');
  $self->assert_num_equals(2, scalar(@params));

}

#--------------------------------------------------------------------------------


sub test_mapAssays {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $arrayDesigns = $t->mapArrayDesigns($voAssays);
  my $protocols = $t->mapProtocols($voProtocols);
  my $study = $t->mapStudy($voStudy, $contacts);
  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  my $assays = $t->mapAssays($voAssays, $contacts, $arrayDesigns, $studyDesigns, $protocols, $bioMaterials);

  # Test the number of assays
  $self->assert_equals(2, scalar(@$assays));

  foreach(@$assays) {
    $self->assert_str_equals('GUS::Model::RAD::Assay', ref($_));
  }

  # Test Assay Attributes
  my $name0 = $assays->[0]->getName();
  my $name1 = $assays->[1]->getName();

  $self->assert_str_equals('assay_1', $name0);
  $self->assert_str_equals('assay_2', $name1);

  # Check the number of acquisition children
  my @acquisitions0 = $assays->[0]->getChildren('RAD::Acquisition');
  my @acquisitions1 = $assays->[0]->getChildren('RAD::Acquisition');

  $self->assert_equals(2, scalar(@acquisitions0));  
  $self->assert_equals(2, scalar(@acquisitions1));  

  my $arrayDesign0 = $assays->[0]->getParent('GUS::Model::RAD::ArrayDesign');
  my $arrayDesign1 = $assays->[1]->getParent('GUS::Model::RAD::ArrayDesign');


  $self->assert_equals('Affymetrix HG_U95E', $arrayDesign0->getName);
  $self->assert_equals('Affymetrix Mu11KsubA', $arrayDesign1->getName);

  my $protocol0 = $assays->[0]->getParent('GUS::Model::RAD::Protocol');
  my $protocol1 = $assays->[1]->getParent('GUS::Model::RAD::Protocol');

  $self->assert_equals('protocol_extraction', $protocol0->getName());
  $self->assert_equals('protocol_labeling', $protocol1->getName());

  # Test the AssayParams
  my @nullAssayParams = $assays->[0]->getChildren('RAD::AssayParam');
  $self->assert_null(@nullAssayParams);

  my @assayParams = $assays->[1]->getChildren('RAD::AssayParam');
  $self->assert_equals(2, scalar(@assayParams));
  foreach(@assayParams) {
    $self->assert_equals('GUS::Model::RAD::AssayParam', ref($_));
    my $value = $_->getValue();
    my $protParamName = $_->getParent('RAD::ProtocolParam')->getName();

    if($value =~ /assay_param_value_([1|2])/) {
      my $expectedParamName = 'protocol_param_' . $1;
      $self->assert_equals($expectedParamName, $protParamName);
    }
    else {
      $self->assert(0, "AssayParam [$value] did not match expected");
    }
  }

  # Test AssayLex

  foreach(@$assays) {
    my $assayLex = $_->getChild('RAD::AssayLabeledExtract');
    my $lex = $assayLex->getParent('Study::LabeledExtract');
    $self->assert_equals('labeledExtract', $lex->getName());
  }


}

#--------------------------------------------------------------------------------

sub test_mapAcquisitions {
  my $self = shift;

  my $t = $self->getTranslator();
  my $assay = GUS::Model::RAD::Assay->new({name=>'test'});

  my $protocols = $t->mapProtocols($voProtocols);
  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);
  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);
  my $voAcquisitions = $voAssays->[0]->getAcquisitions();

  my $acquisitions = $t->mapAcquisitions($voAcquisitions, $assay, $studyDesigns, $protocols);

  # Test the number of acquisitions
  $self->assert_equals(2, scalar(@$acquisitions));

  foreach(@$acquisitions) {
    $self->assert_str_equals('GUS::Model::RAD::Acquisition', ref($_));
  }

  # Test Acquisition Attributes
  my $name0 = $acquisitions->[0]->getName();
  my $name1 = $acquisitions->[1]->getName();

  my $uri0 = $acquisitions->[0]->getUri();
  my $uri1 = $acquisitions->[1]->getUri();

  my $oe0 = $acquisitions->[0]->getParent('Study::OntologyEntry')->getValue();
  my $oe1 = $acquisitions->[1]->getParent('Study::OntologyEntry')->getValue();

  $self->assert_equals('acquisition_1', $name0);
  $self->assert_equals('acquisition_1', $name1);

  $self->assert_equals('acquisition_1.uri', $uri0);
  $self->assert_equals('acquisition_1.uri', $uri1);

  $self->assert_equals('Cy3', $oe0);
  $self->assert_equals('Cy5', $oe1);

  # Test the parent
  my $assayName0 = $acquisitions->[0]->getParent('RAD::Assay')->getName();
  my $assayName1 = $acquisitions->[1]->getParent('RAD::Assay')->getName();
  
  $self->assert_equals('test', $assayName0);
  $self->assert_equals('test', $assayName1);

  # Test the number of quantification children (includes processes)
  my @quantifications0 = $acquisitions->[0]->getChildren('RAD::Quantification');
  my @quantifications1 = $acquisitions->[1]->getChildren('RAD::Quantification');

  $self->assert_equals(7, scalar(@quantifications0));
  $self->assert_equals(7, scalar(@quantifications1));


  # Test the AcquisitionParams
  my @params = $acquisitions->[0]->getChildren('RAD::AcquisitionParam');
  $self->assert_equals(2, scalar(@params));
  foreach(@params) {
    $self->assert_equals('GUS::Model::RAD::AcquisitionParam', ref($_));
    my $value = $_->getValue();
    my $protParamName = $_->getParent('RAD::ProtocolParam')->getName();

    if($value =~ /acquisition_param_value_([1|2])/) {
      my $expectedParamName = 'protocol_param_' . $1;
      $self->assert_equals($expectedParamName, $protParamName);
    }
    else {
      $self->assert(0, "AcquisitinoParam [$value] did not match expected");
    }
  }

}

#--------------------------------------------------------------------------------

sub test_mapQuantifications {
  my $self = shift;

  my $t = $self->getTranslator();

  my $voAcquisitions = $voAssays->[0]->getAcquisitions();
  my $protocols = $t->mapProtocols($voProtocols);

  my $voQuantifications = $voAcquisitions->[0]->getQuantifications();

  my $acquisition = GUS::Model::RAD::Acquisition->new({name=>'test'});

  my $quantifications = $t->mapQuantifications($voQuantifications, $acquisition, $protocols);

  # Test the number of quants
  $self->assert_equals(2, scalar(@$quantifications));

  foreach(@$quantifications) {
    $self->assert_str_equals('GUS::Model::RAD::Quantification', ref($_));
  }

  # Test Quantification Attributes
  my $name0 = $quantifications->[0]->getName();
  my $name1 = $quantifications->[1]->getName();

  my $uri0 = $quantifications->[0]->getUri();
  my $uri1 = $quantifications->[1]->getUri();

  $self->assert_equals('quantification_1', $name0);
  $self->assert_equals('quantification_3', $name1);

  $self->assert_equals('quantification_1.uri', $uri0);
  $self->assert_equals('quantification_3.uri', $uri1);

  # Test the parent Acquisition
  my $parentName0 = $quantifications->[0]->getParent('RAD::Acquisition')->getName();
  my $parentName1 = $quantifications->[1]->getParent('RAD::Acquisition')->getName();

  $self->assert_equals('test', $parentName0);
  $self->assert_equals('test', $parentName1);

  # Test the QuantificationParams
  my @nullQuantParams = $quantifications->[0]->getChildren('RAD::QuantificationParam');
  $self->assert_null(@nullQuantParams);

  my @params = $quantifications->[1]->getChildren('RAD::QuantificationParam');
  $self->assert_equals(2, scalar(@params));
  foreach(@params) {
    $self->assert_equals('GUS::Model::RAD::QuantificationParam', ref($_));
    my $value = $_->getValue();
    my $protParamName = $_->getParent('RAD::ProtocolParam')->getName();

    if($value =~ /quantification_param_value_([1|2])/) {
      my $expectedParamName = 'protocol_param_' . $1;
      $self->assert_equals($expectedParamName, $protParamName);
    }
    else {
      $self->assert(0, "QuantificationParam [$value] did not match expected");
    }
  }

  my @processesAndQuants = $acquisition->getChildren('RAD::Quantification');
  $self->assert_equals(7, scalar(@processesAndQuants));
}

#--------------------------------------------------------------------------------

sub test_mapBioMaterials {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $protocols = $t->mapProtocols($voProtocols);

  my $shouldFail = [ RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'shouldFail',
                                                                    subclassView => 'shouldfail',
                                                                   }) ];
  try {
    $t->mapBioMaterials($shouldFail, $contacts);
    $self->assert(0, "Expected Exception for mapBioMaterial");
  } catch RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject with {};

  $voBioMaterials->[1]->setSubclassView('Extract');

  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  # Test the number of biomaterials
  $self->assert_equals(3, scalar(@$bioMaterials));

  my $source = $bioMaterials->[0];
  my $sample = $bioMaterials->[1];
  my $lex = $bioMaterials->[2];

  $self->assert_str_equals('GUS::Model::Study::BioSource', ref($source));
  $self->assert_str_equals('GUS::Model::Study::BioSample', ref($sample));
  $self->assert_str_equals('GUS::Model::Study::LabeledExtract', ref($lex));


  $self->assert_equals('bioSource', $source->getName);
  $self->assert_equals('bioSample', $sample->getName);
  $self->assert_equals('labeledExtract', $lex->getName);

  # Test the parents
  my $oeSource = $source->getParent('Study::OntologyEntry')->getValue();
  my $oeSample = $sample->getParent('Study::OntologyEntry')->getValue();
  my $oeLex = $lex->getParent('Study::OntologyEntry')->getValue();

  $self->assert_equals('whole_organism', $oeSource);
  $self->assert_equals('total_RNA', $oeSample);
  $self->assert_equals('DNA', $oeLex);

  my $contactSource = $source->getParent('SRes::Contact');

  $self->assert_equals('person_2', $contactSource->getName);

  # Test the Characteristics for the BioSource
  my @characteristics = $source->getChildren('Study::BioMaterialCharacteristic');
  $self->assert_equals(2, scalar(@characteristics));
}

#--------------------------------------------------------------------------------

sub test_mapTreatments {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $protocols = $t->mapProtocols($voProtocols);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials,  $contacts);

  my $treatments = $t->mapTreatments($voTreatments, $protocols, $bioMaterials);

  # Test the correct number have been created
  $self->assert_equals(4, scalar(@$treatments));

  # Test the correct type
  foreach(@$treatments) {
    $self->assert_equals('GUS::Model::RAD::Treatment', ref($_));
  }

  # Test the treatment attributes
  my $treatPool = $treatments->[0];
  my $treatGrow = $treatments->[1];
  my $treatExtraction = $treatments->[2];
  my $treatLabeling = $treatments->[3];

  $self->assert_equals('treatment_pool', $treatPool->getName);
  $self->assert_equals('treatment_grow', $treatGrow->getName);
  $self->assert_equals('treatment_extraction', $treatExtraction->getName);
  $self->assert_equals('treatment_labeling', $treatLabeling->getName);

   # Test the parents of the grow protocol
  my $growOe = $treatGrow->getParent('Study::OntologyEntry');
  my $growBm = $treatGrow->getParent('Study::BioSample');
  my $growProt = $treatGrow->getParent('RAD::Protocol');

  $self->assert_equals('bioSample', $growBm->getName);
  $self->assert_equals('protocol_extraction', $growProt->getName);
  $self->assert_equals('grow', $growOe->getValue);

  # Test the BioMaterialMeasurment Children
  my @growBmm = $treatGrow->getChildren('RAD::BioMaterialMeasurement');
  my @poolBmm = $treatPool->getChildren('RAD::BioMaterialMeasurement');

  $self->assert_equals(1, scalar(@growBmm));
  $self->assert_equals(2, scalar(@poolBmm)); 

  # Test the QuantificationParams
  my @nullQuantParams = $treatments->[0]->getChildren('RAD::TreatmentParam');
  $self->assert_null(@nullQuantParams);

  my @params = $treatments->[3]->getChildren('RAD::TreatmentParam');
  $self->assert_equals(2, scalar(@params));
  foreach(@params) {
    $self->assert_equals('GUS::Model::RAD::TreatmentParam', ref($_));
    my $value = $_->getValue();
    my $protParamName = $_->getParent('RAD::ProtocolParam')->getName();

    if($value =~ /treatment_param_value_([1|2])/) {
      my $expectedParamName = 'protocol_param_' . $1;
      $self->assert_equals($expectedParamName, $protParamName);
    }
    else {
      $self->assert(0, "TreatmentParam [$value] did not match expected");
    }
  }
}

#--------------------------------------------------------------------------------

sub test_mapBioMaterialMeasurements {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $protocols = $t->mapProtocols($voProtocols);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  my $treatment = GUS::Model::RAD::Treatment->new({name => 'testTreatment'});

  my $voBmms = $voTreatments->[1]->getInputBMMs();

  my $bioMaterialMeasurement = $t->mapBioMaterialMeasurements($voBmms, $treatment, $bioMaterials)->[0];

  # Test Type
  $self->assert_equals('GUS::Model::RAD::BioMaterialMeasurement', ref($bioMaterialMeasurement));

  # Test value
  $self->assert_equals(10, $bioMaterialMeasurement->getValue());

  # Test Parents
  my $treatName = $bioMaterialMeasurement->getParent('RAD::Treatment')->getName();
  my $oeValue = $bioMaterialMeasurement->getParent('Study::OntologyEntry')->getValue();

  $self->assert_equals('testTreatment', $treatName);
  $self->assert_equals('cc', $oeValue);

  my $bmName = $bioMaterialMeasurement->getParent('Study::BioSource')->getName();
  $self->assert_equals('bioSource', $bmName);

}

#--------------------------------------------------------------------------------

sub test_createRadOE {
  my $self = shift;

  my $t = $self->getTranslator();

  my $type = 'labelCompound';
  my $voOe = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Cy5', category => 'LabelCompound'});

  # Test require the ontologyEntryvo
  my $notOe = RAD::MR_T::MageImport::VO::BioMaterialVO->new();
  try {
    $t->createRadOE($notOe, $type);
    $self->assert(0, "Should Have thrown an Exeption");
  } catch RAD::MR_T::MageImport::ObjectMapperException with {};

  # Test 
  my $notInDb = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'notInDb', category => 'LabelCompound'});

  try {
    $t->createRadOE($notInDb, $type);
    $self->assert(0, "Should Have thrown an Exeption");
  } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {};

  try {
    $t->createRadOE($notInDb, 'notSupported');
    $self->assert(0, "Should Have thrown an Exeption");
  } catch RAD::MR_T::MageImport::ObjectMapperException with { };

  my $oe = $t->createRadOE($voOe, $type);

  # Test the type
  $self->assert_equals('GUS::Model::Study::OntologyEntry', ref($oe));

  # Test the value
  my $oeValue = $oe->getValue();

  $self->assert_equals('Cy5', $oeValue);
}

#--------------------------------------------------------------------------------

sub test_searchObjArrayByObjName {
  my $self = shift;

  my $t = $self->getTranslator();

  my $ar = [ GUS::Model::RAD::Assay->new({name => 'name1'}),
             GUS::Model::RAD::Assay->new({name => 'name2'}),
             GUS::Model::RAD::Assay->new({name => 'name3'}),
             GUS::Model::RAD::Assay->new({name => 'name4'}),
           ];

  try {
    $t->searchObjArrayByObjName($ar, 'notFound');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {};

  my $assay = $t->searchObjArrayByObjName($ar, 'name1');
  $self->assert_equals('GUS::Model::RAD::Assay', ref($assay));
  $self->assert_equals('name1', $assay->getName);

  push(@$ar, GUS::Model::RAD::Assay->new({name => 'name2'}));

  try {
    $t->searchObjArrayByObjName($ar, 'name2');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {};

}

#--------------------------------------------------------------------------------

sub test_mapLabelMethod {
  my $self = shift;

  my $t = $self->getTranslator();

  my $protocol = GUS::Model::RAD::Protocol->new({name => 'GeneChip Labeling protocol for Eukaryotic Target polyA-mRNA'});
  my $newChannel = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"new_test", category=>"new_test"});

  try {
    $t->mapLabelMethod($newChannel, $protocol);
    $self->assert(0, "We require that the ontology entry is retrieved");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {};

  my $voChannel = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"Cy3", category=>"LabelCompound"});

  my @labelMethods;

  my $newLabelMethod = $t->mapLabelMethod($voChannel, $protocol, \@labelMethods);
  push(@labelMethods, $newLabelMethod);

  $self->assert_equals('GUS::Model::RAD::LabelMethod', ref($newLabelMethod));
  $self->assert_null($newLabelMethod->getLabelUsed());

  my $shouldExistLabelMethod = $t->mapLabelMethod($voChannel, $protocol, \@labelMethods);
  $self->assert($shouldExistLabelMethod == $labelMethods[0], "These should be the same object");

  $protocol->retrieveFromDB();

  my $anotherLabelMethod = $t->mapLabelMethod($voChannel, $protocol);
  $self->assert_equals('Name', $anotherLabelMethod->getLabelUsed());
  $self->assert($anotherLabelMethod != $labelMethods[0], "These should not be the same object");

}

#--------------------------------------------------------------------------------

sub test_searchObjArrayByObjSourceId {
  my $self = shift;

  my $t = $self->getTranslator();

  my $ar = [ GUS::Model::RAD::ArrayDesign->new({source_id => 'name1'}),
             GUS::Model::RAD::ArrayDesign->new({source_id => 'name2'}),
             GUS::Model::RAD::ArrayDesign->new({source_id => 'name3'}),
             GUS::Model::RAD::ArrayDesign->new({source_id => 'name4'}),
           ];

  try {
    $t->searchObjArrayByObjSourceId($ar, 'notFound');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {};

  my $assay = $t->searchObjArrayByObjSourceId($ar, 'name1');
  $self->assert_equals('GUS::Model::RAD::ArrayDesign', ref($assay));
  $self->assert_equals('name1', $assay->getSourceId);

  push(@$ar, GUS::Model::RAD::ArrayDesign->new({source_id => 'name2'}));

  try {
    $t->searchObjArrayByObjSourceId($ar, 'name2');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {};

}

#--------------------------------------------------------------------------------

sub test_mapArrayDesigns {
  my $self = shift;

  my $t = $self->getTranslator();

  my $arrayDesigns = $t->mapArrayDesigns($voAssays);

  $self->assert_equals(2, scalar(@$arrayDesigns));

  foreach(@$arrayDesigns) {
    $self->assert_equals('GUS::Model::RAD::ArrayDesign', ref($_));
  }

  my $ad0 = $arrayDesigns->[0];
  my $ad1 = $arrayDesigns->[1];

  $self->assert_equals('Affymetrix HG_U95E', $ad0->getName);
  $self->assert_equals('Affymetrix Mu11KsubA', $ad1->getName);
}

#--------------------------------------------------------------------------------

sub test_camelCapsToUnderscore {
  my $self = shift;

  my $t = $self->getTranslator();

  my $value = $t->camelCapsToUnderscore('GeneticModification');
  $self->assert_equals('genetic_modification', $value);

  my $another = $t->camelCapsToUnderscore('thisIsAnotherTest');
  $self->assert_equals('this_is_another_test', $another);

}

#--------------------------------------------------------------------------------

sub test_mapBioMaterialCharacteristics {
  my $self = shift;

  my $t = $self->getTranslator();

 my $voChars = [ RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"NOD", category=>"StrainOrLine"}),
                 RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"female", category=>"Sex"}),
               ];

  my $bioMaterial = GUS::Model::Study::BioSource->new({name => 'test'});

  my $characteristics = $t->mapBioMaterialCharacteristics($voChars, $bioMaterial);

  $self->assert_equals(2, scalar(@$characteristics));

  foreach (@$characteristics) {
    $self->assert_equals('GUS::Model::Study::BioMaterialCharacteristic', ref($_));

    $self->assert_null($_->getValue());
  }

  # TEST OE Parent
  my $value0 = $characteristics->[0]->getParent('Study::OntologyEntry')->getValue();
  my $value1 = $characteristics->[1]->getParent('Study::OntologyEntry')->getValue();

  $self->assert_equals('NOD', $value0);
  $self->assert_equals('female', $value1);

  # TEST Biomaterial Parent
  my $name0 = $characteristics->[0]->getParent('Study::BioSource')->getName();
  my $name1 = $characteristics->[1]->getParent('Study::BioSource')->getName();

  $self->assert_equals('test', $name0);
  $self->assert_equals('test', $name1);

  push(@$voChars, RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"p53 knock out", category=>"GeneticModification"}));

  my $testSkip = $t->mapBioMaterialCharacteristics($voChars, $bioMaterial);
  $self->assert_equals(2, scalar(@$testSkip));

}

#--------------------------------------------------------------------------------

sub test_mapProtocolParams {
  my $self = shift;

  my $t = $self->getTranslator();

  my $voParams =   [  RAD::MR_T::MageImport::VO::ProtocolParamVO->
                      new({name => 'protocol_param_1',
                           value => 'protocol_param_1_value',
                           dataType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"float", category=>"DataType"}),
                          }),
                      RAD::MR_T::MageImport::VO::ProtocolParamVO->
                      new({name => 'protocol_param_2',
                           value => 'protocol_param_2_value',
                           unitType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"months", category=>"TimeUnit"}),
                          })
                   ];

  # Test a new protocol... add all params
  my $protocol = GUS::Model::RAD::Protocol->new({name => 'test_protocol'});
  my $params = $t->mapProtocolParams($voParams, $protocol);

  $self->assert_equals(2, scalar(@$params));

  foreach(@$params) {
    $self->assert_equals('GUS::Model::RAD::ProtocolParam', ref($_));
  }

  # Unit type of first should be null
  my $ut0 = $params->[0]->getUnitTypeId;
  my $ut1 = $params->[1]->getUnitTypeId;

  $self->assert_null($ut0);
  $self->assert_not_null($ut1);

  # Unit type of second should be null
  my $dt0 = $params->[0]->getDataTypeId;
  my $dt1 = $params->[1]->getDataTypeId;

  $self->assert_null($dt1);
  $self->assert_not_null($dt0);

  # Test the name
  my $name0 = $params->[0]->getName;
  my $name1 = $params->[1]->getName;

  $self->assert_equals('protocol_param_1', $name0);
  $self->assert_equals('protocol_param_2', $name1);

  my $existingProtocol = GUS::Model::RAD::Protocol->new({name => 'GenePix quantification'});
  $existingProtocol->retrieveFromDB() or die "Cannot retrieve GenePix quantification protocol";

  # try with wrong params for an existing protocol
  try {
    my $existingParams = $t->mapProtocolParams($voParams, $existingProtocol);
    $self->assert(0, "Should Have failed");
  } catch  RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject with {};

  #$self->assert_equals(scalar($existingProtocol->getChildren('RAD::ProtocolParam', 1)), scalar(@$existingParams));

  # try with no params... (should be fine)
  my $params = $t->mapProtocolParams([], $existingProtocol);

  $self->assert(scalar(@$params > 1));

}

#--------------------------------------------------------------------------------

sub test_mapTaxon {
  my $self = shift;

  my $t = $self->getTranslator();

  my $bioMaterial = GUS::Model::Study::BioSource->new({name => 'test'});

  $t->mapTaxon(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Mus musculus', category => 'Organism'}), $bioMaterial);

  $self->assert_equals(14, $bioMaterial->getTaxonId());

  $t->mapTaxon(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'mouse', category => 'Organism'}),  $bioMaterial);

  $self->assert_equals(14, $bioMaterial->getTaxonId());
}

#--------------------------------------------------------------------------------

sub test_mapStudyFactorValues {
  my $self = shift;

  my $t = $self->getTranslator();

  my $assay = GUS::Model::RAD::Assay->new({name => 'test_assay'});
  my $channel = GUS::Model::Study::OntologyEntry->new({value => 'Cy3', category => 'LabelCompound'});
  $channel->retrieveFromDB();

  my $voFactorValues = [ RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_1',
                                                                        value => RAD::MR_T::MageImport::VO::OntologyEntryVO->
                                                                                       new({value => 'C57BL', category => 'StrainOrLine'}),
                                                                       }),
                         RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_2',
                                                                        value => RAD::MR_T::MageImport::VO::OntologyEntryVO->
                                                                                    new({value => 'studyFactorValue_Cy3_2', category => ''}),
                                                                       }),
                       ];

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);

  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);

  my $studyFactorValues = $t->mapStudyFactorValues($voFactorValues, $assay, $channel, $studyDesigns, 1);

  foreach(@$studyFactorValues) {
    $self->assert_equals('GUS::Model::RAD::StudyFactorValue', ref($_));
  }

  $self->assert_equals(2, scalar(@$studyFactorValues));

  my $nm0 = $studyFactorValues->[0]->get('string_value');
  my $nm1 = $studyFactorValues->[1]->get('string_value');

  $self->assert_equals('C57BL', $nm0);
  $self->assert_equals('studyFactorValue_Cy3_2', $nm1);

  my $channel0 = $studyFactorValues->[0]->get('channel_id');
  my $channel1 = $studyFactorValues->[1]->get('channel_id');

  $self->assert_matches(qr/\d+/, $channel0);
  $self->assert($channel0 == $channel1, "Cy3 ontologyId should be the same for both studyfactorvalues");

  my $valueOe0 = $studyFactorValues->[0]->get('value_ontology_entry_id');
  my $valueOe1 = $studyFactorValues->[1]->get('value_ontology_entry_id');

  $self->assert_matches(qr/\d+/, $valueOe0);
  $self->assert_null($valueOe1);

  my $seenShouldBeNull = $t->mapStudyFactorValues($voFactorValues, $assay, $channel, $studyDesigns, 2);
  $self->assert_equals(0, scalar(@$seenShouldBeNull));
}

#--------------------------------------------------------------------------------



sub test_mapStudyDesignsAssays {
  my $self = shift;

  my $t = $self->getTranslator();

  my $affiliations = $t->mapAffiliations($voAffiliations);
  my $contacts = $t->mapPersons($voPersons, $affiliations);
  my $study = $t->mapStudy($voStudy, $contacts);
  my $studyDesigns = $t->mapStudyDesigns($voStudyDesigns, $study);
  my $arrayDesigns = $t->mapArrayDesigns($voAssays);
  my $protocols = $t->mapProtocols($voProtocols);
  my $bioMaterials = $t->mapBioMaterials($voBioMaterials, $contacts);

  my $assays = $t->mapAssays($voAssays, $contacts, $arrayDesigns, $studyDesigns, $protocols, $bioMaterials);

  my $studyDesignAssays = $t->mapStudyDesignsAssays($studyDesigns, $assays);

  foreach(@$studyDesignAssays) {
    $self->assert_equals('GUS::Model::RAD::StudyDesignAssay', ref($_));
  }

  my $expectedCount = scalar(@$assays) * scalar(@$studyDesigns);
  $self->assert_equals($expectedCount, scalar(@$studyDesignAssays));
}


sub test_mapBibliographicReference {
  my $self = shift;

  my $t = $self->getTranslator();

  my $pubmed = 15314688;

  my $bibRef = $t->mapBibliographicReference($pubmed);

  $self->assert_equals('2004', $bibRef->getYear());
  $self->assert_equals('The Journal of clinical investigation', $bibRef->getPublication());
  $self->assert_equals($pubmed, $bibRef->getSourceId());

  my $bibRefType = $bibRef->getParent('SRes::BibRefType');
  $self->assert_equals('journal article', $bibRefType->getName());

  my $extDb = $bibRef->getParent('SRes::ExternalDatabaseRelease')->getParent('SRes::ExternalDatabase');
  $self->assert_equals('pubmed', $extDb->getName());

}


1;
