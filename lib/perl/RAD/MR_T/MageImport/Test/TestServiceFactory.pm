package RAD::MR_T::MageImport::Test::TestServiceFactory;

use base qw(Test::Unit::TestCase);
use Carp;
use Log::Log4perl qw(get_logger :levels);
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::ServiceFactory;

sub set_up {
  my $self = shift;

 # set up logger
  Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
  my $mLogger = get_logger("RAD::MR_T::MageImport");
  my $config = XMLin("config.xml", ForceArray => 1);

  $mLogger->info("Start the ServiceFactory");
  $mLogger->info("****Dump the config****", sub { Dumper($config) } );
  $self->{logger} = $mLogger;
  $self->{config} = $config;
  $self->{serviceFactory} =  RAD::MR_T::MageImport::ServiceFactory->new($config);

  $mLogger->info("Finish making the serices");

}

sub test_getServiceByName{
  my $self = shift;
  my $reader = $self->{serviceFactory}->getServiceByName("reader");

  $self->assert(UNIVERSAL::isa($reader,'RAD::MR_T::MageImport::Service::Reader::MagemlReader'), "check object type ok");
}


sub test_getServiceByName2{
  my $self = shift;
  my $reader = $self->{serviceFactory}->getServiceByName("translator");

  $self->assert(UNIVERSAL::isa($reader,'RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator'), "check object type ok");
}


sub test_getServiceByName3{
  my $self = shift;
  my $reader = $self->{serviceFactory}->getServiceByName("validator");

  $self->assert(UNIVERSAL::isa($reader,'RAD::MR_T::MageImport::Service::ValidatorRule::BlankRule'), "check object type ok");
}

sub test_getServiceByName4{
  my $self = shift;
  my $reader = $self->{serviceFactory}->getServiceByName("processor");

  $self->assert(UNIVERSAL::isa($reader,'RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries'), "check object type ok");

  $self->assert_equals("purify", $reader->getTermStr());
}


1;
