package RAD::MR_T::MageImport::Test::VOTestSuite;

use IO::File; 
use Carp;

sub new {
    my $class = shift;
    return bless {}, $class;
}
# I am doing the test one by class, ideally I should do "find" to get all testcases
sub suite {
    my $class = shift;
    my $suite = Test::Unit::TestSuite->empty_new("VO Tests");

    opendir(DIR, ".") || croak("could not read current directory");
    my @dir = grep (/^Test/, readdir(DIR));

    foreach my $test (@dir){
      $test =~ s/([^\.]+)\.pm/$1/g;
#      print STDOUT "\t", $test;
      $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::$test")) unless ($test eq "TestMain" || $test eq "TestMagemlReader" || $test eq "TestVoToMage");

#    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::$test")) if ($test eq "TestMagemlReader");
#    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::$test")) if ($test eq "TestMain");
    }
    return $suite;
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestStudyVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestPersonVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestAffiliationVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestAssayVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestAcquisitionVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestQuantificationVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestExternalDatabaseVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestProtocolParamVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestQuantificationVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestProtocolVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestBioMaterialMeasurementVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestBioMaterialVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestOntologyEntryVO"));

    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestStudyDesignVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestStudyFactorVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestTreatmentVO"));
    $suite->add_test(Test::Unit::TestSuite->new("RAD::MR_T::MageImport::Test::TestDocRoot"));
    return $suite;
}

1;
