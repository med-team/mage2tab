package RAD::MR_T::MageImport::Test::TestProcessModule;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Service::ProcessModule::BlankModule;
use RAD::MR_T::MageImport::Service::Processor;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;
 $self->{vo} =  RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
}

sub test_blankModule{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::BlankModule->new($processor);

  $self->assert ($decoProcessor->process($self->{vo}) eq "RAD::MR_T::MageImport::Service::ProcessModule::BlankModule");
}


sub test_blankModule2{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my $decoProcessor = _BlankModule2->new(_BlankModule1->new($processor));

  $self->assert ($decoProcessor->process($self->{vo}) eq "_BlankModule2");
}


sub test_blankModule3{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my @modules = qw (_BlankModule1 _BlankModule2);
  foreach $rule(@modules){
    $processor = $rule->new($processor);
  }

  $self->assert ($processor->process($self->{vo}) eq "_BlankModule2");
}

package _BlankModule1;

# this is blank module impl1

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myprocess($docRoot);
 return ref($self);
}

sub myprocess {
  my ($self, $docRoot) = @_;
  return 1;
}

package _BlankModule2;

# this is blank module impl2

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->yourprocess($docRoot);
 return ref($self);
}

sub yourprocess {
  my ($self, $docRoot) = @_;
  return 1;
}

1;
