package RAD::MR_T::MageImport::Test::TestParameterValueVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::ParameterValueVO;

use strict;

my $parameterValue;

sub set_up {
  my $self = shift;
  $parameterValue = RAD::MR_T::MageImport::VO::ParameterValueVO->new({value=>"testValue", parameterName=>"testParameterName"}); 
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($parameterValue,'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
}

sub test_getValue {
  my $self = shift;
  $self->assert_equals("testValue", $parameterValue->getValue());
}

sub test_setValue {
  my $self = shift;
  $parameterValue->setValue("newValue");
  $self->assert_equals("newValue", $parameterValue->getValue());
}

sub test_getParameterName {
  my $self = shift;
  $self->assert_equals("testParameterName", $parameterValue->getParameterName());
}

sub test_setParameterName {
  my $self = shift;
  $parameterValue->setParameterName("newParameterName");
  $self->assert_equals("newParameterName", $parameterValue->getParameterName());
}


1;
