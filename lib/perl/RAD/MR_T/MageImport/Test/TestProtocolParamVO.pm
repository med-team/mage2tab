package RAD::MR_T::MageImport::Test::TestProtocolParamVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::ProtocolParamVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = 
    RAD::MR_T::MageImport::VO::ProtocolParamVO->new({
					      name=>"test", 
					      value=>"test", 
					      dataType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
					      unitType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new()
					     });
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::ProtocolParamVO'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getValue {
  my $self = shift;
  my $setValue = $self->{vo}->getValue();
  $self->assert("test" eq $setValue);
}

sub test_setValue {
  my $self = shift;
  $self->{vo}->setValue("test2");
  my $setValue = $self->{vo}->getValue();
  $self->assert("test2" eq $setValue);
  $self->assert("test" ne $setValue);
}

sub test_getUnitType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getUnitType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setUnitType {
  my $self = shift;
  $self->{vo}->setUnitType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getUnitType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getDataType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getDataType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setDataType {
  my $self = shift;
  $self->{vo}->setDataType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getDataType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
