package RAD::MR_T::MageImport::Test::TestQuantificationVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::QuantificationVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::QuantificationVO->new({name=>"test",
                                                                  uri=>"test",
                                                                  parameterValues=>[RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'testName', value => 'testValue'}),
                                                                                ],
                                                                 }); 
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::QuantificationVO'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq $setValue);
}

sub test_getUri {
  my $self = shift;
  my $setValue = $self->{vo}->getUri();
  $self->assert("test" eq  $setValue);
}

sub test_setUri {
  my $self = shift;
  $self->{vo}->setUri("test2");
  my $setValue = $self->{vo}->getUri();
  $self->assert("test2" eq $setValue);
}

sub test_getParameterValues {
  my ($self) = @_;
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('testValue', $setValue->[0]->getValue);
}

sub test_setParameterValues {
  my $self = shift;
  $self->{vo}->setParameterValues([RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'newValue'})]);
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('newValue', $setValue->[0]->getValue);
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
