package RAD::MR_T::MageImport::Test::TestTreatmentVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::TreatmentVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;

use Data::Dumper;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;

  $self->{vo} = 
    RAD::MR_T::MageImport::VO::TreatmentVO->new({
						 name=>"test", 
					 	 orderNum=>2, 
						 outputBM=>RAD::MR_T::MageImport::VO::BioMaterialVO->new(),
						 inputBM=>RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new(),
					 	 protocol=>RAD::MR_T::MageImport::VO::ProtocolVO->new(),
						 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
                                                 parameterValues=>[RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'testName', value => 'testValue'}),
                                                                                ],
						});

}



  sub test_construct{
    my $self = shift;
    $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::TreatmentVO'), "check object type ok");

  }

   sub test_getName {
     my $self = shift;
     my $setValue = $self->{vo}->getName();

     $self->assert_equals ("test", $setValue);
   }

   sub test_setName {
     my $self = shift;
     $self->{vo}->setName("test2");
     my $setValue = $self->{vo}->getName();
     $self->assert("test2" eq $setValue);
   }

  sub test_getOrderNum {
    my $self = shift;
    my $setValue = $self->{vo}->getOrderNum();
    $self->assert(2 == $setValue);
  }

  sub test_setOrderNum { 
    my $self = shift;
    $self->{vo}->setOrderNum(3);
    my $setValue = $self->{vo}->getOrderNum();
    $self->assert(3 == $setValue);
  }

  sub test_getOutputBM {
    my ($self) = @_;
    my $setValue = $self->{vo}->getOutputBM();
    $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
  }

  sub test_setOutputBM {
    my $self = shift;
    $self->{vo}->setOutputBM(RAD::MR_T::MageImport::VO::BioMaterialVO->new());
    my $setValue = $self->{vo}->getOutputBM();
    $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check OutputBM object type ok");
  }

 #  sub test_getInputBM { 
#     my ($self) = @_;
#     my $setValue = $self->{vo}->getInputBM();
#     $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO'), "check object type ok");
#   }

#   sub test_setInputBM {
#     my $self = shift;


#     $self->{vo}->setInputBM(RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new());
#     my $setValue = $self->{vo}->getInputBM();

#     $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO'), "check InputBM object type ok");
#   }
 

sub test_getParameterValues {
  my ($self) = @_;
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('testValue', $setValue->[0]->getValue);
}

sub test_setParameterValues {
  my $self = shift;
  $self->{vo}->setParameterValues([RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'newValue'})]);
  my $setValue = $self->{vo}->getParameterValues();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::ParameterValueVO'), "check object type ok");
  $self->assert_equals('newValue', $setValue->[0]->getValue);
}


sub tear_down {
  my $self = shift;


  $self->{vo} = undef;
 $self->{vo1} = undef;


}

1;
