package RAD::MR_T::MageImport::Test::TestUtil;

use strict;

use  RAD::MR_T::MageImport::VO::AssayVO;
use  RAD::MR_T::MageImport::VO::TreatmentVO;
use  RAD::MR_T::MageImport::VO::BioMaterialVO;
use  RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO;;

use  RAD::MR_T::MageImport::Util qw(searchObjArrayByObjName findAllAssayBioMaterials);

use Data::Dumper;

use Error qw(:try);

use base qw(Test::Unit::TestCase); 

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

# TODO:  There are utility methods which still need testing!!

sub test_searchObjArrayByObjName {
  my $self = shift;

  my $ar = [ RAD::MR_T::MageImport::VO::AssayVO->new({name => 'name1'}),
             RAD::MR_T::MageImport::VO::AssayVO->new({name => 'name2'}),
             RAD::MR_T::MageImport::VO::AssayVO->new({name => 'name3'}),
             RAD::MR_T::MageImport::VO::AssayVO->new({name => 'name4'}),
           ];

  try {
    searchObjArrayByObjName($ar, 'notFound');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch RAD::MR_T::MageImport::MageImportError with {};

  my $assay = searchObjArrayByObjName($ar, 'name1');
  $self->assert_equals('RAD::MR_T::MageImport::VO::AssayVO', ref($assay));
  $self->assert_equals('name1', $assay->getName);

  push(@$ar,  RAD::MR_T::MageImport::VO::AssayVO->new({name => 'name2'}));

  try {
    searchObjArrayByObjName($ar, 'name2');
    $self->assert_equals(0, "Should Have thrown an error");
  } catch  RAD::MR_T::MageImport::MageImportError with {};

}


sub test_findAllAssayBioMaterials {
  my $self = shift;

  my $bioMaterials = [RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'source', subclassView => 'BioSource'}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample1', subclassView => 'BioSample'}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample2', subclassView => 'BioSample'}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'sample3', subclassView => 'BioSample'}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'samplePool', subclassView => 'BioSample'}),
                      RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'lex', subclassView => 'LabeledExtract'}),
                     ];

  my $assay = RAD::MR_T::MageImport::VO::AssayVO->new({name => 'assay', labeledExtract => $bioMaterials->[5] });

  my $treatments = [RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[5], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[4]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[4], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[1]}),
                                                                               RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[2]}),
                                                                               RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[3]}),
                                                                              ],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[3], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[2], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                    RAD::MR_T::MageImport::VO::TreatmentVO->new({outputBM => $bioMaterials->[1], 
                                                                 inputBMMs => [RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $bioMaterials->[0]})],
                                                                }),
                   ];

  my $output = findAllAssayBioMaterials($assay, $treatments);

  $self->assert_equals(6, scalar(@$output));
}

1;
