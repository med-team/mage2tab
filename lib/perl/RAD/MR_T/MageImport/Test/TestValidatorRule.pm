package RAD::MR_T::MageImport::Test::TestValidatorRule;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Service::ValidatorRule::BlankRule;
use RAD::MR_T::MageImport::Service::Validator;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;

use Log::Log4perl qw(get_logger :levels);

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;
 $self->{vo} =  RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
 # set up logger
 Log::Log4perl->init("$ENV{GUS_HOME}/config/log4perl.config");
 $self->{logger} = get_logger("RAD::MR_T::MageImport::Service");
}

sub test_blankRule{
  my $self = shift;
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;

  $validator->setLogger($self->{logger});

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::BlankRule->new($validator);

  $self->assert ($decoValidator->check($self->{vo}) eq "RAD::MR_T::MageImport::Service::ValidatorRule::BlankRule");
}


sub test_blankRule2{
  my $self = shift;
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($self->{logger});
  my $decoValidator = _BlankRule2->new(_BlankRule1->new($validator));

  $self->assert ($decoValidator->check($self->{vo}) eq "_BlankRule2");
}


sub test_blankRule3{
  my $self = shift;
  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($self->{logger});
  my @rules = qw (_BlankRule1 _BlankRule2);
  foreach $rule(@rules){
    $validator = $rule->new($validator);
  }

  $self->assert ($validator->check($self->{vo}) eq "_BlankRule2");
}

package _BlankRule1;

# this is blank rule impl1

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

sub check {
 my ($self, $docRoot) = @_;
 $self->getValidator->check($docRoot);
 $self->mycheck($docRoot);
 return ref($self);
}

sub mycheck {
  my ($self, $docRoot) = @_;
  return 1;
}

package _BlankRule2;

# this is blank rule impl2

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

sub check {
 my ($self, $docRoot) = @_;
 $self->getValidator->check($docRoot);
 $self->yourcheck($docRoot);
 return ref($self);
}

sub yourcheck {
  my ($self, $docRoot) = @_;
  return 1;
}

1;
