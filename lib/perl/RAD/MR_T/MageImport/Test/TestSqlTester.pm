package RAD::MR_T::MageImport::Test::TestSqlTester;
use base qw(Test::Unit::TestCase);

use strict;

use GUS::ObjRelP::DbiDatabase;

use GUS::Supported::GusConfig;

use RAD::MR_T::MageImport::Service::Tester::SqlTester;

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use Error qw(:try);

my ($tester, $testCount);

my $lines = [join("|", '\w+', 'select \'test_example\' from dual', 'ex_test'),
             join("|", '\w+', 'select * from (select \'$$ex_test$$\' from dual)', ''),
             join("|", '[,123]+', 'select 1,2,3 from dual', 'multi_test'),
             join("|", '[,123]+', 'select * from (select \'$$multi_test$$\' from dual)', ''),
            ];

#================================================================================

sub new {
  my $self = shift()->SUPER::new(@_);

  $testCount = scalar $self->list_tests();
  
  return $self;
}

#--------------------------------------------------------------------------------

sub set_up {
  my $self = shift;

  my @properties;

  my $configFile = "$ENV{GUS_HOME}/config/gus.config";

  unless(-e $configFile) {
    my $error = "Config file $configFile does not exist.";
    die $error;
  }

  my $config = GUS::Supported::GusConfig->new($configFile);
  
  my $login       = $config->getDatabaseLogin();
  my $password    = $config->getDatabasePassword();
  my $core        = $config->getCoreSchemaName();
  my $dbiDsn      = $config->getDbiDsn();
  my $oraDfltRbs  = $config->getOracleDefaultRollbackSegment();

  my $database;
#  unless($database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    $database = GUS::ObjRelP::DbiDatabase->new($dbiDsn, $login, $password,
                                                  0,0,1,$core, $oraDfltRbs);
#  }
  
  my $fn = "dummyFile";
  my $handle = $database->getDbHandle();

 # set up logger
  Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
  my $sLogger = get_logger("RAD::MR_T::MageImport::Service");

  $tester = RAD::MR_T::MageImport::Service::Tester::SqlTester->new($fn, $handle);
  $tester->{_lines_array} = $lines;
  $tester->setLogger($sLogger);

}

#--------------------------------------------------------------------------------

sub tear_down {
  my $self = shift;

   $testCount--;

  my $database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase();
  $database->{'dbh'}->rollback();

  # LOG OUT AFTER ALL TESTS ARE FINISHED
  if($testCount <= 0) {
    $database->logout();
 #   GUS::ObjRelP::DbiDatabase->setDefaultDatabase(undef);
    print STDERR "LOGGING OUT FROM DBI DATABASE\n";
  }
}

#--------------------------------------------------------------------------------

sub test_parseLines {
  my $self = shift;

  my $sqlHash = $tester->parseLines();

  foreach(keys %$sqlHash) {
    my $regex = $sqlHash->{$_}->{expected};
    $self->assert_matches(qr($regex), $sqlHash->{$_}->{actual});
  }

}

#--------------------------------------------------------------------------------

sub test_parseLine {
  my $self = shift;

  my %params;

  my ($expected, $actual, $sql) = $tester->_parseLine($lines->[0], \%params);
  $self->assert_equals('\w+', $expected);
  $self->assert_equals('test_example', $actual);
  $self->assert_equals('select \'test_example\' from dual', $sql);

  ($expected, $actual, $sql) = $tester->_parseLine($lines->[1], \%params);
  $self->assert_equals('\w+', $expected);
  $self->assert_equals('test_example', $actual);
  $self->assert_equals('select * from (select \'test_example\' from dual)', $sql);

  ($expected, $actual, $sql) = $tester->_parseLine($lines->[2], \%params);
  $self->assert_equals('[,123]+', $expected);
  $self->assert_equals('1,2,3', $actual);
  $self->assert_equals('select 1,2,3 from dual', $sql);

  ($expected, $actual, $sql) = $tester->_parseLine($lines->[3], \%params);
  $self->assert_equals('[,123]+', $expected);
  $self->assert_equals('1,2,3', $actual);
  $self->assert_equals('select * from (select \'1,2,3\' from dual)', $sql);

}

#--------------------------------------------------------------------------------

sub test_runSql {
  my $self = shift;

  try {
    $tester->_runSql("select * from Study.Study where contact_id = 9");
    $self->assert(0, "Should have thrown Exception");
  } catch RAD::MR_T::MageImport::TesterException::SqlError with {};

  my $res = $tester->_runSql("select study_id from Study.Study where study_id = 2560");
  $self->assert_equals(2560, $res);

  my $many = $tester->_runSql("select 1, 2, 3 from dual");
  $self->assert_equals('1,2,3', $many);

}

1;
