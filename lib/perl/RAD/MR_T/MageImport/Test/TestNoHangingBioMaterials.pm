package RAD::MR_T::MageImport::Test::TestNoHangingBioMaterials;
use base qw(Test::Unit::TestCase);

use strict;
use Error qw(:try);

use Log::Log4perl qw(get_logger :levels);

use Data::Dumper;

use RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials;
use RAD::MR_T::MageImport::Service::Validator;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

my $docRoot;;
Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $logger = get_logger("RAD::MR_T::MageImport::Service");

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

sub set_up {
 my $self = shift;

 my $reader =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("toxoTemplate.tab");
 $reader->setLogger($logger);
 $docRoot = $reader->parse();
}

sub test_AssayHasContact {
  my $self = shift;

  my $validator = RAD::MR_T::MageImport::Service::Validator->new;
  $validator->setLogger($logger);

  my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials->new($validator);
  $self->assert("RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials", $decoValidator->check($docRoot));

  my @bioMaterials = @{$docRoot->getBioMaterialVOs()};

  push(@bioMaterials, RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'hanging', subclassView => 'Source'}));
  $docRoot->bioMaterialVOs(\@bioMaterials);

  try {
    $decoValidator->check($docRoot);
  } catch RAD::MR_T::MageImport::ValidatorException with {  };

  pop(@bioMaterials);
  push(@bioMaterials, RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'hanging', subclassView => 'LabeledExtract'}));
  $docRoot->bioMaterialVOs(\@bioMaterials);

  try {
    $decoValidator->check($docRoot);
  } catch RAD::MR_T::MageImport::ValidatorException with {  };
}


1;
