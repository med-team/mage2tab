package RAD::MR_T::MageImport::Test::TestSetQuanProtocolToGusProtocol;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocolToGusProtocol;
use RAD::MR_T::MageImport::Service::Processor;
use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::Reader::MockReader;


sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;

 $self->{vo} = RAD::MR_T::MageImport::Service::Reader::MockReader->new->parse();
}

sub test_SetQuanProtocol{
  my $self = shift;
  my $processor = RAD::MR_T::MageImport::Service::Processor->new;

  my $decoProcessor = RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocolToGusProtocol->new($processor, "test");

  $self->assert ($decoProcessor->process($self->{vo}) eq "RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocolToGusProtocol");
  my $docRoot = $self->{vo};
  foreach my $assay(@{$docRoot->getAssayVOs}){
    foreach my $acq(@{$assay->getAcquisitions}){
       foreach my $quan(@{$acq->getQuantifications}){
	$self->assert_equals("test", $quan->getProtocolName);
      }
    }
  }
  my $found = 0;
  foreach my $protocol(@{$docRoot->getProtocolVOs}){
    $found = 1 if $protocol->getName eq "test";
  }

  $self->assert($found);
}


1;
