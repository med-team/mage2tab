package RAD::MR_T::MageImport::Test::TestBioMaterialMeasurementVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({
									  value=>"test", 
									  bioMaterial=>RAD::MR_T::MageImport::VO::BioMaterialVO->new, 
									  unitType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new}
									); 
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getValue {
  my $self = shift;
  my $setValue = $self->{vo}->getValue();
  $self->assert("test" eq $setValue);
}

sub test_setValue {
  my $self = shift;
  $self->{vo}->setValue("test2");
  my $setValue = $self->{vo}->getValue();
  $self->assert("test2" eq $setValue);
  $self->assert("test" ne $setValue);
}

sub test_getBioMaterial {
  my ($self) = @_;
  my $setValue = $self->{vo}->getBioMaterial();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
}

sub test_setBioMaterial {
  my $self = shift;
  $self->{vo}->setBioMaterial(RAD::MR_T::MageImport::VO::BioMaterialVO->new());
  my $setValue = $self->{vo}->getBioMaterial();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
}


sub test_getUnitType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getUnitType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setUnitType {
  my $self = shift;
  $self->{vo}->setUnitType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getUnitType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
