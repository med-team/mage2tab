package RAD::MR_T::MageImport::Test::TestSqlReporter;

use base qw(Test::Unit::TestCase);
use Carp;
use Log::Log4perl qw(get_logger :levels);
use XML::Simple;
use Data::Dumper;
use Error qw(:try);

use GUS::Supported::GusConfig;
use GUS::ObjRelP::DbiDatabase;
use RAD::MR_T::MageImport::Service::Reporter::SqlReporter;
use RAD::MR_T::MageImport::MageImportError;

my $testCount;

sub new {
  my $self = shift()->SUPER::new(@_);
  $testCount = scalar $self->list_tests();

  return $self;
}

sub set_up {
  my $self = shift;
  my $configFile = "$ENV{GUS_HOME}/config/gus.config";

  unless(-e $configFile) {
    my $error = "Config file $configFile does not exist.";
    die $error;
  }

  my $config = GUS::Supported::GusConfig->new($configFile);
  
  my $login       = $config->getDatabaseLogin();
  my $password    = $config->getDatabasePassword();
  my $core        = $config->getCoreSchemaName();
  my $dbiDsn      = $config->getDbiDsn();
  my $oraDfltRbs  = $config->getOracleDefaultRollbackSegment();

  my $database;
  unless($database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    $database = GUS::ObjRelP::DbiDatabase->new($dbiDsn, $login, $password,
                                                  0,0,1,$core, $oraDfltRbs);
  }

  $self->{_dbi_database} = $database;

 # set up logger
  Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
  my $mLogger = get_logger("RAD::MR_T::MageImport::Service");

  my $reporter = RAD::MR_T::MageImport::Service::Reporter::SqlReporter->new("sqlReporter.txt");

  $reporter->setLogger($mLogger);
  $self->{logger} = $mLogger;
  $self->{reporter} = $reporter;


  $database =   GUS::ObjRelP::DbiDatabase->getDefaultDatabase();
  $database->setDefaultAlgoInvoId(376473);

}

sub test_report {
  my $self = shift;

  try {
    $self->{reporter} ->report();
  } catch RAD::MR_T::MageImport::ReporterException with {
     my $ex = shift;
     $self->{logger}->debug("stacktrace");
     $ex->throw;
   };
}


sub tear_down {
  my $self = shift;

   $testCount--;

  my $database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase();
  $database->{'dbh'}->rollback();

  # LOG OUT AFTER ALL TESTS ARE FINISHED
  if($testCount <= 0) {
    $database->logout();
 #   GUS::ObjRelP::DbiDatabase->setDefaultDatabase(undef);
    print STDERR "LOGGING OUT FROM DBI DATABASE\n";
  }
}
1;
