package RAD::MR_T::MageImport::Test::TestStudyDesignVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::StudyDesignVO;
use RAD::MR_T::MageImport::VO::StudyFactorVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} 
    = RAD::MR_T::MageImport::VO::StudyDesignVO->new({
						     name=>"test", 
						     types=>[RAD::MR_T::MageImport::VO::OntologyEntryVO->new()], 
						     factors=>[RAD::MR_T::MageImport::VO::StudyFactorVO->new()]
						    });
}

sub test_construct{
  my $self = shift;

  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::StudyDesignVO'), "check object type ok");
}

sub test_getName {
  my $self = shift;
  my $setName = $self->{vo}->getName();
  $self->assert("test" eq $setName);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("splitting");
  my $setValue = $self->{vo}->getName();
  $self->assert("splitting" eq $setValue);
}

sub test_getTypes {
  my ($self) = @_;
  my $setValue = $self->{vo}->getTypes();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setTypes {
  my $self = shift;
  $self->{vo}->setTypes([RAD::MR_T::MageImport::VO::OntologyEntryVO->new()]);
  my $setValue = $self->{vo}->getTypes();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_addTypes {
  my $self = shift;
  $self->{vo}->addTypes((RAD::MR_T::MageImport::VO::OntologyEntryVO->new()));
  my $setValue = $self->{vo}->getTypes();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check added studydesign object type ok");
}

sub test_setFactors {
  my $self = shift;
  $self->{vo}->setFactors([RAD::MR_T::MageImport::VO::StudyFactorVO->new()]);
  my $setValue = $self->{vo}->getFactors();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::StudyFactorVO'), "check studydesign object type ok");
}

sub test_addFactors {
  my $self = shift;
  $self->{vo}->addFactors((RAD::MR_T::MageImport::VO::StudyFactorVO->new()));
  my $setValue = $self->{vo}->getFactors();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::StudyFactorVO'), "check added studydesign object type ok");
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
