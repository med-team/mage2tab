package RAD::MR_T::MageImport::Test::TestExceptions;

use base qw(Test::Unit::TestCase);
use Error qw(:try);
use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);
use RAD::MR_T::MageImport::VO::StudyDesignVO;
use RAD::MR_T::MageImport::VO::StudyFactorVO;
use RAD::MR_T::MageImport::VO::StudyVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;

  return $self;
}

sub set_up {
 my $self = shift;
 $self->{vo} = {
		method=>"setFactors",
		class=>RAD::MR_T::MageImport::VO::StudyDesignVO->new,
		expect=>"RAD::MR_T::MageImport::VO::StudyFactorVO",
		factors=>[RAD::MR_T::MageImport::VO::StudyVO->new()]
	       };

}


sub test_checkArrayArgumentSameType{
  my $self = shift;
  try {
    &checkArrayArgumentSameType($self->{vo}->{method}, $self->{vo}->{class}, $self->{vo}->{expect}, $self->{vo}->{factors}->[0]); 
    $self->assert(0);
  } catch RAD::MR_T::MageImport::MageImportObjectTypeError with {};

}


sub test_checkArgumentType{
  my $self = shift;
  try {
    &checkArgumentType($self->{vo}->{method}, $self->{vo}->{class}, $self->{vo}->{expect}, $self->{vo}->{factors}); 
    $self->assert(0);
  } catch RAD::MR_T::MageImport::MageImportObjectTypeError with {};

}


sub test_checkArgument{
  my $self = shift;
  try {
    &checkArgument($self->{vo}->{method}, $self->{vo}->{class}); 
    $self->assert(0);
  } catch RAD::MR_T::MageImport::MageImportArgumentError with {};
}


sub test_checkArgument2{
  my $self = shift;
  try {
    &checkArgument($self->{vo}->{method}, $self->{vo}->{class}, $self->{vo}->{factors}, $self->{vo}->{factors}); 
    $self->assert(0);
  } catch RAD::MR_T::MageImport::MageImportArgumentError with {};
}
1;
