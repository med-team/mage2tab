package RAD::MR_T::MageImport::Test::TestVoToMageTab;

use base qw(Test::Unit::TestCase);
use Carp;
use Log::Log4perl qw(get_logger :levels);
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::ServiceFactory;

Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
my $sLogger = get_logger("RAD::MR_T::MageImport::Service");

my ($loadMageDoc, $testCount);

sub new {
  my $self = shift()->SUPER::new(@_);

  $testCount = scalar $self->list_tests();


  return $self;
}

sub set_up {
  my ($self) = @_;
}

sub tear_down {
  my $self = shift;

  $testCount--;

}

sub test_run{
  my ($self) = @_;
  my $mLogger = get_logger("RAD::MR_T::MageImport");
  my $config = XMLin("config_formagetab.xml");
  my $serviceFactory = RAD::MR_T::MageImport::ServiceFactory->new($config);

  my $reader =  $serviceFactory->getServiceByName('reader');

  $mLogger->info("parse the mage file", $config->{service}->{reader}->{property}->{value});
  my $docRoot = $reader->parse();

  my $processor = $serviceFactory->getServiceByName('processor');;

  $mLogger->info("process the value objects using ", ref($processor));
  $processor->process($docRoot);

  my $translator = $serviceFactory->getServiceByName('translator');

  $mLogger->info("translate the value objects to GUS objects using ", ref($translator));
  my $study = $translator->mapAll($docRoot);

}


1;
