package RAD::MR_T::MageImport::Test::TestMagemlReader;

use base qw(Test::Unit::TestCase);
use Carp;
use Log::Log4perl qw(get_logger :levels);

use RAD::MR_T::MageImport::Service::Reader::MagemlReader;
 
sub new {
  my $self = shift()->SUPER::new(@_);

 # set up logger
  Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
  my $sLogger = get_logger("RAD::MR_T::MageImport::Service");
  $sLogger->debug("Start the MagemlReader testers");

  $self->{logger} = $sLogger;
  $self->{reader} =  RAD::MR_T::MageImport::Service::Reader::MagemlReader->new("test2.xml");
  $self->{reader}->setLogger($sLogger);
  $self->{doc} = RAD::MR_T::MageImport::VO::DocRoot->new();
  $self->{mage} = $self->{reader} ->getMage();
  $sLogger->debug("finish reading the mage-ml");
  return $self; 
}

sub set_up {
  my $self = shift;

}

sub test_setExtDBVOs {
  my $self = shift;
  $self->{reader}->setExtDBVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
  $self->assert(1);
}

sub test_setPersonAndAffiliationVOs {
  my $self = shift;
  my $logger =  $self->{logger};

  $self->{reader}->setPersonAndAffiliationVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
}

sub test_setProtocolAndParamVOs {
  my $self = shift;
  my $logger =  $self->{logger};

  $self->{reader}->setProtocolAndParamVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
}

sub test_setStudyVO {
  my $self = shift;
  $self->{reader}->setPersonAndAffiliationVOs($self->{doc}, $self->{mage});
  $self->{reader}->setStudyVO($self->{doc}, $self->{mage});
#check the $self->{doc}
  $self->assert(1);
}

sub test_setBioMatVOs {
  my $self = shift;
  $self->{reader}->setBioMatVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
  $self->assert(1);
}

sub test_setTreatmentVOs {
  my $self = shift;
  $self->{reader}->setBioMatVOs($self->{doc}, $self->{mage});
  $self->{reader}->setTreatmentVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
  $self->assert(1);
}

sub test_setAssayVOs {
  my $self = shift;
  $self->{reader}->setPersonAndAffiliationVOs($self->{doc}, $self->{mage});
  $self->{reader}->setStudyVO($self->{doc}, $self->{mage});
  $self->{reader}->setAssayVOs($self->{doc}, $self->{mage});
#check the $self->{doc}
  $self->assert(1);
}

sub tear_down {
  my $self = shift;

}

1;
