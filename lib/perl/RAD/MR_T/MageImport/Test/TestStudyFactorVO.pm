package RAD::MR_T::MageImport::Test::TestStudyFactorVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::StudyFactorVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::StudyFactorVO->new({name=>"test", type=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new()});
}

sub test_construct{
  my $self = shift;

  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::StudyFactorVO'), "check object type ok");
}

sub test_getName {
  my $self = shift;
  my $setName = $self->{vo}->getName();
  $self->assert("test" eq $setName);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("splitting");
  my $setValue = $self->{vo}->getName();
  $self->assert("splitting" eq $setValue);
}

sub test_getType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setType {
  my $self = shift;
  $self->{vo}->setType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
