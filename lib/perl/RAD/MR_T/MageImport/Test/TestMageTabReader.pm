package RAD::MR_T::MageImport::Test::TestMageTabReader;

use strict;

use Log::Log4perl qw(get_logger :levels);

use RAD::MR_T::MageImport::Service::Reader::MageTabReader;

use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::PersonVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;

use RAD::MR_T::MageImport::Util qw(searchObjArrayByObjName);

use RAD::MR_T::MageImport::Service::Reader::MockReader;

use Data::Dumper;

use Error qw(:try);

use base qw(Test::Unit::TestCase); 

my $sdrfHeader = [ 'Source ID',
                     'MaterialType',
                     'MaterialType Term Source',
                     'Characteristics [StrainOrLine]',
                     'Characteristics [DevelopmentalStage]',
                     'Characteristics [DevelopmentalStage] Term Source',
                     'Characteristics [IndividualGeneticCharacteristic]',
                     'Characteristics [IndividualGeneticCharacteristic] Term Source',
                     'Characteristics [Organism]',
                     'Characteristics [Organism] Term Source',
                     'Protocol REF',
                     'ParameterValue [time_of_extraction]',
                     'Unit [TimeUnit] (Term Source: MO)',
                     'Sample ID',
                     'MaterialType',
                     'MaterialType Term Source',
                     'Protocol REF',
                     'Extract ID',
                     'MaterialType',
                     'MaterialType Term Source',
                     'Protocol REF',
                     'ParameterValue [input_total_RNA]',
                     'Unit [MassUnit] (Term Source: MO)',
                     'ParameterValue [cRNA_yield]',
                   'Unit [MassUnit] (Term Source: MO)',
                     'Labeled Extract ID',
                     'MaterialType',
                     'MaterialType Term Source',
                     'Label',
                     'Protocol REF',
                     'ParameterValue [cRNA_hybridized]',
                   'Unit [MassUnit] (Term Source: MO)',
                     'Hybridization ID',
                     'ArrayDesign REF',
                     'Protocol REF',
                     'Image File',
                     'Protocol REF',
                     'ArrayData File',
                     'Protocol REF',
                     'ParameterValue [BG correct]',
                     'ParameterValue [Normalization]',
                     'ParameterValue [Summarization]',
                     'Normalization ID',
                     'DerivedArrayData File',
                     'FactorValue [Strain]',
                     'FactorValue [Time Grown]',
                     'Unit [TimeUnit] (Term Source: MO)'
                   ];

my $sdrfLine = [ 'RH 1',
                 'whole_organism',
                 'MO',
                 'RG (Type I)',
                 'tachyzoite',
                 'nci_meta',
                 'wild_type',
                 'MO',
                 'Toxoplasma gondii RH',
                 'ncbitax',
                 'Parasite Growth (Roos)',
                 '48',
                 'hours',
                 'RH 1 grown in HFF Cells',
                 'cell',
                 'MO',
                 'Total RNA via RNAqueous-4PCR Kit (Ambion).',
                 'RH 1 - RNA',
                 'total_RNA',
                 'MO',
                 'One-Cycle Eukaryotic Target Labeling Assay (Affymetrix)',
                 '1',
                 'ug',
                 '46.1',
                 'ug',
                 'RH 1 - LEX',
                 'synthetic_RNA',
                 'MO',
                 'biotin',
                 'Affymetrix Hybridization Protocol (Penn Microarray Core Facility)',
                 '10',
                 'ug',
                 'RH 1 - Affymetrix Hybridization',
                 'TOXO_CHIP',
                 'Affymetrix GeneChip Scanner 3000 (GCS3000)',
                 'RH 1.DAT',
                 'Affymetrix MAS 5.0 Probe Cell Analysis',
                 'RH 1.CEL',
                 'Robust Multi-Array Average expression measure (Bioconductor affy package)',
                 'RMA 2',
                 'quantile',
                 'median polish',
                 'RH 1 - RMA Normalization',
                 'RH 1.TXT',
                 'RG (Type I)',
                 '48',
                 'hours'
               ];

#--------------------------------------------------------------------------------

sub new {
  my $self = shift()->SUPER::new(@_);
  return $self;
}

#--------------------------------------------------------------------------------

sub set_up {
  my $self = shift;

  Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
  my $sLogger = get_logger("RAD::MR_T::MageImport::Service");
  $sLogger->debug("Start the MageTabReader testers");

  $self->{logger} = $sLogger;

  my $mockReader = RAD::MR_T::MageImport::Service::Reader::MockReader->new();
  $self->{mock_docroot} = $mockReader->parse();

  $self->{reader} =  RAD::MR_T::MageImport::Service::Reader::MageTabReader->new("mageTabExample.tab");

  my $fileLines = $self->{reader}->readTabFile();
  $self->{reader}->setLines($fileLines);

  $self->{reader}->setLogger($sLogger);

}

#--------------------------------------------------------------------------------

sub getReader {$_[0]->{reader}}
sub getMockDocRoot {$_[0]->{mock_docroot}}

#--------------------------------------------------------------------------------

sub test_parseStudy {
  my $self = shift;

  my $r = $self->getReader();

  my $externalDatabaseVos = $r->parseExternalDatabases();
  my $affiliations = $r->parseAffiliations();
  my $persons = $r->parsePersons($affiliations);

  my $study = $r->parseStudy($persons, $externalDatabaseVos);

  $self->assert_null($study->getPubMedId());

  $self->assert_matches(qr(^Expression profiling ), $study->getName());
  $self->assert_matches(qr(^Determination of  st), $study->getDescription());

  $self->assert_equals(1, scalar(@{$study->getDesigns()}));

  my $contact = $study->getContact();
  $self->assert_equals('Roos', $contact->getLast());

}

#--------------------------------------------------------------------------------
# Not really testing too much here... this method essentially just calls the others
sub test_parseSdrf {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $externalDatabases = $r->parseExternalDatabases();
  $docRoot->externalDatabaseVOs($externalDatabases);

  my $protocols = $r->parseProtocols($externalDatabases);
  $docRoot->protocolVOs($protocols);

  my $affiliations = $r->parseAffiliations();
  $docRoot->affiliationVOs($affiliations);

  my $persons = $r->parsePersons($affiliations);
  $docRoot->personVOs($persons);

  my $study = $r->parseStudy($persons, $externalDatabases);
  $docRoot->studyVO($study);

  $r->parseSdrf('^Source ID', $docRoot);

  # some pooling in here...(34 biomaterials instead of 32)
  my $bioMaterials = $docRoot->getBioMaterialVOs();
  $self->assert_equals(34, scalar(@$bioMaterials));

  my $treatments = $docRoot->getTreatmentVOs();
  $self->assert_equals(24, scalar(@$treatments));

  my $assays = $docRoot->getAssayVOs();
  $self->assert_equals(8, scalar(@$assays));

}

#--------------------------------------------------------------------------------

sub test_doAssays {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $externalDatabases = $r->parseExternalDatabases();
  $docRoot->externalDatabaseVOs($externalDatabases);

  my $protocols = $r->parseProtocols($externalDatabases);
  $docRoot->protocolVOs($protocols);

  my $affiliations = $r->parseAffiliations();
  $docRoot->affiliationVOs($affiliations);

  my $persons = $r->parsePersons($affiliations);
  $docRoot->personVOs($persons);


  my $study = $r->parseStudy($persons, $externalDatabases);
  $docRoot->studyVO($study);

  # Set the DocRoot BioMaterialVOs and TreatmentVOs
  $r->doBioMaterialsAndCharacteristics($sdrfHeader, $sdrfLine, $docRoot);
  $r->doTreatments($sdrfHeader, $sdrfLine, $docRoot);

  my $assays = $r->doAssays($sdrfHeader, $sdrfLine, $docRoot);

  #*******************************
  #assayStuff
  $self->assert_equals(1, scalar(@$assays));

  my $assay = $assays->[0];

  $self->assert_equals('TOXO_CHIP', $assay->getArraySourceId());
  $self->assert_equals('RH 1 - Affymetrix Hybridization', $assay->getName());
  $self->assert_equals('Affymetrix Hybridization Protocol (Penn Microarray Core Facility)', $assay->getProtocolName());
  $self->assert_equals('Expression profiling of the 3 archetypal T. gondii lineages', $assay->getStudyName());

  my $assayParamValues = $assay->getParameterValues();
  $self->assert_equals(1, scalar(@$assayParamValues));

  my $assayParamValue0 = $assayParamValues->[0];


  $self->assert_equals('cRNA_hybridized', $assayParamValue0->getParameterName());


  $self->assert_equals(10, $assayParamValue0->getValue());

  my $assayLex = $assay->getLabeledExtracts()->[0];
  $self->assert_equals('RH 1 - LEX', $assayLex->getName());

  #*******************************
  #acquisitionStuff
  my $acquisitions = $assay->getAcquisitions();

  $self->assert_equals(1, scalar(@$acquisitions));

  my $acquisition = $acquisitions->[0];

  $self->assert_equals('Affymetrix GeneChip Scanner 3000 (GCS3000)', $acquisition->getProtocolName());
  $self->assert_equals('RH 1.DAT', $acquisition->getUri());
  $self->assert_null($acquisition->getName());
  $self->assert_equals(0, scalar(@{$acquisition->getParameterValues()}));

  my $channels = $acquisition->getChannels();
  $self->assert_equals(1, scalar(@$channels));
  $self->assert_equals('biotin', $channels->[0]->getValue());

  my $factorValues = $acquisition->getFactorValues();
  $self->assert_equals(2, scalar(@$factorValues));

  my $factorValue0 = $factorValues->[0];
  my $factorValue1 = $factorValues->[1];

  $self->assert_equals('Time Grown', $factorValue0->getFactorName());
  $self->assert_equals('Strain', $factorValue1->getFactorName());

  $self->assert_equals('48 hours', $factorValue0->getValue()->getValue());
  $self->assert_equals('RG (Type I)', $factorValue1->getValue()->getValue());

  #*******************************
  #quantificationStuff

  my $quantifications = $acquisition->getQuantifications();

  $self->assert_equals(1, scalar(@$quantifications));

  my $quant = $quantifications->[0];

  $self->assert_null($quant->getName());

  $self->assert_equals('RH 1.CEL', $quant->getUri());
  $self->assert_equals('Affymetrix MAS 5.0 Probe Cell Analysis', $quant->getProtocolName());

  my $nullParamValues = $quant->getParameterValues();
  $self->assert_equals(0, scalar(@{$nullParamValues}));

 #*******************************
  #processStuff

  my $processes = $quant->getProcesses();
  $self->assert_equals(1, scalar(@$processes));

  my $process = $processes->[0];

  $self->assert_equals('RH 1 - RMA Normalization', $process->getName());
  $self->assert_equals('RH 1.TXT', $process->getUri());
  $self->assert_equals('Robust Multi-Array Average expression measure (Bioconductor affy package)', $process->getProtocolName());
  my $paramValues = $process->getParameterValues();
  $self->assert_equals(3, scalar(@$paramValues));

  my $quantParam0 = $paramValues->[0];
  my $quantParam1 = $paramValues->[1];
  my $quantParam2 = $paramValues->[2];

  $self->assert_equals('Summarization', $quantParam0->getParameterName());
  $self->assert_equals('Normalization', $quantParam1->getParameterName());
  $self->assert_equals('BG correct', $quantParam2->getParameterName());

  $self->assert_equals('median polish', $quantParam0->getValue());
  $self->assert_equals('quantile', $quantParam1->getValue());
  $self->assert_equals('RMA 2', $quantParam2->getValue());


}


#--------------------------------------------------------------------------------

sub test_doTreatments {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $externalDatabases = $r->parseExternalDatabases();
  $docRoot->externalDatabaseVOs($externalDatabases);

  my $protocols = $r->parseProtocols($externalDatabases);
  $docRoot->protocolVOs($protocols);

  # Set the DocRoot BioMaterialVOs
  $r->doBioMaterialsAndCharacteristics($sdrfHeader, $sdrfLine, $docRoot);

  my $treatments = $r->doTreatments($sdrfHeader, $sdrfLine, $docRoot);

  $self->assert_equals(3, scalar(@$treatments));
  foreach(@$treatments) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::TreatmentVO', ref($_));

    $self->assert_equals(1, scalar(@{$_->getInputBMMs()}));
  }

  my $t0 = $treatments->[0];
  my $t1 = $treatments->[1];
  my $t2 = $treatments->[2];

  my $t0InputBmm = $t0->getInputBMMs()->[0];
  my $t1InputBmm = $t1->getInputBMMs()->[0];
  my $t2InputBmm = $t2->getInputBMMs()->[0];

  $self->assert_equals('RH 1', $t2InputBmm->getBioMaterial()->getName());
  $self->assert_equals('RH 1 grown in HFF Cells', $t1InputBmm->getBioMaterial()->getName());
  $self->assert_equals('RH 1 - RNA', $t0InputBmm->getBioMaterial()->getName());

  $self->assert_equals('Parasite Growth (Roos)', $t2->getProtocol()->getName());
  $self->assert_equals('Total RNA via RNAqueous-4PCR Kit (Ambion).', $t1->getProtocol()->getName());
  $self->assert_equals('One-Cycle Eukaryotic Target Labeling Assay (Affymetrix)', $t0->getProtocol()->getName());

  $self->assert_equals('RH 1 grown in HFF Cells', $t2->getOutputBM()->getName());
  $self->assert_equals('RH 1 - RNA', $t1->getOutputBM()->getName());
  $self->assert_equals('RH 1 - LEX', $t0->getOutputBM()->getName());

  $self->assert_equals(1, scalar(@{$t2->getParameterValues()}));
  $self->assert_equals(0, scalar(@{$t1->getParameterValues()}));

  my $labelParamValues = $t0->getParameterValues();

  $self->assert_equals(2, scalar(@$labelParamValues));

  my $labelParamValue0 = $labelParamValues->[0];
  my $labelParamValue1 = $labelParamValues->[1];

  $self->assert_equals('cRNA_yield', $labelParamValue1->getParameterName());
  $self->assert_equals('input_total_RNA', $labelParamValue0->getParameterName());

  $self->assert_equals(46.1, $labelParamValue1->getValue());
  $self->assert_equals(1, $labelParamValue0->getValue());


  my $growParam = $t2->getParameterValues()->[0];
  $self->assert_equals('time_of_extraction', $growParam->getParameterName());
  $self->assert_equals(48, $growParam->getValue());
}

#--------------------------------------------------------------------------------

sub test_makeTreatments {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $inBioMaterial = RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'inBioMaterial'});
  my $outBioMaterial = RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'outBioMaterial'});

  my $protocols = [RAD::MR_T::MageImport::VO::ProtocolVO->new({name => 'protocol1', params => [RAD::MR_T::MageImport::VO::ProtocolParamVO->new({name => 'param1'}),
                                                                                               RAD::MR_T::MageImport::VO::ProtocolParamVO->new({name => 'param2'})]}),
                   RAD::MR_T::MageImport::VO::ProtocolVO->new({name => 'protocol2', params => [RAD::MR_T::MageImport::VO::ProtocolParamVO->new({name => 'param3'})]}),
                   RAD::MR_T::MageImport::VO::ProtocolVO->new({name => 'protocol3'}),
                  ];

  $protocols->[0]->setProtocolType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'type0'}));
  $protocols->[1]->setProtocolType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'type1'}));
  $protocols->[2]->setProtocolType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'type2'}));


  my $paramValues = [RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'param1', value => 'value1'}),
                     RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'param2', value => 'value2'}),
                     RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'param3', value => 'value3', unit => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'oeValue'})}),
                     RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => 'param4', value => 'value4'}),
                    ];

  my $treatments = $r->makeTreatments($protocols, $paramValues, $outBioMaterial, $inBioMaterial, $docRoot);

  $self->assert_equals(3, scalar(@$treatments));

  foreach(@$treatments) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::TreatmentVO', ref($_));

    $self->assert_equals(1, scalar(@{$_->getInputBMMs()}));
    $self->assert_equals('inBioMaterial', $_->getInputBMMs()->[0]->getBioMaterial()->getName());

    $self->assert_equals('outBioMaterial', $_->getOutputBM()->getName());
  }

  my $t0 = $treatments->[0];
  my $t1 = $treatments->[1];
  my $t2 = $treatments->[2];

  #print Dumper $t0;

  $self->assert_equals(2, scalar(@{$t0->getParameterValues()}));
  $self->assert_equals(1, scalar(@{$t1->getParameterValues()}));
  $self->assert_equals(0, scalar(@{$t2->getParameterValues()}));

  $self->assert_equals('protocol1', $t0->getProtocol()->getName());
  $self->assert_equals('protocol2', $t1->getProtocol()->getName());
  $self->assert_equals('protocol3', $t2->getProtocol()->getName());

  my $t1Param = $t1->getParameterValues()->[0];
  $self->assert_equals('value3', $t1Param->getValue());
}

#--------------------------------------------------------------------------------

sub test_findExistingTreatment {
  my $self = shift;

  my $r = $self->getReader();
  my $mockRoot = $self->getMockDocRoot();
  my $treatments = $mockRoot->getTreatmentVOs();

  my $bioMaterial = searchObjArrayByObjName($mockRoot->getBioMaterialVOs(), 'labeledExtract');
  my $protocol = searchObjArrayByObjName($mockRoot->getProtocolVOs(), 'protocol_labeling');

  my $treatment = $r->findExistingTreatment($treatments, $bioMaterial, $protocol);

  $self->assert_equals('treatment_labeling', $treatment->getName());

  my $notFoundBioMaterial = RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => 'isNotFound'});
  $self->assert_equals('', $r->findExistingTreatment($treatments, $notFoundBioMaterial, $protocol));

  my $notFoundProtocol = RAD::MR_T::MageImport::VO::ProtocolVO->new({name => 'isNotFound'});
  $self->assert_equals('', $r->findExistingTreatment($treatments, $bioMaterial, $notFoundProtocol));

}

#--------------------------------------------------------------------------------

sub test_doBioMaterialsAndCharacteristics {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $externalDatabases = $r->parseExternalDatabases();
  $docRoot->externalDatabaseVOs($externalDatabases);

  my $biomaterials = $r->doBioMaterialsAndCharacteristics($sdrfHeader, $sdrfLine, $docRoot);

  $self->assert_equals(4, scalar(@$biomaterials));

  my $organism = $biomaterials->[0];
  my $cell = $biomaterials->[1];
  my $rna = $biomaterials->[2];
  my $lex = $biomaterials->[3];

  $self->assert_equals('RH 1', $organism->getName());
  $self->assert_equals('RH 1 grown in HFF Cells', $cell->getName());
  $self->assert_equals('RH 1 - RNA', $rna->getName());
  $self->assert_equals('RH 1 - LEX', $lex->getName());

  $self->assert_equals('whole_organism', $organism->getBioMaterialType()->getValue());
  $self->assert_equals('cell', $cell->getBioMaterialType()->getValue());
  $self->assert_equals('total_RNA', $rna->getBioMaterialType()->getValue());
  $self->assert_equals('synthetic_RNA', $lex->getBioMaterialType()->getValue());

  $self->assert_equals('MO', $organism->getBioMaterialType()->getExternalDatabase()->getName());
  $self->assert_equals('MO', $cell->getBioMaterialType()->getExternalDatabase()->getName());
  $self->assert_equals('MO', $rna->getBioMaterialType()->getExternalDatabase()->getName());
  $self->assert_equals('MO', $lex->getBioMaterialType()->getExternalDatabase()->getName());

  $self->assert_null($organism->getChannel());
  $self->assert_null($cell->getChannel());
  $self->assert_null($rna->getChannel());
  $self->assert_equals('biotin', $lex->getChannel()->getValue());

  $self->assert_null($cell->getBioMaterialChars());
  $self->assert_null($rna->getBioMaterialChars());
  $self->assert_null($lex->getBioMaterialChars());
  my $chars = $organism->getBioMaterialChars();

  $self->assert_equals(4, scalar(@$chars));

  my $strain = $chars->[0];
  my $stage = $chars->[1];
  my $geneticChar = $chars->[2];
  my $organism = $chars->[3];

  $self->assert_equals('RG (Type I)', $strain->getValue());
  $self->assert_equals('tachyzoite', $stage->getValue());
  $self->assert_equals('wild_type', $geneticChar->getValue());
  $self->assert_equals('Toxoplasma gondii RH', $organism->getValue());

  $self->assert_equals('StrainOrLine', $strain->getCategory());
  $self->assert_equals('DevelopmentalStage', $stage->getCategory());
  $self->assert_equals('IndividualGeneticCharacteristic', $geneticChar->getCategory());
  $self->assert_equals('Organism', $organism->getCategory());

  $self->assert_null($strain->getExternalDatabase());
  $self->assert_equals('nci_meta', $stage->getExternalDatabase()->getName());
  $self->assert_equals('MO', $geneticChar->getExternalDatabase()->getName());
  $self->assert_equals('ncbitax', $organism->getExternalDatabase()->getName());

}

#--------------------------------------------------------------------------------

sub test_makeBioMaterial {
  my $self = shift;

  my $r = $self->getReader();

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();
  $docRoot->bioMaterialVOs([]);

  my $biomaterial = $r->makeBioMaterial($docRoot, 'TestSubclassView', 'TestBioMaterialName');

  $self->assert_equals(1, scalar(@{$docRoot->getBioMaterialVOs}));
  $self->assert_equals('TestBioMaterialName', $biomaterial->getName());
  $self->assert_equals('TestSubclassView', $biomaterial->getSubclassView());

  my $shouldHaveRetrieved = $r->makeBioMaterial($docRoot, 'TestSubclassView', 'TestBioMaterialName');
  $self->assert_equals(1, scalar(@{$docRoot->getBioMaterialVOs}));
  $self->assert_equals('TestBioMaterialName', $shouldHaveRetrieved->getName());
}

#--------------------------------------------------------------------------------

sub test_parseStudyFactors {
  my $self = shift;

  my $r = $self->getReader();

  my $externalDatabaseVos = $r->parseExternalDatabases();
  my $factors = $r->parseStudyFactors($externalDatabaseVos);

  $self->assert_equals(2, scalar(@$factors));

  foreach(@$factors) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::StudyFactorVO', ref($_));
  }

  my $strain = $factors->[0];
  my $grow = $factors->[1];

  $self->assert_equals('Strain', $strain->getName());
  $self->assert_equals('Time Grown', $grow->getName());

  my $strainType = $strain->getType();
  my $growType = $grow->getType();

  $self->assert_equals('strain_or_line', $strainType->getValue());
  $self->assert_null($strainType->getExternalDatabase());

  $self->assert_equals('timepoint', $growType->getValue());
  my $extDb = $growType->getExternalDatabase();

  $self->assert_equals('MO', $extDb->getName());
}

#--------------------------------------------------------------------------------
sub test_parseStudyDesign {
  my $self = shift;

  my $r = $self->getReader();

  my $externalDatabaseVos = $r->parseExternalDatabases();
  my $factors = $r->parseStudyFactors($externalDatabaseVos);
  my $studyDesign = $r->parseStudyDesign($factors);

  $self->assert_equals('RAD::MR_T::MageImport::VO::StudyDesignVO', ref($studyDesign));

  #$self->assert_null($studyDesign->getName());

  $self->assert_equals(2, scalar(@{$studyDesign->getFactors()}));
  $self->assert_equals(2, scalar(@{$studyDesign->getTypes()}));

  my $strainOrLine = $studyDesign->getTypes()->[0];
  $self->assert_equals('strain_or_line_design', $strainOrLine->getValue());
  $self->assert_null($strainOrLine->getCategory());
  $self->assert_null($strainOrLine->getExternalDatabase());

}

#--------------------------------------------------------------------------------

sub test_getRowByHeader {
  my $self = shift;

  my $r = $self->getReader();

  $r->{lines} = ["Test1\tValue1\tValue2\tValue3",
                 "Test2\tValue4\tValue5\tValue6",
                 "Test3\tValue7\tValue8\tValue9",
                 "Test4\tValue10\tTest5",
                 "Test5\tValue11",
                ];

  my $line1 = $r->getRowByHeader('^Test1');
  $self->assert_equals(3, scalar(@$line1));
  $self->assert_equals("Value3", $line1->[2]);

  my $line5 = $r->getRowByHeader('^Test5');
  $self->assert_equals(1, scalar(@$line5));
  $self->assert_equals("Value11", $line5->[0]);
}

#--------------------------------------------------------------------------------

sub _test_parse {
  my $self = shift;

  my $r = $self->getReader();
  my $docRoot = $r->parse();

  my $protocols = $docRoot->getProtocolVOs();
  my $affiliations = $docRoot->getAffiliationVOs();
  my $persons = $docRoot->getPersonVOs();
  my $study = $docRoot->getStudyVO();

  $self->assert_not_null($study);
  $self->assert_equals(3, scalar(@$protocols));
  $self->assert_equals(4, scalar(@$affiliations));
  $self->assert_equals(8, scalar(@$persons));
}

#--------------------------------------------------------------------------------


sub test_parseProtocols {
  my $self = shift;

  my $r = $self->getReader();

  my $externalDatabaseVos = $r->parseExternalDatabases();
  my $protocols = $r->parseProtocols($externalDatabaseVos);

  $self->assert_equals(7, scalar(@$protocols));
  foreach(@$protocols) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::ProtocolVO', ref($_));
  }

  my $grow = $protocols->[0];
  my $norm = $protocols->[6];

  $self->assert_equals('Parasite Growth (Roos)', $grow->getName());
  $self->assert_matches(qr(^Wild Type parasite in), $grow->getProtocolDescription());

  my $type = $grow->getProtocolType();
  $self->assert_equals('grow', $type->getValue());

  my $typeExtDb = $type->getExternalDatabase();
  $self->assert_equals('MO', $typeExtDb->getName());

  my $params = $grow->getParams();
  $self->assert_equals(1, scalar(@$params));
  $self->assert_equals('time_of_extraction', $params->[0]->getName());

  my $normParams = $norm->getParams();
  $self->assert_equals(3, scalar(@$normParams));
  $self->assert_equals('BG correct', $normParams->[0]->getName());
  $self->assert_equals('Normalization', $normParams->[1]->getName());
  $self->assert_equals('Summarization', $normParams->[2]->getName());
}

sub test_parseExternalDatabases {
  my $self = shift;

  my $r = $self->getReader();
  my $extDbs = $r->parseExternalDatabases();

  $self->assert_equals(3, scalar(@$extDbs));
  foreach(@$extDbs) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::ExternalDatabaseVO', ref($_));
  }

  my $mo = $extDbs->[0];
  my $tax = $extDbs->[1];
  my $meta = $extDbs->[2];

  $self->assert_equals('MO', $mo->getName());
  $self->assert_equals('ncbitax', $tax->getName());
  $self->assert_equals('nci_meta', $meta->getName());

  $self->assert_equals('1.3.1', $mo->getVersion());
  $self->assert_equals('unknown', $tax->getVersion());
  $self->assert_equals('unknown', $meta->getVersion());

  $self->assert_equals('http://mged.sourceforge.net/ontologies/MGEDontology.php', $mo->getUri());
}

sub test_parseAffiliations {
  my $self = shift;

  my $r = $self->getReader();
  my $affiliations = $r->parseAffiliations();

  $self->assert_equals(2, scalar(@$affiliations));
  foreach(@$affiliations) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::AffiliationVO', ref($_));
  }

  $self->assert_matches(qr(^Department of Biology), $affiliations->[0]->getName());
  $self->assert_matches(qr(^University of Pennsylvania), $affiliations->[1]->getName());
}

sub test_parsePersons {
  my ($self) = @_;

  my $r = $self->getReader();
  my $affiliations = $r->parseAffiliations();
  my $persons = $r->parsePersons($affiliations);

  $self->assert_equals(3, scalar(@$persons));
  foreach(@$persons) {
    $self->assert_equals('RAD::MR_T::MageImport::VO::PersonVO', ref($_));
  }

  my $person0 = $persons->[0];
  my $person1 = $persons->[1];
  my $person2 = $persons->[2];

  $self->assert_equals('Bahl', $person0->getLast());
  $self->assert_equals('Amit', $person0->getFirst());
  $self->assert_equals('abahl@mail.med.upenn.edu', $person0->getEmail());
  $self->assert_equals('', $person0->getPhone());
  $self->assert_equals('215 898 2118', $person1->getPhone());
  $self->assert_matches(qr(^423 Guardian), $person0->getAddress());
  
  my $affiliation0 = $person0->getAffiliation();
  $self->assert_matches(qr(^Department of Biology), $affiliation0->getName());
}


1;
