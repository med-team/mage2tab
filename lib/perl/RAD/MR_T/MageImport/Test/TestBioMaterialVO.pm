package RAD::MR_T::MageImport::Test::TestBioMaterialVO;
use base qw(Test::Unit::TestCase);

use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

sub new {
  my $self = shift()->SUPER::new(@_);
  $self->{vo} = undef;
  return $self;
}

sub set_up {
  my $self = shift;
  $self->{vo} = RAD::MR_T::MageImport::VO::BioMaterialVO->new({
							       name=>"test",
							       description=>"test", 
							       subclassView=>"test", 
							       labelMethod=>RAD::MR_T::MageImport::VO::ProtocolVO->new, 
							       bioMaterialType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new(),
							       bioMaterialChars=>[RAD::MR_T::MageImport::VO::OntologyEntryVO->new()],
							      }); 
}

sub test_construct{
  my $self = shift;
  $self->assert(UNIVERSAL::isa($self->{vo},'RAD::MR_T::MageImport::VO::BioMaterialVO'), "check object type ok");
#  $self->assert_not_null($self->{vo});
}

sub test_getName {
  my $self = shift;
  my $setValue = $self->{vo}->getName();
 # print STDOUT "test_getName $setValue";
  $self->assert("test" eq $setValue);
}

sub test_setName {
  my $self = shift;
  $self->{vo}->setName("test2");
  my $setValue = $self->{vo}->getName();
  $self->assert("test2" eq  $setValue);
}

sub test_getDescription {
  my $self = shift;
  my $setValue = $self->{vo}->getDescription();
  $self->assert("test" eq $setValue);
}

sub test_setDescription {
  my $self = shift;
  $self->{vo}->setDescription("test2");
  my $setValue = $self->{vo}->getDescription();
  $self->assert("test2", $setValue);
}

sub test_getSubclassView {
  my ($self) = @_;
  my $setValue = $self->{vo}->getSubclassView();
  $self->assert("test" eq $setValue);
}

sub test_setSubclassView {
  my $self = shift;
  $self->{vo}->setSubclassView("test2");
  my $setValue = $self->{vo}->getSubclassView();
  $self->assert("test2" eq  $setValue);
  $self->assert("test" ne  $setValue);
}

# sub test_getLabelMethod {
#   my ($self) = @_;
#   my $setValue = $self->{vo}->getLabelMethod();
#   $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::ProtocolVO'), "check object type ok");
# }

# sub test_setLabelMethod {
#   my $self = shift;
#   $self->{vo}->setLabelMethod(RAD::MR_T::MageImport::VO::ProtocolVO->new());
#   my $setValue = $self->{vo}->getLabelMethod();
#   $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::ProtocolVO'), "check object type ok");
# }


sub test_getBioMaterialType {
  my ($self) = @_;
  my $setValue = $self->{vo}->getBioMaterialType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_setBioMaterialType {
  my $self = shift;
  $self->{vo}->setBioMaterialType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new());
  my $setValue = $self->{vo}->getBioMaterialType();
  $self->assert(UNIVERSAL::isa($setValue,'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type ok");
}

sub test_getBioMaterialChars {
  my ($self) = @_;
  my $setValue = $self->{vo}->getBioMaterialChars();
  $self->assert(UNIVERSAL::isa($setValue->[0],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check object type again ok");
}

sub test_addBioMaterialChars {
  my $self = shift;
  $self->{vo}->addBioMaterialChars((RAD::MR_T::MageImport::VO::OntologyEntryVO->new()));
  my $setValue = $self->{vo}->getBioMaterialChars();
  $self->assert(UNIVERSAL::isa($setValue->[1],'RAD::MR_T::MageImport::VO::OntologyEntryVO'), "check added BioMaterialChars object type ok");
}

sub tear_down {
  my $self = shift;
  $self->{vo} = undef;
}

1;
