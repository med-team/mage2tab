package RAD::MR_T::MageImport::VO::ContactVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;


sub new {
  my $class = shift;
  croak "try to instantiate an abstract class ContactVO" if ref($class) eq 'ContactVO';
  my $arg = shift;
  bless {
	 _name => $arg->{name},
	 _first => $arg->{first},
	 _last => $arg->{last},
	 _address => $arg->{address},
	 _email => $arg->{email},
         _role => $arg->{role},
         _phone => $arg->{phone},
	}, $class;
}

sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getFirst  {$_[0]->{_first}}
sub setFirst  {$_[0]->{_first} = $_[1]}

sub getLast  {$_[0]->{_last}}
sub setLast  {$_[0]->{_last} = $_[1]}

sub getAddress  {$_[0]->{_address}}
sub setAddress {$_[0]->{_address} = $_[1]}

sub getEmail  {$_[0]->{_email}}
sub setEmail {$_[0]->{_email} = $_[1]}

sub getRole  {$_[0]->{_role}}
sub setRole {$_[0]->{_role} = $_[1]}

sub getPhone  {$_[0]->{_phone}}
sub setPhone {$_[0]->{_phone} = $_[1]}

1;



