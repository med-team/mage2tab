package RAD::MR_T::MageImport::VO::ProtocolVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
	 _name => $arg->{name},
	 _protocolDescription => $arg->{protocolDescription},
	 _softwareDescription => $arg->{softwareDescription},
	 _hardwareDescription => $arg->{hardwareDescription},
	 _uri => $arg->{uri},
	}, $class;


  if(my $protocolType = $arg->{protocolType}) {
    $self->setProtocolType($protocolType);
  }

  if(my $softwareType = $arg->{softwareType}) {
    $self->setSoftwareType($softwareType);
  }

  if(my $hardwareType = $arg->{hardwareType}) {
    $self->setHardwareType($hardwareType);
  }

  if(my $params = $arg->{params}) {
    $self->setParams($params);
  }

  return $self;
} 


sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getProtocolDescription  {$_[0]->{_protocolDescription}}
sub setProtocolDescription  {$_[0]->{_protocolDescription} = $_[1]}

sub getSoftwareDescription  {$_[0]->{_softwareDescription}}
sub setSoftwareDescription  {$_[0]->{_softwareDescription} = $_[1]}

sub getHardwareDescription  {$_[0]->{_hardwareDescription}}
sub setHardwareDescription  {$_[0]->{_hardwareDescription} = $_[1]}

sub getUri  {$_[0]->{_uri}}
sub setUri  {$_[0]->{_uri} = $_[1]}

sub getProtocolType  {$_[0]->{_protocolType}}
sub setProtocolType  {
  my ($self, $protocolType) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setProtocolType';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $protocolType);

  return $self->{_protocolType} = $protocolType;
}

sub getSoftwareType  {$_[0]->{_softwareType}}
sub setSoftwareType  {
  my ($self, $softwareType) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setSoftwareType';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $softwareType);

  return $self->{_softwareType} = $softwareType;
}

sub getHardwareType  {$_[0]->{_hardwareType}}
sub setHardwareType  {
  my ($self, $hardwareType) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setHardwareType';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $hardwareType);

  return $self->{_hardwareType} = $hardwareType;
}


sub getParams  {$_[0]->{_params}}
sub setParams  {
  my ($self, $params) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::ProtocolParamVO';
  my $methodName = 'setParams';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $params);

  return $self->{_params} = $params;
}

sub addParams {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ProtocolParamVO';
  my $methodName = 'addParams';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_params}}, @_);
}
1;

