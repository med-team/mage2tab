package RAD::MR_T::MageImport::VO::AcquisitionVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;

use RAD::MR_T::MageImport::VO::QuantificationVO;
use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);


sub new {
  my ($class, $arg) = @_;

  my $self = bless {
	 _name => $arg->{name},
	 _uri => $arg->{uri},
         _protocolName => $arg->{protocolName},
	 _factorValues => $arg->{factorValues},
         _parameterValues => [],
#	 _factor => $arg->{factor},
	}, $class;


  if(my $quantifications = $arg->{quantifications}) {
    $self->setQuantifications($quantifications);
  }

  if(my $channels = $arg->{channels}) {
    $self->setChannels($channels);
  }

  if(my $parameterValues = $arg->{parameterValues}) {
    $self->setParameterValues($parameterValues);
  }

  return($self);
}

sub getFactorValues  {$_[0]->{_factorValues}}
sub setFactorValues  {$_[0]->{_factorValues} = $_[1]}
sub addFactorValues {
  my $self = shift;

  my $expected = "RAD::MR_T::MageImport::VO::FactorValueVO";
  my $methodName = "addFactorValuess";

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_factorValues}}, @_);
}


sub getFactor  {$_[0]->{_factor}}
sub setFactor  {$_[0]->{_factor} = $_[1]}

sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getProtocolName  {$_[0]->{_protocolName}}
sub setProtocolName  {$_[0]->{_protocolName} = $_[1]}

sub getUri  {$_[0]->{_uri}}
sub setUri  {$_[0]->{_uri} = $_[1]}

sub getChannels  {$_[0]->{_channels}}
sub setChannels  {
  my ($self, $channels) = @_;

  my $expected = "RAD::MR_T::MageImport::VO::OntologyEntryVO";
  my $methodName = 'setChannels';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $channels);

  return $self->{_channels} = $channels;
}
sub addChannels {
  my $self = shift;

  my $expected = "RAD::MR_T::MageImport::VO::OntologyEntryVO";
  my $methodName = "addChannelss";

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_channels}}, @_);
}

sub getQuantifications  {$_[0]->{_quantifications}}
sub setQuantifications  {
  my ($self, $quantifications) = @_;

  my $expected = "RAD::MR_T::MageImport::VO::QuantificationVO";
  my $methodName = "setQuantifications";

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $quantifications);

  return $self->{_quantifications} = $quantifications;
}

sub addQuantifications {
  my $self = shift;

  my $expected = "RAD::MR_T::MageImport::VO::QuantificationVO";
  my $methodName = "addQuantifications";

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_quantifications}}, @_);
}

sub getParameterValues  {$_[0]->{_parameterValues}}
sub setParameterValues  {
  my ($self, $parameterValues) = @_;

  my $expected =  "RAD::MR_T::MageImport::VO::ParameterValueVO";
  my $methodName = 'setParameterValues';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $parameterValues);

  return $self->{_parameterValues} = $parameterValues;
}

sub addParameterValues {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ParameterValueVO';
  my $methodName = 'addParameterValues';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_parameterValues}}, @_);
}

1;

