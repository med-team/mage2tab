package RAD::MR_T::MageImport::VO::DocRoot;

################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;

# TODO AUTO USE/REQUIRE
use RAD::MR_T::MageImport::VO::StudyVO;
use RAD::MR_T::MageImport::VO::AssayVO;
use RAD::MR_T::MageImport::VO::AcquisitionVO;
use RAD::MR_T::MageImport::VO::QuantificationVO;
use RAD::MR_T::MageImport::VO::PersonVO;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::VO::ProtocolParamVO;
use RAD::MR_T::MageImport::VO::AffiliationVO;
use RAD::MR_T::MageImport::VO::ExternalDatabaseVO;
use RAD::MR_T::MageImport::VO::BioMaterialVO;
use RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO;
use RAD::MR_T::MageImport::VO::TreatmentVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::ContactVO;
use RAD::MR_T::MageImport::VO::FactorValueVO;
use RAD::MR_T::MageImport::VO::ParameterValueVO;
use RAD::MR_T::MageImport::VO::ProcessVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);


sub new {
  my $class = shift;
  my $arg = shift;
  bless {
	 _studyVO => $arg->{studyVO},
	 _assayVOs => $arg->{assayVOs},
	 _personVOs => $arg->{personVOs},
	 _affiliationVOs => $arg->{affiliationVOs},
	 _externalDatabaseVOs => $arg->{externalDatabaseVOs},
	 _protocolVOs => $arg->{protocolVOs},
	 _bioMaterialVOs => $arg->{bioMaterialVOs},
	 _treatmentVOs => $arg->{treatmentVOs},
	}, $class;
}

sub getStudyVO  {my $self = shift; return $self->studyVO();}
sub setStudyVO  {my $self = shift; $self->{_studyVO} = shift;}
sub studyVO {
  my $self = shift;
  if (@_) {
    $self->{_studyVO} = shift;
  }
  return $self->{_studyVO};
}

sub getProtocolVOs { $_[0]->{_protocolVOs}}

sub addProtocolVOs {
  my $self = shift;

  my $expected =  "RAD::MR_T::MageImport::VO::ProtocolVO";
  my $methodName = 'addProtocolsVOs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_protocolVOs}}, @_);
}

sub protocolVOs {
  my $self = shift;
  if (@_) {
    $self->{_protocolVOs} = shift;
  }
  return $self->{_protocolVOs};
}

sub getAssayVOs { $_[0]->{_assayVOs}}

sub addAssayVOs {
  my $self = shift;

  my $expected =  "RAD::MR_T::MageImport::VO::AssayVO";
  my $methodName = 'addAssaysVOs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_assayVOs}}, @_);
}

sub assayVOs {
  my $self = shift;
  if (@_) {
    $self->{_assayVOs} = shift;
  }
  return $self->{_assayVOs};
}


sub getBioMaterialVOs {$_[0]->{_bioMaterialVOs}}

sub addBioMaterialVOs {
  my $self = shift;

  my $expected = "RAD::MR_T::MageImport::VO::BioMaterialVO";
  my $methodName = 'addBioMaterials';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_bioMaterialVOs}}, @_);
}

sub bioMaterialVOs {
  my $self = shift;
  if (@_) {
    $self->{_bioMaterialVOs} = shift;
  }
  return $self->{_bioMaterialVOs};
}

sub getTreatmentVOs {
  my $self = shift;
  return $self->treatmentVOs();
}

sub addTreatmentVOs {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::TreatmentVO';
  my $methodName = 'addTreatmentVOs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_treatmentVOs}}, @_);
}

sub treatmentVOs {
  my $self = shift;
  if (@_) {
    $self->{_treatmentVOs} = shift;
  }
  return $self->{_treatmentVOs};
}

sub getPersonVOs {
  my $self = shift;
  return $self->personVOs();
}

sub addPersonVOs {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::PersonVO';
  my $methodName = 'addPersonVOs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_personVOs}}, @_);
}

sub personVOs {
  my $self = shift;
  if (@_) {
    $self->{_personVOs} = shift;
  }
  return $self->{_personVOs};
}

sub getAffiliationVOs {
  my $self = shift;
  return $self->affiliationVOs();
}

sub addAffiliationVOs {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::AffiliationVO';
  my $methodName = 'addAffiliations';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_affiliationVOs}}, @_);
}

sub affiliationVOs {
  my $self = shift;
  if (@_) {
    $self->{_affiliationVOs} = shift;
  }
  return $self->{_affiliationVOs};
}

sub getExternalDatabaseVOs {
  my $self = shift;
  return $self->externalDatabaseVOs();
}
sub addExternalDatabaseVOs {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ExternalDatabaseVO';
  my $methodName = 'addExternalDatabaseVOs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_externalDatabaseVOs}}, @_);
}

sub externalDatabaseVOs {
  my $self = shift;
  if (@_) {
    $self->{_externalDatabaseVOs} = shift;
  }
  return $self->{_externalDatabaseVOs};
}



1;






