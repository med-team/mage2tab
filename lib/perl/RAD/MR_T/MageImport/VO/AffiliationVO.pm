package RAD::MR_T::MageImport::VO::AffiliationVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::ContactVO;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::VO::ContactVO /;

1;



