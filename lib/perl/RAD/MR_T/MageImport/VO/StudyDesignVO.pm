package RAD::MR_T::MageImport::VO::StudyDesignVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::VO::StudyFactorVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
	 _name => $arg->{name},
	}, $class;

  if(my $types = $arg->{types}) {
    $self->setTypes($types);
  }

  if(my $factors = $arg->{factors}) {
    $self->setFactors($factors);
  }

  return $self;
} 


sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getTypes  {$_[0]->{_types}}
sub setTypes  {
  my ($self, $types) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setTypes';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $types);

  return $self->{_types} = $types;
}

sub addTypes {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'addFactors';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_types}}, @_);
}

sub getFactors  {$_[0]->{_factors}}

sub setFactors  {
  my ($self, $factors) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::StudyFactorVO';
  my $methodName = 'setFactors';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $factors);  

  return $self->{_factors} = $factors;
}

sub addFactors {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::StudyFactorVO';
  my $methodName = 'setFactors';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_factors}}, @_);
}

1;


