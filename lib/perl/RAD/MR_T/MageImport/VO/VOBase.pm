package RAD::MR_T::MageImport::VO::VOBase;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use RAD::MR_T::MageImport::MageImportError;
use vars qw( $AUTOLOAD );
{
# Encapsulated class data
  my %_attr_data = # DEFAULT           Type
    (
    );

 sub _default_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[0];
  }

  sub _type_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[1];
  }

 sub _exist_for {
    my ($self, $inattr) = @_;
    return 1 if exists $_attr_data{ $inattr };
    return 0;
  }

  sub _standard_keys{
    my ($self, $attr) = @_;
    my $class = ref($self);

    if($class eq 'RAD::MR_T::MageImport::VO::VOBase') {
      RAD::MR_T::MageImport::MageImportError->new("try to instantiate an abstract class VOBase")->throw();
    }
    keys %_attr_data;
  }

}

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless{}, $class;

  my $debug;

  foreach my $attrname ($self->_standard_keys()){
    print  STDOUT $attrname."\n" if $debug;
    my $argname;
    if($attrname =~ /^_(.*)/){
      $argname = $1;
    }
    print STDOUT  $argname."\n" if $debug;
    if(exists $arg->{$argname}){
      print STDOUT  $argname."\n" if $debug;
      $self->{$attrname} = $arg->{$argname};
    }
    else{
      print STDOUT "hello", $argname."\n" if $debug;
      $self->{$attrname} = $self->_default_for($attrname);
    }
  }

 # print "\nnew construct", $self->{_inputBM}, "\n"; 
  return $self;
}


sub AUTOLOAD{
  no strict "refs";
  my ($self, $newval) = @_;
#print STDOUT $AUTOLOAD;
# was it a get method?
  if ($AUTOLOAD =~ /.*::get(\w+)/  ){
    my $attr_name  = "_".lcfirst($1);
 #   *{$AUTOLOAD} = sub { return $_[0]->{'$attr_name'} };
 #   print $attr_name, $self->{$attr_name}; 
   return $self->{$attr_name};
  }

# Was it a set method?
  if ($AUTOLOAD =~ /.*::set(\w+)/ ){
    my $attr_name = "_".lcfirst($1);

    if($self->_exist_for($attr_name)){

      my $type=$self->_type_for($attr_name);

      unless ( !$type || UNIVERSAL::isa($newval,$type) ) {
	RAD::MR_T::MageImport::MageImportObjectTypeError->new("$AUTOLOAD", $type, ref($newval))->throw() ;
      }


 #   *{$AUTOLOAD} = sub { $_[0]->{'$attr_name'} = $_[1]; return };
 #   print STDOUT "att $attr_name\t newval $newval";

      $self->{$attr_name} = $newval;
      return
   }
  }
#Must have been a mistake then...
  RAD::MR_T::MageImport::MageImportError->new("No such method: $AUTOLOAD")->throw() 
 unless  $AUTOLOAD =~ /.*::DESTROY/  ;

}

 

1;

