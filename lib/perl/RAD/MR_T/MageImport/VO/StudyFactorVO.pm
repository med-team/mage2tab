package RAD::MR_T::MageImport::VO::StudyFactorVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use RAD::MR_T::MageImport::VO::OntologyEntryVO;
use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
	 _name => $arg->{name},
	}, $class;

  if(my $type = $arg->{type}) {
    $self->setType($type);
  }

  return $self;
} 


sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getType  {$_[0]->{_type}}
sub setType  {
  my ($self, $type) = @_;

  my $methodName = 'setType';
  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $type);

  return $self->{_type} = $type;
}

1;

