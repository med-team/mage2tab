package RAD::MR_T::MageImport::VO::PersonVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::ContactVO;
use RAD::MR_T::MageImport::VO::AffiliationVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::VO::ContactVO /;

sub new {
  my $class = shift;
  my $arg = shift;
  my $obj = $class->SUPER::new($arg);
  
  if(my $affiliation = $arg->{affiliation}) {
    $obj->setAffiliation($affiliation);
  }
  return $obj;
}

sub getAffiliation  {$_[0]->{_affiliation}}
sub setAffiliation  {
  my ($self, $affiliation) = @_;

  my $methodName = 'setAffiliation';
  my $expected = 'RAD::MR_T::MageImport::VO::AffiliationVO';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $affiliation);

  return $self->{_affiliation} = $affiliation;
}

1;
