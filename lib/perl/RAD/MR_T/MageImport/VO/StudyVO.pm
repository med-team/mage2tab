package RAD::MR_T::MageImport::VO::StudyVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::StudyDesignVO;
use RAD::MR_T::MageImport::VO::StudyVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
	 _name => $arg->{name},
	 _description => $arg->{description},
         _pubMedId => $arg->{pubMedId},
	}, $class;

  if(my $designs = $arg->{designs}) {
    $self->setDesigns($designs);
  }
  if(my $contact = $arg->{contact}) {
    $self->setContact($contact);
  }

  return $self;
}

sub getPubMedId  {$_[0]->{_pubMedId}}
sub setPubMedId  {$_[0]->{_pubMedId} = $_[1]}

sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getDescription  {$_[0]->{_description}}
sub setDescription  {$_[0]->{_description} = $_[1]}

sub getContact  {$_[0]->{_contact}}
sub setContact  {
  my ($self, $contact) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::PersonVO';
  my $methodName = 'setContact';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $contact);

  return $self->{_contact} = $contact;

}

sub getDesigns  {$_[0]->{_designs}}

sub setDesigns  {
  my ($self, $designs) = @_;

  my $methodName = 'setDesigns';
  my $expected = 'RAD::MR_T::MageImport::VO::StudyDesignVO';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $designs);

  return $self->{_designs} = $designs;
}

sub addDesigns {
  my $self = shift;

  my $methodName = 'setDesigns';
  my $expected = 'RAD::MR_T::MageImport::VO::StudyDesignVO';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_designs}}, @_);
}

1;


