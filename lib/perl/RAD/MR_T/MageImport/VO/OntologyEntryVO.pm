package RAD::MR_T::MageImport::VO::OntologyEntryVO;

use RAD::MR_T::MageImport::Util qw(checkArgumentType checkArgument);

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
                    _value => $arg->{value},
                    _category => $arg->{category},
                    #	 _parentName => $arg->{parentName}
                   }, $class;

  if(my $extDb = $arg->{externalDatabase}) {
    $self->setExternalDatabase($extDb);
  }
  return $self;
}

 
sub getValue  {$_[0]->{_value}}
sub setValue  {$_[0]->{_value} = $_[1]}

sub getCategory  {$_[0]->{_category}}
sub setCategory  {$_[0]->{_category} = $_[1]}

sub setExternalDatabase {
  my ($self, $extDb) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::ExternalDatabaseVO';
  my $methodName = 'setExternalDatabase';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $extDb);

  return $self->{_externalDatabase} = $extDb;
}

sub getExternalDatabase {$_[0]->{_externalDatabase}}

#sub getParentName  {$_[0]->{_parentName}}
#sub setParentName  {$_[0]->{_parentName} = $_[1]}

1;
