package RAD::MR_T::MageImport::VO::AssayVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);
use RAD::MR_T::MageImport::MageImportError;
use RAD::MR_T::MageImport::VO::AcquisitionVO;

sub new {
  my ($class, $arg) =@_;

  my $self = bless {
	 _name => $arg->{name},
	 _arraySourceId => $arg->{arraySourceId},
	 _protocolName => $arg->{protocolName},
	 _studyName => $arg->{studyName},
         _parameterValues => [],
	}, $class;

  if(my $operator = $arg->{operator}) {
    $self->setOperator($operator);
  }

  if(my $lex = $arg->{labeledExtracts}) {
    $self->setLabeledExtracts($lex);
  }

  if(my $acquisitions = $arg->{acquisitions}) {
    $self->setAcquisitions($acquisitions);
  }

  if(my $parameterValues = $arg->{parameterValues}) {
    $self->setParameterValues($parameterValues);
  }

  return($self);
}


sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getProtocolName  {$_[0]->{_protocolName}}
sub setProtocolName  {$_[0]->{_protocolName} = $_[1]}

sub getArraySourceId  {$_[0]->{_arraySourceId}}
sub setArraySourceId  {$_[0]->{_arraySourceId} = $_[1]}

#sub getOperator  {$_[0]->{_operator}}
#sub setOperator  {$_[0]->{_operator} = $_[1]}

sub getStudyName  {$_[0]->{_studyName}}
sub setStudyName  {$_[0]->{_studyName} = $_[1]}

sub getOperator {$_[0]->{_operator}}
sub setOperator {
  my ($self, $operator) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::PersonVO';
  my $methodName = 'setOperator';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $operator);

  return $self->{_operator} = $operator;

}

sub getAcquisitions  {$_[0]->{_acquisitions}}
sub setAcquisitions  {
  my ($self, $acquisitions) = @_;

  my $expected =  "RAD::MR_T::MageImport::VO::AcquisitionVO";
  my $methodName = 'setAcquisitions';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $acquisitions);

  return $self->{_acquisitions} = $acquisitions;
}

sub addAcquisitions {
  my $self = shift;

  my $expected =  "RAD::MR_T::MageImport::VO::AcquisitionVO";
  my $methodName = 'addAcquisitions';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_acquisitions}}, @_);
}

sub getParameterValues  {$_[0]->{_parameterValues}}
sub setParameterValues  {
  my ($self, $parameterValues) = @_;

  my $expected =  "RAD::MR_T::MageImport::VO::ParameterValueVO";
  my $methodName = 'setParameterValues';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $parameterValues);

  return $self->{_parameterValues} = $parameterValues;
}

sub addParameterValues {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ParameterValueVO';
  my $methodName = 'addParameterValues';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_parameterValues}}, @_);
}

sub getLabeledExtracts {$_[0]->{_labeledExtracts}}
sub setLabeledExtracts {
  my ($self, $lexs) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::BioMaterialVO';
  my $methodName = 'setLabeledExtracts';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $lexs);

  foreach my $lex (@$lexs){
    my $subclassView = $lex->getSubclassView();
    unless($subclassView eq 'LabeledExtract') {
      RAD::MR_T::MageImport::VOException::ObjectTypeError->new("BioMaterial Associated With an Assay must be [LabeledExtract] found:  $subclassView")->throw();
    }
  }

  return $self->{_labeledExtracts} = $lexs;

}
sub addLabeledExtracts {
  my $self = shift;

  my $expected =  "RAD::MR_T::MageImport::VO::BioMaterialVO";
  my $methodName = 'addLabeledExtracts';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  foreach my $lex (@_){
    my $subclassView = $lex->getSubclassView();
    unless($subclassView eq 'LabeledExtract') {
      RAD::MR_T::MageImport::VOException::ObjectTypeError->new("BioMaterial Associated With an Assay must be [LabeledExtract] found:  $subclassView")->throw();
    }
  }
  push(@{$self->{_labeledExtracts}}, @_);
}
1;

