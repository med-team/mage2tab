package RAD::MR_T::MageImport::VO::BioMaterialVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::VOBase;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::VO::VOBase /;


=head1 BioMaterialVO

=over

=item attribute hash

Description: convert C<$mage> material objects to vo objects and add them into C<$doc> 

To-dos: need type check for setBioMaterialChars

=cut

{
# Encapsulated class data
  my %_attr_data = # DEFAULT           Type
    (
     _name             => [undef,        ""],
     _description      => [undef,        ""],
     _subclassView     => [undef,        ""],
     #_labelMethod      => [undef,        'RAD::MR_T::MageImport::VO::ProtocolVO'],
     _channel          => [undef,        'RAD::MR_T::MageImport::VO::OntologyEntryVO'],
     _bioMaterialType  => [undef,        'RAD::MR_T::MageImport::VO::OntologyEntryVO'],
     _bioMaterialChars => [undef,        ""],
     #_externalDatabase => [undef,        'RAD::MR_T::MageImport::VO::ExternalDatabaseVO'],
     #_sourceId         => [undef,        ""],
     _provider         => [undef,        'RAD::MR_T::MageImport::VO::ContactVO'],    
    );


# private class methods, to operate on encapsulated class data

  sub _default_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[0];
  }

  sub _type_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[1];
  }

  sub _standard_keys{
    keys %_attr_data;
  }

  sub _exist_for {
    my ($self, $inattr) = @_;

    return 1 if exists $_attr_data{ $inattr };
    return 0;
  }
}


sub addBioMaterialChars {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'addBioMaterialChars';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_bioMaterialChars}}, @_);
}

1;

