package RAD::MR_T::MageImport::VO::QuantificationVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;

  my $self = bless {
                    _name => $arg->{name},
                    _uri => $arg->{uri},
                    _protocolName => $arg->{protocolName},
                    _parameterValues => [],
                   }, $class;
  if(my $processes = $arg->{processes}) {
    $self->setProcesses($processes);
  }

  if(my $parameterValues = $arg->{parameterValues}) {
    $self->setParameterValues($parameterValues);
  }

  return $self;
}


sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getUri  {$_[0]->{_uri}}
sub setUri  {$_[0]->{_uri} = $_[1]}

sub getProtocolName  {$_[0]->{_protocolName}}
sub setProtocolName  {$_[0]->{_protocolName} = $_[1]}

sub getProcesses  {$_[0]->{_processes}}
sub setProcesses  {
  my ($self, $processes) = @_;

  my $expected = "RAD::MR_T::MageImport::VO::ProcessVO";
  my $methodName = "setProcesses";

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $processes);

  return $self->{_processes} = $processes;
}

sub addProcesses {
  my $self = shift;

  my $expected = "RAD::MR_T::MageImport::VO::ProcessVO";
  my $methodName = "addProcesses";

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);  

  push(@{$self->{_processes}}, @_);
}

sub getParameterValues  {$_[0]->{_parameterValues}}
sub setParameterValues  {
  my ($self, $parameterValues) = @_;

  my $expected =  "RAD::MR_T::MageImport::VO::ParameterValueVO";
  my $methodName = 'setParameterValues';

  &checkArgument($methodName, @_);
  &checkArrayArgumentSameType($methodName, $self, $expected, $parameterValues);

  return $self->{_parameterValues} = $parameterValues;
}

sub addParameterValues {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ParameterValueVO';
  my $methodName = 'addParameterValues';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_parameterValues}}, @_);
}

1;

