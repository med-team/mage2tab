package RAD::MR_T::MageImport::VO::ProtocolParamVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use RAD::MR_T::MageImport::Util qw(checkArgument checkArgumentType);

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {
	 _name => $arg->{name},
	 _value => $arg->{value}
	}, $class;

  if(my $dataType = $arg->{dataType}) {
    $self->setDataType($dataType);
  }

  if(my $unitType = $arg->{unitType}) {
    $self->setUnitType($unitType);
  }

  return $self;
}

sub getName  {$_[0]->{_name}}
sub setName  {$_[0]->{_name} = $_[1]}

sub getValue  {$_[0]->{_value}}
sub setValue  {$_[0]->{_value} = $_[1]}

sub getDataType  {$_[0]->{_dataType}}
sub setDataType  {
  my ($self, $dataType) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setDataType';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $dataType);

  return $self->{_dataType} = $dataType;
}

sub getUnitType  {$_[0]->{_unitType}}
sub setUnitType  {
  my ($self, $unitType) = @_;

  my $expected = 'RAD::MR_T::MageImport::VO::OntologyEntryVO';
  my $methodName = 'setUnitType';

  &checkArgument($methodName, @_);
  &checkArgumentType($methodName, $self, $expected, $unitType);

  return $self->{_unitType} = $unitType;
}

1;



