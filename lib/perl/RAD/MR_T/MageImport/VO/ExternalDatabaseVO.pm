package RAD::MR_T::MageImport::VO::ExternalDatabaseVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::VOBase;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::VO::VOBase /;

#use vars qw( $AUTOLOAD );

{
# Encapsulated class data
  my %_attr_data = # DEFAULT           Type
    (
     _version        => ['unknown',    ""],
     _name           => [undef,        ""],
     _uri            => [undef,        ""],
    );

# private class methods, to operate on encapsulated class data

  sub _default_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[0];
  }

  sub _type_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[1];
  }

  sub _standard_keys{
    keys %_attr_data;
  }

 sub _exist_for {
    my ($self, $inattr) = @_;

    return 1 if exists $_attr_data{ $inattr };
    return 0;
  }
}

1;

