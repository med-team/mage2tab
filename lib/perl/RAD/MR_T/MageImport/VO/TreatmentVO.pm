package RAD::MR_T::MageImport::VO::TreatmentVO;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict;
use Carp;
use RAD::MR_T::MageImport::VO::VOBase;
use RAD::MR_T::MageImport::Util qw(checkArgument checkArrayArgumentSameType checkArgumentType);

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::VO::VOBase /;

#use vars qw( $AUTOLOAD );
  
{
# Encapsulated class data
  my %_attr_data = # DEFAULT           Type
    (
     _orderNum         =>  [1,            ""],
     _outputBM         => [undef,        "RAD::MR_T::MageImport::VO::BioMaterialVO"],
     _treatmentType    => [undef,        'RAD::MR_T::MageImport::VO::OntologyEntryVO'],
     _inputBMMs        => [undef,        ""],
     _name             => [undef,        ""],
     _protocol         => [undef,        'RAD::MR_T::MageImport::VO::ProtocolVO'],
     _parameterValues  => [undef,        ""],
    );

# private class methods, to operate on encapsulated class data

  sub _default_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[0];
  }

  sub _type_for {
    my ($self, $attr) = @_;
    $_attr_data{$attr}[1];
  }

  sub _standard_keys{
    keys %_attr_data;
  }

  sub _exist_for {
    my ($self, $inattr) = @_;
 
    return 1 if exists $_attr_data{ $inattr };
    return 0;
  }

}

 
sub addInputBMMs {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO';
  my $methodName = 'addInputBMMs';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_inputBMMs}}, @_);
}

sub getParameterValues {
  my ($self) = @_;

  if(my $paramValues = $self->{_parameterValues}) {
    return $paramValues;
  }
  return [];
}

sub addParameterValues {
  my $self = shift;

  my $expected = 'RAD::MR_T::MageImport::VO::ParameterValueVO';
  my $methodName = 'addParameterValues';

  &checkArrayArgumentSameType($methodName, $self, $expected, \@_);

  push(@{$self->{_parameterValues}}, @_);
}

1;

