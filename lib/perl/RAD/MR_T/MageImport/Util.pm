package RAD::MR_T::MageImport::Util;

use LWP::Simple;

use Data::Dumper;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(checkArgument checkArrayArgumentSameType checkArgumentType searchObjArrayByObjName getPubMedXml findAllAssayBioMaterials);

use strict;

use RAD::MR_T::MageImport::MageImportError;

#--------------------------------------------------------------------------------

sub checkArgument {
  my ($method, $class, @args) = @_;

  unless(@args) {
    RAD::MR_T::MageImport::MageImportArgumentError->new($class . "::$method: NO ARG PROVIDED")->throw();
  }

  if(scalar(@args) > 1) {
    RAD::MR_T::MageImport::MageImportArgumentError->new($class . "::$method: TOO MANY ARGS")->throw();
  }
  return($args[0]);
}

#--------------------------------------------------------------------------------

sub checkArgumentType {
  my ($method, $class, $expected, $arg) = @_;

  unless(UNIVERSAL::isa($arg, $expected)) {
    my $error = ref($class) . "::$method";
    RAD::MR_T::MageImport::MageImportObjectTypeError->new($error, $expected, ref($arg))->throw();
  }
  return($arg);
}

#--------------------------------------------------------------------------------

sub checkArrayArgumentSameType {
  my ($method, $class, $expected, $arg) = @_;

  unless(UNIVERSAL::isa($arg,'ARRAY')) {
    RAD::MR_T::MageImport::MageImportObjectTypeError->new($class. "::$method", 'ARRAY', $arg)->throw();
  }
  
  foreach my $val (@{$arg}) {
    unless(UNIVERSAL::isa($val, $expected)) {
      my $error = ref($class) . "::$method";
      RAD::MR_T::MageImport::MageImportObjectTypeError->new($error, $expected, ref($val))->throw();
    }
  }
  return($arg);
}

#--------------------------------------------------------------------------------

sub searchObjArrayByObjName {
 my ($objArray, $name) = @_;

 my (@ar, $type);

 foreach my $obj(@$objArray){
   $type = ref($obj);

   push(@ar, $obj) if($obj->getName eq $name);
 }

 if(scalar(@ar) > 1) {
   RAD::MR_T::MageImport::MageImportError->
        new("Could not find a DISTINCT object with name [$name] of type [$type]")->throw();
 }
 if(scalar(@ar) == 0) {
   RAD::MR_T::MageImport::MageImportError->
       new("Could not find ANY object with name [$name] of type [$type]")->throw();
 }

 return $ar[0];
}

#--------------------------------------------------------------------------------

=item

AURHOR: Oleg Khovayko
This Util is Based on the eutils example from NCBI

=cut

sub getPubMedXml {
  my ($query) = @_;

  my $utils = "http://www.ncbi.nlm.nih.gov/entrez/eutils";

  my $db     = "pubmed";
  my $report = "xml";

  my $esearch = "$utils/esearch.fcgi?db=$db&retmax=1&usehistory=y&term=";
  my $esearch_result = get($esearch . $query);

  $esearch_result =~ 
    m|<Count>(\d+)</Count>.*<QueryKey>(\d+)</QueryKey>.*<WebEnv>(\S+)</WebEnv>|s;

  my $Count    = $1;
  my $QueryKey = $2;
  my $WebEnv   = $3;

  my $efetch = "$utils/efetch.fcgi?rettype=$report&retmode=text&retmax=1&db=$db&query_key=$QueryKey&WebEnv=$WebEnv";
	
  my $efetch_result = get($efetch);

  return($efetch_result);
}


#--------------------------------------------------------------------------------

sub findAllAssayBioMaterials {
  my ($voAssay, $voTreatments) = @_;

  my %bioMaterials;

    my $recurse; $recurse  = sub {
      my ($bioMaterial) = @_;

      foreach my $treatment (@$voTreatments) {
        if($treatment->getOutputBM() == $bioMaterial) {
          my @inputBioMaterials = map { $_->getBioMaterial() } @{$treatment->getInputBMMs()};

          foreach my $input (@inputBioMaterials) {
            my $name = $input->getName();
            $bioMaterials{$name} = $input;

            $recurse->($input);
          }
        }
      }
    };

  foreach my $lex (@{$voAssay->getLabeledExtracts()}) {

    # Handle the Labeled Extracts ... then recurse through the others
    my $lexName = $lex->getName();
    $bioMaterials{$lexName} = $lex;

    $recurse->($lex);
  }

  my @bioMaterials = map { $bioMaterials{$_} } keys %bioMaterials;

  return \@bioMaterials;
}



1;
