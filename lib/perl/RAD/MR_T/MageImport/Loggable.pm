package RAD::MR_T::MageImport::Loggable;

=head1 loggable abstract class

Description : this is an abstract class, every service class should be loggable

=over

=item abstract method

=cut

sub setLogger {$_[0]->{_logger} = $_[1]}

sub getLogger {$_[0]->{_logger}}

1;
