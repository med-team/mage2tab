package RAD::MR_T::MageImport::Service::Validator;

# this is validator does the minimum checks on VOs

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidator;
use vars qw / @ISA / ;

@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidator/;

sub new {
  my $class = shift;
  my $arg = shift;
  bless {}, $class;
}

sub check {
  my ($self, $docRoot) = @_;

# need to check if logger is set
# otherwise throw warning
  my $logger = $self->getLogger();
  $logger->warn("\n******", ref($self),   "********");

  $logger->debug("validation begin");
  $self->objHasName($docRoot);
  $self->objNameUnique($docRoot);
  $logger->debug("validation end");
  return 1;
}


sub objHasName{
  my ($self, $docRoot) = @_;

  throw RAD::MR_T::MageImport::MageImportUnNamedVOError
    unless $docRoot->getStudyVO()->getName;
  my $sds = $docRoot->getStudyVO()->getDesigns;
  foreach my $sd (@ $sds){
    throw RAD::MR_T::MageImport::MageImportUnNamedVOError
      unless $sd->getName;
    my $sfs = $sd->getFactors;
    foreach my $sf (@$sfs){
      throw RAD::MR_T::MageImport::MageImportUnNamedVOError	unless $sf->getName;
    }
  }

  $self->checkObjHasName($docRoot, "Protocol");
  $self->checkObjHasName($docRoot, "ExternalDatabase");

# for searchObjByName
  $self->checkObjHasName($docRoot, "Person");
  $self->checkObjHasName($docRoot, "Affiliation");
  $self->checkObjHasName($docRoot, "Treatment");
  $self->checkObjHasName($docRoot, "BioMaterial");

}

sub checkObjHasName{
  no strict "refs";
  my ($self, $docRoot, $Obj) = @_;
  my $logger = $self->getLogger();
  my $getter = "get".$Obj."VOs";
  my $vos = $docRoot->$getter;
  foreach my $vo (@ $vos){
    unless ($vo->getName) {
      $self->appendResportStr("One of the $Obj doesn't have Name\n");
      
      $logger->warn("One of the $Obj doesn't have Name");

      RAD::MR_T::MageImport::MageImportUnNamedVOError->new("checkObjHasName: $Obj")->throw();
    }
  }
}


sub objNameUnique{
  my ($self, $docRoot) = @_;

  $self->checkObjNameUnique($docRoot, "Protocol");
  $self->checkObjNameUnique($docRoot, "ExternalDatabase");

# for searchObjByName
  $self->checkObjNameUnique($docRoot, "Person");
  $self->checkObjNameUnique($docRoot, "Affiliation");
  $self->checkObjNameUnique($docRoot, "Treatment");
  $self->checkObjNameUnique($docRoot, "BioMaterial");

}

sub checkObjNameUnique{
  no strict "refs";

  my ($self, $docRoot, $Obj) = @_;
  my $logger = $self->getLogger();
  my @names;
  my $getter = 'get'.$Obj.'VOs';

  my $vos = $docRoot->$getter;
  foreach my $vo (@ $vos){
    push @names, $vo->getName;
  }

  unless ( @names ==1 || $self->checkArrayElementUnique(\@names)){ 
    $logger->warn("One of the $Obj doesn't have unique Name");
    $self->appendResportStr("One of the $Obj doesn't have unique Name\n");
    RAD::MR_T::MageImport::MageImportVONameNotUniqueError->new("Validator::checkObjNameUnique: $Obj"."VOs")->throw();
  }
}
 

#util
sub checkArrayElementUnique{
  my ($self, $names)= @_;

  my %nameHash = map {$_, 1} @$names;
  my @unique = keys %nameHash;

  if( scalar @unique == scalar @$names) {return 1;}
  else{return 0;}
}


1;
