package RAD::MR_T::MageImport::Service::AbstractTranslator;

use strict 'vars';
use base qw(RAD::MR_T::MageImport::Loggable);
use RAD::MR_T::MageImport::MageImportError;

=head1 AbstractTranslator

Translators are a subclass of AbstractTranslator.  The Translators must implement 
mapAll

The result of mapAll is specific to the application.  Could return an XML object (mageml), 
GUS object (GUS Loader), or write a file (mage tab)...

=over 4

=item C<new>

B<Parameters:>

$class: Caller which is the subclass of AbstractReader
$docRoot(): The Reader only knows about one file

=cut

sub new {
  my $class = shift;

  if(ref($class) eq 'AbstractTranslator') {
    RAD::MR_T::MageImport::MageImportError->
        new("try to instantiate an abstract class AbstractTranslator")->throw();
  }
  bless {}, $class; 
}

sub mapAll { }

1;
