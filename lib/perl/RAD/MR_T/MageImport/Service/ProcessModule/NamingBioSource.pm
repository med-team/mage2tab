package RAD::MR_T::MageImport::Service::ProcessModule::NamingBioSource;

# this is process module impl

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

=head1 NamingBioSource Module

=over

=cut

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}


=item myProcess($docRoot)

Description: numbering the biosource

=cut

sub myProcess {
  my ($self, $docRoot) = @_;
  my $counter=1;
  foreach my $biomat(@{$docRoot->getBioMaterialVOs}){
    if($biomat->getSubclassView eq 'BioSource'){
      $biomat->setName($biomat->getName." source ".$counter);
      $counter++;
    }
  }
}
1;
