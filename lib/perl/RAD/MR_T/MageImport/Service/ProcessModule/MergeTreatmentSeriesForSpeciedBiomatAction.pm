package RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeriesForSpeciedBiomatAction;

# this is process module impl

use strict 'vars';
use Carp;
use Data::Dumper;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;
use RAD::MR_T::MageImport::VO::TreatmentVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

=head1 AddQuanProtocol Module

=over

=cut

sub new {
  my $self = shift()->SUPER::new(@_);
  shift; 
  $self->{termStr} = shift;
  return $self;
}

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}


=item myProcess($docRoot)

Description: MakeTreatmentSeriesForSpeciedBiomatAction

Side effect: only deal with TreatmentType which is specified_biomaterial_action
             only make oe.categroy ComplexAction, for translator should retrieveFromDB based on value first

=cut

sub myProcess { 
  my ($self, $docRoot) = @_;

  my @newTreatments;

  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt && $trt->getTreatmentType->getValue eq "specified_biomaterial_action"){
      my $start = $trt->getOrderNum;
      my $outputBM = $trt->getOutputBM;
#need reOrder the next trt series
 
     foreach my $trt2(@{$docRoot->getTreatmentVOs}){
	if($trt2 && $trt2->getOutputBM == $outputBM && $trt2->getOrderNum != $start){
	  $trt2 = undef;
	}
      }

      $trt->setTreatmentType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({category=>"ComplexAction", 
									      value=>$self->getTermStr,
									     })
			    );
      $trt->setOrderNum(1);
    }#if(specified_biomaterial_action)
  }

  foreach my $trt (@{$docRoot->getTreatmentVOs}) {
    push(@newTreatments, $trt) if($trt);
  }

  $docRoot->treatmentVOs(\@newTreatments);

}

sub getTermStr{
  my $self = shift;
  return $self->{termStr};
}
1;
