package RAD::MR_T::MageImport::Service::ProcessModule::SetQuanProtocolToGusProtocol;

# this is process module impl

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::VO::ProtocolVO;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

=head1 SetQuanProtocol Module

Description: set quantification protocol to GUS protocol by giving GUS protocol name 

Use Case: the quantifications in mage doc use some standard affy protocols or the exsting GUS protocols, but the mage doc either has its own protocol defined or may not have it at all, so we want to set its quantification protocol to existing protocol in GUS

Input:  existing GUS quantification protocol name exactly as in database

=cut

=item constructor

input: $processor, gus_quan_protocol_name

=cut

sub new {
  my $self = shift()->SUPER::new(@_);
  shift; 
  $self->{quanName} = shift;
  return $self;
}

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}


=item myProcess($docRoot)

Description: set quantification protocol

=cut

sub myProcess {
  my ($self, $docRoot) = @_;
  my $counter=1;
  foreach my $assay(@{$docRoot->getAssayVOs}){
    foreach my $acq(@{$assay->getAcquisitions}){
      foreach my $quan(@{$acq->getQuantifications}){
      $quan->setProtocolName($self->{quanName});
      }
    }
  }

  $docRoot->addProtocolVOs(RAD::MR_T::MageImport::VO::ProtocolVO->new({name=>$self->{quanName}}));
}

sub getQuanName{
  my $self = shift;
  return $self->{quanName};
}
1;
