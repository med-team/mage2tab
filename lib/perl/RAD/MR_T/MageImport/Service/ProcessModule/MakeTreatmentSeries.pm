package RAD::MR_T::MageImport::Service::ProcessModule::MakeTreatmentSeries;

# this is process module impl

use strict 'vars';
use Carp;
use Data::Dumper;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;
use RAD::MR_T::MageImport::VO::TreatmentVO;
use RAD::MR_T::MageImport::VO::OntologyEntryVO;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

=head1 AddQuanProtocol Module

=over

=cut

sub new {
  my $self = shift()->SUPER::new(@_);
  shift; 
  $self->{termStr} = shift;
  return $self;
}

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}


=item myProcess($docRoot)

Description: MakeTreatmentSeries

Side effect: only deal with TreatmentType which is specified_biomaterial_action
             only make oe.categroy ComplexAction, for translator should retrieveFromDB based on value first

=cut

sub myProcess { 
  my ($self, $docRoot) = @_;
  foreach my $trt(@{$docRoot->getTreatmentVOs}){
    if($trt->getTreatmentType->getValue eq "specified_biomaterial_action"){
      my @terms = split(",", $self->getTermStr);
      my $start = $trt->getOrderNum;
      my $next = $start+ scalar(@terms);
      my $outputBM = $trt->getOutputBM;
#need reOrder the next trt series
      foreach my $trt2(@{$docRoot->getTreatmentVOs}){
	if($trt2->getOutputBM == $outputBM && $trt2->getOrderNum > $start){
	  $trt2->setOrderNum($next);
	  $next++;
	}
      }
#then split into series
      foreach my $term(@terms){
	if($start == $trt->getOrderNum){
	  $trt->setTreatmentType(RAD::MR_T::MageImport::VO::OntologyEntryVO->new({category=>"ComplexAction", 
										  value=>$term
										 })
				);
	}
	else{
	  my $trt = RAD::MR_T::MageImport::VO::TreatmentVO->new({
								 orderNum=>$start,
								 outputBM=>$trt->getOutputBM,
								 inputBMMS=>$trt->getInputBMMS,
								 protocol=>$trt->getProtocol,
								 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({category=>"ComplexAction", 
																 value=>$term
																}),
								 name=>$term
								});
	  $docRoot->addTreatmentVOs($trt);
	}
	$start++;
      }
    }#if(specified_biomaterial_action)
  }
}

sub getTermStr{
  my $self = shift;
  return $self->{termStr};
}
1;
