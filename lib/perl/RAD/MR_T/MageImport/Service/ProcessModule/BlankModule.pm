package RAD::MR_T::MageImport::Service::ProcessModule::BlankModule;

# this is blank module impl

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}

sub myProcess {
  my ($self, $docRoot) = @_;
  return 1;
}
1;
