package RAD::MR_T::MageImport::Service::ProcessModule::NamingAcqAndQuan;

# this is process module impl

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractProcessModule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessModule/;

=head1 NamingAcqAndQuan Module

=over

=cut

sub process {
 my ($self, $docRoot) = @_;
 $self->getProcessor->process($docRoot);
 $self->myProcess($docRoot);
 return ref($self);
}


=item myProcess($docRoot)

Description: naming acquisition and quantification according to assay name

Side Effect: it only adjust the one of the channels

=cut

sub myProcess {
  my ($self, $docRoot) = @_;
  foreach my $assay(@{$docRoot->getAssayVOs}){
    foreach my $acq(@{$assay->getAcquisitions}){
      if($acq->getProtocolName){
	$acq->setName($assay->getName."-".$acq->getProtocolName."-".$acq->getChannels->[0]->getValue);
      }
      else{
	$acq->setName($assay->getName."-image acquisition-".$acq->getChannels->[0]->getValue);
      }

      foreach my $quan(@{$acq->getQuantifications}){
	if($quan->getProtocolName){
	  $quan->setName($assay->getName."-".$quan->getProtocolName."-".$acq->getChannels->[0]->getValue);
	}
	else{
	  $quan->setName($assay->getName."-quantification-".$acq->getChannels->[0]->getValue);
	}
      }
    }
  }
}
1;
