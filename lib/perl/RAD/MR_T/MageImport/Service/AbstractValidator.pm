package RAD::MR_T::MageImport::Service::AbstractValidator;

# this is an abstract class

use base qw(RAD::MR_T::MageImport::Loggable);

sub check {}

my $passed = 1;

sub isPassed {return $passed;}
sub setPassed {$passed = $_[1];}
1;
