package RAD::MR_T::MageImport::Service::Processor;

# this is processor does the default processing

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::Service::AbstractProcessor;
use vars qw / @ISA / ;

@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessor/;

sub new {
  my $class = shift;
  my $arg = shift;
  bless {}, $class;
}

sub process {
  my ($self, $docRoot) = @_;
  foreach my $vo (@{$docRoot->getProtocolVOs}){
    if(my $type = $vo->getProtocolType){
      $type->setCategory("ExperimentalProtocolType") if $type->getCategory eq 'ProtocolType';
    }
  }

  foreach my $vo (@{$docRoot->getTreatmentVOs}){
    if(my $type = $vo->getTreatmentType){
      $type->setCategory("ComplexAction") if $type->getCategory eq 'Action';
    }
  }

  return $docRoot;
}

1;
