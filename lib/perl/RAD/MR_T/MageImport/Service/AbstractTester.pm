package RAD::MR_T::MageImport::Service::AbstractTester;

# this is an abstract class

use base qw(RAD::MR_T::MageImport::Loggable);

# return boolean

sub test {return $_[0]->{success}}

1;
