package RAD::MR_T::MageImport::Service::Reporter::SqlReporter;

use RAD::MR_T::MageImport::MageImportError;
use GUS::ObjRelP::DbiDatabase;

use strict;

use DBI;
#use DBD::Oracle;
use Data::Dumper;

use base qw(RAD::MR_T::MageImport::Service::AbstractReporter);

=head1 SqlReporter

this class absolutely needs to be refactored together with SqlTester in the next release

=over

=item

 sql reporting file format
 sql  parameter
 select count(*) from study.study where name='test'
 select study_id from study.study where name='test'  study_id
 select count(*) from study.studyDesign where study_id=$$study_id$$
 select count(*) from rad.studyassay where study_id=$$study_id$$
 select count(*) from rad.assay where name="test assay" 
 select assay_id from rad.assay where name="test assay"  assay_id
 select count(*) from rad.assaybiomaterial where assay_id in ($$assay_id$$)

=cut

sub new {
  my $class = shift;
  my $file = shift;
  my $queryHandler = shift;

  bless {_sqlReportingFile => $file, _queryHandler => $queryHandler}, $class;
}

sub _getLinesArray {wantarray ? @{$_[0]->{_lines_array}} : $_[0]->{_lines_array}}

sub _setLinesArray { $_[0]->{_lines_array} = $_[1] }

sub _getSqlReportingFile {$_[0]->{_sqlReportingFile}}

sub _getQueryHandler {
  if($_[0]->{_queryHandler}){
    return $_[0]->{_queryHandler};
  }
  elsif(GUS::ObjRelP::DbiDatabase->getDefaultDatabase()){
    return GUS::ObjRelP::DbiDatabase->getDefaultDatabase()->getDbHandle();
  }
  else{
    RAD::MR_T::MageImport::ReporterException::DefaultDbNotFound->new->throw();
  }
}

sub setAlgoInvoId { $_[0]->{_algoInvoId} = $_[1] }

sub getAlgoInvoId { 
 if($_[0]->{_algoInvoId}){
    return $_[0]->{_algoInvoId};
  }
  elsif(GUS::ObjRelP::DbiDatabase->getDefaultDatabase()){
   if(my $id = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()->getDefaultAlgoInvoId()){
      return $id;
    }
    else{
      RAD::MR_T::MageImport::ReporterException::DefaultAlgoInvoIdNotFound->new->throw();
    }
  }
}


#--------------------------------------------------------------------------------

sub _readFile {
 my $self = shift;

 my @fileLines;

 my $fn = $self->_getSqlReportingFile();

 open(FILE, $fn) or RAD::MR_T::MageImport::ReporterException->
   new("Cannot open file $fn for reading: $!")->throw();;

 <FILE>; # Remove the header

 while(<FILE>) {
   chomp;

   push(@fileLines, $_);
 }
 close(FILE);

 $self->_setLinesArray(\@fileLines);

 return wantarray ? @fileLines : \@fileLines;
}

sub report {
  my ($self) = @_;

  my $sLogger = $self->getLogger();
  my $testHash = $self->parseLines();

  $sLogger->info("*** SQL REPORT ***\n");

  my @noResults;
  foreach my $key (keys %$testHash) {

    if(scalar(@{$testHash->{$key}}) == 0) {
      push(@noResults, $key);
    }
    else {
      my $outString = "*** QUERY RESULT ***   $key\n";

      foreach my $row (@{$testHash->{$key}}) {
        $outString = $outString . DBI::neat_list($row, 100, "|") . "\n";
      }

      $outString =~ s/undef/''/g;
      $sLogger->info($outString, "\n");
    }
  }

  my $noResults = join("\n -", @noResults);
  $sLogger->info("*** QUERIES WITH NO RESULTS ***\n -$noResults");
}

#--------------------------------------------------------------------------------

sub parseLines {
  my ($self) = @_;

  my @lines = $self->_getLinesArray() ? $self->_getLinesArray() : $self->_readFile();

  my (%params, %sqlAsserts);

  foreach my $line (@lines) {
    next unless $line;

    next if($line =~ /^--/);
    $line =~ s/;$//;

    my ($result, $statement) = $self->_parseLine($line, \%params);

    if($statement =~ /^select +count/i) {
      my $name = $result->[0]->[0];
      my $count = $result->[1]->[0];

      push(@{$sqlAsserts{ALL_COUNTS}}, [$name, $count]);
    }
    else {
      $sqlAsserts{$statement} = $result;
    }
  }

  return \%sqlAsserts;
}

#--------------------------------------------------------------------------------

sub _parseLine {
  my ($self, $line, $params) = @_;

  my ($sql, $param) = split(/\|/, $line);
  my $sLogger = $self->getLogger();
  $sLogger->debug("sql: ", $sql);
  $sLogger->debug("param: ", $param) if $param;

  if($sql =~ /.*\$\$algoInvoId\$\$/ && !($params->{algoInvoId})){
    $params->{algoInvoId} = $self->getAlgoInvoId;
  }

  $sLogger->debug("Param hash: ",sub {Dumper($params)});
  $sLogger->debug("sql: ", $sql);
  if($sql =~ s/(\$\$(\w+)\$\$)/$params->{$2}/) {
    RAD::MR_T::MageImport::ReporterException->
        new("No Param for $2 was defined")->throw() unless(exists $params->{$2});
  }

  my $result = $self->_runSql($sql);

  if($param && $result) {
    $params->{$param} = $result->[1]->[0]; 
  }

  return ($result, $sql);
}

#--------------------------------------------------------------------------------
=item _runSql

 return: array of result row array 

=cut

sub _runSql {
  my ($self, $sql) = @_;

  my @rv;
  my $sLogger = $self->getLogger();
  $sLogger->debug($sql);
  my $dbh = $self->_getQueryHandler();

  my $sh;
  eval {$sh = $dbh->prepare($sql); $sh->execute();};
  RAD::MR_T::MageImport::ReporterException::SqlError->new("Sql execution error: $sql\n".$@)->throw() if $@;

  while(my $row = $sh->fetchrow_hashref()) {

    my @attrs = sort keys %$row;

    unless(@rv) {
      push(@rv, \@attrs);
    }

    my @ar = map { $row->{$_} } @attrs;

    push(@rv, \@ar);
  }
  $sh->finish();

  return \@rv;

}

#--------------------------------------------------------------------------------

1;
