package RAD::MR_T::MageImport::Service::AbstractValidatorRule;

# this is ValidatorRule interface,a  decorator interface

use strict 'vars';
use Error qw(:try);

use  RAD::MR_T::MageImport::Service::AbstractValidator;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidator/;


sub new {
  my $class = shift;
  my $arg = shift;
  bless {_validator=>$arg}, $class;
}

sub mycheck {}

sub check {
 my ($self, $docRoot) = @_;
 my $logger = $self->getLogger();

 try{
   $self->getValidator->check($docRoot);
 }
   catch Error with {
     my $e = shift;
     $logger->warn("Error:", $e->text());
     $self->setPassed(0);
   };

 $logger->warn("\n******", ref($self),   "********");
 $self->mycheck($docRoot);

 return ref($self);
}

sub getValidator  {$_[0]->{_validator}}
sub setValidator  {$_[0]->{_validator} = $_[1]}


1;
