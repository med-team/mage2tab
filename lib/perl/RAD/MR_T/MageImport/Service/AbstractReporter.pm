package RAD::MR_T::MageImport::Service::AbstractReporter;

# this is an abstract class

use base qw(RAD::MR_T::MageImport::Loggable);

# return boolean

sub report {return $_[0]->{success}}

1;
