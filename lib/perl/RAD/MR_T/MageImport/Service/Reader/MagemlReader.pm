package RAD::MR_T::MageImport::Service::Reader::MagemlReader;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

use Bio::MAGE qw(:ALL);
use Bio::MAGE::Association;
use Bio::MAGE::XMLUtils;

use RAD::MR_T::MageImport::Service::AbstractReader;
use RAD::MR_T::MageImport::VO::DocRoot;

use Data::Dumper;
use Benchmark;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractReader/;


=head1 Main METHODS

=over

=item parse($magefile)

Side-effect: the order of parsing is important

=cut

sub parse  {
  my ($self) = @_;

  my $doc = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $mage = $self->getMage();

  $self->setPersonAndAffiliationVOs($doc, $mage);
  $self->setExtDBVOs($doc, $mage);
  $self->setProtocolAndParamVOs($doc, $mage);
  $self->setStudyVO($doc, $mage);
  $self->setBioMatVOs($doc, $mage);
  $self->setTreatmentVOs($doc, $mage);
  $self->setAssayVOs($doc, $mage);

  my $logger = $self->getLogger();
  $logger->info("****Dump the whole docRoot****", sub { Dumper($doc) } );
  return $doc;

}


=head1 CLASS PRIVATE METHODS

=over


=item getMage

=cut

sub getMage {
 my $self = shift;
  my $reader = Bio::MAGE::XML::Reader->new(
					   #log_file=>"mage.log",
					   external_data=>1,
					   verbose=>0
					  );
 my $magefile = $self->getFile();
 croak "File '$magefile' does not exist!\n" unless (-f $magefile || $magefile eq '-' );
 return $reader->read($magefile);
}


=item setPersonAndAffiliationVOs($doc, $mage)

Description: convert C<$mage> objects to vo objects and add them into C<$doc> .

Input parameters: C<$mage> and C<$doc>

Return value: none 

Side effects: assume only person has affiliation, organization doesn't

Exceptions: none

to-dos:  1) write searchObjArrayByObjName method (done)
         2) refactor the method of making affvo and personvo

=cut

sub setPersonAndAffiliationVOs {
  my ($self, $doc, $mage) = @_;

  my $logger = $self->getLogger();

  my $auditAndSecurity_pkg = $mage->getAuditAndSecurity_package(); 
  my $contacts = $auditAndSecurity_pkg->getContact_list();
  $logger->debug("Number of contacts:", scalar @$contacts);
  foreach my $contact (@$contacts){
     if(UNIVERSAL::isa ($contact, 'Bio::MAGE::AuditAndSecurity::Organization')){
       my %h;
       $h{name} = $contact->getName;
       $h{address} = $contact->getAddress if $contact->getAddress;
       $h{email} = $contact->getEmail if $contact->getEmail;
       $h{role} = $contact->getRoles()->[0]->getValue if $contact->getRoles();
    #   $name = $contact->getParent->getName." ".$name if $contact->getParent;
    #   $address = $contact->getParent->getAddress if $contact->getParent;

       my $af = RAD::MR_T::MageImport::VO::AffiliationVO->new(\%h);
       $logger->debug("Dump new affiliation: ", sub { Dumper($af) } );
       $logger->debug("Organization name: ", $h{name});

       $doc->addAffiliationVOs($af);
    }
  }



  $logger->info("Dump affiliations from docRoot: ", sub { Dumper($doc->getAffiliationVOs) } );

  foreach my $contact (@$contacts){
    if(UNIVERSAL::isa ($contact, 'Bio::MAGE::AuditAndSecurity::Person')){
      my %h;
      $h{name} = $contact->getName if $contact->getName;
# need to croak if  
      $h{name} = $contact->getFirstName." ".$contact->getLastName  unless $contact->getName;

      $h{first} = $contact->getFirstName if $contact->getFirstName;
      $h{last} = $contact->getLastName if $contact->getLastName;
      $h{address} = $contact->getAddress if $contact->getAddress;
      $h{email} = $contact->getEmail if $contact->getEmail;

      $h{role} = $contact->getRoles()->[0]->getValue if $contact->getRoles();

      my $p = RAD::MR_T::MageImport::VO::PersonVO->new(\%h);
      if($contact->getAffiliation){
	$logger->debug("Affiliation name: ", $contact->getAffiliation->getName);
	my $af = $self->searchObjArrayByObjName($doc->getAffiliationVOs, $contact->getAffiliation->getName);
	$p->setAffiliation($af) if $af;
      }
      $doc->addPersonVOs($p);
    }
  }

  $logger->info("Dump persons from docRoot: ", sub { Dumper($doc->getPersonVOs) } );

}

=item setExtDBVOs($doc, $mage)

Description: convert C<$mage> db objects to vo objects and add them into C<$doc> .

=cut

sub setExtDBVOs {
  my ($self, $doc, $mage) = @_;
  my $logger = $self->getLogger();

  my $description_pkg = $mage->getDescription_package(); 

  $logger->debug("Number of dbs:", scalar @{$description_pkg->getDatabase_list});

  foreach my $db (@{$description_pkg->getDatabase_list}){
    my $db = 
      RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new({
							  name=>$db->getName, 
							  version=>$db->getVersion
							 });
    $doc->addExternalDatabaseVOs($db);
  }

  $logger->info("Dump extDBs from docRoot: ", sub { Dumper($doc->getExternalDatabaseVOs) } );

}


=item setProtocolVOs($doc, $mage)

Description: convert C<$mage> protocol objects to vo objects and add them into C<$doc> .

Side effects: assume protocol only has one software and one hardware association

=cut

sub setProtocolAndParamVOs {
  my ($self, $doc, $mage) = @_;

  my %unitcv2oe = ('h'=>"hours", 's'=>"seconds", 'm'=>"minutes", 'd'=>"days", 'degree_C'=>"degrees_C", 'degree_F'=>"degrees_F");

  my $logger = $self->getLogger();
  my $protocol_pkg = $mage->getProtocol_package(); 

  foreach my $p (@{$protocol_pkg->getProtocol_list}){
    my $protocol = 
      RAD::MR_T::MageImport::VO::ProtocolVO->new({
						  name=>$p->getName?$p->getName:$p->getIdentifier, 
						  uri=>$p->getURI,
						  protocolDescription=>$p->getText
						 });
    my $type = $self->map2OEVO($p->getType);

    $protocol->setProtocolType($type);

# only take one software
    if($p->getSoftwares()){
      my $sw=$p->getSoftwares()->[0] ;
      my $swtype = $self->map2OEVO($sw->getType);

      $protocol->setSoftwareType($swtype);
      $protocol->setSoftwareDescription($sw->getName);
    }
# only take one hardware
    if($p->getHardwares()){
      my $hw=$p->getHardwares()->[0];
      my $hwtype = $self->map2OEVO($hw->getType);

      $protocol->setHardwareType($hwtype);
      $protocol->setHardwareDescription($hw->getName);
    }

    if($p->getParameterTypes()){
      foreach my $param (@{$p->getParameterTypes()}){
	my %h;
	$h{name} = $param->getName;

	$h{dataType} = $self->map2OEVO($param->getDataType) if $param->getDataType;

	if(my $default = $param->getDefaultValue){
	  my @types = split('::', ref($default->getUnit));
	  if(my $unitcv = $default->getUnit->getUnitNameCV){
	    my $value = $unitcv2oe{$unitcv}?$unitcv2oe{$unitcv}:lc($unitcv);
	    $h{unitType} = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
									    value=>$value,
									    category=>$types[scalar(@types) - 1]
									   });
	  }

	 $h{value} = $default->getValue;
	}

	$protocol->addParams(RAD::MR_T::MageImport::VO::ProtocolParamVO->new(\%h));
      }
    }
    $doc->addProtocolVOs($protocol);
  }

 $logger->info("Dump protocols from docRoot: ", sub { Dumper($doc->getProtocolVOs) } );
}



=item setStudyVO($doc, $mage)

Description: convert C<$mage> study,studydesign, studyfactor objects to vo objects and add them into C<$doc> .

to-dos: 1) retrieve bibliographRef
        2) handle mutiple study designs
        3) need method: getPersonVOByLastAndFirst

=cut

sub setStudyVO {
  my ($self, $doc, $mage) = @_;
  my $logger = $self->getLogger();
  my $experiment_pkg = $mage->getExperiment_package();
#note we only handle MAGE-ML with only one experiment obj
  my $experiment = $experiment_pkg->experiment_list()->[0];

  my $studyvo = 
    RAD::MR_T::MageImport::VO::StudyVO->new({
					     name=>$experiment->getName 
					    });

  foreach my $contact (@{$experiment->getProviders}){
    next unless $contact->getRoles();
    foreach my $role(@{ $contact->getRoles()}){
      if ($role->getValue eq "submitter"){
	my $name;
	$name = $contact->getName if $contact->getName;
	$name = $contact->getFirstName." ".$contact->getLastName  unless $contact->getName;
	my $p = $self->searchObjArrayByObjName($doc->getPersonVOs, $name);
	$studyvo->setContact($p);
      }
    }
  }

  foreach my $desc (@{$experiment->getDescriptions()}){
    $studyvo->setDescription($desc->getText) if $desc->getText;
    if(my $bbfs = $desc->getBibliographicReferences){
      if(my $dbref = $bbfs->[0]->getAccessions){
	$studyvo->setPubMedId($dbref->[0]->getAccession);
      }
    }
  }

  my $design = $experiment->getExperimentDesigns()->[0];

  my $sdvo = 
    RAD::MR_T::MageImport::VO::StudyDesignVO->new({
						   name=>$experiment->getName." design" 
						  });

  foreach my $type(@{$design->getTypes}){
    my $sdoe = $self->map2OEVO($type);
    $sdvo->addTypes($sdoe);
  }

  $studyvo->addDesigns($sdvo);

  my $experimentFactor = $design->getExperimentalFactors();

  $self->makeFactorValue2FactorHash($experimentFactor);


  foreach my $factor(@$experimentFactor){
    my $type = $factor->getCategory;
    my $sfoe = $self->map2OEVO($type);

    my $name = $factor->getName?$factor->getName:$type->getValue;

    my $sfvo = 
       RAD::MR_T::MageImport::VO::StudyFactorVO->new({
						      name=>$name 
						     });



    $sfvo->setType($sfoe);
    $sdvo->addFactors($sfvo);
  }

  $doc->studyVO($studyvo);

 $logger->info("Dump studyvo from docRoot: ", sub { Dumper($doc->getStudyVO) } );
}

=item setBioMatVOs($doc, $mage)

Description: convert C<$mage> material objects to vo objects and add them into C<$doc> .

Side effect: 1)assume each biomat only have one text field for all descriptions
             2)assume one label per lex

To-dos: 1) need to figure out how to handle label method for lex

=cut

sub setBioMatVOs{
  my ($self, $doc, $mage) = @_;
  my $logger = $self->getLogger();

  my $biomat_pkg = $mage->getBioMaterial_package();

  foreach my $biomat (@{$biomat_pkg->getBioMaterial_list}){
    my %h;

    if((UNIVERSAL::isa($biomat, 'Bio::MAGE::BioMaterial::LabeledExtract' ))){
      $h{subclassView} = 'LabeledExtract';

      $h{channel} = 
	RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
							 value=>$biomat->getLabels()->[0]->getName, 
							 category=>"LabelCompound" 
							});
    }

    if((UNIVERSAL::isa($biomat, 'Bio::MAGE::BioMaterial::BioSample' ))){
      $h{subclassView} = 'BioSample';
    }

    if((UNIVERSAL::isa($biomat, 'Bio::MAGE::BioMaterial::BioSource' ))){
      $h{subclassView} = 'BioSource';
    }

    $h{name}=$biomat->getName;

#assume each biomat only have one text field for all descriptions
    if(my $descs = $biomat->getDescriptions()){
      foreach my $desc (@$descs){
	$h{description} = $desc->getText if $desc->getText;
      }
    }

    $h{bioMaterialType}=$self->map2OEVO($biomat->getMaterialType);

    my @oevos;

    if(my $chars = $biomat->getCharacteristics()){
      foreach my $mageoe (@$chars){
	push @oevos, $self->map2OEVO($mageoe);
      }
    }


    $h{bioMaterialChars} = \@oevos;

    my $biomatvo = RAD::MR_T::MageImport::VO::BioMaterialVO->new(\%h);
    $self->addToId2BioMatVoHash($biomat->getIdentifier, $biomatvo);
    $doc->addBioMaterialVOs($biomatvo);

  }

 $logger->info("Dump BioMaterialVOs from docRoot: ", sub { Dumper($doc->getBioMaterialVOs) } );
 $logger->info("Dump Id2BioMatVoHash from docRoot: ", sub { Dumper($self->getId2BioMatVoHash) } );
}

=item setTreatmentVOs($doc, $mage)

Description: convert C<$mage> material objects to vo objects and add them into C<$doc> .

Side effects: treatment only has one pa 

To-dos: 1)need to read treatment param

=cut

sub setTreatmentVOs{
  my ($self, $doc, $mage) = @_;
  my $logger = $self->getLogger();

  my $biomat_pkg = $mage->getBioMaterial_package();
  foreach my $biomat (@{$biomat_pkg->getBioMaterial_list}){
    #this will handle treatment steps
    #biosource doesn't have treatment
    if(my $trts = $biomat->getTreatments()){
      foreach my $trt (@$trts){
	my %h;
	$h{name} = $trt->getName? $trt->getName : $trt->getIdentifier;
	$h{outputBM} = $self->searchObjArrayByObjName($doc->getBioMaterialVOs, $biomat->getName);
	#treatment only have order if it is series
	$h{orderNum} = $trt->getOrder if $trt->getOrder;
	$h{treatmentType}=$self->map2OEVO($trt->getAction);

	# treatment only has one pa
	if($trt->getProtocolApplications()){
	  $h{protocol} = $self->searchObjArrayByObjName($doc->getProtocolVOs,
							$trt->getProtocolApplications()->[0]->getProtocol()->getName
						       );
	}

	$self->setProtocolAndParamValues(\%h,$trt);

	my $trtVO = RAD::MR_T::MageImport::VO::TreatmentVO->new(\%h);

	foreach my $sbmm (@{$trt->getSourceBioMaterialMeasurements()}){
	    $trtVO->addInputBMMs($self->map2BMMVO($sbmm, $doc));
	}

	$doc->addTreatmentVOs($trtVO);
      }
    }
  }

 $logger->info("Dump TreatmentVOs from docRoot: ", sub { Dumper($doc->getTreatmentVOs) } );
}

=item setAssayVOs

Side effect: 1) assign the assay operator to the submitter,
                for the mage doc doesn't have it
             2) only deal with the DBA which is one level down from MBA

To-dos: 1) code for 2-channel assays (done)
        2) code for link assayLex and assayBioMat (done)
        3) code for array identifier and batch for assay table
        4) code for assayParam (done)

=cut

sub setAssayVOs{
  my ($self, $doc, $mage) = @_;
  my $logger = $self->getLogger();

  my %id2assayvo = ();
  my %id2acquisitionvo = ();
  my %id2quantificationvo = ();
  my %mbaid2acquisitionvo = ();
  my @channelNames = ();

  my $assay_pkg = $mage->getBioAssay_package();

  foreach my $assay (@{$assay_pkg->getBioAssay_list}){
    my $assayvo;
    if((UNIVERSAL::isa($assay, 'Bio::MAGE::BioAssay::PhysicalBioAssay' ))){
      my $bac = $assay->getBioAssayCreation();
      if((UNIVERSAL::isa($bac, 'Bio::MAGE::BioAssay::Hybridization' ))){
	@channelNames = ();
	$assayvo = $self->makeAssayVO($bac, $doc);
	$logger->debug("Dump AssayVO created: ", sub { Dumper($assayvo) } );

	#get lex, this code is very vunerable to XML file, we need to watch this line
	foreach my $sbmm (@{$bac->getSourceBioMaterialMeasurements()}){
	  push @channelNames, $sbmm->getBioMaterial->getLabels()->[0]->getName;
	  $assayvo->addLabeledExtracts($self->getId2BioMatVoHash->{$sbmm->getBioMaterial()->getIdentifier}); 
	}
	$logger->debug("Dump channel names for this assay: ", sub {Dumper(\@channelNames)});
	$id2assayvo{$assay->getIdentifier} = $assayvo;  
	$doc->addAssayVOs($assayvo); 
      }
    }
  }

  foreach my $assay (@{$assay_pkg->getBioAssay_list}){
    if((UNIVERSAL::isa($assay, 'Bio::MAGE::BioAssay::PhysicalBioAssay' ))){
      if($assay->getBioAssayTreatments){ 
	foreach my $bat (@{$assay->getBioAssayTreatments}){
	  if((UNIVERSAL::isa($bat, 'Bio::MAGE::BioAssay::ImageAcquisition' ))){
	    my $acquisitionvo = $self->makeAcquisitionVO($bat, $assay, \%id2assayvo, \%id2acquisitionvo);
	    if(!$acquisitionvo->getChannels){
	      my @channelOEs = ();
	      foreach my $channelName(@channelNames){
		push @channelOEs, RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
										   value=>$channelName, 
										   category=>"LabelCompound" 
										  });
	      }
	      $acquisitionvo->setChannels(\@channelOEs);
	    }
	    $logger->debug("Dump AcquisitionVO created: ", sub { Dumper($acquisitionvo) } );
	  }#if imageAcquisition
	}#foreach
      }
    }#if(PhysicalBioAssay)
  }

  $logger->debug("Dump AssayVO created with AcquisitionVO: ", sub { Dumper($doc->getAssayVOs) } );

  foreach my $assay (@{$assay_pkg->getBioAssay_list}){
    if((UNIVERSAL::isa($assay, 'Bio::MAGE::BioAssay::MeasuredBioAssay' ))){
      my $fe = $assay->getFeatureExtraction();
      my $quantificationvo = $self->makeQuantificationVO($assay);
      $logger->debug("Dump QuantificationVO created: ", sub { Dumper($quantificationvo) } );
      if(my $pba = $fe->getPhysicalBioAssaySource()){
	if(my $acquisitionvo = $id2acquisitionvo{$pba->getIdentifier}){
	  $logger->debug("acquisition found: ", sub {Dumper($acquisitionvo)});
	  $acquisitionvo->addQuantifications($quantificationvo);
	  $id2quantificationvo{$assay->getIdentifier} = $quantificationvo;
	  $mbaid2acquisitionvo{$assay->getIdentifier} = $acquisitionvo;
	}
      }
    }#if(MeasuredBioAssay)
  }

  my %id2processvo = ();
  foreach my $assay (@{$assay_pkg->getBioAssay_list}){
    if(UNIVERSAL::isa($assay, 'Bio::MAGE::BioAssay::DerivedBioAssay' )){
      my $processvo = $self->makeProcessVO($assay);
      $id2processvo{$assay->getIdentifier} = $processvo;
    }
  }
  if( $mage->getBioAssayData_package() && $mage->getBioAssayData_package()->getBioAssayMap_list()){
    foreach my $assayMap (@{$mage->getBioAssayData_package()->getBioAssayMap_list()}){
      my $processvo = $id2processvo{$assayMap->getBioAssayMapTarget->getIdentifier};
      foreach my $sba (@{$assayMap->getSourceBioAssays}){
	if(UNIVERSAL::isa($sba, 'Bio::MAGE::BioAssay::MeasuredBioAssay')){
	  $id2quantificationvo{$sba->getIdentifier}->addProcesses($processvo);
	}
	if(UNIVERSAL::isa($sba, 'Bio::MAGE::BioAssay::DerivedBioAssay')){
	  $id2processvo{$sba->getIdentifier}->addTargets($processvo);
	}
      }
    }
  }

#   if($channelNames[0] =~ /biotin/i){

#   foreach my $assay (@{$assay_pkg->getBioAssay_list}){
#     if(UNIVERSAL::isa($assay, 'Bio::MAGE::BioAssay::DerivedBioAssay' )){
#       my $assayMap;
#       if ($assay->getDerivedBioAssayMap){
# 	$assayMap = $assay->getDerivedBioAssayMap->[0];
#       }
#       elsif($assay->getDerivedBioAssayData && $assay->getDerivedBioAssayData->[0]->getProducerTransformation){
# 	$assayMap = $assay->getDerivedBioAssayData->[0]->getProducerTransformation->getBioAssayMapping->getBioAssayMaps->[0];
#       }
#       else{
# 	next;
#       }
#       my $chpQuantificationvo = $self->makeCHPQuantificationVO($assay);
#       foreach my $sba (@{$assayMap->getSourceBioAssays}){
# 	if(UNIVERSAL::isa($sba, 'Bio::MAGE::BioAssay::MeasuredBioAssay')){
# 	  $mbaid2acquisitionvo{$sba->getIdentifier}->addQuantifications($chpQuantificationvo);
# 	}
#       }
#     }#if(DerivedBioAssay) only deal with the DBA which is one level down from MBA#if(DerivedBioAssay)
#   }
# }

 $logger->info("Dump AssayVOs from docRoot: ", sub { Dumper($doc->getAssayVOs) } );
}


=item makeAssayVO()

=cut

sub makeAssayVO{
 my ($self, $bac, $doc) = @_;
 my %h;
 $h{name} = $bac->getName? $bac->getName:$bac->getIdentifier;
 # $h{name} = $assay->getName? $assay->getName:$assay->getIdentifier;
 $h{arraySourceId} = $bac->getArray->getArrayDesign->getIdentifier;
 #set submitter name to operator
 $h{operator} = $doc->getStudyVO->getContact;
 $self->setProtocolAndParamValues(\%h,$bac);

 return RAD::MR_T::MageImport::VO::AssayVO->new(\%h);
}

sub setProtocolAndParamValues{
  my ($self, $h, $bac) = @_;
  if($bac->getProtocolApplications() && $bac->getProtocolApplications()->[0]->getProtocol()){
    my $pas = $bac->getProtocolApplications();
    my $p = $pas->[0]->getProtocol();
    $h->{protocolName} = $p->getName?$p->getName:$p->getIdentifier;
    if(my $pvs = $pas->[0]->getParameterValues){
      my @parameterValueVOs = ();
      foreach my $magePV (@$pvs){
	if(my $pname = $magePV->getParameterType->getName){
	  my %h;
	  $h{value} = $magePV->getValue;
	  $h{parameterName} = $pname;
	  push @parameterValueVOs, RAD::MR_T::MageImport::VO::ParameterValueVO->new(\%h);
	}
	else{
	  my %h;
	  $h{value} = $magePV->getValue;
	  $h{parameterName} = $magePV->getParameterType->getIdentifier;
	  push @parameterValueVOs, RAD::MR_T::MageImport::VO::ParameterValueVO->new(\%h);
	}
      }
      $h->{parameterValues} = \@parameterValueVOs if $parameterValueVOs[0];
    }
  }
}


sub makeAcquisitionVO{
  my ($self, $bat, $assay, $id2assayvo, $id2acquisitionvo) = @_;
  my %h;

  $self->setProtocolAndParamValues(\%h,$bat);

  $h{uri} = $assay->getPhysicalBioAssayData->[0]->getName if($assay->getPhysicalBioAssayData && $assay->getPhysicalBioAssayData->[0]->getName);

  my ($assayvo, $mageassay);
  #AE's way: IA targets Hyb 
  if($assayvo = $id2assayvo->{$bat->getTarget->getIdentifier}){
    $mageassay = $assay;
  }
  #Standard way: IA targets Image, the $assay itself is the hyb
  elsif($assayvo = $id2assayvo->{$assay->getIdentifier}){
    $mageassay = $bat->getTarget;
  }
  else{
    die "could not find assay for acquisition";
  }

  $h{name} = $mageassay->getName?$mageassay->getName:$bat->getIdentifier;

  if($mageassay->getChannels){
    my $channelName = $mageassay->getChannels->[0]->getName;
    $h{channels} = [RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
								     value=>$channelName, 
								     category=>"LabelCompound" 
								    })
		   ];
  }
  my $acquisitionvo = RAD::MR_T::MageImport::VO::AcquisitionVO->new(\%h);
  $id2acquisitionvo->{$mageassay->getIdentifier} = $acquisitionvo;
  $assayvo->addAcquisitions($acquisitionvo);
  $self->addFactorValue($mageassay, $acquisitionvo);
  return $acquisitionvo;
}

sub addFactorValue{
  my ($self, $assay, $acquisitionvo) = @_;
  #some MAGE-ML doesn't have factorValues
  if($assay->getBioAssayFactorValues()){
    foreach my $fv (@{$assay->getBioAssayFactorValues()}){
      my $magefactor = $fv->getExperimentalFactor() if $fv->getExperimentalFactor();
      $magefactor = $self->getFactorValue2FactorHash->{$fv->getIdentifier};
      #deal with OE factor value for now
      my $mageoe = $fv->getValue;
      my $oevo=$self->map2OEVO($mageoe) if $mageoe;
      $acquisitionvo->addFactorValues(RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName=>$magefactor->getName, value=>$oevo}));
    }
  }
}

sub makeQuantificationVO{
  my ($self, $assay) = @_;
  my $fe = $assay->getFeatureExtraction();
  my %h;
  $h{name} = $assay->getName? $assay->getName:$assay->getIdentifier;
  $h{uri} = $assay->getMeasuredBioAssayData->[0]->getBioDataValues->getCube
    if ($assay->getMeasuredBioAssayData && $assay->getMeasuredBioAssayData->[0]->getBioDataValues) ;
  $self->setProtocolAndParamValues(\%h,$fe);
  return RAD::MR_T::MageImport::VO::QuantificationVO->new(\%h);

}

=item makeCHPQuantificationVO

Side effect: assume everything works perfect here!!!

To-do: need a lot of checks

=cut

sub makeCHPQuantificationVO{
  my ($self, $assay) = @_;
  my %h;
  $h{name} = $assay->getName? $assay->getName:$assay->getIdentifier;

  if(my $dbad = $assay->getDerivedBioAssayData){
    $h{uri} = $dbad->[0]->getBioDataValues->getCube;
    $self->setProtocolAndParamValues(\%h,$dbad->[0]->getProducerTransformation);
  }

  return RAD::MR_T::MageImport::VO::QuantificationVO->new(\%h);
}

sub makeProcessVO{
  my ($self, $assay) = @_;
  my %h;
  $h{name} = $assay->getName? $assay->getName:$assay->getIdentifier;

  if(my $dbad = $assay->getDerivedBioAssayData){
    $h{uri} = $dbad->[0]->getBioDataValues->getCube;
    $self->setProtocolAndParamValues(\%h,$dbad->[0]->getProducerTransformation);
  }

  return RAD::MR_T::MageImport::VO::ProcessVO->new(\%h);
}

=item map2BMMVO

Side Effect: note the unit oe only has value no category, we should be able to retrieve the unit oe from rad based on value

=cut

sub map2BMMVO{
  my ($self, $mageBMM, $doc) = @_;
  my $logger = $self->getLogger();
  my %h;

  $logger->debug("search biomat id:", $mageBMM->getBioMaterial()->getIdentifier);

  $h{bioMaterial} = $self->getId2BioMatVoHash->{$mageBMM->getBioMaterial()->getIdentifier}; 

  if($mageBMM->getMeasurement()){
  $h{value} = $mageBMM->getMeasurement()->getValue();
  $h{unitType} = 
    RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
						     value=>$mageBMM->getMeasurement()->getUnit()->getUnitName(), 
						     category=>'Unit'
						    }) if $mageBMM->getMeasurement()->getUnit();
}
  return RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new(\%h);
}

#Util methods
sub map2OEVO{
  my ($self, $mageoe) = @_;
  return RAD::MR_T::MageImport::VO::OntologyEntryVO->new({
							  value=>$mageoe->getValue, 
							  category=>$mageoe->getCategory 
							 });
}

sub addToId2BioMatVoHash{
    my ($self, $id, $vo) = @_;
    $self->{_id2biomatvo}->{$id} = $vo;
}

sub getId2BioMatVoHash{
    my $self = shift;
    return $self->{_id2biomatvo};
}


sub addToMBA2AcquisitionVOHash{
  my ($self, $mbaId, $acquisitionvo) = @_;
  $self->{_mba2acqvo} ->{$mbaId} = $acquisitionvo;
}

sub makeFactorValue2FactorHash{
  my ($self, $experimentFactors)=@_;
  my %fv2f;
  foreach my $factor (@$experimentFactors){
    if($factor->getFactorValues){
      foreach my $fv (@{$factor->getFactorValues}){
	$fv2f{$fv->getIdentifier} = $factor;
      }  
    }
  }
  $self->{_fv2f} = \%fv2f;
}

sub getFactorValue2FactorHash{
  my $self=shift;
  return $self->{_fv2f};
}


sub searchObjArrayByObjName{
 my ($self, $objArray, $name) = @_;

 my $logger = $self->getLogger();

 foreach my $obj(@$objArray){
   $logger->debug("search name: ", $name, "obj name: ", $obj->getName);
   return $obj if $obj->getName eq $name;
 }
}

1;
