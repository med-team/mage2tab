package RAD::MR_T::MageImport::Service::Reader::MageTabReader;

use strict;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::Util qw(searchObjArrayByObjName);

use Error qw(:try);

use Data::Dumper;

use base qw(RAD::MR_T::MageImport::Service::AbstractReader);

=head1 NAME

RAD::MR_T::MageImport::Service::Reader::MageTabReader;

=head1 SYNOPSIS

 my $reader = RAD::MR_T::MageImporter::Service::Reader::MageTabReader->new();
 my $docRoot = $reader->parse();

=head1 DESCRIPTION

Subclass of AbstractReader.  It implements the parse method by returning 
a RAD::MR_T::MageImport::VO::DocRoot object

WARNING:  The Reader does NOT check that the Mage file is valid.  It reads the file as it is!!!

Currently only one SDRF is supported.

=cut

#--------------------------------------------------------------------------------

sub getLines {$_[0]->{lines}}
sub setLines {$_[0]->{lines} = $_[1]}

#--------------------------------------------------------------------------------

=head2 Subroutines

=over 4

=item C<readTabFile>

Opens and reads the Tab File.  Creates an array of lines.  Removes some characters
 from All the Strings (currently only quotes are removed)

B<Return Type:> 

 C<ARRAY_REF> Array_ref representation of the file

=cut

sub readTabFile {
  my ($self) = @_;

  my $fn = $self->getFile();

  unless(-e $fn) {
    RAD::MR_T::MageImport::MageImportError->
      new("File [$fn] does not exist!")->throw();
  }

  my @lines;

  if($fn =~ /\.xls$/) {
    eval {
      require Spreadsheet::ParseExcel;
    };
    if(@$) {
      RAD::MR_T::MageImport::MageImportError->
          new("Spreadsheet::ParseExcel is a required package")->throw();
    }

    my $oExcel = new Spreadsheet::ParseExcel;

    my $oBook = $oExcel->Parse($fn);

    for(my $iSheet=0; $iSheet < $oBook->{SheetCount} ; $iSheet++) {

      my $oSheet = $oBook->{Worksheet}[$iSheet];
      my $minRow = $oSheet->{MinRow};
      my $maxRow = $oSheet->{MaxRow};
      my $minCol = $oSheet->{MinCol};
      my $maxCol = $oSheet->{MaxCol};

      if($maxRow) {
        for(my $i = $minRow; $i <= $maxRow; $i++) {

          my @row;
          for(my $j = $minCol; $j <= $maxCol; $j++) {

            my $oCell = $oSheet->{Cells}[$i][$j];
            my $cellValue = $oCell ? $oCell->Value : undef;

            $cellValue =~ s/["\']//g;
            push(@row, $cellValue);
          }
          push @lines, join("\t", @row);
        }
      }
    }
  }
  elsif($fn =~ /\.idf$/) {
   open(FILE, $fn) or RAD::MR_T::MageImport::MageImportError->
      new("Could Not open File $fn for Reading: $!")->throw();

    while(<FILE>) {
      chomp;

      s/["\']//g;
      push @lines, $_;
    }

   close FILE;

   $self->setLines(\@lines);
   my $sdrf = $self->getRowByHeader('^SDRF File$')->[0];
   open(FILE, $sdrf) or RAD::MR_T::MageImport::MageImportError->
     new("Could Not open File $fn for Reading: $!")->throw();

    while(<FILE>) {
      chomp;

      s/["\']//g;
      push @lines, $_;
    }

   close FILE;

  }
  else {
    open(FILE, $fn) or RAD::MR_T::MageImport::MageImportError->
      new("Could Not open File $fn for Reading: $!")->throw();

    while(<FILE>) {
      chomp;

      s/["\']//g;
      push @lines, $_;
    }
    close FILE;

  }

  return \@lines;
}

#--------------------------------------------------------------------------------

=item C<parse>

Create and return a DocRoot from a MageTab File.  Does NOT check that the file
is valid.  The file is read in as it is.  (The parsing will fail if there 
are null pointers in the file... like Using a Protocol REF without 
declaring it in the idf.)

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::DocRoot> 

=cut

sub parse {
  my ($self) = @_;

  my $docRoot = RAD::MR_T::MageImport::VO::DocRoot->new();

  my $fileLines = $self->readTabFile();
  $self->setLines($fileLines);

  my $externalDatabases = $self->parseExternalDatabases();
  $docRoot->externalDatabaseVOs($externalDatabases);

  my $affiliations = $self->parseAffiliations();
  $docRoot->affiliationVOs($affiliations);

  my $persons = $self->parsePersons($affiliations);
  $docRoot->personVOs($persons);

  my $protocols = $self->parseProtocols($externalDatabases);
  $docRoot->protocolVOs($protocols);

  my $study = $self->parseStudy($persons, $externalDatabases);
  $docRoot->studyVO($study);

  $self->parseSdrf('^Source Name', $docRoot);

  return $docRoot;
}

#--------------------------------------------------------------------------------

=item C<parseSdrf>

Creates a _BioMaterialAnnotation object (Inner Class) to simplify getting the header and lines relating
to the biomaterialAnnotation.  This method deals with a chunk of annotation which begins
with the regex parameter.  Will continually add treatments and biomaterials to the 
DocRoot.

POSSIBLE TODO:  Call this method recursivly... Any Id could be the start of another SDRF

B<Parameters:>

 $regex(string):  Pattern to match
 $bioMaterials([RAD::MR_T::MageImport::VO::BioMaterialVO]):  ArrayRef of BioMaterial VOs
 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::DocRoot> 

=cut

sub parseSdrf {
  my ($self, $regex, $docRoot) = @_;

  my $logger = $self->getLogger();

  my $lines = $self->getLines();

  my $bioMaterialAnnotation = _BioMaterialAnnotation->new($lines, $regex, $logger);

  my $headers = $bioMaterialAnnotation->getHeader();

  while($bioMaterialAnnotation->hasNextLine()) {
    my $annotationLine = $bioMaterialAnnotation->nextLine();

    $self->doBioMaterialsAndCharacteristics($headers, $annotationLine, $docRoot);
    $self->doTreatments($headers, $annotationLine, $docRoot);
    $self->doAssays($headers, $annotationLine, $docRoot);
  }

  #TODO:: parseSdrf('someOtherID', $docRoot);

  return $docRoot;
}

#--------------------------------------------------------------------------------

=item C<doAssays>

Each line of the SDRF corresponds to one acquisition.  Must check for existing
Assays from the DocRoot.

TODO:  Date, Description (Currently not in the VOs)
TODO:  Unit for Factor values... currently only concatenates value and unit

B<Parameters:>

 $headerValues(ARRAYREF):  ArrayRef of strings which correspond to the sdrf header
 $lineValues(ARRAYREF):  ArrayRef of strings which correspond to an sdrf line
 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::TreatmentVO]> ArrayRef of Treatment VO objects which have been added to the DocRoot

=cut

sub doAssays {
  my ($self, $headerValues, $lineValues, $docRoot) = @_;

  my $logger = $self->getLogger();

  my $protocols = $docRoot->getProtocolVOs();
  my $externalDatabases = $docRoot->getExternalDatabaseVOs();
  my $assays = $docRoot->getAssayVOs();
  my $persons = $docRoot->getPersonVOs();
  my $bioMaterials = $docRoot->getBioMaterialVOs();

  my $currentThing; # Can be AssayVO, AcquisitionVO, or QuantificationVO

  my ($label, $performer, $arrayDesign, @factorValues, $assay, $acquisition, $quantification, $labeledExtract, $process);

  # Read From Right to Left
  for(my $i = scalar(@$headerValues) - 1; $i >= 0; $i--) {
    my $key = $headerValues->[$i];
    my $value = $lineValues->[$i];

    next unless($value);

    if($key =~ /Labeled Extract Name/i) {
      $labeledExtract = searchObjArrayByObjName($bioMaterials, $value);
    }

    if($key =~ /ArrayDesign REF/i) {
      $arrayDesign = $value;
    }

    if($key =~ /Label\s*$/i) {
      $label = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $value, category => 'LabelCompound'});
    }

    #TODO:  Unit for Factor Values
    if($key =~ /FactorValue\s*\[(.+)\]/i) {
      my $factorName = $1;

      if($headerValues->[$i+1] =~ /Unit\s*\[(.+)\]\s*$/i) {
        my $unit = $lineValues->[$i+1];
        $value = $value . " " . $unit;
      }
      my $valueOe = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $value});
      my $factorValue = RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => $factorName, value => $valueOe});
      push(@factorValues, $factorValue);
    }

    if($key =~ /Performer/i) {
      $performer = searchObjArrayByObjName($persons, $value);
    }

    if($key =~ /Protocol REF/i) {
      my $protocol = $self->findReferencedProtocol($protocols, $value);

      # Assay to quant can only have one protocol each...(takes the right most protocol)
      if($currentThing->getProtocolName()) {
        my $name = $currentThing->getName();
        $logger->info("SKIP:  Protocol [$value] For [$name] has already been set");
      }
      else {
        $currentThing->setProtocolName($value);
      }
    }

    if($key =~ /ParameterValue\s*\[(.+)\]/i) {
      my $paramName = $1;

      # Check the next column for the unit...
      my ($category, $extDbName, $unitValue);
      if($headerValues->[$i+1] =~ /Unit\s*\[(.+)\]\s*$/i) {
        $category = $1;
        $unitValue = $lineValues->[$i+1];
      }

      my $paramValue = $self->makeParameterValue($value, $paramName,  $unitValue, $category, $extDbName, $docRoot);

      unless(UNIVERSAL::isa($currentThing, 'RAD::MR_T::MageImport::VO::AssayVO') && $currentThing->getProtocolName()) {
        $currentThing->addParameterValues($paramValue);
      }
    }

    if($key =~ /Normalization Name/i || $key =~ /DerivedArrayData File/i) {
      my $isProcess = UNIVERSAL::isa($currentThing, 'RAD::MR_T::MageImport::VO::ProcessVO');
      my $newProcess = RAD::MR_T::MageImport::VO::ProcessVO->new();

      # If NOT a process OR the current thing is a process which already has a protocol... Start with Fresh Process.
      if(!$isProcess || ($isProcess &&  $currentThing->getProtocolName())) {
        $logger->info("Overwriting Existing Process with $value") if($process);

        $currentThing = $newProcess;
        $process = $newProcess
      }
      $currentThing->setName($value) if($key =~ /Normalization Name/i);
      $currentThing->setUri($value) if($key =~ /DerivedArrayData File/i);
    }

    if($key =~ /^ArrayData File/i) {
      my $isQuantification = UNIVERSAL::isa($currentThing, 'RAD::MR_T::MageImport::VO::QuantificationVO');
      my $newQuantification = RAD::MR_T::MageImport::VO::QuantificationVO->new();

      # If NOT a quant OR the current thing is a quant which already has a protocol... Start with Fresh Quant.
      if(!$isQuantification || ($isQuantification &&  $currentThing->getProtocolName())) {
        $logger->info("Overwriting Existing Quantification with $value") if($quantification);

        $currentThing = $newQuantification;
        $quantification = $newQuantification;
      }
      $currentThing->setUri($value) if($key =~ /ArrayData File/i);
    }

    if($key =~ /Scan Name/i || $key =~ /Image File/i) {
      my $isAcquisition = UNIVERSAL::isa($currentThing, 'RAD::MR_T::MageImport::VO::AcquisitionVO');
      my $newAcquisition = RAD::MR_T::MageImport::VO::AcquisitionVO->new();

      # If NOT a acquisition OR the current thing is an acquisition which already has a protocol... Start with Fresh Acquisition.
      if(!$isAcquisition || ($isAcquisition &&  $currentThing->getProtocolName())) {
        $logger->info("Overwriting Existing Acquisition with $value") if($acquisition);

        $currentThing = $newAcquisition;
        $acquisition = $newAcquisition;
      }
      $currentThing->setName($value) if($key =~ /Scan Name/i);
      $currentThing->setUri($value) if($key =~ /Image File/i);
    }

    if($key =~ /Hybridization Name/i) {

        try {
          $assay = searchObjArrayByObjName($assays, $value);
        }
        catch  RAD::MR_T::MageImport::MageImportError with {
          $assay = RAD::MR_T::MageImport::VO::AssayVO->new({name => $value});
          $docRoot->addAssayVOs($assay);
        };

      $currentThing = $assay;
    }
  }

  $quantification->addProcesses($process) if($process);

  $acquisition->addFactorValues(@factorValues);
  $acquisition->addQuantifications($quantification);
  $acquisition->addChannels($label);

  my $studyName = $docRoot->getStudyVO()->getName();
  $assay->setArraySourceId($arrayDesign);

  try {
    searchObjArrayByObjName($assay->getAcquisitions(), $acquisition->getName());
  } catch RAD::MR_T::MageImport::MageImportError with {
    my $assayName = $assay->getName();
    my $acquisitionName = $acquisition->getName();
    $logger->debug("New Acquisition [$acquisitionName] being set to Assay [$assayName]");
    $assay->addAcquisitions($acquisition);
  };

  $assay->setStudyName($studyName);

  $performer = $docRoot->getStudyVO()->getContact()  unless($performer);
  $assay->setOperator($performer) ;

  try {
    searchObjArrayByObjName($assay->getLabeledExtracts(), $labeledExtract->getName());
  }  catch  RAD::MR_T::MageImport::MageImportError with {
    $assay->addLabeledExtracts($labeledExtract);
  };

  return $docRoot->getAssayVOs();
}

#--------------------------------------------------------------------------------

=item C<findReferencedProtocol>

If a protocol is refereneced in the sdrf it must have been declared in the idf.
This protocol tries to find it or logs an informative error.

B<Parameters:>

 $protocols([RAD::MR_T::MageImport::VO::ProtocolVO]):  ArrayRef of ProtocolVO objects
 $protocolName(string):  Name of a supposed existing protocol

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::ProtocolVO>

=cut

sub findReferencedProtocol {
  my ($self, $protocols, $protocolName) = @_;

  my $protocol;

  my $logger = $self->getLogger();

  try {
    $protocol = searchObjArrayByObjName($protocols, $protocolName);
  }
    catch  RAD::MR_T::MageImport::MageImportError with {
      $logger->fatal("Protocol REF [$protocolName] found in SDRF but missing in IDF");
      $_[0]->throw();
    };

  return $protocol;
}


#--------------------------------------------------------------------------------

=item C<makeParameterValue>

Create the Parameter value (and possibly the OntologyEntryVO for the Unit)

B<Parameters:>

 $paramValue(string): The Parmaeter Value
 $paramName(string):  Name of the ProtocolParameter
 $unitValue(string):  Optional... the name of the Unit
 $unitCategory(string):  Optional... the name of the Unit Category
 $extDbName(string):  Optional... the name of the ExternalDatabase for the Unit

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::ProtocolVO>

=cut

sub makeParameterValue {
  my ($self, $paramValue, $paramName, $unitValue, $unitCategory, $extDbName, $docRoot) = @_;

  my $externalDatabases = $docRoot->getExternalDatabaseVOs();
  my $protocols = $docRoot->getProtocolVOs();

  if($unitValue) {
    my $extDb = $extDbName ? searchObjArrayByObjName($externalDatabases, $extDbName) : undef;

    foreach my $protocol (@$protocols) {
      foreach my $param (@{$protocol->getParams()}) {

        if($param->getName() eq $paramName) {
            my $unitOe = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $unitValue,
                                                                          category => $unitCategory,
                                                                          externalDatabase => $extDb,
                                                                         });
            $param->setUnitType($unitOe);
        }
      }
    }
  }

  my $paramValueVo = RAD::MR_T::MageImport::VO::ParameterValueVO->new({parameterName => $paramName,
                                                                       value => $paramValue,
                                                                      });
  return $paramValueVo;
}


#--------------------------------------------------------------------------------

=item C<doTreatments>

Reads the SDRF...  Calls a method which creates (or finds) the treatment.
Determines which protocols and parameter values are associated with that treatment. 

B<Parameters:>

 $headerValues(ARRAYREF):  ArrayRef of strings which correspond to the sdrf header
 $lineValues(ARRAYREF):  ArrayRef of strings which correspond to an sdrf line
 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::TreatmentVO]> ArrayRef of Treatment VO objects which have been added to the DocRoot

=cut

sub doTreatments {
  my ($self, $headerValues, $lineValues, $docRoot) = @_;

  my $logger = $self->getLogger();

  my $bioMaterials = $docRoot->getBioMaterialVOs();
  my $protocols = $docRoot->getProtocolVOs();
  my $externalDatabases = $docRoot->getExternalDatabaseVOs();

  my (@paramValues, @treatmentProtocols, $currentOutputBioMaterial);

  my $allowed_rx = "Source Name|Sample Name|Extract Name|Labeled Extract Name";

  # Read From Right to Left
  for(my $i = scalar(@$headerValues) - 1; $i >= 0; $i--) {
    my $key = $headerValues->[$i];
    my $value = $lineValues->[$i];

    next unless($value);

    if($key =~ /$allowed_rx/i) {
      my $newBioMaterial = searchObjArrayByObjName($bioMaterials, $value);

      if($currentOutputBioMaterial) {
        $self->makeTreatments(\@treatmentProtocols, \@paramValues, $currentOutputBioMaterial, $newBioMaterial, $docRoot);
      }
      # Reset for the next treatment...
      $currentOutputBioMaterial = $newBioMaterial;
      @paramValues = ();
      @treatmentProtocols = ();
    }

    if($key =~ /Protocol REF/i) {
      my $prot = $self->findReferencedProtocol($protocols, $value);
      unshift(@treatmentProtocols, $prot); # Don't push because order matters
    }

    if($key =~ /ParameterValue \[(.+)\]/i) {
      my $paramName = $1;

      # Check the next column for the unit...
      my ($category, $extDbName, $unitValue);
      if($headerValues->[$i+1] =~ /Unit\s*\[(.+)\]\s*$/i) {
        $category = $1;
        $unitValue = $lineValues->[$i+1];
      }

      my $paramValue = $self->makeParameterValue($value, $paramName,  $unitValue, $category, $extDbName, $docRoot);
      push(@paramValues, $paramValue);
    }

  }
  return $docRoot->getTreatmentVOs();
}

#--------------------------------------------------------------------------------

=item C<makeTreatments>

Grunt work for creating a TreatmentVO.  Will also make the BioMaterialMeasurementVO from the input
BioMaterial.  If there is an existing treatment with the same OutputBM and Same Protocol...(ie pooling)
then just add inputBMM to the existing treatment.  Will add the newly created TreatmentVO to the DocRoot.

# TODO:  value and unit for BioMaterialMeasurementVO??

B<Parameters:>

 $protocols([RAD::MR_T::MageImport::VO::ProtocolVO]):  ArrayRef of Protocol VO objects
 $paramValues([RAD::MR_T::MageImport::VO::ParameterValueVO]):  ArrayRef of ParameterValue VO objects
 $outBioMaterial(RAD::MR_T::MageImport::VO::BioMaterialVO):  BioMaterialVO object which is the Input to the treatment
 $inBioMaterial(RAD::MR_T::MageImport::VO::BioMaterialVO):  BioMaterialVO which will be made into a BioMaterialMeasuremnt
 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::TreatmentVO>

=cut

sub makeTreatments {
  my ($self, $protocols, $paramValues, $outBioMaterial, $inBioMaterial, $docRoot) = @_;

  my $logger = $self->getLogger();

  my $treatments = $docRoot->getTreatmentVOs();

  for(my $i = 0; $i < scalar(@$protocols); $i++) {
    my $protocol = $protocols->[$i];
    my $orderNum = $i + 1;


    my $bioMaterialMeasurement = RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({bioMaterial => $inBioMaterial});

    if(my $treatment = $self->findExistingTreatment($treatments, $outBioMaterial, $protocol)) {
      my $existingBmms = $treatment->getInputBMMs();

      if($self->okToAddBmm($existingBmms, $inBioMaterial)) {
        $treatment->addInputBMMs($bioMaterialMeasurement);
      }
    }
    else {
      my $treatmentTypeName = $protocol->getProtocolType()->getValue();
      my $treatmentName = $treatmentTypeName . " - " .  $outBioMaterial->getName();

      my $treatmentType = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $treatmentTypeName,
                                                                           category => 'ComplexAction',
                                                                           externalDatabase => $protocol->getProtocolType()->getExternalDatabase(),
                                                                          });

      my $newTreatment = RAD::MR_T::MageImport::VO::TreatmentVO->new({orderNum => $orderNum,
                                                                      outputBM => $outBioMaterial,
                                                                      inputBMMs => [$bioMaterialMeasurement],
                                                                      protocol => $protocol,
                                                                      name => $treatmentName,
                                                                      treatmentType => $treatmentType,
                                                                     });

      if(my $protocolParams = $protocol->getParams()) {
        foreach my $param (@$protocolParams) {
          my $paramName = $param->getName();

          $logger->debug(Dumper $param);

          foreach my $paramValue (@$paramValues) {
            $logger->debug(Dumper $paramValue);
            if($paramValue->getParameterName() eq $paramName) {
              $newTreatment->addParameterValues($paramValue);
            }
          }
        }
      }

      $docRoot->addTreatmentVOs($newTreatment);
    }
  }
  return $docRoot->getTreatmentVOs();
}


#--------------------------------------------------------------------------------

sub okToAddBmm {
  my ($self, $existingBmms, $inBioMaterial) = @_;

  my @bioMaterials = map { $_->getBioMaterial() } @$existingBmms;

  try {
    my $newBioMaterial = searchObjArrayByObjName(\@bioMaterials, $inBioMaterial->getName());
    return 0;
  } catch RAD::MR_T::MageImport::MageImportError with {
    return 1;
  };
}

#--------------------------------------------------------------------------------

=item C<findExistingTreatment>

Loop through an arrayref of treatmentVOs and find based on protocol and output biomaterial.
A treatment is existing if it has the same protocol and the same output biomaterial

B<Parameters:>

 $treatments([RAD::MR_T::MageImport::VO::TreatmentVO]):  ArrayRef of Treatment VO objects
 $outBioMaterial(RAD::MR_T::MageImport::VO::BioMaterialVO):  BioMaterialVO object which is the Input to the treatment
 $protocol(RAD::MR_T::MageImport::VO::ProtocolVO):  ProtocolVO Object

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::TreatmentVO>: TreatmentVO if it was found

=cut


sub findExistingTreatment {
  my ($self, $treatments, $outBioMaterial, $protocol) = @_;

  foreach my $t (@$treatments) {
    if($t->getProtocol->getName eq $protocol->getName && $t->getOutputBM->getName eq $outBioMaterial->getName) {
      return $t;
    }
  }
}

#--------------------------------------------------------------------------------

=item C<doBioMaterialsAndCharacteristics>

Grunt work for looping through the sdrf and pulling out biomaterials.  MaterialType
and Characteristics are assigned to the last BioMaterial (read left to right).  The code 
only worries about one biomaterial at a time (the current biomaterial).  Once it is 
decided which bioMaterial is the "current BIomaterial" all the Characteristics are 
assigned to it.  

B<Parameters:>

 $headerValues(ArrayRef):  ArrayRef of Strings corresponding to the header
 $lineValues(ArrayRef):  ArrayRef of Strings corresponding to the values associated with the header
 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::BioMaterialVO]> ArrayRef of BioMaterial VOs

=cut

sub doBioMaterialsAndCharacteristics {
  my ($self, $headerValues, $lineValues, $docRoot) = @_;

  my $logger = $self->getLogger();

  my $currentBioMaterial;
  my $externalDatabaseVos = $docRoot->getExternalDatabaseVOs();
  my $personVos = $docRoot->getPersonVOs();

  my $allowed_rx = "Source Name|Sample Name|Extract Name|Labeled Extract Name";

  for(my $i = 0; $i < scalar(@$headerValues); $i++) {
    my $key = $headerValues->[$i];
    my $value = $lineValues->[$i];

    next unless($value);

    # Find or Create the currentBioMaterial
    if($key =~ /$allowed_rx/i) {
      $key =~ s/ |Name//gi;  # Remove spaces and "ID"
      $currentBioMaterial = $self->makeBioMaterial($docRoot, $key, $value);
    }

    if($key =~ /^MaterialType\s*$/i) {
      my $typeOe = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $value, category => $key});
      $currentBioMaterial->setBioMaterialType($typeOe);
    }

    if($key =~ /Characteristics\s*\[(.+)\]\s*$/i) {
      my $category = $1;
      my $characteristic = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $value, category => $category});

      unless($self->isOntologyEntryIncluded($characteristic, $currentBioMaterial->getBioMaterialChars())) {
        $currentBioMaterial->addBioMaterialChars($characteristic);
      }

      if($headerValues->[$i+1] =~ /Term Source REF/) {
        my $termSource = $lineValues->[$i+1];

        try {
          my $extDb = searchObjArrayByObjName($externalDatabaseVos, $termSource);
          $characteristic->setExternalDatabase($extDb);
        } catch RAD::MR_T::MageImport::MageImportError with {
          my $e = shift;
          $logger->fatal($e->text());
        };
      }
    }

    if($key =~ /Term Source REF/i && $headerValues->[$i-1] =~ /MaterialType/) {

      try {
        my $extDb = searchObjArrayByObjName($externalDatabaseVos, $value);
        my $type = $currentBioMaterial->getBioMaterialType();
        $type->setExternalDatabase($extDb);
      } catch RAD::MR_T::MageImport::MageImportError with {
        my $e = shift;
        $logger->fatal($e->text());
      };
    }

    if($key =~ /Label/i) {
      my $channel = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $value, category => 'LabelCompound'});
      $currentBioMaterial->setChannel($channel);
    }

    if($key =~ /Provider/i) {
      my $provider = searchObjArrayByObjName($personVos, $value);
      $currentBioMaterial->setProvider($provider);
    }

    if($key =~ /Description/i) {
      $currentBioMaterial->setDescription($value);
    }

  }
  return $docRoot->getBioMaterialVOs();
}

#--------------------------------------------------------------------------------

=item C<makeBioMaterial>

Try to find the biomaterial from existing (from name) or create a new one.

B<Parameters:>

 $docRoot(RAD::MR_T::MageImport::VO::DocRoot):  DocRoot Object
 $key(string):  Header from the sdrf which will become the subclass view
 $value(string):  Value from the  sdrf which will become the name

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::BioMaterialVO> BioMaterial VO

=cut

sub makeBioMaterial {
  my ($self, $docRoot, $key, $value) = @_;

  my $logger = $self->getLogger();

  my $bioMaterial;

  try {
    $bioMaterial = searchObjArrayByObjName($docRoot->getBioMaterialVOs(), $value);
  }
  catch RAD::MR_T::MageImport::MageImportError with {
    my $e = shift;
    $logger->info($e->text());

    $bioMaterial = RAD::MR_T::MageImport::VO::BioMaterialVO->new({name => $value,
                                                                  subclassView => $key
                                                                 });
    $docRoot->addBioMaterialVOs($bioMaterial);
    $logger->debug("Create Biomaterial: $value");
  };

  return $bioMaterial;
}

#--------------------------------------------------------------------------------

=item C<parseStudy>

Create the VO Study object.  Find the submitter from the list of Persons and sets 
as the StudyContact.  

B<Parameters:>

 $persons([RAD::MR_T::MageImport::VO::PersonVO]):  ArrayRef of Person VOs
 $externalDatabaseVos([RAD::MR_T::MageImport::VO::ExternalDatabaseVO]):  ArrayRef of ExternalDatabaseVos

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::StudyVO> Study VO 

=cut

sub parseStudy {
  my ($self, $persons, $externalDatabaseVos) = @_;

  my $logger = $self->getLogger();

  my $title = $self->getRowByHeader('^Investigation Title$')->[0];
  $logger->debug("Study Title: $title");

  my $desc = $self->getRowByHeader('^Experiment Description$')->[0];
  $logger->debug("Study Description:  $desc");

  my $pubmedId = $self->getRowByHeader('^PubMed ID$')->[0];
  $logger->debug("Main Publication:  $pubmedId");

  my $studyVo = RAD::MR_T::MageImport::VO::StudyVO->new({name => $title,
                                                         description => $desc,
                                                         pubMedId => $pubmedId,
                                                        });
  my $submitter;
  foreach my $person (@$persons) {
    if($person->getRole() eq 'submitter') {
      $studyVo->setContact($person);
    }
  }

  my $factors = $self->parseStudyFactors($externalDatabaseVos);
  my $design = $self->parseStudyDesign($factors);

  $studyVo->setDesigns([$design]);

  return $studyVo;
}

#--------------------------------------------------------------------------------

=item C<parseStudyDesign>

Because of the current implementation of MageTab... only one Study Design is 
parsed.  Creates the OntologyEntryVOs and set as the types.  The OntologyEntrys
are created having only values.

B<Parameters:>

 $studyFactorVos([RAD::MR_T::MageImport::VO::StudyFactorVO]):  ArrayRef of StudyFactor VOs

B<Return Type:> 

 C<RAD::MR_T::MageImport::VO::StudyDesignVO> StudyDesign Vo object

=cut

sub parseStudyDesign {
  my ($self, $studyFactorVos) = @_;

  my $logger = $self->getLogger();

  my $designs = $self->getRowByHeader('^Experimental Designs');

  my @types;

  foreach my $design (@$designs) {
    $logger->debug("Study Design:  $design");

    my $typeOeVo = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $design});
    push(@types, $typeOeVo);
  }

  my @typesForString = map { $_->getValue() } @types;
  my $nameString = join(', ', @typesForString);

  return RAD::MR_T::MageImport::VO::StudyDesignVO->new({factors => $studyFactorVos,
                                                        types => \@types,
                                                        name => $nameString,
                                                       });
}

#--------------------------------------------------------------------------------

=item C<parseStudyFactors>

Creates a Study Factor VO for each that is named.  Creates the OntologyEntryVo for 
the types (loggs any missing term sources).

B<Parameters:>

 $externalDatabaseVos([RAD::MR_T::MageImport::VO::ExternalDatabaseVO]):  ArrayRef of ExternalDatabase VOs

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::StudyFactorVO]> ArrayRef of VO StudyFactors

=cut


sub parseStudyFactors {
  my ($self, $externalDatabaseVos) = @_;

  my $logger = $self->getLogger();

  my @studyFactors;

  my $names = $self->getRowByHeader('^Experimental Factor Name');
  my $types = $self->getRowByHeader('^Experimental Factor Type');
  my $sources = $self->getRowByHeader('^Experimental Factor Type Term Source REF');

  for(my $i = 0; $i < scalar(@$names); $i++) {
    my $name = $names->[$i];
    my $type = $types->[$i];
    my $termSource = $sources->[$i];

    my $typeOeVo = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $type});

    if($termSource) {

      try {
        my $extDb = searchObjArrayByObjName($externalDatabaseVos, $termSource);
        $typeOeVo->setExternalDatabase($extDb);
      } catch RAD::MR_T::MageImport::MageImportError with {
        my $e = shift;
        $logger->fatal($e->text());
      };
    }

    my $studyFactor = RAD::MR_T::MageImport::VO::StudyFactorVO->new({name => $name,
                                                                     type => $typeOeVo,
                                                                    });

    push(@studyFactors, $studyFactor);
  }

  return \@studyFactors;
}


#--------------------------------------------------------------------------------

=item C<parsePersons>

Get the rows corresponding to the Person Info.  Returns a list of PersonVO objects

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::PersonVO]> ArrayRef of PersonVO objects

=cut

sub parsePersons {
  my ($self, $affiliations) = @_;

  my $logger = $self->getLogger();

  my @persons;

  my $personLastNames = $self->getRowByHeader('^Person Last Name$');
  my $personFirstNames = $self->getRowByHeader('^Person First Name$');
  my $personEmails = $self->getRowByHeader('^Person Email$');
  my $personPhones = $self->getRowByHeader('^Person Phone$');
  my $personAddresss = $self->getRowByHeader('^Person Address$');
  my $personRoles = $self->getRowByHeader('^Person Roles$');
  my $personAffiliations = $self->getRowByHeader('^Person Affiliation$');

  for(my $i = 0; $i < scalar(@$personLastNames); $i++) {
    my $last = $personLastNames->[$i];
    my $first = $personFirstNames->[$i];
    my $email = $personEmails->[$i];
    my $address = $personAddresss->[$i];
    my $role = $personRoles->[$i];
    my $phone = $personPhones->[$i];

    my $name = "$first $last";

    my $affiliation = $personAffiliations->[$i];

    my $affiliationVo;
    $affiliationVo = searchObjArrayByObjName($affiliations, $affiliation) if($affiliation);

    my $person =  RAD::MR_T::MageImport::VO::PersonVO->new({first => $first,
                                                            last => $last,
                                                            email => $email,
                                                            address => $address,
                                                            role => $role,
                                                            affiliation => $affiliationVo,
                                                            phone => $phone,
                                                            name => $name,
                                                           });
    push(@persons, $person);
  }
  return \@persons;
}


#--------------------------------------------------------------------------------

=item C<parseAffiliations>

Get the rows corresponding to the Affiliation Info. Will just Dump this into the 
Name attribute of AffiliationVO

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::AffiliationVO]> ArrayRef of AffiliationVO objects

=cut

sub parseAffiliations {
  my ($self) = @_;

  my $logger = $self->getLogger();

  my @affiliations;

  my $personAffiliations = $self->getRowByHeader('^Person Affiliation$');

  my %seen;
  foreach my $affiliation (@$personAffiliations) {
    unless($seen{$affiliation}) {
      my $affiliationVo = RAD::MR_T::MageImport::VO::AffiliationVO->new({name => $affiliation});
      push(@affiliations, $affiliationVo);
    }
    $seen{$affiliation} = 1;
  }
  return \@affiliations;
}



#--------------------------------------------------------------------------------

=item C<parseExternalDatabases>

Get the rows corresponding to the ExternalDatabases (Term Source).

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::ExternalDatabaseVO]> ArrayRef of ExternalDatabaseVO objects

=cut

sub parseExternalDatabases {
  my ($self) = @_;

  my $logger = $self->getLogger();

  my @externalDatabaseVos;

  my $termSources = $self->getRowByHeader('^Term Source Name$');
  my $termSourceVersions = $self->getRowByHeader('^Term Source version');
  my $termSourceUris = $self->getRowByHeader('^Term Source File');

  return unless($termSources);

  for(my $i = 0; $i < scalar(@$termSources); $i++) {
    my $term = $termSources->[$i];
    my $version = $termSourceVersions->[$i];
    my $uri = $termSourceUris->[$i];

    my $externalDatabase = RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new({name => $term,
                                                                               version => $version,
                                                                               uri => $uri,
                                                                              });
    $logger->debug("ExternalDatabase:  " . Dumper($externalDatabase));

    push(@externalDatabaseVos, $externalDatabase);
  }
  return \@externalDatabaseVos;
}


#--------------------------------------------------------------------------------

=item C<parseProtocols>

Get the rows corresponding to the Protocols.  Will make the ProtocolTypes and the 
Paramaters.  If the Term Source ExternalDatabase is not found... a warning message
is logged.  

B<Parameters:>

 $externalDatabaseVos([RAD::MR_T::MageImport::VO::ExternalDatabaseVO]):  ArrayRef of ExternalDatabaseVos

B<Return Type:> 

 C<[RAD::MR_T::MageImport::VO::ProtocolVO]> ArrayRef of ProtocolVO objects

=cut

sub parseProtocols {
  my ($self, $externalDatabaseVos) = @_;

  my $logger = $self->getLogger();

  my @protocolVos;

  my $protocolNames = $self->getRowByHeader('^Protocol Name');
  my $protocolDescriptions = $self->getRowByHeader('^Protocol Description');
  my $protocolParameters = $self->getRowByHeader('^Protocol Parameters');
  my $protocolTypes = $self->getRowByHeader('^Protocol Type$');
  my $protocolTermSources = $self->getRowByHeader('^Protocol Type Term');

  for(my $i = 0; $i < scalar(@$protocolNames); $i++) {
    my $name = $protocolNames->[$i];
    my $desc = $protocolDescriptions->[$i];
    my $type = $protocolTypes->[$i];
    my $termSource = $protocolTermSources->[$i];

    my $typeOeVo = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => $type,
                                                                   category => 'ExperimentalProtocolType'
                                                                   });

    if($termSource) {

      try {
        my $extDb = searchObjArrayByObjName($externalDatabaseVos, $termSource);
        $typeOeVo->setExternalDatabase($extDb);
      } catch RAD::MR_T::MageImport::MageImportError with {
        my $e = shift;
        $logger->fatal($e->text());
      };

    }

    my @params = split(';', $protocolParameters->[$i]);
    my @protocolParamVos;

    foreach my $param (@params) {
      $param =~ s/^\s//g;
      next unless($param);

      my $paramVo = RAD::MR_T::MageImport::VO::ProtocolParamVO->new({name => $param});
      push(@protocolParamVos, $paramVo);
    }

    my $protocol = RAD::MR_T::MageImport::VO::ProtocolVO->
      new({name => $name,
           protocolDescription => $desc,
           params => \@protocolParamVos,
           protocolType => $typeOeVo,
          });

    push(@protocolVos, $protocol);
  }
  return \@protocolVos;
}

#--------------------------------------------------------------------------------

=item C<getRowByHeader>

Finds the first line from the file corresponding to a regular expression.  Split the 
line on the tabs and returns all rows AFTER the row header

B<Parameters:>

 $rowHeaderRegex(string):  match this string

B<Return Type:> 

 C<ARRAYREF> ArrayRef of Strings 

=cut

sub getRowByHeader {
  my ($self, $rowHeaderRegex) = @_;

  my $lines = $self->getLines();

  foreach my $line (@$lines) {
    my ($name, @rest) = split(/\t/, $line);

    if($name =~ /$rowHeaderRegex/i) {
      return \@rest;
    }
  }
  return [];
}

#--------------------------------------------------------------------------------

sub isOntologyEntryIncluded {
  my ($self, $oe, $ar) = @_;

  foreach(@$ar) {
    return 1 if($oe->getValue() eq $_->getValue() && $oe->getCategory() eq $_->getCategory());
  }
  return 0;
}

#================================================================================
#BEGIN INNER CLASS
package _BioMaterialAnnotation;

use RAD::MR_T::MageImport::MageImportError;
use strict;

#--------------------------------------------------------------------------------

sub new {
  my ($class, $fileLines, $regex, $logger) = @_;

  my ($start, @lines);
  foreach my $line (@$fileLines) {
    $start = 1 if($line =~ /$regex/i);
    next unless($start);
    unless($line) {
      last;
    }
    else {
      my @splitLine = split(/\t/, $line);

      # Trim trailing whitespace
      @splitLine = map {$_ =~ s/\s*$//; $_;} @splitLine;
      push(@lines, \@splitLine);
    }
  }

  unless($start) {
    RAD::MR_T::MageImport::ReaderException::FileError->
        new("Expected ROW header [$regex] not found in file")->throw();
  }

  my $header = shift(@lines);
  checkSdrfHeader($header);

  bless( {_lines => \@lines,
          _regex => $regex,
          _header => $header,
          _currentLine => 0,
          _logger => $logger,
         }, $class);
}

#--------------------------------------------------------------------------------

sub getLines {$_[0]->{_lines}}
sub getHeader {$_[0]->{_header}}
sub getRegex {$_[0]->{_regex}}
sub getLogger {$_[0]->{_logger}}

sub getCurrentLine {$_[0]->{_currentLine}}
sub incrementCurrentLine {$_[0]->{_currentLine}++}

#--------------------------------------------------------------------------------

sub hasNextLine {
  my ($self) = @_;

  my $i = $self->getCurrentLine();
  my $lines = $self->getLines();

  if($lines->[$i]) {
    return 1;
  }
  return 0;
}

#--------------------------------------------------------------------------------

sub nextLine {
  my ($self) = @_;

  my $logger = $self->getLogger();

  my $i = $self->getCurrentLine();
  my $lines = $self->getLines();
  my $header = $self->getHeader();

  $self->incrementCurrentLine();

  my $thisLine = $lines->[$i];

  unless(scalar(@$header) == scalar(@$thisLine)) {
    $logger->fatal("\nHEADER=", join('|', @$header), "\nLINE=", join('|', @$thisLine));

    RAD::MR_T::MageImport::ReaderException::FileError->
        new("Number of Header lines not equal to BioMaterialAnnotation Lines")->throw();
  }

  return $thisLine
}

#--------------------------------------------------------------------------------

sub checkSdrfHeader {
  my ($header) = @_;

  my @allowed = ('Comment\s?\[.+\]',
		 'term\s+source\s+ref',
		 'source\s+name',
                 'sample\s+name',
                 'extract\s+name',
                 'labeledextract\s+name',
                 'hybridization\s+name',
                 'scan\s+name',
                 'normalization\s+name',
                 'label',
                 'material\s?type',
                 'characteristics\s*\[.+\]',
                 'factor\s?value\s*\[.+\]',
                 'parameter\s?value\s*\[.+\]',
                 'unit\s*\[.+\]',
                 'protocol\s+ref',
                 'array\s?design\s+ref',
                 'array\s?data\s+file',
                 'derived\s?arraydata\s+file',
                 'image\s+file',
                 'date',
                 'description',
                 'performer',
                 'provider',
		 'image file',
                );

  foreach my $col (@$header) {
    unless(isInArray(\@allowed, $col)) {
      RAD::MR_T::MageImport::ReaderException::FileError->
        new("Unrecognized Header [$col] not supported by MageTab Reader.")->throw();
    }
  }
}

#--------------------------------------------------------------------------------

sub isInArray {
  my ($ar, $val) = @_;

  foreach(@$ar) {
    return 1 if($val =~ /$_/i);
  }
  return 0;
}


#================================================================================
# END INNER CLASS
package RAD::MR_T::MageImport::Service::Reader::MageTabReader;

1;
