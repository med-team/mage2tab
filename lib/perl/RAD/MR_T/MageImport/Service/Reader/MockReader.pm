package RAD::MR_T::MageImport::Service::Reader::MockReader;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::Service::AbstractReader;
use RAD::MR_T::MageImport::VO::DocRoot;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractReader/;

sub parse  {
  my $self = shift;
  my $doc = RAD::MR_T::MageImport::VO::DocRoot->new();

  $self->setProtocolVOs($doc);
  $self->setExtDBVOs($doc);

  my $studyContact = $self->setPersonAndAffiliationVOs($doc);
  $self->setStudyVO($doc, $studyContact);

  $self->setBioMatAndTreatmentVOs($doc, $studyContact);

  $self->setAssayToQuantVOs($doc);

  return $doc;

}

sub setAssayToQuantVOs {
  my ($self, $doc) = @_;

  my $process1 =  RAD::MR_T::MageImport::VO::ProcessVO->new({name => 'process_1',
                                                             uri => 'process_1.uri',
                                                            });

  my $process2 =  RAD::MR_T::MageImport::VO::ProcessVO->new({name => 'process_2',
                                                             uri => 'process_2.uri',
                                                            });

  my $process3 =  RAD::MR_T::MageImport::VO::ProcessVO->new({name => 'process_3',
                                                             uri => 'process_3.uri',
                                                             targets => [$process1, $process2],
                                                            });


  my $quantification1 = RAD::MR_T::MageImport::VO::QuantificationVO->new({name => 'quantification_1',
                                                                          uri => 'quantification_1.uri',
                                                                          processes => [$process1, $process2],
                                                                         });

  my $quantification2 = RAD::MR_T::MageImport::VO::QuantificationVO->new({name => 'quantification_2',
                                                                          uri => 'quantification_2.uri',
                                                                          processes => [$process3],
                                                                         });

  my $quantification3 = RAD::MR_T::MageImport::VO::QuantificationVO->new({name => 'quantification_3',
                                                                          uri => 'quantification_3.uri',
                                                                          protocolName => 'protocol_labeling',
                                                                          processes => [$process3],
                                                                          parameterValues => [ RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'quantification_param_value_1' , parameterName => 'protocol_param_1' }),
                                                                                               RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'quantification_param_value_2' , parameterName => 'protocol_param_2' }),
                                                                                             ],
                                                                         });

  my $quantification4 = RAD::MR_T::MageImport::VO::QuantificationVO->new({name => 'quantification_4',
                                                                          uri => 'quantification_4.uri',
                                                                         });

  my $quantifications1 = [$quantification1, $quantification3];
  my $quantifications2 = [$quantification2, $quantification4];

  my $factorValues1 = [ RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_1',
                                                                      value => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'studyFactorValue_Cy3_1', category => ''}),
                                                                      }),
                       RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_2',
                                                                      value => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'studyFactorValue_Cy3_2', category => ''}),
                                                                      }),
                     ];

  my $factorValues2 = [ RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_1',
                                                                      value => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'studyFactorValue_Cy5_1', category => ''}),
                                                                      }),
                       RAD::MR_T::MageImport::VO::FactorValueVO->new({factorName => 'studyFactor_2',
                                                                      value => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'studyFactorValue_Cy5_2', category => ''}),
                                                                      }),
                     ];


  my $acquisition1 = RAD::MR_T::MageImport::VO::AcquisitionVO->new({name => 'acquisition_1',
                                                                    uri => 'acquisition_1.uri',
                                                                    channels => [RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Cy3', category => 'LabelCompound'}),
                                                                                 RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Cy5', category => 'LabelCompound'})],
                                                                    quantifications => $quantifications1,
                                                                    factorValues => $factorValues1,
                                                                    protocolName => 'protocol_labeling',
                                                                    parameterValues => [ RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'acquisition_param_value_1' , parameterName => 'protocol_param_1' }),
                                                                                         RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'acquisition_param_value_2' , parameterName => 'protocol_param_2' }),
                                                                                       ],
                                                                   });

  my $acquisition2 = RAD::MR_T::MageImport::VO::AcquisitionVO->new({name => 'acquisition_2',
                                                                    uri => 'acquisition_2.uri',
                                                                    channels => [RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Cy5', category => 'LabelCompound'}),
                                                                                 RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value => 'Cy3', category => 'LabelCompound'})],
                                                                    quantifications => $quantifications2,
                                                                    factorValues => $factorValues2,
                                                                   });

  my $acquisitions1 = [$acquisition1];
  my $acquisitions2 = [$acquisition2];

  my $persons = $doc->getPersonVOs();
  my $lex = $doc->getBioMaterialVOs()->[2];

  my $assay1 = RAD::MR_T::MageImport::VO::AssayVO->new({name => 'assay_1' , 
                                                        studyName => 'study', 
                                                        acquisitions => $acquisitions1,
                                                        arraySourceId => 'A-AFFY-13',
                                                        operator => $persons->[0],
                                                        protocolName => 'protocol_extraction',
                                                        labeledExtracts => [$lex],
                                                       });

  my $assay2 = RAD::MR_T::MageImport::VO::AssayVO->new({name => 'assay_2' , 
                                                        studyName => 'study', 
                                                        acquisitions => $acquisitions2,
                                                        arraySourceId => 'A-AFFY-14',
                                                        operator => $persons->[1],
                                                        protocolName => 'protocol_labeling',
                                                        labeledExtracts => [$lex],
                                                        parameterValues => [ RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'assay_param_value_1' , parameterName => 'protocol_param_1' }),
                                                                             RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'assay_param_value_2' , parameterName => 'protocol_param_2' }),
                                                                           ],
                                                       });

  $doc->assayVOs([$assay1,$assay2]);

}

sub setBioMatAndTreatmentVOs{
  my $class = shift;
  my $doc = shift;
  my $contact = shift;
 
  my $biosource = 
    RAD::MR_T::MageImport::VO::BioMaterialVO->new({
						   name=>"bioSource",
						   description=>"bioSource_description", 
						   subclassView=>"BioSource", 
						   bioMaterialType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"whole_organism", category=>"MaterialType"}),
                                                   provider => $contact,
                                                   bioMaterialChars =>  [ RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"NOD", category=>"StrainOrLine"}),
                                                                          RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"female", category=>"Sex"}),
                                                                        ]
						  }); 

  my $biosample = 
    RAD::MR_T::MageImport::VO::BioMaterialVO->new({
						   name=>"bioSample",
						   description=>"bioSample_description", 
						   subclassView=>"BioSample", 
						   bioMaterialType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"total_RNA", category=>"MaterialType"})
						  }); 

  my $protocols = $doc->getProtocolVOs();
  my $lp = $protocols->[0]; # This one has 2 parameters

 # my $lp = RAD::MR_T::MageImport::VO::ProtocolVO->new({
#						       name=>"protocol_labeling", 
#						       protocolDescription=>"protocol_labeling_description",
#						       uri=>"protocol_labeling_uri", 
#						       protocolType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"labeling", category=>"ExperimentalProtocolType"}),
#						      });

  my $lex = 
    RAD::MR_T::MageImport::VO::BioMaterialVO->new({
						   name=>"labeledExtract",
						   description=>"labeledExtract_description", 
						   subclassView=>"LabeledExtract", 
						   channel=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"Cy3", category=>"LabelCompound"}),
						   bioMaterialType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"DNA", category=>"MaterialType"})
						  }); 

  $doc->bioMaterialVOs([$biosource, $biosample, $lex]);

  my $bm1 = 
    RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({
							      value=>"10", 
							      bioMaterial=>$biosource, 
							      unitType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"cc", category=>"VolumeUnit"})}
							    ); 


  my $lp1 =
    RAD::MR_T::MageImport::VO::ProtocolVO->new({
						name=>"protocol_extraction", 
						protocolDescription=>"protocol_extraction_description",
						uri=>"protocol_extraction_uri", 
						protocolType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"extraction", category=>"ExperimentalProtocolType"}),
					       });

 my $bm2 = 
    RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO->new({
							      value=>"10", 
							      bioMaterial=>$biosample, 
							      unitType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"ml", category=>"VolumeUnit"})}
							    ); 

 my $t0 = 
    RAD::MR_T::MageImport::VO::TreatmentVO->new({
						 name=>"treatment_pool", 
						 orderNum=> 1, 
						 outputBM=>$biosample,
						 inputBMMs=>[$bm1, $bm2],
						 protocol=>$lp1,
						 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"pool", category=>"ExperimentalProtocolType"}),
						});

 my $t1 = 
    RAD::MR_T::MageImport::VO::TreatmentVO->new({
						 name=>"treatment_grow", 
						 orderNum=> 2, 
						 outputBM=>$biosample,
						 inputBMMs=>[$bm1],
						 protocol=>$lp1,
						 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"grow", category=>"ExperimentalProtocolType"}),
						});

  my $t2 = 
    RAD::MR_T::MageImport::VO::TreatmentVO->new({
						 name=>"treatment_extraction", 
						 orderNum=> 1, 
						 outputBM=>$biosample,
						 inputBMMs=>[$bm1],
						 protocol=>$lp1,
						 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"nucleic_acid_extraction", category=>"ExperimentalProtocolType"}),
						});

  # THis on has the parameters...
  my $t3 = 
    RAD::MR_T::MageImport::VO::TreatmentVO->new({
						 name=>"treatment_labeling", 
						 orderNum=>1, 
						 outputBM=>$lex,
						 inputBMMs=>[$bm2],
						 protocol=>$lp,
						 treatmentType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"labeling", category=>"ExperimentalProtocolType"}),
                                                 parameterValues => [ RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'treatment_param_value_1' , parameterName => 'protocol_param_1' }),
                                                                      RAD::MR_T::MageImport::VO::ParameterValueVO->new({value => 'treatment_param_value_2' , parameterName => 'protocol_param_2' }),
                                                                    ],
						});

  $doc->treatmentVOs([$t0, $t1, $t2, $t3]);

}

sub setPersonAndAffiliationVOs{
  my $class = shift;
  my $doc = shift;

  my $af1 = RAD::MR_T::MageImport::VO::AffiliationVO->new({name=>"affiliation_1"});
  my $af2 = RAD::MR_T::MageImport::VO::AffiliationVO->new({name=>"affiliation_2"});
  my $p1 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"person_1",
					      first=>"person_1_first",
					      last=>"person_1_last",
					      address=>"person_1_address",
					      email=>"person_1_email",
					      affiliation=>$af1
					     });

  my $p2 = 
    RAD::MR_T::MageImport::VO::PersonVO->new({
					      name=>"person_2",
					      first=>"person_2_first",
					      last=>"person_2_last",
					      address=>"person_2_address",
					      email=>"person_2_email",
					      affiliation=>$af2
					     });


  $doc->personVOs([$p1, $p2]);
  $doc->affiliationVOs([$af1, $af2]);

  return($p2)
}

sub setExtDBVOs{
  my $class = shift;
  my $doc = shift;
  my $db1 = RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new({name=>"Entrez gene", version=>"2005-10-13"});
  my $db2 = RAD::MR_T::MageImport::VO::ExternalDatabaseVO->new({name=>"NCBI RefSeq", version=>"2005-10-13"});

  $doc->externalDatabaseVOs([$db1, $db2]);

}

sub setProtocolVOs {
  my $class = shift;
  my $doc = shift;

  my $param1 =  RAD::MR_T::MageImport::VO::ProtocolParamVO->new({
                                                                 name => 'protocol_param_1',
                                                                 value => 'protocol_param_1_value',
                                                                 dataType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"float", category=>"DataType"}),
                                                                });

  my $param2 =  RAD::MR_T::MageImport::VO::ProtocolParamVO->new({
                                                                 name => 'protocol_param_2',
                                                                 value => 'protocol_param_2_value',
                                                                 unitType => RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"months", category=>"TimeUnit"}),
                                                                });

  my $prot1 = RAD::MR_T::MageImport::VO::ProtocolVO->new({
                                                          name=>"protocol_labeling", 
                                                          protocolDescription=>"protocol_labeling_description",
                                                          uri=>"protocol_labeling_uri", 
                                                          protocolType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"labeling", category=>"ExperimentalProtocolType"}),
                                                          params=>[$param1, $param2],
						      });

  my $prot2 =
    RAD::MR_T::MageImport::VO::ProtocolVO->new({
						name=>"protocol_extraction", 
						protocolDescription=>"protocol_extraction_description",
						uri=>"protocol_extraction_uri", 
						protocolType=>RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"nucleic_acid_extraction", category=>"ExperimentalProtocolType"}),
					       });

  $doc->protocolVOs([$prot1, $prot2]);

}

sub setStudyVO{
  my $class = shift;
  my $doc = shift;
  my $studyContact = shift;

  my $sfoe1 = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"genetic_modification", category=>"ExperimentalProtocolType"});
  my $sfoe2 = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"organism_part", category=>"BioMaterialCharacteristicCategory"});

  my $sf1 = 
    RAD::MR_T::MageImport::VO::StudyFactorVO->new({
						   name=>"studyFactor_1", 
						   type=>$sfoe1
						  });

  my $sf2 = 
    RAD::MR_T::MageImport::VO::StudyFactorVO->new({
						   name=>"studyFactor_2", 
						   type=>$sfoe2
						  });

  my $sdoe1 = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"strain_or_line_design", category=>"BiologicalProperty"});
  my $sdoe2 = RAD::MR_T::MageImport::VO::OntologyEntryVO->new({value=>"species_design", category=>"BiologicalProperty"});
  my $sd = 
    RAD::MR_T::MageImport::VO::StudyDesignVO->new({
						   name=>"studyDesign", 
						   types=>[$sdoe1,$sdoe2],
						   factors=>[$sf1, $sf2],
						  });


  my $study = 
    RAD::MR_T::MageImport::VO::StudyVO->new({
					     name=>"study", 
                                             description=>"studyDescription",
					     designs=>[$sd],
                                             contact=>$studyContact,
					    });

  $doc->studyVO($study);

}

1;



