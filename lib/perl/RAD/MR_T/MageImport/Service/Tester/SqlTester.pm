package RAD::MR_T::MageImport::Service::Tester::SqlTester;

use RAD::MR_T::MageImport::MageImportError;
use GUS::ObjRelP::DbiDatabase;
use Data::Dumper;

=head1 SqlTester

=over

=item
 sql testing file format
 regex  sql  parameter
 1  select count(*) from study.study where name='test'
 NA   select study_id from study.study where name='test'  study_id
 3  select count(*) from study.studyDesign where study_id=$$study_id$$
 34 select count(*) from rad.studyassay where study_id=$$study_id$$
 1  select count(*) from rad.assay where name="test assay" 
 NA  select assay_id from rad.assay where name="test assay"  assay_id
 4  select count(*) from rad.assaybiomaterial where assay_id in ($$assay_id$$)
=cut

use base qw(RAD::MR_T::MageImport::Service::AbstractTester);

sub new {
  my $class = shift;
  my $file = shift;
  my $queryHandler = shift;

  bless {_sqlTestingFile => $file, _queryHandler => $queryHandler}, $class;
}

sub _getLinesArray {wantarray ? @{$_[0]->{_lines_array}} : $_[0]->{_lines_array}}

sub _setLinesArray { $_[0]->{_lines_array} = $_[1] }

sub _getSqlTestingFile {$_[0]->{_sqlTestingFile}}

sub _getQueryHandler {
  if($_[0]->{_queryHandler}){
    return $_[0]->{_queryHandler};
  }
  elsif(GUS::ObjRelP::DbiDatabase->getDefaultDatabase()){
    return GUS::ObjRelP::DbiDatabase->getDefaultDatabase()->getDbHandle();
  }
  else{
    RAD::MR_T::MageImport::TesterException::DefaultDbNotFound->new->throw();
  }
}

sub setAlgoInvoId { $_[0]->{_algoInvoId} = $_[1] }

sub getAlgoInvoId { 
 if($_[0]->{_algoInvoId}){
    return $_[0]->{_algoInvoId};
  }
  elsif(GUS::ObjRelP::DbiDatabase->getDefaultDatabase()){
    if(my $id = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()->getDefaultAlgoInvoId()){
      return $id;
    }
    else{
      RAD::MR_T::MageImport::TesterException::DefaultAlgoInvoIdNotFound->new->throw();
    }
  }
}


#--------------------------------------------------------------------------------

sub _readFile {
 my $self = shift;

 my @fileLines;

 my $fn = $self->_getSqlTestingFile();

 open(FILE, $fn) or RAD::MR_T::MageImport::TesterException->
   new("Cannot open file $fn for reading: $!")->throw();;

 <FILE>; # Remove the header

 while(<FILE>) {
   chomp;

   push(@fileLines, $_);
 }
 close(FILE);

 $self->_setLinesArray(\@fileLines);

 return wantarray ? @fileLines : \@fileLines;
}

sub test {
  my ($self) = @_;
  my $testHash = $self->parseLines();

  my $sLogger = $self->getLogger();

  my $counter=0;
  my $success=0;

  foreach my $test (keys %$testHash){
    $counter++;
    my $match = $testHash->{$test}->{expected};
    my $actual = $testHash->{$test}->{actual};
    my $actual1 = $testHash->{$test}->{actual};
    $sLogger->debug("the match: ",  $match, "the actual: ",  $actual);
    if ($actual =~ m/^$match$/i) {
      $success++;
      $sLogger->info("PASSED", "\ttest sql:",$test, "\texpected:", $match,"\tactual:", $actual1);
    }
    else{
      $sLogger->info("FAILED", "\ttest sql:",$test, "\texpected:", $match,"\tactual:", $actual1);
    }
  }

  $sLogger->info("SQL test result: $success/$counter PASSED");
  return 1 if($counter == $success);
  return 0;
}

#--------------------------------------------------------------------------------

sub parseLines {
  my ($self) = @_;

  my @lines = $self->_getLinesArray() ? $self->_getLinesArray() : $self->_readFile();

  my (%params, %sqlAsserts);

  foreach my $line (@lines) {
    next unless $line;
    my ($expected, $actual, $statement) = $self->_parseLine($line, \%params);

    unless($expected eq 'NA') {
      $sqlAsserts{$statement} = {expected => $expected,
                                 actual => $actual,
                                };
    }
  }

  return \%sqlAsserts;
}

#--------------------------------------------------------------------------------

sub _parseLine {
  my ($self, $line, $params) = @_;

  my ($regex, $sql, $param) = split(/\|/, $line);
  my $sLogger = $self->getLogger();
  $sLogger->debug($regex, $sql, $param);
  $sLogger->debug(sub {Dumper($params)});

  if($sql =~ m/\$\$algoInvoId\$\$/ && !($params->{algoInvoId})){ 
    $params->{algoInvoId} = $self->getAlgoInvoId;
  }

  if($sql =~ s/(\$\$(\w+)\$\$)/$params->{$2}/) {
    RAD::MR_T::MageImport::TesterException->
        new("No Param for $2 was defined")->throw() unless(exists $params->{$2});
  }

  my $result = $self->_runSql($sql);

  if($param) {
    $params->{$param} = $result; 
  }

  return ($regex, $result, $sql);
}

#--------------------------------------------------------------------------------

sub _runSql {
  my ($self, $sql) = @_;

  my @rv;
  my $sLogger = $self->getLogger();
  $sLogger->debug($sql);
  my $dbh = $self->_getQueryHandler();

  my $sh;

  eval {$sh = $dbh->prepare($sql); $sh->execute();};

  RAD::MR_T::MageImport::TesterException::SqlError->new("Sql execution error: $sql")->throw() if $@;

  while(my @ar = $sh->fetchrow_array()) {
    push(@rv, @ar);
  }
  $sh->finish();

  if(scalar(@rv) == 0) {
    RAD::MR_T::MageImport::TesterException::SqlError->
      new("No Result for sql: $sql")->throw();
  }

  if(scalar(@rv) > 1) {
    return join(',', @rv);
  }

  return $rv[0];

}

#--------------------------------------------------------------------------------

1;
