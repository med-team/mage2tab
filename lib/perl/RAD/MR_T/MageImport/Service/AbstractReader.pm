package RAD::MR_T::MageImport::Service::AbstractReader;

#################################################
# $Revision: 8341 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: junmin $
#################################################

use strict 'vars';
use Carp;
use base qw(RAD::MR_T::MageImport::Loggable);

=head1 AbstractReader

Readers are a subclass of AbstractReader.  The Readers must implement the 'parse' method 

=over 4

=item C<new>

B<Parameters:>

$class: Caller which is the subclass of AbstractReader
$file(scalar): The Reader only knows about one file

=cut

sub new {
  my $class = shift;
  croak "try to instantiate an abstract class AbstractReader" if ref($class) eq 'AbstractReader';

  my $file = shift;

  bless {_file => $file}, $class;
}

sub getFile {$_[0]->{_file}}

sub setFile {$_[0]->{_file} = $_[1]}

=item C<docRoot>

Wrapper to call the parse method and require it returns the correct type of object.

B<Return type:> C<RAD::MR_T::MageImport::VO::DocRoot> 

=cut

sub docRoot {
  my ($self) = @_;

  my $docRoot;
  unless($docRoot = $self->{_doc_root}) {
    $docRoot = $self->parse();
  }

  my $myName = ref($self);
  my $docRootName = 'RAD::MR_T::MageImport::VO::DocRoot';

  croak "$myName::parse did not return a $docRootName"  unless($docRoot->isa($docRootName));

  return($docRoot);
}


=item C<parse>

The parse method should not be called directly... use 'docRoot'

B<Return type:> C<RAD::MR_T::MageImport::VO::DocRoot> 

=back

=cut

sub parse { }


1;



