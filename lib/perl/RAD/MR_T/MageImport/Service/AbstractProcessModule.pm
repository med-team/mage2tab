package RAD::MR_T::MageImport::Service::AbstractProcessModule;

# this is ProcessModule interface, a decorator interface

use strict 'vars';

use  RAD::MR_T::MageImport::Service::AbstractProcessor;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractProcessor/;

sub new {
  my $class = shift;
  my $arg = shift;
  bless {_processor=>$arg}, $class;
}

sub process {}

sub getProcessor  {$_[0]->{_processor}}
sub setProcessor  {$_[0]->{_processor} = $_[1]}

1;
