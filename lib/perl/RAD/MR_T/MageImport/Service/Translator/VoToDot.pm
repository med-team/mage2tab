package RAD::MR_T::MageImport::Service::Translator::VoToDot;

use base qw(RAD::MR_T::MageImport::Service::AbstractTranslator);

use strict;

use Data::Dumper;

use Error qw(:try);

use RAD::MR_T::MageImport::MageImportError;

my $sLogger;
#--------------------------------------------------------------------------------

sub mapAll {
  my ($self, $docRoot) = @_;

  $sLogger = $self->getLogger();
  $sLogger->info("start the translating to GraphViz Dot method");

  my $voAssays = $docRoot->getAssayVOs();
  my $voBioMaterials = $docRoot->getBioMaterialVOs();
  my $voTreatments = $docRoot->getTreatmentVOs();

  my $fileBuffer = "
     digraph D{\n
     rankdir=LR;\n
     node[shape=box];\n";
#  HEADER
	
  my $channel2color = {'cy3'=>'red', 'cy5'=>'green', 'biotin'=>'black'};
  foreach my $biomat (@$voBioMaterials) {
      my $color = "black";
      if(my $channel = $biomat->getChannel){
	  $color = $channel2color->{lc($channel->getValue)}
      }
      
      my $charsStr = "";
      foreach my $char (@{$biomat->getBioMaterialChars()}) {
	$charsStr  = $charsStr. '\n' . $char->getCategory ."=" . $char->getValue();
      }
      if($charsStr ne ""){
	$fileBuffer  =  $fileBuffer. '"'.$biomat->getName . "\"[ label =\"" . $biomat->getSubclassView.":".$biomat->getName . " |{$charsStr}\", color=".$color."];\n";
      }else{
	$fileBuffer  =  $fileBuffer. '"'.$biomat->getName . "\"[ label =\"" . $biomat->getSubclassView.":".$biomat->getName . "\", color=".$color."];\n";
      }
  }		

  foreach my $hyb (@$voAssays){
      my $pvStr = "";
      foreach my $pv (@{$hyb->getParameterValues}){
	my $name = $pv->getParameterName;
	$pvStr  = $pvStr.'\n' . "$name =".$pv->getValue;
      }
      if($pvStr ne ""){
	$fileBuffer  = $fileBuffer.'"Hyb:'.$hyb->getName . "\"[ label =\"Hyb:" . $hyb->getName . " |{$pvStr}\"];\n";
      }
      else{
	$fileBuffer  = $fileBuffer.'"Hyb:'.$hyb->getName . "\"[ label =\"Hyb:" . $hyb->getName . "\"];\n";
      }

      foreach my $acq (@{$hyb->getAcquisitions}){
	$fileBuffer  = $fileBuffer.$self->printObjNameAndPVs($acq, "Scan");
	foreach my $quan (@{$acq->getQuantifications}){
	  $fileBuffer  = $fileBuffer.$self->printObjNameAndPVs($quan, "Feature Ex");
	  next unless $quan->getProcesses;
	  foreach my $proc (@{$quan->getProcesses}){
	    $fileBuffer  = $fileBuffer.$self->printObjNameAndPVs($proc, "Normalization");
	  }

	}
      }
  }
 
  foreach my $trt (@$voTreatments){
      my $inputBioMaterials = $trt->getInputBMMs();
      my $outputBioMaterial = $trt->getOutputBM();
      foreach my $input (@$inputBioMaterials) {
	  $fileBuffer  = $fileBuffer . '"'.$input->getBioMaterial->getName . "\"->\"" . $outputBioMaterial->getName."\";\n";
      }
  }

  foreach my $hyb (@$voAssays){
    my $lexs = $hyb->getLabeledExtracts;
    foreach my $lex (@$lexs){
      $fileBuffer  = $fileBuffer.'"'.$lex->getName . "\"->\"Hyb:" . $hyb->getName."\";\n";
    }
    foreach my $acq (@{$hyb->getAcquisitions}){
      $fileBuffer  = $fileBuffer.'"Hyb:'.$hyb->getName . "\"->\"" . $acq->getName."-".$acq->getUri."\";\n";
      foreach my $quan (@{$acq->getQuantifications}){
	$fileBuffer  = $fileBuffer.'"'.$acq->getName ."-".$acq->getUri."\"->\"" . $quan->getName."-".$quan->getUri."\";\n";
	next unless $quan->getProcesses;
	foreach my $proc (@{$quan->getProcesses}){
	  $fileBuffer  = $fileBuffer.'"'.$quan->getName ."-". $quan->getUri."\"->\"" . $proc->getName."-".$proc->getUri."\";\n";
	}
      }
    }
  }
  print $fileBuffer."}";
}


sub printObjNameAndPVs{
  my ($self, $obj, $label, $fileBuffer) = @_;

  my $pvStr = "";
  foreach my $pv (@{$obj->getParameterValues}){
    my $name = $pv->getParameterName;
    $pvStr  = $pvStr.'\n' . "$name =".$pv->getValue;
  }
  if($pvStr ne ""){
    $fileBuffer  = $fileBuffer.'"'.$obj->getName . "-".$obj->getUri."\"[ label =\"$label:" . $obj->getName . '\n'.$obj->getUri." |{$pvStr}\"];\n";
  }
  else{
    $fileBuffer  = $fileBuffer.'"'.$obj->getName ."-". $obj->getUri. "\"[ label =\"$label:" . $obj->getName . '\n'.$obj->getUri."\"];\n";
  }
  return $fileBuffer;
}
