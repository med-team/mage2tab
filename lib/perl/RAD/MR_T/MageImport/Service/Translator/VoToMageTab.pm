package RAD::MR_T::MageImport::Service::Translator::VoToMageTab;

use base qw(RAD::MR_T::MageImport::Service::AbstractTranslator);

use strict;

use Data::Dumper;

use Error qw(:try);

use RAD::MR_T::MageImport::MageImportError;

my $sLogger;

#--------------------------------------------------------------------------------
sub writeIDF {
  my ($self, $docRoot) = @_;

  $sLogger = $self->getLogger();
  $sLogger->info("start the translating to MAGE-TAB IDF");

  my $voAffiliations = $docRoot->affiliationVOs();
  my $voPersons = $docRoot->personVOs();
  my $voExternalDatabases = $docRoot->externalDatabaseVOs();
  my $voStudy = $docRoot->getStudyVO();
  my $voProtocols = $docRoot->getProtocolVOs();

  $self->writeStudy($voStudy);
  $self->writePersons($voPersons);
  $self->writeProtocols($voProtocols);
  $self->writeExternalDatabases($voExternalDatabases);
}

sub writeSDRF {
  my ($self, $docRoot) = @_;

  $sLogger = $self->getLogger();
  $sLogger->info("start the translating to MAGE-TAB SDRF");

  my $voProtocols = $docRoot->getProtocolVOs();

  $self->writeSdrf($docRoot, $voProtocols);

}

sub mapAll {
  my ($self, $docRoot) = @_;

  $sLogger = $self->getLogger();
  $sLogger->info("start the translating to MAGE-TAB method");

  my $voAffiliations = $docRoot->affiliationVOs();
  my $voPersons = $docRoot->personVOs();
  my $voExternalDatabases = $docRoot->externalDatabaseVOs();
  my $voStudy = $docRoot->getStudyVO();
  my $voProtocols = $docRoot->getProtocolVOs();

  my $voAssays = $docRoot->getAssayVOs();
  my $voBioMaterials = $docRoot->getBioMaterialVOs();
  my $voTreatments = $docRoot->getTreatmentVOs();

  $self->writeStudy($voStudy);
  $self->writePersons($voPersons);
  $self->writeProtocols($voProtocols);
  $self->writeExternalDatabases($voExternalDatabases);

  $self->writeSdrf($docRoot, $voProtocols);

#  $self->writeSdrfPart1($voBioMaterials, $voTreatments);

#  $self->writeSdrfPart2($voAssays, $voStudy);
}

#--------------------------------------------------------------------------------

sub writeSdrf {
  my ($self, $docRoot, $voProtocols) = @_;

  my $voAssays = $docRoot->getAssayVOs();
  my $voBioMaterials = $docRoot->getBioMaterialVOs();
  my $voTreatments = $docRoot->getTreatmentVOs();
  my $voStudy = $docRoot->getStudyVO();

  my $allCharacteristicCategories = $self->getDistinctBmCharacteristics($voBioMaterials);
  my $allParameterNamesAndUnits = $self->getDistinctParameterNamesAndUnits($voProtocols);

  my $sdrfGrid = SDRF_Grid->new($voProtocols, $voTreatments, $allCharacteristicCategories, $allParameterNamesAndUnits, $voAssays, $voStudy);

  $sdrfGrid->printGrid();
}

#--------------------------------------------------------------------------------

sub getDistinctParameterNamesAndUnits {
  my ($self, $protocols) = @_;

  my %distinctParameterNames;

  foreach my $p (@$protocols) {
    my $params = $p->getParams();
    foreach my $param (@$params) {
      my $name = $param->getName();

      my ($unit, $category);

      if(my $unitType = $param->getUnitType()) {
        $unit = $unitType->getValue();
        $category = $unitType->getCategory();
      }

      $distinctParameterNames{$name} = [$name, $unit, $category];
    }
  }

  my @paramNames;
  foreach my $paramName (keys %distinctParameterNames) {
    push(@paramNames, $distinctParameterNames{$paramName});
  }

  return \@paramNames;
}

#--------------------------------------------------------------------------------

sub getDistinctBmCharacteristics {
  my ($self, $voBioMaterials) = @_;

  my %distinctCharCategories;

  foreach my $bm (@$voBioMaterials) {
    my $bmChars = $bm->getBioMaterialChars() ? $bm->getBioMaterialChars() : [];
    my @bmChars = map {$_->getCategory} @$bmChars;

    foreach (@bmChars) {
      $distinctCharCategories{$_} = 1;
    }
  }
  my @categories = keys %distinctCharCategories;

  return \@categories;
}




sub writeSdrfPart2 {
  my ($self, $voAssays, $voStudy) = @_;

  $self->writeHeaderFromHybToDerivedData($voAssays, $voStudy);
  $self->writeHybToDerivedData($voAssays, $voStudy);
}

sub writeHybToDerivedData {
  my ($self, $voAssays, $voStudy) = @_;

#   my %lex2assay;
#   my %lex2assay;
#   foreach my $assay (@$voAssays){
#     foreach my $lex (@{$assay->getLabeledExtracts}){
#       $lex2assay{$lex} = $assay;
#     }
#   }

 Hyb: foreach my $assay (@$voAssays) {
# Lex: for my $lex (keys %lex2assay) {
  foreach my $lex (@{$assay->getLabeledExtracts}){
    my @row = ();

    push (@row, $lex->getName);
#  Hyb: {
 #   my $assay = $lex2assay{$lex};
    my ($self, $voAssays, $voStudy) = @_;

    push (@row, $assay->getProtocolName);
    foreach my $pv(@{$assay->getParameterValues}){
      push (@row, $pv->getValue);
    }

    push (@row, $assay->getName);

  #   my @lex = map {$_->getName} @{$assay->getLabeledExtracts};
#     my $lexString = join(',', @lex);
#     push(@row, $lexString);

    Scan: foreach my $acq(@{$assay->getAcquisitions}){
      my @row_copy = @row;

      if($acq->getProtocolName){
	push (@row_copy, $acq->getProtocolName);
      }
      else{
	push (@row_copy, "N.A");
      }

      foreach my $pv(@{$acq->getParameterValues}){
	push (@row_copy, $pv->getValue);
      }

      push (@row_copy, $acq->getName);
      push (@row_copy, $acq->getUri?$acq->getUri:"N.A.");

      if($acq->getFactorValues){
	foreach my $fv(@{$acq->getFactorValues}){
	  if($fv->getValue){
	    push (@row_copy, $fv->getValue->getValue);
	  }
	  else{
	    push (@row_copy, "undef");
	  }
	}
      }
      else{
	push (@row_copy, "N.A");
      }

      ArrayData: foreach my $quan(@{$acq->getQuantifications}){
	my @row_copy_copy = @row_copy;
	#push (@row_copy_copy, $quan->getName);
	if($quan->getProtocolName){
	  push (@row_copy_copy, $quan->getProtocolName);
	}
	else{
	  push (@row_copy_copy, "N.A.");
	}

	foreach my $pv(@{$quan->getParameterValues}){
	  push (@row_copy_copy, $pv->getValue);
	}

	push (@row_copy_copy, $quan->getUri);

	if($quan->getProcesses){
	DerivedData:  foreach my $process(@{$quan->getProcesses}){
	    my @row_copy_copy_copy = @row_copy_copy;
	    if($process->getProtocolName){
	      push (@row_copy_copy_copy, $process->getProtocolName);
	    }
	    else{
	      push (@row_copy_copy_copy, "N.A.");
	    }

	    foreach my $pv(@{$process->getParameterValues}){
	      push (@row_copy_copy_copy, $pv->getValue);
	    }

	    push (@row_copy_copy_copy, $process->getName);
	    push (@row_copy_copy_copy, $process->getUri);
	    $self->stringByRow('',\@row_copy_copy_copy);
	  }
	}
	else{
	  push (@row_copy_copy, "N.A.\tN.A.\tN.A.");
	  $self->stringByRow('',\@row_copy_copy);
	}
      }#ArrayData
    }#Scan
  }#Hyb
  }#lex
}

sub writeHeaderFromHybToDerivedData{
  my ($self, $voAssays, $voStudy) = @_;
  my %lex2assay;

  my @row = ();
  push (@row, "LabeledExtract Name");

  push (@row, "Protocol REF");
  my $assay = $voAssays->[0];
  foreach my $pv(@{$assay->getParameterValues}){
    push (@row, "Parameter Value [".$pv->getParameterName."]");
  }

  push (@row, "Hybridization ID");



  push (@row, "Protocol REF");

  my $acq = $assay->getAcquisitions->[0];
  foreach my $pv(@{$acq->getParameterValues}){
    push (@row, "Parameter Value [".$pv->getParameterName."]");
  }

  push (@row, "Scan Name");
  push (@row, "Image File");

  my @factorNames = ();
  foreach my $studyDesign (@{$voStudy->getDesigns}) {
    foreach my $studyFactor (@{$studyDesign->getFactors()}) {
        push(@factorNames, "Factor Value [".$studyFactor->getName()."]");
    }
  }
  push (@row, @factorNames);

  push (@row, "Protocol REF");

  my $quan = $acq->getQuantifications->[0];
  foreach my $pv(@{$quan->getParameterValues}){
    push (@row, "Parameter Value [".$pv->getParameterName."]");
  }

  push (@row, "Array Data File");
  push (@row, "Protocol REF");

  if($quan->getProcesses){
    my $process = $quan->getProcesses->[0];
    foreach my $pv(@{$process->getParameterValues}){
      push (@row, "Parameter Value [".$pv->getParameterName."]");
    }
  }

  push (@row, "Normalization Name");
  push (@row, "Derived Array Data File");

  $self->stringByRow('',\@row);

}


sub writeSdrfPart1{
 my ($self, $voBioMaterials, $voTreatments) = @_;

  my $voBioSources = $self->getBioSources($voBioMaterials);
  my @headerRow = ();
# $logger->info("****Dump the whole docRoot****", sub { Dumper($voBioSources->[0]) } );
  $self->writeHeaderFromBioSourceToLex($voTreatments,$voBioSources->[0], \@headerRow);

  foreach my $bioSource (@$voBioSources) {
    my @row = ();
    my @headers = @headerRow;
    $self->writeBioSampleAndTreatment($voTreatments,$bioSource, \@row, \@headers)
  }




}
#----------------------------------
sub writeHeaderFromBioSourceToLex{
 my ($self, $voTreatments,$biosample, $row) = @_;

 push(@$row, $biosample->getSubclassView." Name");

# push (@$row, "Material Type") if $biosample->getBioMaterialType;
 if($biosample->getBioMaterialChars()){
   foreach my $char (@{$biosample->getBioMaterialChars()}) {
     push(@$row, "Characteristics [".$char->getCategory()."]");
   }
 }

 my $treatment = $self->findTreatmentFromInputBioMaterialName($voTreatments, $biosample);
 if($treatment){
     my $trt = $treatment->[0];
     push(@$row, "Protocol REF");

     foreach my $pv(@{$trt->getParameterValues}){
       push (@$row, "Parameter Value [".$pv->getParameterName."]");
     }

     $self->writeHeaderFromBioSourceToLex($voTreatments,$trt->getOutputBM(), $row);
 }
 else{
   $self->stringByRow('',$row);
   return;
 }
}

#--------------------------------------------------------------------------------
sub writeBioSampleAndTreatment{
 my ($self, $voTreatments,$biosample, $row, $headerRow) = @_;

 push(@$row, $biosample->getName());
 shift (@$headerRow);

 my $c=0;
 for($c=0; $headerRow->[$c] && $headerRow->[$c] ne "Protocol REF"; $c++){}

 if($biosample->getBioMaterialChars()){
 for(my $i=0; $i<$c; $i++){
   my $found = 0;
   foreach my $char (@{$biosample->getBioMaterialChars()}) {
     if("Characteristics [".$char->getCategory."]" eq $headerRow->[$i]){
       push(@$row, $char->getValue());
       $found = 1;
       last;
     }
   }
   push (@$row, "N.A.A.") unless $found;
 }
}

 for(my $i=0; $i<$c; $i++){
   shift (@$headerRow);
 }

 my $treatment = $self->findTreatmentFromInputBioMaterialName($voTreatments, $biosample);
 if($treatment){
   foreach my $trt (@$treatment){
     my @row_copy = @$row; 
     my @header_copy = @$headerRow;
     if(my $treatmentProtocol = $trt->getProtocol()){
       push(@row_copy, $treatmentProtocol->getName());
     }
     else{
       push(@row_copy, "N.A.");
     }

     shift (@header_copy);

     foreach my $pv(@{$trt->getParameterValues}){
       push (@row_copy, $pv->getValue);
       shift (@header_copy);
     }

     $self->writeBioSampleAndTreatment($voTreatments,$trt->getOutputBM(), \@row_copy, \@header_copy);
   }
 }
 else{
   $self->stringByRow('',$row);
   return;
 }
}


sub writeTreatments {
  my ($self, $voBioSources, $voTreatments) = @_;

  foreach my $bioSource (@$voBioSources) {
    my $treatment = $self->findTreatmentFromInputBioMaterialName($voTreatments, $bioSource);

    my $inputBioMaterials = $treatment->getInputBMMMs();

    foreach my $inputBM ($inputBioMaterials) {
      my @row;

      push(@row, $inputBM->getName());

      foreach my $char (@{$inputBM->getBioMaterialChars()}) {
        push(@row, $char->getValue());
      }

      my $treatmentProtocol = $treatment->getProtocol();
      push(@row, $treatmentProtocol->getName());

    }
  }

}

#--------------------------------------------------------------------------------

sub findTreatmentFromInputBioMaterialName {
  my ($self, $voTreatments,$bioSource) = @_;

  my @treatments;

  foreach my $treatment (@$voTreatments) {
    my $inputBioMaterials = $treatment->getInputBMMs();

    foreach my $input (@$inputBioMaterials) {
      push(@treatments, $treatment) if($input->getBioMaterial == $bioSource);
     }
  }

  $sLogger->debug("More than one treatment for a biomaterial") if(scalar(@treatments) > 1);

  return 0 if (scalar(@treatments) ==0);

  return \@treatments;
}



#--------------------------------------------------------------------------------

sub getBioSources {
  my ($self, $voBioMaterials) = @_;

  my @bioSources;

  foreach my $bioMaterial (@$voBioMaterials) {
    push(@bioSources, $bioMaterial) if($bioMaterial->getSubclassView() =~ /source/i);
  }
  return \@bioSources;
}



#--------------------------------------------------------------------------------

sub writeStudy {
  my ($self, $voStudy) = @_;

  $sLogger->info("Investigation Title\t", $voStudy->getName);

  my $voDesigns = $voStudy->getDesigns();

  my $title = $self->stringByRow("Investigation Title", [$voStudy->getName]);
  my $pubMed = $self->stringByRow("PubMed ID", [$voStudy->getPubMedId()]);
  my $description = $self->stringByRow("Experiment Description", [$voStudy->getDescription()]);

  $self->writeStudyDesigns($voDesigns);
  $self->writeStudyFactors($voDesigns);

  return ($title, $pubMed, $description);
}


#--------------------------------------------------------------------------------

sub writeStudyDesigns {
  my ($self, $voDesigns) = @_;

  my @designTypeValues;

  foreach my $studyDesign (@$voDesigns) {
    foreach my $studyDesignType (@{$studyDesign->getTypes()}){
      push(@designTypeValues, $studyDesignType->getValue());
    }
  }

  return $self->stringByRow("Experimental Design", \@designTypeValues);
}

#--------------------------------------------------------------------------------

sub writeStudyFactors {
  my ($self, $voDesigns) = @_;

  $sLogger->info("Experimental Factor Names and Types"); 

  my @factorTypes;
  my @factorNames;

  foreach my $studyDesign (@$voDesigns) {
    foreach my $studyFactor (@{$studyDesign->getFactors()}) {
      push(@factorTypes, $studyFactor->getType()->getValue());
      push(@factorNames, $studyFactor->getName());
    }
  }

  my $experimentalFactorTypes = $self->stringByRow("Experimental Factor Type", \@factorTypes);
  my $experimentalFactorNames = $self->stringByRow("Experimental Factor Name", \@factorNames);

  return($experimentalFactorTypes, $experimentalFactorNames);
}

#--------------------------------------------------------------------------------

sub writePersons {
  my ($self, $voPersons) = @_;

  my @lastNames = map {$_->getLast} @$voPersons;
  my @firstNames = map {$_->getFirst} @$voPersons;
  my @addresss = map {$_->getAddress?$_->getAddress:($_->getAffiliation?$_->getAffiliation->getAddress:'')} @$voPersons;
  my @emails = map {$_->getEmail} @$voPersons;
  my @roles = map {$_->getRole} @$voPersons;

  my @affiliations;

  foreach my $person (@$voPersons) {
    my $affiliation = $person->getAffiliation();
    my $affiliationName = $affiliation ? $affiliation->getName() : '';
    push(@affiliations, $affiliationName);
  }

  $self->stringByRow("Person Last Name", \@lastNames);
  $self->stringByRow("Person First Name", \@firstNames);
  $self->stringByRow("Person Email", \@emails);
  $self->stringByRow("Person Address", \@addresss);
  $self->stringByRow("Person Affiliation", \@affiliations);
  $self->stringByRow("Person Roles", \@roles);
}

#--------------------------------------------------------------------------------

sub writeExternalDatabases {
  my ($self, $externalDatabases) = @_;

  return unless($externalDatabases);

  my @names = map {$_->getName} @$externalDatabases;
  my @versions = map {$_->getVersion} @$externalDatabases;
  my @uris = map {$_->getUri} @$externalDatabases;

  $self->stringByRow("Term Source Name", \@names);
  $self->stringByRow("Term Source Version", \@versions);
  $self->stringByRow("Term Source File", \@uris);
}

#--------------------------------------------------------------------------------

sub writeProtocols {
  my ($self, $voProtocols) = @_;

  my @protocolNames = map {$_->getName} @$voProtocols;
  my @protocolDescriptions = map {$_->getProtocolDescription} @$voProtocols;
  my @protocolTypes;
  my @protocolParams;
  my @protocolParamUnits;

  foreach my $protocol (@$voProtocols) {
    my $typeOe = $protocol->getProtocolType();
    my $type = $typeOe ? $typeOe->getValue() : '';
    push(@protocolTypes, $type);

    my $params = $protocol->getParams();

    if($params) {
      my @names = map {$_->getName} @$params;
      my $paramsString = join(';', @names);
      push(@protocolParams, $paramsString);

       my @units = map {$_->getUnitType?$_->getUnitType->getValue:$_->getUnitType} @$params;
       my $unitsString = join(';', @units);
       push(@protocolParamUnits, $unitsString);
    }
    else{
      push(@protocolParams, "");
      push(@protocolParamUnits, "");
    }
  }

  $self->stringByRow("Protocol Name", \@protocolNames);
  $self->stringByRow("Protocol Description", \@protocolDescriptions);
  $self->stringByRow("Protocol Type", \@protocolTypes);
  $self->stringByRow("Protocol Parameters", \@protocolParams);
 # $self->stringByRow("Protocol Parameter Units", \@protocolParamUnits);
}


#--------------------------------------------------------------------------------

sub stringByRow {
  my ($self, $rowHeader, $values) = @_;

  return unless(scalar(@$values) > 0);

  my $rowString = join("\t", $rowHeader, @$values);

  print $rowString."\n";

  return $rowString;

}


#  $sLogger->info("Source Id\tProtocol REF\tSample Id\tProtocol REF\tExtract Id\tProtocol REF\tLEX Id\t\Protocol REF\tHyb Id\tArrayData URI\tDerivedArrayData URI");

################
#1.foreach biosource put biosource name
  #2.put biomatChars
  #3.found the treatment which has it as input, put treatment protocol name, put outputBM
   #4.for the outputBM, do the same above 2-4, until no treatment found

#we don't have assay to lex link so
#print assay to quantification table separately for now


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
package SDRF_Grid;
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

use Data::Dumper;

sub getSdrfGrid {$_[0]->{sdrf_grid}}
sub getHeader {$_[0]->{header}}
sub getCharacteristics {$_[0]->{characteristics}}

#--------------------------------------------------------------------------------

sub new {
  my ($class, $protocols, $treatments, $allCharacteristics, $allParameterNamesAndUnits, $assays, $voStudy) = @_;

  my $rawGrid = &makeInternalGrid($treatments, $assays);

  my $orderedProtocols = &orderProtocols($rawGrid);

  my $header = &makeHeader($orderedProtocols, $allCharacteristics, $allParameterNamesAndUnits, $voStudy);

  my $indexes = &makeIndexes($orderedProtocols, $header);

  my $grid = [$header];

  my $self = bless {header => $header,
                    sdrf_grid => $grid,
                    characteristics => $allCharacteristics,
                   }, $class;

  $self->formatGrid($rawGrid, $allParameterNamesAndUnits, $allCharacteristics, $indexes);

  return $self;
}

#--------------------------------------------------------------------------------

sub orderProtocols {
  my ($rawGrid) = @_;

  my @rows = @{$rawGrid};
  my (%indexes, %protocolNames);

  for(my $i = 0; $i < scalar(@rows); $i++) {
    my $row = $rows[$i];
    my $protocolRow = [];

    my $emptyCount = 1;
    for(my $j = 0; $j < scalar(@$row); $j++) {
      my $obj = $row->[$j];

      unless($obj) {
        my $name = "EMPTY_PROTOCOL $emptyCount";
        my $nullProtocol = NULL_PROTOCOL->new($name);

        $protocolNames{$name} = $nullProtocol;
        push(@{$indexes{$name}}, $j);

        $emptyCount++;
      }

      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::ProtocolVO') {
        my $name = $obj->getName();
        $protocolNames{$name} = $obj;
        push(@{$indexes{$name}}, $j);
      }
    }
  }

  my @unsorted;
  foreach(keys %indexes) {
    my @values = @{$indexes{$_}};
    my $max = &max(\@values);
    my $min = &min(\@values);

    push(@unsorted, [$_, $max, $min]);
  }

  # sort the index, first on the max then on the min
  my @sorted = map { $protocolNames{$_->[0]} }
    reverse sort { $a->[1] == $b->[1] ? $b->[2] <=> $a->[2] : $b->[1] <=> $a->[1] }
      @unsorted;

  return \@sorted;
}

#--------------------------------------------------------------------------------

sub min {
  my ($values) = @_;

  my $min = shift(@$values);

  foreach (@$values) {
    $min = $_ if $min > $_;
  }
  return $min;
}

#--------------------------------------------------------------------------------

sub max {
  my ($values) = @_;

  my $max = shift(@$values);

  foreach(@$values) {
    $max = $_ if $max < $_;
  }

  return $max
}

#--------------------------------------------------------------------------------

sub formatGrid {
  my ($self, $rawGrid, $allParameterNamesAndUnits, $allCharacteristics, $indexes) = @_;

  my @rows = @$rawGrid;

  for(my $i = 0; $i < scalar @rows; $i++) {
    my $row = $rows[$i];
    my @rowObjects = @$row;

    my $j = 0;

    my $lex;

    foreach my $obj (@rowObjects) {

      #print STDERR $obj . "\n";

      if(!$obj && $j > 0) {
        #my $grid = $self->getSdrfGrid();
        #$grid->[$i]->[$j] = "->";
        $j++;
      }

      if(ref($obj) eq 'NULL_PROTOCOL') {
        print STDERR "MADE IT HERE\n";
      }

      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::BioMaterialVO') {
        #print STDERR "Adding BioMaterial to GRID\n";
        my $newIndex = $self->addBioMaterialToGrid($obj, $i, $j);

        $lex =  $obj;
        $j = $newIndex;
      }
      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::ProtocolVO') {
        #print STDERR "Adding Protocol to GRID\n";
        my $name = $obj->getName();
        $j = $indexes->{$name};

        my $newIndex= $self->addProtocolToGrid($obj, $i, $j);
        $j = $newIndex;
      }
      #Parameter Values
      if(ref($obj) eq 'ARRAY') {
        #print STDERR "Adding ParameterValues to GRID\n";

        my $newIndex= $self->addParameterValueToGrid($obj, $allParameterNamesAndUnits, $i, $j);
        $j = $newIndex;
      }

      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::AssayVO') {
        #print STDERR "Adding ASSAY to GRID\n";

        $j = $indexes->{_LABEL_START};
        $j = $self->addLabelToGrid($lex, $i, $j);

        my $newIndex= $self->addAssayToGrid($obj, $allParameterNamesAndUnits, $lex, $i, $j);
        $j = $newIndex;
      }
    }
  }
}

#--------------------------------------------------------------------------------

sub addAssayToGrid {
  my ($self, $assay, $allParametersAndUnits, $lex, $i, $j) = @_;
  
  my $grid = $self->getSdrfGrid();

  my $channelValue = $lex->getChannel->getValue;

  my $assayName = $assay->getName;
  my $assayProtocolName = $assay->getProtocolName;
  my $assayParamValues = $assay->getParameterValues();
  my $arraySourceId = $assay->getArraySourceId();

  my $acquisitions = $assay->getAcquisitions();

  my ($acquisitionName, $acquisitionProtocolName, $acquisitionParamValues, $imageFile);

  my ($quantProtocolName, $quantParamValues, $arrayDataFile);

  my ($normName, $processProtocolName, $processParamValues, $derivedDataFile);

  my $factorValues;

  foreach my $acq (@$acquisitions) {
    my $channels = $acq->getChannels();
    foreach my $channel (@$channels) {
      my $label = $channel->getValue();

      if($channelValue eq $label) {
        $acquisitionName = $acq->getName;

	$factorValues = $acq->getFactorValues;

        $acquisitionProtocolName = $acq->getProtocolName;
        $acquisitionParamValues = $acq->getParameterValues;
        $imageFile = $acq->getUri;


        my $quantifications = $acq->getQuantifications();
        die "More than one quantification found for acquisition [$acquisitionName]" if(scalar @$quantifications > 1);

        my $quantification = $quantifications->[0];
        $quantProtocolName = $quantification->getProtocolName();
        $quantParamValues = $quantification->getParameterValues();
        $arrayDataFile = $quantification->getUri();

        if(my $processes = $quantification->getProcesses()) {
          die "More than one process found for quantification [$arrayDataFile]" if(scalar @$processes > 1);

          my $process = $processes->[0];

          $normName = $process->getName();
          $processProtocolName = $process->getProtocolName();
          $processParamValues = $process->getParameterValues();
          $derivedDataFile = $process->getUri();
        }
      }
    }
  }

  my $assayParamAddToGrid = $self->getUsedParamValues($allParametersAndUnits, $assayParamValues);  
  my $acquisitionParamAddToGrid = $self->getUsedParamValues($allParametersAndUnits, $acquisitionParamValues);  
  my $quantParamAddToGrid = $self->getUsedParamValues($allParametersAndUnits, $quantParamValues);  
  my $processParamAddToGrid = $self->getUsedParamValues($allParametersAndUnits, $processParamValues);  

 
  my @fvs = map {$_->getValue?$_->getValue->getValue:""} @$factorValues;

  my @addToGrid = ($assayProtocolName,
                   @$assayParamAddToGrid,
                   $assayName,
                   $arraySourceId,
                   $acquisitionProtocolName,
                   @$acquisitionParamAddToGrid,
		   @fvs,        
		   $acquisitionName,
	
                   $imageFile,
                   $quantProtocolName,
                   @$quantParamAddToGrid,
                   $arrayDataFile,
                   $processProtocolName,
                   @$processParamAddToGrid,
                   $normName,
                   $derivedDataFile,
                   );
 
  foreach(@addToGrid) {
    $grid->[$i]->[$j] = $_;
    $j++;
  }

  return $j;
}

#--------------------------------------------------------------------------------

sub getUsedParamValues {
  my ($self, $allParametersAndUnits, $paramValues) = @_;

  my %paramValues;
  foreach(@$paramValues) {
    my $name = $_->getParameterName();
    my $value = $_->getValue();

    $paramValues{$name} = $value;
  }

  my @values;

  foreach(@$allParametersAndUnits) {
    my $name = $_->[0];
    my $unit = $paramValues{$name} ? $_->[1] : undef;

    push(@values, $paramValues{$name}, $unit);
  }

  return \@values;
}


#--------------------------------------------------------------------------------

sub addParameterValueToGrid {
  my ($self, $paramValues, $allParametersAndUnits, $i, $j) = @_;

  my $grid = $self->getSdrfGrid();

  my $addToGrid = $self->getUsedParamValues($allParametersAndUnits, $paramValues);

  foreach (@$addToGrid) {
    $grid->[$i]->[$j] = $_;
    $j++;
  }

  return $j;
}

#--------------------------------------------------------------------------------

sub addProtocolToGrid {
  my ($self, $protocol, $i, $j) = @_;

  my $grid = $self->getSdrfGrid();

  my $name = $protocol ? $protocol->getName() : "->";

  $grid->[$i]->[$j] = $name;

  $j++;

  return $j;
}

#--------------------------------------------------------------------------------

sub addBioMaterialToGrid {
  my ($self, $bm, $i, $j) = @_;

  my $grid = $self->getSdrfGrid();
  my $characteristics = $self->getCharacteristics();

  my $name = $bm->getName();
  my $description = $bm->getDescription();

  my $typeOe = $bm->getBioMaterialType();
  my $materialType; $materialType = $typeOe->getValue() if($typeOe);

  my $chars = $bm->getBioMaterialChars() ? $bm->getBioMaterialChars : [];
  
  my %charValues;
  my %termSources;
  foreach my $char (@$chars) {
    my $category = $char->getCategory();
    my $value = $char->getValue();
    my $externalDatabase = $char->getExternalDatabase();

    if($externalDatabase) {
      my $extDbName = $externalDatabase->getName();
      $termSources{$category} = $extDbName;
    }
    $charValues{$category} = $value;
  }

  my @addToGrid = ($name, $description, $materialType);

  for(my $i = 0; $i < scalar @$characteristics; $i++) {
    my $category = $characteristics->[$i];

    push (@addToGrid, $charValues{$category}, $termSources{$category});
  }

  foreach (@addToGrid) {
    $grid->[$i]->[$j] = $_;
    $j++;
  }

  return $j;
}


sub addLabelToGrid {
  my ($self, $bm, $i, $j) = @_;

  my $grid = $self->getSdrfGrid();

  my $channel = $bm->getChannel();
  my $label = $channel->getValue();

  $grid->[$i]->[$j] = $label;

  $j++;

  return $j;
}

#--------------------------------------------------------------------------------

sub makeInternalGrid {
  my ($treatments, $assays) = @_;

  my @rows = ([]);

  foreach my $t (@$treatments) {
    my @inputBioMaterials = map {$_->getBioMaterial()} @{$t->getInputBMMs()};
    my $outputBioMaterial = $t->getOutputBM();
    my $protocol = $t->getProtocol();
    my $parameterValues = $t->getParameterValues();

    foreach my $inputBioMaterial (@inputBioMaterials) {

      my $didSomething;
      foreach my $rowArray (@rows) {
        if(&canAppend($inputBioMaterial, $rowArray)) {
          push @$rowArray, $protocol, $parameterValues, $outputBioMaterial;
          $didSomething = 1;
        }
        if(&canPrepend($outputBioMaterial, $rowArray)) {
          unshift @$rowArray, $inputBioMaterial, $protocol, $parameterValues;
          $didSomething = 1;
        }
      }

      unless($didSomething) {
        my $new = [$inputBioMaterial, $protocol, $parameterValues, $outputBioMaterial];
        push @rows, $new;
      }
    }
  }

  my %prepend;
  for my $i ( 0 .. $#rows ) {

    my $row = $rows[$i];
    my @ar;
    for my $j ( 0 .. $#{$row} ) {

      my $obj = $row->[$j];
      push @ar, $obj;
      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::BioMaterialVO') {
        my $name = $obj->getName();

        push @{$prepend{$name}}, @ar unless(scalar @ar < 1);
        pop  @{$prepend{$name}};
      }
    }
  }

  my %append;
  for(my $i = scalar @rows - 1; $i >= 0; $i--) {

    my $row = $rows[$i];
    next unless($row);
    my @ar;
    for(my $j = scalar @$row - 1; $j >= 0; $j--) {

      my $obj = $row->[$j];
      push @ar, $obj;
      if(ref($obj) eq 'RAD::MR_T::MageImport::VO::BioMaterialVO') {
        my $name = $obj->getName();

        push @{$append{$name}}, @ar unless(scalar @ar < 1);
        pop  @{$append{$name}};
      }
    }
  }

  my %lexToAssay;
  foreach my $assay (@$assays) {
    my $labeledExtracts = $assay->getLabeledExtracts();

    foreach my $lex (@$labeledExtracts) {
      my $lexName = $lex->getName();
      $lexToAssay{$lexName} = $assay;
    }
  }


  foreach my $row (@rows) {
    my $first = $row->[0];
    my $last = $row->[scalar @$row - 1];

    next unless($first && $last);

    my $firstName = $first->getName();
    my $lastName = $last->getName();

    unshift @$row, @{$prepend{$firstName}} if($prepend{$firstName});

    if(scalar @{$append{$lastName}} > 0) {
      push @$row, reverse @{$append{$lastName}} ;
    }

    my $newLast = $row->[scalar @$row - 1];
    my $newLastName = $newLast->getName();

    push @$row, $lexToAssay{$newLastName};
  }

  return \@rows;
}

#--------------------------------------------------------------------------------

sub canAppend {
  my ($bm, $row) = @_;

  my $lastIndex = scalar @$row;
  my $lastElement = $row->[$lastIndex];

  return 0 unless($lastElement);

  if($bm->getName eq $lastElement->getName) {
    return 1;
  }
}

#--------------------------------------------------------------------------------

sub canPrepend {
  my ($bm, $row) = @_;

  my $firstElement = $row->[0];

  return 0 unless($firstElement);

  if($bm->getName eq $firstElement->getName) {
    return 1;
  }
}

#--------------------------------------------------------------------------------

sub makeIndexes {
  my ($protocols, $header) = @_;

  my (@indexes, %index);

  for(my $i = 0; $i < scalar @$header; $i++) {
    my $name = $header->[$i];

    if($name eq 'Protocol REF') {
      push @indexes, $i;
    }

    if($name eq 'Label') {
      $index{_LABEL_START} = $i;
    }
  }

  for(my $i = 0; $i < scalar @$protocols; $i++) {
    my $name = $protocols->[$i]->getName;

    my $index = $indexes[$i];

    $index{$name} = $index;
  }
  return \%index;
}

#--------------------------------------------------------------------------------

sub printGrid {
  my ($self) = @_;

  my @sdrf = @{$self->getSdrfGrid()};

  # If an entire column is empty... then don't print it
  my @counts;
  for my $i ( 0 .. $#sdrf ) {
    my $row = $sdrf[$i];
    my @ar;

    for my $j ( 0 .. $#{$row} ) {
      $counts[$j]++ if($row->[$j]);
    }
  }

  for my $i ( 0 .. $#sdrf ) {
    my $row = $sdrf[$i];
    my @ar;

    for my $j ( 0 .. $#{$row} ) {
      push @ar, $row->[$j] unless($counts[$j] < 2);
    }

    print join("\t", @ar) ."\n";
  }
}

#--------------------------------------------------------------------------------

sub makeHeader {
  my ($protocols, $characteristics, $parameterNamesAndUnits, $voStudy) = @_;

  my @characteristics;
  foreach(@$characteristics) {
    push(@characteristics, 'Characteristics ['. $_ . ']', 'Term Source REF');
  }

  my @parameters;
  foreach(@$parameterNamesAndUnits) {
    my $name = $_->[0];
    my $category = $_->[2];

   push(@parameters, 'Parameter Value [' . $name . ']', 'Unit [' . $category . ']');
  }

  my $header = [];

  push @$header, 'Source Name', 'Description', 'Material Type', @characteristics;

  # Treatments and Protocols
  for(my $i = 0; $i < scalar @$protocols; $i++) {
    my $protocol = $protocols->[$i];

    my $bmType = $i == scalar(@$protocols - 1) ? 'Labeled Extract Name' : 'Sample Name';

    push @$header, 'Protocol REF', @parameters, $bmType, 'Description', 'Material Type', @characteristics;
  }

  my @factorNames = ();
  foreach my $studyDesign (@{$voStudy->getDesigns}) {
    foreach my $studyFactor (@{$studyDesign->getFactors()}) {
        push(@factorNames, "Factor Value [".$studyFactor->getName()."]");
      }
  }

  my @assayToDataHeaders = ('Label', 
                            'Protocol REF', 
                            @parameters,
                            'Hybridization Name',
                            'ArrayDesign REF',
                            'Protocol REF',
                            @parameters,
			    @factorNames,
                            'Scan Name',
		
                            'Image File',
                            'Protocol REF',
                            @parameters,
                            'Array Data File',
                            'Protocol REF',
                            @parameters,
                            'Normalization Name',
                            'Derived Array Data File',
                           );

  push(@$header, @assayToDataHeaders);

  return $header;
}

package NULL_PROTOCOL;

sub getName {$_[0]->{name}}

sub new {
  my ($class, $name) = @_;

  bless {name => $name}, $class;
}



package RAD::MR_T::MageImport::Service::Translator::VoToMageTab;


1;
