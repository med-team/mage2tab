package RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator;

use base qw(RAD::MR_T::MageImport::Service::AbstractTranslator);

use strict;

use Data::Dumper;

use Error qw(:try);

use XML::Simple;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::Util qw(checkArgumentType getPubMedXml findAllAssayBioMaterials);

use GUS::ObjRelP::DbiDatabase;

use GUS::Model::SRes::BibliographicReference;
use GUS::Model::SRes::BibRefType;
use GUS::Model::SRes::Abstract;

use GUS::Model::RAD::AssayParam;
use GUS::Model::RAD::AcquisitionParam;
use GUS::Model::RAD::QuantificationParam;
use GUS::Model::RAD::TreatmentParam;
use GUS::Model::RAD::AssayLabeledExtract;
use GUS::Model::RAD::AssayBioMaterial;

=head1 NAME

RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator

=head1 SYNOPSIS

 my $reader = RAD::MR_T::MageImporter::Service::Reader::MockReader->new();
 my $docRoot = $reader->parse();
 my $translator = RAD::MR_T::MageImporter::Service::Translator::VoToGusTranslator->new($args);
 my $study = $translator->mapAll($docRoot);

=head1 DESCRIPTION

Subclass of AbstractTranslator.  It implements the mapAll method by returning 
a GUS::Model::Study::Study object.  

This class contains 2 static hashes which control which objects are created, whether or
not they are retrieved from the database, and what to do is the retrieveFromDB fails 
(ie. failure to retrieve means either 0 or more than 1 object found with the provided 
attributes).  

methodToObjectMap provides a link between method name ane the type of 
GUS object to be created.  

retrieveObjectRules controls How things are retrieved from the database
Every time a GUS::Model object is created... if that object is a key in 
this hash, an attempt is made to retrieve it from the database.  A value
of 1 means Create NEW if Unable to Retrieve.  A value of 0 means Fail if 
unable to Retrieve.

=cut

my $methodToObjectMap = 
  { mapAffiliations                => 'GUS::Model::SRes::Contact',
    mapPersons                     => 'GUS::Model::SRes::Contact',
    mapExternalDatabase            => 'GUS::Model::SRes::ExternalDatabase',
    mapExternalDatabaseRelease     => 'GUS::Model::SRes::ExternalDatabaseRelease',
    mapStudy                       => 'GUS::Model::Study::Study',
    mapStudyDesigns                => 'GUS::Model::Study::StudyDesign',
    mapStudyFactors                => 'GUS::Model::Study::StudyFactor',
    mapStudyDesignType             => 'GUS::Model::Study::StudyDesignType',
    mapBioSource                   => 'GUS::Model::Study::BioSource',
    mapBioSample                   => 'GUS::Model::Study::BioSample',
    mapLabeledExtract              => 'GUS::Model::Study::LabeledExtract',
    mapProtocols                   => 'GUS::Model::RAD::Protocol',
    mapProtocolParams              => 'GUS::Model::RAD::ProtocolParam',
    mapAssays                      => 'GUS::Model::RAD::Assay',
    mapAcquisitions                => 'GUS::Model::RAD::Acquisition',
    mapQuantifications             => 'GUS::Model::RAD::Quantification',
    mapTreatments                  => 'GUS::Model::RAD::Treatment',
    mapBioMaterialMeasurements     => 'GUS::Model::RAD::BioMaterialMeasurement',
    createRadOE                    => 'GUS::Model::Study::OntologyEntry',
    mapStudyAssays                 => 'GUS::Model::RAD::StudyAssay',
    mapStudyDesignsAssays          => 'GUS::Model::RAD::StudyDesignAssay',
    mapStudyBioMaterials           => 'GUS::Model::RAD::StudyBioMaterial',
    mapLabelMethod                 => 'GUS::Model::RAD::LabelMethod',
    mapArrayDesigns                => 'GUS::Model::RAD::ArrayDesign',
    mapBioMaterialCharacteristics  => 'GUS::Model::Study::BioMaterialCharacteristic',
    mapTaxon                       => 'GUS::Model::SRes::TaxonName',
    mapStudyFactorValues           => 'GUS::Model::RAD::StudyFactorValue',
    __ANON__                       => undef, 
  };


#
my $retrieveObjectsRules = 
  { 'GUS::Model::SRes::Contact' => 1,
    'GUS::Model::SRes::ExternalDatabase' => 0,
    'GUS::Model::SRes::ExternalDatabaseRelease' => 0,
    'GUS::Model::RAD::Protocol' => 1,
    'GUS::Model::RAD::ProtocolParam' => 1,
    'GUS::Model::Study::OntologyEntry' => 0,
    'GUS::Model::RAD::LabelMethod' => 1,
    'GUS::Model::RAD::ArrayDesign' => 0,
    'GUS::Model::SRes::TaxonName' => 0,
  };

#--------------------------------------------------------------------------------

=head2 Subroutines

=over 4

=item C<new>

Create an instance of VoToGusTranslator.

B<Parameters:>

 $class(string): name of the translator 
 $argsHash(hashRef): not used

B<Return type:> 

 C<RAD::MR_T::MageImport::Service::Translator::VoToGusTranslator> 

=cut

sub new {
  my ($class, $argsHash) = @_;

  my $args = ref($argsHash) eq 'HASH' ? $argsHash : {};

  if($args->{allowNewOntologyEntry}) {
    $retrieveObjectsRules->{'GUS::Model::Study::OntologyEntry'} = 1;
    $args->{allowNewExternalDatabase} = 1;
  }

  if($args->{allowNewExternalDatabase}) {
    $retrieveObjectsRules->{'GUS::Model::SRes::ExternalDatabase'} = 1;
    $retrieveObjectsRules->{'GUS::Model::SRes::ExternalDatabaseRelease'} = 1;
  }

  foreach(keys %$methodToObjectMap) { 
    my $gusObj = $methodToObjectMap->{$_};

    unless($_ eq '__ANON__') {
      my $requireStatement = "{require $gusObj}";
      eval $requireStatement; 
    }

    RAD::MR_T::MageImport::ObjectMapperException->new($@)->throw() if($@);
  }

  my $self = bless $args, $class;

  $self->setRetrieveObjectsRules($retrieveObjectsRules);

  if(my $prefix = $args->{quantificationFilePrefix}) {
    $self->setQuantificationPrefix($prefix);
  }

  return($self);
}

#--------------------------------------------------------------------------------

sub setRetrieveObjectsRules { $_[0]->{_retrieve_objects_rules} = $_[1]}
sub getRetrieveObjectsRules { $_[0]->{_retrieve_objects_rules}}

sub setQuantificationPrefix { $_[0]->{_quantification_prefix} = $_[1]}
sub getQuantificationPrefix { $_[0]->{_quantification_prefix}}

# Make a mapping of ExternalDatabase Names to their SRes::ExternalDatabaseRelease Objects
sub setExternalDatabases {
  my ($self, $extDbRls) = @_;

  my %externalDatabases;
  $extDbRls = [] unless($extDbRls);

  foreach my $release (@$extDbRls) {
    my $database = $release->getParent('SRes::ExternalDatabase');
    my $name = $database->getName();

    $externalDatabases{$name} = $release;
  }

  $self->{_external_databases} = \%externalDatabases;
}

sub getExternalDatabases { $_[0]->{_external_databases}}


sub addOntologyEntry {
  my ($self, $oe) = @_;

  push(@{$self->{_ontology_entries}}, $oe);
}

sub getOntologyEntries {$_[0]->{_ontology_entries}}

sub searchForExistingOntologyEntry {
  my ($self, $value, $category) = @_;

  my $logger = $self->getLogger();

  my $ontologyEntries = $self->getOntologyEntries();

  return unless $ontologyEntries;

  my ($onlyValueCount, $onlyValue);

  foreach my $oe (@$ontologyEntries) {
    if($oe->getValue eq $value && $oe->getCategory eq $category) {
      return $oe;
    }
    if($oe->getValue() eq $value) {
      $onlyValueCount++;
      $onlyValue = $oe;
    }
  }
  return $onlyValueCount == 1 ? $onlyValue : undef;
}

#--------------------------------------------------------------------------------


=item C<addMethodToObjectMap>

Should only be used when calling the objectMapper from inside a try block... 
The try block is an annonymous function (__ANON__).  The objectMapper uses a 
method to Object hash to determine which type of object should be created.  
Make sure to set the value to undef when finished.

B<Parameters:>

 $method(string): almost always will be __ANON__
 $object(string): A GUS::Model::xxxx::xxxx object

B<Return Type:> 

 C<object string> 

=cut

sub addMethodToObjectMap {
  my ($self, $method, $object) = @_;
  $methodToObjectMap->{$method} = $object;
}

sub getMethodToObjectMap { return  $methodToObjectMap }

#--------------------------------------------------------------------------------

=item C<objectMapper>

Generically create a GUS object.  Looks at the static hashes to find which object
to create, whether to retrieve it and what to do if the retrieve fails based on 
the caller method

B<Parameters:>

 $hash(hashRef): Attributes for the GUS object

B<Return Type:> 

 C<GUS::Model::xxxx::xxxx> 

=cut

sub objectMapper {
  my ($self, $hash) = @_;

  my $sLogger = $self->getLogger();

  unless(GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    my $msg = "A Default GUS::ObjRelP::DbiDatabase is required but not found";
    RAD::MR_T::MageImport::ObjectMapperException->new($msg)->throw();
  }

  # get the method which called this subroutine
  my ($caller, $file, $line, $subname) = caller(1);
  my ($method) = $subname =~ /(\w+)$/;

  unless(exists $methodToObjectMap->{$method}) {
    my $msg = "Unknown method $method called the objectMapper";
    RAD::MR_T::MageImport::ObjectMapperException->new($msg)->throw();
  }

  my $gusObj = $methodToObjectMap->{$method};

  my $obj = eval {   $gusObj->new($hash) };

  if($@) {
    my $msg = $@ . "\nCould not create the object $gusObj from Translator method $method";

    RAD::MR_T::MageImport::ObjectMapperException->new($msg)->throw();
  }

  my $retrievalRules = $self->getRetrieveObjectsRules();

  my $tryToRetrieve = exists $retrievalRules->{$gusObj};

  $sLogger->debug("+++$gusObj+++\n".$obj->toXML());

  if($tryToRetrieve) { 
    my $createOnNoRetrieve = $retrievalRules->{$gusObj};

    my $wasRetrieved = $obj->retrieveFromDB();
    my $numberRowsRetrieved = $obj->getNumberOfDatabaseRows();

    if(!$wasRetrieved && $numberRowsRetrieved > 1) {
      RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError::MultipleRowsReturned->new("$gusObj retrieved Multiple Rows from DB")->throw();
    }
    elsif(!$wasRetrieved && !$createOnNoRetrieve) { 
      RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError->new("$gusObj not retrieved from Db")->throw();
    }
    else {}
  }



  return($obj);
}

#--------------------------------------------------------------------------------

=item C<mapAll>

This is the public face for the Translator class.  It is the only method
which should be called from another program (other than Tests).  It takes a VO Docroot object
and returns a GUS::Model::Study::Study object with the children set correctly.

B<Parameters:>

 $docRoot(RAD::MR_T::MageImport::VO::DocRoot): The output from the Reader's parse Method

B<Return Type:> 

 C<GUS::Model::Study::Study> 

=cut

sub mapAll {
  my ($self, $docRoot) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->info("start the mapAll method");

 unless(ref($docRoot) eq 'RAD::MR_T::MageImport::VO::DocRoot') {
    RAD::MR_T::MageImport::VOException::ObjectTypeError->
        new("Must provide the docRoot to mapAll Method")->throw();
  }

  my $voAffiliations = $docRoot->affiliationVOs();
  my $voPersons = $docRoot->personVOs();
  my $voExternalDatabases = $docRoot->externalDatabaseVOs();
  my $voStudy = $docRoot->getStudyVO();
  my $voStudyDesigns = $voStudy->getDesigns();
  my $voProtocols = $docRoot->getProtocolVOs();

  my $voAssays = $docRoot->getAssayVOs();
  my $voBioMaterials = $docRoot->getBioMaterialVOs();
  my $voTreatments = $docRoot->getTreatmentVOs();

  my $affiliations = $self->mapAffiliations($voAffiliations);

  my $contacts = $self->mapPersons($voPersons, $affiliations);

  my $externalDbs = $self->mapExternalDatabases($voExternalDatabases);
  $self->setExternalDatabases($externalDbs);

  my $study = $self->mapStudy($voStudy, $contacts);

  my $studyDesigns = $self->mapStudyDesigns($voStudyDesigns, $study);

  my $protocols = $self->mapProtocols($voProtocols);

  my $arrayDesigns = $self->mapArrayDesigns($voAssays);

  my $bioMaterials = $self->mapBioMaterials($voBioMaterials, $contacts);

  my $assays = $self->mapAssays($voAssays, $contacts, $arrayDesigns, $studyDesigns, $protocols, $bioMaterials);

  $self->mapTreatments($voTreatments, $protocols, $bioMaterials);

  $self->mapStudyAssays($study, $assays);
  $self->mapStudyBioMaterials($study, $bioMaterials);
  $self->mapStudyDesignsAssays($studyDesigns, $assays);
  $self->mapAssayBioMaterials($voAssays, $voTreatments, $assays, $bioMaterials);

  return $study;
}

#--------------------------------------------------------------------------------

=item C<mapArrayDesigns>

Loop through the Vo Assays and get a list of GUS ArrayDesigns.  The AssaySourceId
is required for every Assay.

B<Parameters:>

 $voAssays([RAD::MR_T::MageImport::VO::AssayVo] ): ArrayRef of AssayVo objects

B<Return Type:> 

 C<[GUS::Model::RAD::ArrayDesign]> ArrayRef of Distinct GUS ArrayDesign objects

=cut

sub mapArrayDesigns {
  my ($self, $voAssays) = @_;

  my (%seen, @arrayDesigns);
  foreach my $voAssay (@$voAssays) {
    my $sourceId = $voAssay->getArraySourceId();

    next if(exists $seen{$sourceId});

    if($sourceId) {
      my $arrayDesign = $self->objectMapper({source_id => $sourceId});
      push(@arrayDesigns, $arrayDesign);

      $seen{$sourceId} = 1;
    }
  }
  return(\@arrayDesigns);
}

#--------------------------------------------------------------------------------

=item C<mapAffiliations>

Loop through the Vo Affiliations and get a list of GUS Contacts.  

B<Parameters:>

 $mageAffs([RAD::MR_T::MageImport::VO::AffiliationVO] ): ArrayRef of Affiliation VO objects

B<Return Type:> 

 C<[GUS::Model::SRes::Contact]> ArrayRef of GUS Contacts

=cut

sub mapAffiliations { 
  my ($self, $mageAffs) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO affiliations\n",sub {Dumper($mageAffs)});

  my @affiliations;

  foreach my $mageAff (@$mageAffs) {
    my $affiliation = $self->objectMapper({name => $mageAff->getName});

    push @affiliations, $affiliation;
  }
  return \@affiliations;
}

#--------------------------------------------------------------------------------

=item C<mapPersons>

Loop through the Vo Persons and get a list of GUS Contacts

B<Parameters:>

 $magePersons([RAD::MR_T::MageImport::VO::PersonVO] ): ArrayRef of Person VO objects
 $affiliations([GUS::Model::SRes::Contact] ): ArrayRef of GUS Contacts

B<Return Type:> 

 C<[GUS::Model::SRes::Contact]> ArrayRef of GUS Contacts

=cut

sub mapPersons { 
 my ($self, $magePersons, $affiliations) = @_;

 my $sLogger = $self->getLogger();
 $sLogger->debug("VO Persons\n",sub {Dumper($magePersons)});

 my @contacts;

 foreach my $mageContact (@$magePersons) {
   my $contact = $self->objectMapper({last => $mageContact->getLast,
                                      first => $mageContact->getFirst,
                                     });

   $contact->setName($mageContact->getName) unless($contact->getName);
   $contact->setEmail($mageContact->getEmail) unless($contact->getEmail);
   $contact->setAddress1($mageContact->getAddress()) unless($contact->getAddress1);

   if(my $affiliation = $mageContact->getAffiliation()) {
     my $name = $affiliation->getName();
     my $parentAffiliation = $self->searchObjArrayByObjName($affiliations, $name);
     $contact->setParent($parentAffiliation) unless($contact->getParent('SRes::Contact'));
   }

   push @contacts, $contact;
 }

 return(\@contacts);
}

#--------------------------------------------------------------------------------

=item C<mapExternalDatabases>

Loop through the Vo ExternalDatabases and get a list of GUS ExternalDatabaseReleases

B<Parameters:>

 $mageExtDbs([RAD::MR_T::MageImport::VO::ExternalDatabaseVO] ): ArrayRef of ExternalDatabaseVO objects

B<Return Type:> 

 C<[GUS::Model::SRes::ExternalDatabaseRelease]> ArrayRef of GUS ExternalDatabaseRelease

=cut

sub mapExternalDatabases { 
  my ($self, $mageExtDbs) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO ExternalDatabases\n",sub {Dumper($mageExtDbs)});

  my @extDbRls;

  foreach my $mageExtDb (@$mageExtDbs) {
    my $extDb = $self->mapExternalDatabase($mageExtDb->getName);
    my $extDbRls = $self->mapExternalDatabaseRelease($extDb, $mageExtDb->getVersion);

    push @extDbRls, $extDbRls;
  }
  return \@extDbRls;
}

#--------------------------------------------------------------------------------

=item C<mapExternalDatabase>

Calls the objectMapper for ExternalDatabase.  This will be retrieved from the DefaultDB

B<Parameters:>

$name(string): Existing GUS External Database Name

B<Return Type:> 

 C<GUS::Model::SRes::ExternalDatabase> GUS ExternalDatabase

=cut

sub mapExternalDatabase {
  my ($self, $name) = @_;

  return $self->objectMapper({name => $name});
}

#--------------------------------------------------------------------------------

=item C<mapExternalDatabaseRelease>

Calls the objectMapper for ExternalDatabaseRelease.  This will be retrieved from the DefaultDB

B<Parameters:>

 $extDb(GUS::Model::SRes::ExternalDatabase): GUS ExternalDatabase object
 $version(string): Existing GUS External Database Release Version for the External Database

B<Return Type:> 

 C<GUS::Model::SRes::ExternalDatabase> GUS ExternalDatabase

=cut

sub mapExternalDatabaseRelease {
  my ($self, $extDb, $version) = @_;

  my $extDbName = $extDb->getName();
  my $extDbRls;

  if($self->wasRetrieved($extDb)) {
    $extDbRls = $self->objectMapper({version => $version,
                                     external_database_id => $extDb->getId(),
                                    });
  }
  # If we are allowed to make this new...
  elsif($retrieveObjectsRules->{'GUS::Model::SRes::ExternalDatabaseRelease'}) {
    $extDbRls = GUS::Model::SRes::ExternalDatabaseRelease->new({version => $version});
  }
  else {
    RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject->
        new("External Database $extDbName Was not Correctly Retrieved from Database")->throw();
  }

  $extDbRls->setParent($extDb);

  return($extDbRls);
}

#--------------------------------------------------------------------------------

=item C<mapStudy>

Create the main GUS parent object.  Must provide a main contact in the StudyVO.  Searches
the previously made array of GUS contacts for the match

B<Parameters:>

 $mageStudy(RAD::MR_T::MageImporter::StudyVO): Main Study VO object
 $contacts([GUS::Model::SRes::Contact]): List of gus GUS Contact objects to search

B<Return Type:> 

 C<GUS::Model::Study::Study> GUS Study

=cut

sub mapStudy { 
  my ($self, $mageStudy, $contacts) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO Study\n",sub {Dumper($mageStudy)});

  my $study = $self->objectMapper({name => $mageStudy->getName(),
                                   description => $mageStudy->getDescription()
                                  });

  my $name = $mageStudy->getContact()->getName();

  my $contact = $self->searchObjArrayByObjName($contacts, $name);
  $study->setParent($contact);

  if(my $pubmed = $mageStudy->getPubMedId) {
    my $bibRef = $self->mapBibliographicReference($pubmed);

    $study->setParent($bibRef);
  }

  return $study;
}

#--------------------------------------------------------------------------------

=item C<mapBibliographicReference>

Creates a SRes::BibliographicReference (and SRes::Abstract child) from a pubmed id.
Fetches the xml representation of the pubmed reference from ncbi and parses it for 
the relevant SRes fields

B<Parameters:>

 $mageStudy(RAD::MR_T::MageImporter::StudyVO): Main Study VO object

B<Return Type:> 

 C<GUS::Model::Study::Study> GUS Study

=cut

sub mapBibliographicReference {
  my ($self, $pubmed) = @_;

  my $logger = $self->getLogger();

  my $extDbRls;

  my $bibRefType = GUS::Model::SRes::BibRefType->new({name => 'journal article'});
  $bibRefType->retrieveFromDB() or RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError->
                                  new("Could Not Retrieve [journal article] from SRes::BibRefType")->throw();

  my $extDb = GUS::Model::SRes::ExternalDatabase->new({name => 'pubmed'});

  if($extDb->retrieveFromDB()) {
    $logger->debug("Retrieved ExternalDatabase for pubmed");

    my $tooMany;
    ($extDbRls, $tooMany) = $extDb->getChildren('SRes::ExternalDatabaseRelease', 1);

    $logger->debug($extDbRls->toString());

    if($tooMany) {
      $logger->warn("Could Not Retrieve a distinct SRes::ExternalDatabaseRelease::id for pubmed");
      $extDbRls = undef;
    }
  }
  else {
    $logger->warn("Could Not Retrieve [pubmed] from SRes::ExternalDatabase");
  }


  my $xmlString = getPubMedXml($pubmed);
  my $root = XMLin($xmlString);

  my $xml = $root->{PubmedArticle}->{MedlineCitation}->{Article};

  my @authors;
  foreach my $a (@{$xml->{AuthorList}->{Author}}) {
    push(@authors, $a->{LastName}." ".$a->{Initials});
  }

  my $authors = join(', ', @authors);
  my $title =  $xml->{ArticleTitle};

  my $journal = $xml->{Journal}->{Title};
  $journal = $root->{PubmedArticle}->{MedlineCitation}->{MedlineJournalInfo}->{MedlineTA} unless($journal);

  my $year = $xml->{Journal}->{JournalIssue}->{PubDate}->{Year};
  my $volume = $xml->{Journal}->{JournalIssue}->{Volume};
  my $issue = $xml->{Journal}->{JournalIssue}->{Issue};
  my $pages = $xml->{Pagination}->{MedlinePgn};

  unless($issue) {
    $pages = '';
    $logger->warn("Pubication seems to be ahead of print??... No PageNum or Issue");
  }

  my $abstract = $xml->{Abstract}->{AbstractText};

  my $bibRef = GUS::Model::SRes::BibliographicReference->new({title => $title,
                                                              authors => $authors,
                                                              publication => $journal,
                                                              year => $year,
                                                              volume => $volume,
                                                              issue => $issue,
                                                              pages => $pages,
                                                             });

  my $abstract = GUS::Model::SRes::Abstract->new({abstract => $abstract});
  $abstract->setParent($bibRef);

  if($extDbRls) {
    $logger->debug("Setting sourceId and Parent for BibliographicReference");
    $bibRef->setSourceId($pubmed);
    $bibRef->setParent($extDbRls) ;
  }

  $bibRef->setParent($bibRefType);

  return $bibRef;
}

#--------------------------------------------------------------------------------

=item C<mapStudy>

Create the GUS StudyDesigns and set the study parent

B<Parameters:>

 $mageSDs([RAD::MR_T::MageImporter::StudyDesignVO]): ArrayRef of VO StudyDesign objects
 $study(GUS::Model::Study::Study): Parent GUS Study Object

B<Return Type:> 

 C<[GUS::Model::Study::StudyDesign]> ArrayRef of GUS StudyDesigns

=cut

sub mapStudyDesigns { 
  my ($self, $mageSDs, $study) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO StudyDesigns\n",sub {Dumper($mageSDs)});

  my @studyDesigns;

  foreach my $mageSD (@$mageSDs) {
    my $studyDesign = $self->objectMapper({name => $mageSD->getName});
    $studyDesign->setParent($study);

    $self->mapStudyDesignType($studyDesign, $mageSD->getTypes());
    $self->mapStudyFactors($studyDesign, $mageSD->getFactors);

    push(@studyDesigns, $studyDesign);
  }
  return(\@studyDesigns);
}


#--------------------------------------------------------------------------------

=item C<mapStudyDesignType>

Create GUS StudyDesignTypes and set the parent

B<Parameters:>

 $studyDesign(GUS::Model::Study::StudyDesign): StudyDesign Parent of the StudyDesignType
 $types(RAD::MR_T::MageImport::VO::OntologyEntryVO): ArrayRef of OntologyEntry VO objects

B<Return Type:> 

 C<[GUS::Model::Study::StudyDesignType]> GUS StudyDesignTypes

=cut

sub mapStudyDesignType {
  my ($self, $studyDesign, $types) = @_;

  my @studyDesignTypes;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO StudyDesigns Type\n",sub {Dumper($types)});

  foreach my $type (@$types) {
    my $oe = $self->createRadOE($type, "studyDesign");

    my $studyDesignType = $self->objectMapper({});
    $studyDesignType->setParent($studyDesign);
    $studyDesignType->setParent($oe);

    push(@studyDesignTypes, $studyDesignType);
  }

  return \@studyDesignTypes;
}

#--------------------------------------------------------------------------------

=item C<mapStudyFactors>

Create GUS StudyFactors and set the parents (StudyDesign and OntologyEntry).  The
StudyFactor type must be provided in the VO.

B<Parameters:>

 $studyDesign(GUS::Model::Study::StudyDesign): StudyDesign Parent of the StudyFactor
 $mageSFs([RAD::MR_T::MageImport::VO::StudyFactorVO]): StudyFactor VO objects to be mapped

B<Return Type:> 

 C<[GUS::Model::Study::StudyFactor]> GUS StudyFactors

=cut

sub mapStudyFactors { 
  my ($self, $studyDesign, $mageSFs) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO StudyFactors Type\n",sub {Dumper($mageSFs)});

  my @studyFactors;

   foreach my $mageSF (@$mageSFs){
     my $type = $mageSF->getType;
     my $oe = $self->createRadOE($type, "studyFactor");

     my $studyFactor = $self->objectMapper({name => $mageSF->getName()});

     $studyFactor->setParent($studyDesign);
     $studyFactor->setParent($oe);

     push(@studyFactors, $studyFactor);
   }
  return \@studyFactors;
}

#--------------------------------------------------------------------------------

=item C<mapProtocols>

Create GUS Protocols and set parents correctly.  ProtocolType must be provided in the
VO object.  

TODO: Privacy

B<Parameters:>

 $mageProtocols([RAD::MR_T::MageImport::VO::ProtocolVO]): Protocol VO objects to be mapped

B<Return Type:> 

 C<[GUS::Model::RAD::Protocol]> GUS Protocols

=cut

sub mapProtocols { 
 my ($self, $mageProtocols) = @_;

 my $sLogger = $self->getLogger();
 $sLogger->debug("VO Protocols Type\n",sub {Dumper($mageProtocols)});

 my @protocols;

 foreach my $mageProtocol(@$mageProtocols) {
   my $protocol = $self->objectMapper({name => $mageProtocol->getName,
                                      });

   unless($self->wasRetrieved($protocol)) {
     my $protocolType = $mageProtocol->getProtocolType();
     my $oe = $self->createRadOE($protocolType, "protocol");
     my $protocolTypeId = $oe->getId();

     $protocol->setUri($mageProtocol->getUri);
     $protocol->setProtocolDescription($mageProtocol->getProtocolDescription);
     $protocol->setSoftwareDescription($mageProtocol->getSoftwareDescription);
     $protocol->setHardwareDescription($mageProtocol->getHardwareDescription);
     $protocol->setProtocolTypeId($protocolTypeId);
   }

   $self->mapProtocolParams($mageProtocol->getParams, $protocol);

   push(@protocols, $protocol);
 }

 return \@protocols;
}

#--------------------------------------------------------------------------------

=item C<mapProtocolParamss>

If the GUS ProtocolExists...get the Children params and setParent.
If no GUS Protocol...Create GUS ProtocolParams and set parents correctly.  
ProtocolType must be provided in the VO object.  

B<Parameters:>

 $mageProtocolParams([RAD::MR_T::MageImport::VO::ProtocolParamVO]): ProtocolParam VO objects to be mapped
 $protocol(GUS::Model::RAD::Protocol): Parent GUS Protocol

B<Return Type:> 

 C<[GUS::Model::Study::ProtocolParam]> GUS ProtocolParams

=cut

sub mapProtocolParams {
  my ($self, $mageProtocolParams, $protocol) = @_;

  my $protocolName = $protocol->getName();

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO ProtocolParams Type\n",sub {Dumper($mageProtocolParams)});

  my @protocolParams;

  if($self->wasRetrieved($protocol)) {
    # Retrieve the ProtocolParams from the DB...
    @protocolParams = $protocol->getChildren('RAD::ProtocolParam', 1);

    foreach my $mageParam (@$mageProtocolParams) {
      my $name = $mageParam->getName();

      try {
        $self->searchObjArrayByObjName(\@protocolParams, $name);
      } catch RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {
        unless($protocolName =~ /^DTPT series: /) {
          RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject->
              new("Param name $name is not valid for Existing Protocol $protocolName")->throw();
        }
      };
    }
  }
  else {
    foreach my $magePP(@$mageProtocolParams) {

      my ($unitTypeId, $dataTypeId);

      if(my $ut = $magePP->getUnitType()) {
        my $unitTypeOe = $self->createRadOE($ut, "unit");
        $unitTypeId = $unitTypeOe->getId();
      }

      if(my $dt = $magePP->getDataType()) {
        my $dataTypeOe = $self->createRadOE($dt, "data");
        $dataTypeId = $dataTypeOe->getId();
      }
 
      # Must set the oe's with id's because multiple parents of same type
      my $protocolParam = $self->objectMapper({name => $magePP->getName,
                                               value => $magePP->getValue,
                                               data_type_id => $dataTypeId,
                                               unit_type_id => $unitTypeId,
                                            });

      $protocolParam->setParent($protocol);

      push(@protocolParams, $protocolParam);
    }
  }

  return \@protocolParams;
}

#--------------------------------------------------------------------------------

=item C<mapAssays>

Create GUS Assays and set parents (ArrayDesign, operator).  Contacts are searched 
by name and arrayDesigns are searched by sourceId.  The ArrayDesign SourceId must
be found in the Database.  Calls mapAcquisitions to map the children also.

TODO: date

B<Parameters:>

 $mageAssays([RAD::MR_T::MageImport::VO::AssayVO]): Assay VO objects to be mapped
 $contacts([GUS::Model::SRes::Contact]): List of GUS Contacts to search
 $arrayDesigns([GUS::Model::RAD::ArrayDesigns]): List of GUS ArrayDesigns to search

B<Return Type:> 

 C<[GUS::Model::RAD::Assay]> GUS Assays

=cut

sub mapAssays { 
 my ($self, $mageAssays, $contacts, $arrayDesigns, $studyDesigns, $protocols, $bioMaterials) = @_;

 my $sLogger = $self->getLogger();
 $sLogger->debug("VO Assays Type\n",sub {Dumper($mageAssays)});

 my @assays;

 foreach my $mageAssay (@$mageAssays) {
   my $assay = $self->objectMapper({name => $mageAssay->getName});

   my $contactName = $mageAssay->getOperator()->getName();
   my $contact = $self->searchObjArrayByObjName($contacts, $contactName);

   if(my $protocolName = $mageAssay->getProtocolName()) {
     my $protocol = $self->searchObjArrayByObjName($protocols, $protocolName);
     $assay->setParent($protocol);

     my @protocolParameters = $protocol->getChildren('RAD::ProtocolParam');

     foreach my $mageParameterValue (@{$mageAssay->getParameterValues()}) {
       my $parameterName = $mageParameterValue->getParameterName();
       my $value = $mageParameterValue->getValue();

       my $assayParam = GUS::Model::RAD::AssayParam->new({value => $value});
       $assayParam->setParent($assay);

       try {
         my $protocolParam = $self->searchObjArrayByObjName(\@protocolParameters, $parameterName);
         $assayParam->setParent($protocolParam);
       } catch RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {
         my $e = shift;

         $sLogger->fatal("Not assigning ProtocolParamId to AssayParam with name [$parameterName]");
         $e->throw();
       };
     }
   }


   foreach my $mageLex (@{$mageAssay->getLabeledExtracts()}) {
     my $lexName = $mageLex->getName();
     my $lex = $self->searchObjArrayByObjName($bioMaterials, $lexName);

     my $channel = $self->createRadOE($mageLex->getChannel(), 'labelCompound');

     my $assayLex = GUS::Model::RAD::AssayLabeledExtract->new();
     $assayLex->setParent($assay);
     $assayLex->setParent($lex);
     $assayLex->setParent($channel);
   }

   my $arrayDesignSourceId = $mageAssay->getArraySourceId();
   my $arrayDesign = $self->searchObjArrayByObjSourceId($arrayDesigns, $arrayDesignSourceId);

   $assay->setParent($contact);
   $assay->setParent($arrayDesign);

   push(@assays, $assay);

   $self->mapAcquisitions($mageAssay->getAcquisitions, $assay, $studyDesigns, $protocols);
 }
 return \@assays
}

#--------------------------------------------------------------------------------

=item C<mapAcquisitions>

Create GUS Acquisitions and set the parent assay.  Also calls mapQuantifications for the 
children.

TODO: date

B<Parameters:>

 $mageAcquisitions([RAD::MR_T::MageImport::VO::AcquisitionVO]): Acquisition VO objects to be mapped
 $assay(GUS::Model::RAD::Assay): Parent GUS Assay

B<Return Type:> 

 C<[GUS::Model::RAD::Acquisition]> ArrayRef of GUS Acquisitions

=cut

sub mapAcquisitions {
  my ($self, $mageAcquisitions, $assay, $studyDesigns, $protocols) = @_;

  my @acquisitions;

  my $logger = $self->getLogger();

  foreach my $mageAcquisition(@$mageAcquisitions) {
    my $channels = $mageAcquisition->getChannels();
    my $channelNumber = scalar(@$channels);

    foreach my $channel (@$channels) {
      my $channelValue = $channel->getValue();
      my $oe = $self->createRadOE($channel, 'labelCompound');

      my $acquisitionName = $mageAcquisition->getName;

      my $acquisition = $self->objectMapper({name => $acquisitionName,
                                             uri => $mageAcquisition->getUri,
                                            });

      if(my $protocolName = $mageAcquisition->getProtocolName()) {
        my $protocol = $self->searchObjArrayByObjName($protocols, $protocolName);
        $acquisition->setParent($protocol);

        my @protocolParameters = $protocol->getChildren('RAD::ProtocolParam');

        foreach my $mageParameterValue (@{$mageAcquisition->getParameterValues()}) {
          my $parameterName = $mageParameterValue->getParameterName();
          my $value = $mageParameterValue->getValue();

          my $acquisitionParam = GUS::Model::RAD::AcquisitionParam->new({value => $value,
                                                                        name => $parameterName});
          $acquisitionParam->setParent($acquisition);

          try {
            my $protocolParam = $self->searchObjArrayByObjName(\@protocolParameters, $parameterName);
            $acquisitionParam->setParent($protocolParam);
          } catch RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {
            $logger->warn("Not assigning ProtocolParamId to AcquisitionParam with name [$parameterName]");
          };
        }
      }

      $acquisition->setParent($oe);
      $acquisition->setParent($assay);

      $self->mapStudyFactorValues($mageAcquisition->getFactorValues(), $assay, $oe, $studyDesigns, $channelNumber);
      $self->mapQuantifications($mageAcquisition->getQuantifications, $acquisition, $protocols);

      push(@acquisitions, $acquisition);
    }
  }

  return \@acquisitions;
}

#--------------------------------------------------------------------------------

=item C<mapStudyFactorValues>

create gus studyfactorvalues and set studyFactor and ontology entry(s) parents.

B<Parameters:>

 $mageFactorValues([RAD::MR_T::MageImport::VO::FactorValueVO]): ArrayRef of FactorValue VO objects to be mapped
 $assay(GUS::Model::RAD::Assay): Parent GUS Assay
 $channel(GUS::Model::Study::OntologyEntry): Parent GUS OntologyEntry for the channel (category = 'LabelCompound')
 $studyDesigns([GUS::Model::Study::StudyDesign]): ArrayRef of GUS StudyDesign objects

B<Return Type:> 

 C<[GUS::Model::RAD::StudyFactorValue]> ArrayRef of GUS StudyFactorValues

=cut

sub mapStudyFactorValues {
  my ($self, $mageFactorValues, $assay, $channel, $studyDesigns, $channelNumber) = @_;

  my $sLogger = $self->getLogger();

  my @studyFactors;
  foreach my $studyDesign (@$studyDesigns) {
    push(@studyFactors, $studyDesign->getChildren('Study::StudyFactor'));
  }

  my @studyFactorValues;

  foreach my $mageFactorValue (@$mageFactorValues) {
    my $factorName = $mageFactorValue->getFactorName();
    my $studyFactor = $self->searchObjArrayByObjName(\@studyFactors, $factorName);

    my $valueOE = $mageFactorValue->getValue();
    my $stringValue = $valueOE->getValue();

    my $channelId = $channelNumber == 1 ? $channel->getId() : undef;

    my $gusOntologyId;

    try {
      my $oe = $self->createRadOE($valueOE, 'studyFactorValue');
      $gusOntologyId = $oe->getId();
    } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {
      $sLogger->warn("NOT Assigning a RAD::StudyFactorValue::value_ontology_entry_id for [$stringValue]");
    };

    my $studyFactorValue = $self->objectMapper({ value_ontology_entry_id => $gusOntologyId,
                                                 string_value => $stringValue,
                                                 channel_id => $channelId,
                                               });

    my $seen;
    foreach my $prev ($assay->getChildren('RAD::StudyFactorValue')) {
      my $prevSfName = $prev->getParent('Study::StudyFactor')->getName();
      my $prevString = $prev->getStringValue;

      $seen = $prev if($prevString eq $stringValue && $prevSfName eq $studyFactor->getName());
    }

    if($seen) {
      $seen->setChannelId('');
    }
    else {
      $studyFactorValue->setParent($assay);
      $studyFactorValue->setParent($studyFactor);

      push(@studyFactorValues, $studyFactorValue);
    }
  }
  return \@studyFactorValues;
}


#--------------------------------------------------------------------------------

=item C<mapQuantifications>

Create GUS Quantifications and set the parent acquisition

TODO: date

B<Parameters:>

 $mageQuantifications([RAD::MR_T::MageImport::VO::QuantificationVO]): ArrayRef of Acquisition VO objects to be mapped
 $acquisition(GUS::Model::RAD::Acquisition): Parent GUS Acquisition

B<Return Type:> 

 C<[GUS::Model::RAD::Quantification]> ArrayRef of GUS Quantifications

=cut

sub mapQuantifications {
  my ($self, $mageQuantifications, $acquisition, $protocols) = @_;

  my @quantifications;

  my $logger = $self->getLogger();

  foreach my $mageQuantification(@$mageQuantifications) {
    my $quantificationName = $mageQuantification->getName;

    if($mageQuantification->getProtocolName() =~ /^DTPT series: /) {
      $logger->warn("Skipping DataTransformation Quantification [$quantificationName]");
      next;
    }

    my $uri = $self->getQuantificationPrefix() . $mageQuantification->getUri();

    my $quantification = $self->objectMapper({name => $quantificationName,
                                              uri => $uri,
                                             });

    if(my $protocolName = $mageQuantification->getProtocolName()) {
      my $protocol = $self->searchObjArrayByObjName($protocols, $protocolName);
      $quantification->setParent($protocol);

      my @protocolParameters = $protocol->getChildren('RAD::ProtocolParam');

      foreach my $mageParameterValue (@{$mageQuantification->getParameterValues()}) {
        my $parameterName = $mageParameterValue->getParameterName();
        my $value = $mageParameterValue->getValue();

        my $quantificationParam = GUS::Model::RAD::QuantificationParam->new({value => $value, 
                                                                            name => $parameterName,
                                                                            });
        $quantificationParam->setParent($quantification);

        try {
          my $protocolParam = $self->searchObjArrayByObjName(\@protocolParameters, $parameterName);
          $quantificationParam->setParent($protocolParam);
        } catch RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError with {
          $logger->warn("Not assigning ProtocolParamId to QuantificationParam with name [$parameterName]");
        };
      }
    }

    if(my $processes = $mageQuantification->getProcesses()) {
      $self->mapQuantifications($processes, $acquisition, $protocols);
    }

    $quantification->setParent($acquisition);
    push(@quantifications, $quantification);
  }
  return \@quantifications;
}

#--------------------------------------------------------------------------------

=item C<mapBioMaterials>

Create GUS BioMaterials.  SubclassView's of BioSource, BioSample, and LabeledExtract are supported.
If the BioMaterial is a BioSource... a provider contact can be provided.  All BioMaterials
must provide a OntologyEntryVO for Type.

B<Parameters:>

 $mageBioMaterials([RAD::MR_T::MageImport::VO::BioMaterialVO]): ArrayRef of BioMaterial VO objects to be mapped
 $contacts([GUS::Model::SRes::Contact]): ArrayRef of GUS Contacts to search if provider is given

B<Return Type:> 

 C<[GUS::Model::Study::BioMaterial]> ArrayRef of GUS BioMaterials (Different Subclasses)

=cut

sub mapBioMaterials { 
  my ($self, $mageBioMaterials, $contacts) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO BioMaterials Type\n",sub {Dumper($mageBioMaterials)});

  my @modes = qw/BioSample BioSource LabeledExtract/;
  my $modes_rx = '^('. join('|',@modes). ')$';

  my @bioMaterials; 

  foreach my $mageBM (@$mageBioMaterials) {
    my $subclass = $mageBM->getSubclassView;

    $subclass =~ s/^Extract|^Sample/BioSample/;
    $subclass =~ s/^Source/BioSource/;

    unless($subclass =~ /$modes_rx/) {
      RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject->
          new("Subclass [ $subclass ] not supported in GUS")->throw();
    }

    my $subCall = "map$subclass";
    my $bioMaterial = $self->$subCall({name => $mageBM->getName,
                                       subclass_view => $subclass,
                                       description => $mageBM->getDescription,
                                      });

    my $type = $mageBM->getBioMaterialType;
    my $oe = $self->createRadOE($type, "biomaterial");
    $bioMaterial->setParent($oe);

    my $mageContact = $mageBM->getProvider();
    if($mageContact && $bioMaterial->isa('GUS::Model::Study::BioSource')) {
      my $contact = $self->searchObjArrayByObjName($contacts, $mageContact->getName());
      $bioMaterial->setParent($contact);
    }

    my $mageChars = $mageBM->getBioMaterialChars();
    $self->mapBioMaterialCharacteristics($mageChars, $bioMaterial);

    push(@bioMaterials, $bioMaterial);
  }
  return \@bioMaterials;
}

#--------------------------------------------------------------------------------

sub mapBioSource { $_[0]->objectMapper($_[1]) }
sub mapBioSample { $_[0]->objectMapper($_[1]) }
sub mapLabeledExtract { $_[0]->objectMapper($_[1]) }

#--------------------------------------------------------------------------------

=item C<mapBioMaterialCharacteristics>

Gets the Create the BioMaterial Characteristic and sets the OntologyEntry and BioMateial
parents.  

Any values which are not found in the OntologyEntry table are SKIPPED and Logged.

# TODO: VO BMCharacteristic must be an OntologyVO
# TODO: AGE... we are currently skipping

B<Parameters:>

 $mageChars([RAD::MR_T::MageImport::VO::OntologyEntryVO]): ArrayRef of OntologyEntry VO objects
 $bioMaterial([GUS::Model::Study::BioMaterial]): GUS BioMaterial (different subclasses)

B<Return Type:> 

 C<[GUS::Model::Study::BioMaterialCharacteristic]> ArrayRef of GUS BioMaterialCharacteristics

=cut

sub mapBioMaterialCharacteristics {
  my ($self, $mageChars, $bioMaterial) = @_;

  my $bioMaterialName = $bioMaterial->getName();

  my $sLogger = $self->getLogger();
  $sLogger->debug($mageChars);

  my @characteristics;

  foreach my $mageChar (@$mageChars) {
    my $oe;

    my $category = $mageChar->getCategory();

    #next if($category eq 'GeneticModification');
    #next if($category eq 'TimeUnit');
    #next if($category eq 'Age');

    if($category eq 'Organism') {
      $self->mapTaxon($mageChar, $bioMaterial);
    }

    try {
      $oe = $self->createRadOE($mageChar, 'characteristic');

      my $characteristic = GUS::Model::Study::BioMaterialCharacteristic->new();

      $characteristic->setParent($oe);
      $characteristic->setParent($bioMaterial);

      push(@characteristics, $characteristic);

    } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {
      $sLogger->warn("Skipping Characteristic for BioMaterial $bioMaterialName\n", sub {Dumper($mageChar)});
    };

  }

  return \@characteristics;
}


# TODO: POD DOC AND TEST

sub mapTaxon {
  my ($self, $oe, $bioMaterial) = @_;

  my $sLogger = $self->getLogger();

  $self->addMethodToObjectMap ('__ANON__',  'GUS::Model::SRes::TaxonName');

  my $taxonName;

  try {
    $taxonName = $self->objectMapper({name => $oe->getValue()});
    my $taxonId = $taxonName->getTaxonId();

    $bioMaterial->setTaxonId($taxonId);
  } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {
    my $name = $bioMaterial->getName();
    $sLogger->warn("Skipping Setting TaxonId for BioSource:  $name");
  };

  $methodToObjectMap ->{'__ANON__'} = undef;

  return($taxonName);
}



=item C<mapTreatments>

Probably the most complicated code in this class because of the GUS schema for Treatments.
This method will create the treatment (has an output biomaterial), and set the parent protocol (if provided) and labelMethod
if it is a LabeledExtract.  It then calls mapBioMaterialMeasurement which provides the
link for the input biomaterials to the treatment. (This assumes that the labeldextract 
will always have a labelMethod and others don't).

B<Parameters:>

 $mageTreatments([RAD::MR_T::MageImport::VO::TreatmentVO]): ArrayRef of Treatment VO objects to be mapped
 $protocols([GUS::Model::RAD::Protocol]): ArrayRef of GUS Protocols to search 
 $protocols([GUS::Model::Study::BioMaterial]): ArrayRef of GUS BioMaterials to search  (Different Subclasses)

B<Return Type:> 

 C<[GUS::Model::RAD::Treatment]> ArrayRef of GUS Treatments

=cut

sub mapTreatments { 
  my ($self, $mageTreatments, $protocols, $bioMaterials) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO Treatments\n",sub {Dumper($mageTreatments)});

  my @treatments;
  my @labelMethods;

  foreach my $mageTrt(@ $mageTreatments){
    my $treatment = $self->objectMapper({name => $mageTrt->getName,
                                         order_num => $mageTrt->getOrderNum,
                                        });

    my $type = $mageTrt->getTreatmentType;
    my $oe = $self->createRadOE($type, "treatment");
    $treatment->setParent($oe);

    my $outBioMaterialName = $mageTrt->getOutputBM->getName();
    my $bioMaterial = $self->searchObjArrayByObjName($bioMaterials, $outBioMaterialName);
    $treatment->setParent($bioMaterial);

    if($mageTrt->getProtocol) {
      my $protocolName = $mageTrt->getProtocol->getName;
      my $protocol = $self->searchObjArrayByObjName($protocols, $protocolName);
      $treatment->setParent($protocol);

      my @protocolParameters = $protocol->getChildren('RAD::ProtocolParam');

      foreach my $mageParameterValue (@{$mageTrt->getParameterValues()}) {
        my $parameterName = $mageParameterValue->getParameterName();
        my $protocolParam = $self->searchObjArrayByObjName(\@protocolParameters, $parameterName);

        my $value = $mageParameterValue->getValue();
        my $treatmentParam = GUS::Model::RAD::TreatmentParam->new({value => $value});
        $treatmentParam->setParent($treatment);
        $treatmentParam->setParent($protocolParam);
      }

      if($mageTrt->getOutputBM->getSubclassView() eq 'LabeledExtract') {
        my $channel = $mageTrt->getOutputBM->getChannel;
        my $labelMethod = $self->mapLabelMethod($channel, $protocol, \@labelMethods);
        push(@labelMethods, $labelMethod);

        $bioMaterial->setParent($labelMethod);
      }
    }

    my $mageBmms = $mageTrt->getInputBMMs();

    if(scalar(@$mageBmms) > 1 && $oe->getValue() ne 'pool') {
      RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject->
          new("Multiple BioMaterialMeasurements but not pool treatment")->throw();
    }
    $self->mapBioMaterialMeasurements($mageBmms, $treatment, $bioMaterials);

    push(@treatments, $treatment);
  }
  return(\@treatments);
}

#--------------------------------------------------------------------------------

=item C<mapLabelMethod>

Create a GUS LabelMethod object and set the parents.  Look through the RAD::LabelMethods
objects which have already been made.  If one with the same name and the same oe value exists...
just return that one.  
For NEW LabelMethods... check whether the protocol and the ontology entry are existing (ie. have id's),
try to retrieve them (Create new if no retrieve).  
If either protocol or oe doesn't have an ID... just create
new.

B<Parameters:>

 $channel(RAD::MR_T::MageImport::VO::OntologyEntryVO): OntologyEntryVO object which of category 'LabelCompound'
 $protocol(GUS::Model::RAD::Protocol): Parent GUS Protocol

B<Return Type:> 

 C<GUS::Model::RAD::LabelMethod> GUS LabelMethod object

=cut

sub mapLabelMethod {
  my ($self, $channel, $protocol, $labelMethods) = @_;

  my $oe = $self->createRadOE($channel, 'labelCompound');

  my $protocolId = $protocol->getId;
  my $oeId = $oe->getId;

  # Look through existing label methods for one with the same protocol and ontology
  # MUST use the names and not ID because they may be NEW!!!
  foreach my $prev (@$labelMethods) {
    my $prevProt = $prev->getParent('RAD::Protocol');
    my $prevOE = $prev->getParent('Study::OntologyEntry');

    if($prevProt->getName() eq $protocol->getName() && $oe->getValue() eq $prevOE->getValue()) {
      return($prev);
    }
  }

  my $labelMethod;

  # If this is an existing protocol and ontology try to retrieve from the DB
  if($protocolId && $oeId) {
    $labelMethod = $self->objectMapper({protocol_id => $protocolId,
                                        channel_id => $oe->getId,
                                       });
  }
  else {
    $labelMethod = GUS::Model::RAD::LabelMethod->new();
  }

  $labelMethod->setParent($protocol);
  $labelMethod->setParent($oe);

  return $labelMethod;
}

#--------------------------------------------------------------------------------

=item C<mapBioMaterialMeasurements>

Create GUS BioMaterialMeasurment objects and set their parents (treatment, ontologyentry?, and biomaterial).

B<Parameters:>

 $mageBmms([RAD::MR_T::MageImport::VO::BioMaterialMeasurementVO]): ArrayRef of BioMaterialMeasurement VO objects
 $treatment(GUS::Model::RAD::Treatment): Parent GUS Treatment
 $bioMaterials(GUS::Model::Study::BioMaterial): ArrayRef of GUS biomaterial objects to search (Parent)

B<Return Type:> 

 C<[GUS::Model::RAD::BioMaterialMeasurement]> ArrayRef of GUS BioMaterialMeasurement objects

=cut

sub mapBioMaterialMeasurements {
  my ($self, $mageBmms, $treatment, $bioMaterials) = @_;

  my @bioMaterialMeasurements;

  foreach my $mageBmm (@$mageBmms) {

    my $bioMaterialMeasurement = $self->objectMapper({value => $mageBmm->getValue});

    $bioMaterialMeasurement->setParent($treatment);

    my $bioMaterialName = $mageBmm->getBioMaterial()->getName();
    my $bioMaterial = $self->searchObjArrayByObjName($bioMaterials, $bioMaterialName);

    $bioMaterialMeasurement->setParent($bioMaterial);

    if(my $ut = $mageBmm->getUnitType) {
      my $oe = $self->createRadOE($ut, "unit");
      $bioMaterialMeasurement->setParent($oe);
    }

    push(@bioMaterialMeasurements, $bioMaterialMeasurement);
  }

  return \@bioMaterialMeasurements;
}

#--------------------------------------------------------------------------------

=item C<createRadOE>

use case: it is hard to manually fix OE across MAGGE-ML, but if its value is right and unique, 
          we should accept it even its cateogry is wrong

side effect: 1) retrieve OE based on value first, if fails, then retrieve based on value and category
             2) we have to do $self->addMethodToObjectMap ('__ANON__',  'GUS::Model::Study::OntologyEntry') to make try catch works
             #) are some of logical should move to processor?

Ontology Entry Objects should NEVER be modified!!!

B<Parameters:>

 $mageOe([RAD::MR_T::MageImport::VO::OntologyEntryVO]): OntologyEntry VO object
 $type(string):  supported mode to distinguish between callers

B<Return Type:> 

 C<GUS::Model::Study::OntologyEntry> GUS OntologyEntry Object.

=cut

sub createRadOE{
  my ($self, $mageOE, $type) = @_;

  my $sLogger = $self->getLogger();
  $sLogger->debug("VO OntologyEntry of type $type\n",sub {Dumper($mageOE)});  

  unless($mageOE->isa('RAD::MR_T::MageImport::VO::OntologyEntryVO')) {
    my $msg = "Expected OntologyEntryVO but found: ". ref($mageOE);
    RAD::MR_T::MageImport::ObjectMapperException->new($msg)->throw();
  }

  my @modes    = qw( studyDesign studyFactor treatment protocol unit data biomaterial labelCompound characteristic studyFactorValue);
  my $modes_rx = '^('. join('|',@modes). ')$';

  unless($type =~ /$modes_rx/) {
    RAD::MR_T::MageImport::ObjectMapperException->new("Type [ $type ] is not supported")->throw();
  }

  my $radOE;
  my $value = $mageOE->getValue();
  my $category = $mageOE->getCategory();

  $category = 'ExperimentalProtocolType' if($type eq 'studyFactor' && $value eq 'genetic_modification');
  $category = 'BioMaterialCharacteristicCategory' if($type eq 'studyFactor' && $value eq 'organism_part');

  if($radOE = $self->searchForExistingOntologyEntry($value, $category)) {
    return $radOE;
  }

  my $externalDatabases = $self->getExternalDatabases();

  $self->addMethodToObjectMap ('__ANON__',  'GUS::Model::Study::OntologyEntry');

  unless($category) {
    $radOE = $self->objectMapper({value => $value });

    # If there is no category after the retrieve... Throw an Exception
    RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError->
        new("You Must provide a Category for the OntologyEntry [$value] of type [$type]")->throw() unless($radOE->getCategory());
  }
  else {

    try {
      $radOE = $self->objectMapper({value => $mageOE->getValue,
                                    category => $category,
                                   });

    } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError::MultipleRowsReturned with {
      my $e = shift;
      $e->throw();

    } catch RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError with {
      my $e = shift;

      if($type eq 'treatment' && $category eq "Action") {
        $radOE = $self->objectMapper({value => $mageOE->getValue, 
                                      category => 'ComplexAction',
                                     });
      }
      elsif($type eq "protocol" && $category eq "ProtocolType") {
        $radOE = $self->objectMapper({value => $mageOE->getValue, 
                                      category => 'ExperimentalProtocolType',
                                     });
      }
      else {
        $sLogger->debug($e->stacktrace());
        $e->throw();
      }
    };
  }

  $methodToObjectMap ->{'__ANON__'} = undef;

  unless($self->wasRetrieved($radOE)) {
    if(my $voExtDb = $mageOE->getExternalDatabase()) {

     my $extDbName = $voExtDb->getName();

      if(my $gusExtDbRls = $externalDatabases->{$extDbName}) {
        $radOE->setUri($voExtDb->getUri());

        my $sourceId = "#" . $value;
        $radOE->setSourceId($sourceId);

        $radOE->setParent($gusExtDbRls);
      } 
      else {
        RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject->
            new("The SRes::ExternalDatabase [$extDbName] was not created prior to making OntologyEntry [$value]")->throw();
      }
    }
    else {
      $sLogger->warn("New Ontology Entry [$value] Being Created Without An ExternalDatabase");
      $radOE->setName('user_defined');
    }
  }

  # Keep running list of seen ontology entries
  $self->addOntologyEntry($radOE);

  return $radOE;
}

#--------------------------------------------------------------------------------

=item C<mapStudyAssays>

Create GUS StudyAssay object and set parents (Study and Assay)

B<Parameters:>

 $study(GUS::Model::Study::Study): Main Study Object
 $assays([GUS::Model::RAD::Assay]): ArrayRef of GUS Assay objects

B<Return Type:> 

 C<[GUS::Model::RAD::StudyAssay]> ArrayRef of GUS StudyAssay Objects

=cut

sub mapStudyAssays {
  my ($self, $study, $assays) = @_;

  my @studyAssays;

  foreach my $assay (@$assays) {
    my $sa = $self->objectMapper();
    $sa->setParent($assay);
    $sa->setParent($study);

    push(@studyAssays, $sa);
  }
  return(\@studyAssays);
}

#--------------------------------------------------------------------------------

=item C<mapStudyBioMaterials>

Create GUS StudyBioMaterial object and set parents (Study and BioMaterial)

B<Parameters:>

 $study(GUS::Model::Study::Study): Main Study Object
 $bioMaterials([GUS::Model::Study::BioMaterial]): ArrayRef of GUS BioMaterial objects (Different SubClasses)

B<Return Type:> 

 C<[GUS::Model::RAD::StudyBioMaterial]> ArrayRef of GUS StudyBioMaterial Objects

=cut

sub mapStudyBioMaterials {
 my ($self, $study, $bioMaterials) = @_;

 my @studyBioMaterials;

  foreach my $bioMaterial (@$bioMaterials){
    my $sb = $self->objectMapper();
    $sb->setParent($bioMaterial);
    $sb->setParent($study);

    push(@studyBioMaterials, $sb);
  }
 return \@studyBioMaterials;
}

#--------------------------------------------------------------------------------

=item C<mapStudyDesignsAssays>

Create GUS StudyDesignAssay objects and set parents (StudyDesign and Assay)

B<Parameters:>

 $studyDesigns([GUS::Model::Study::StudyDesign]): ArrayRef of GUS StudyDesign Objects
 $assays([GUS::Model::RAD::Assay]): ArrayRef of GUS Assay objects 

B<Return Type:> 

 C<[GUS::Model::RAD::StudyDesignAssay]> ArrayRef of GUS StudyDesignAssay objects

=cut

sub mapStudyDesignsAssays {
  my ($self, $studyDesigns, $assays) = @_;

  my @studyDesignAssays;

  foreach my $studyDesign (@$studyDesigns) {
    foreach my $assay (@$assays) {
      my $sda = $self->objectMapper();
      $sda->setParent($studyDesign);
      $sda->setParent($assay);

      push(@studyDesignAssays, $sda);
    }
  }
  return \@studyDesignAssays;
}


#--------------------------------------------------------------------------------

=item C<mapAssayBioMaterials>

Create GUS AssayBioMaterial objects and set parents (BioMaterial and Assay)

B<Parameters:>

 $voAssays([RAD::MR_T::MageImport::VO::AssayVO]): ArrayRef of Assay VO objects
 $voTreatmentss([RAD::MR_T::MageImport::VO::TreatmentVO]): ArrayRef of Treatment VO objects
 $assays([GUS::Model::RAD::Assay]): ArrayRef of GUS Assay objects 
 $bioMaterials([GUS::Model::Study::BioMaterial]): ArrayRef of GUS BioMaterial objects 

B<Return Type:> 

 C<[GUS::Model::RAD::AssayBioMaterial]> ArrayRef of GUS AssayBioMaterial objects

=cut

sub mapAssayBioMaterials {
  my ($self, $voAssays, $voTreatments, $assays, $bioMaterials) = @_;

  my @assayBioMaterials;

  foreach my $voAssay (@$voAssays) {
    my $assay = $self->searchObjArrayByObjName($assays, $voAssay->getName());
    my $voBioMaterials = findAllAssayBioMaterials($voAssay, $voTreatments);

    foreach my $voBioMaterial (@$voBioMaterials) {
      my $bioMaterial = $self->searchObjArrayByObjName($bioMaterials, $voBioMaterial->getName());

      my $assayBioMaterial = GUS::Model::RAD::AssayBioMaterial->new();
      $assayBioMaterial->setParent($assay);
      $assayBioMaterial->setParent($bioMaterial);

      push(@assayBioMaterials, $assayBioMaterial);
    }
  }
  return \@assayBioMaterials;
}

#--------------------------------------------------------------------------------

=head2 Utility Methods

=item C<searchObjArrayByObjName>

Searches an Array for an object of a certain name

B<Parameters:>

 $objArray([]): ArrayRef of objects (should be of the same type) with getName method
 $name(string): string to match

B<Return Type:> 

 C<Object> Whatever type of object populated the input array

=cut

sub searchObjArrayByObjName {
 my ($self, $objArray, $name) = @_;

 my (@ar, $type);

 foreach my $obj(@$objArray){
   $type = ref($obj);

   push(@ar, $obj) if($obj->getName eq $name);
 }

 if(scalar(@ar) > 1) {
    RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError->
        new("Could not find a DISTINCT object with name $name of type $type")->throw();
 }
 if(scalar(@ar) == 0) {
    RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError->
        new("Could not find ANY object with name $name of type $type")->throw();
 }

 return $ar[0];
}

#--------------------------------------------------------------------------------

=item C<searchObjArrayBySourceId>

Searches an Array for an object of a certain sourceId

B<Parameters:>

 $objArray([]): ArrayRef of objects (should be of the same type) with getName method
 $sourceId(string): string to match

B<Return Type:> 

 C<Object> Whatever type of object populated the input array

=cut

sub searchObjArrayByObjSourceId {
 my ($self, $objArray, $sourceId) = @_;

 my (@ar, $type);

 foreach my $obj(@$objArray){
   $type = ref($obj);

   push(@ar, $obj) if($obj->getSourceId eq $sourceId);
 }

 if(scalar(@ar) > 1) {
    RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError->
        new("Could not find a DISTINCT object with source_id $sourceId of type $type")->throw();
 }
 if(scalar(@ar) == 0) {
    RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError->
        new("Could not find ANY object with source_id $sourceId of type $type")->throw();
 }

 return $ar[0];

}

#--------------------------------------------------------------------------------

=item C<wasRetrieved>

Should Only be used for GUS objects.  If an object has 'Id'... it must have been 
retrieved from the database.

B<Parameters:>

 $objArray(GUS::Model::xxxx::xxxx): Any kind of GUS object

B<Return Type:> 

 C<boolean> 

=cut

sub wasRetrieved {
  my ($self, $object) = @_;

  if($object->getId()) {
    return 1;
  }
  return 0;
}

#--------------------------------------------------------------------------------

=item C<camelCapToUnderscore>

CamelCaps are words strung together with the first letter capitalized in each.  
This method changes to all lower case and puts an underscore in front of any character
which was capitalized.

B<Parameters:>

 $string(scalar): word in Camel Caps

B<Return Type:> 

 C<string> 

=cut

sub camelCapsToUnderscore {
  my ($self, $string) = @_;

  my $rv;
  foreach (split ("", $string)) {

    if(tr/A-Z/a-z/) {
      $_ = '_'.$_
    }
    $rv = $rv . $_;
  }

  # Remove the first underscore
  $rv =~ s/^_//;

  return $rv;
}


1;
