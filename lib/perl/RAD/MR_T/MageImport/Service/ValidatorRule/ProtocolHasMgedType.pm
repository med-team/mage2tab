package RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::ProtocolHasMgedType->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

The ProtocolType should be coming from the MGED Ontology

This is Required by the Gus Schema

=cut

#--------------------------------------------------------------------------------

# How else could we get these???  Ideally we don't want to connect to 
# our database instance.... Another option:  there is Existing Java code 
# in GUS/Supported to parse an MGED Owl file.  The terms listed below 
# are very stable and that may not be necessary
my $experimenatProtocolTypes =  ['PCR_amplification',
                                 'acclimatization',
                                 'array_manufacturing_protocol',
                                 'behavioral_stimulus',
                                 'biological_fluid_collection',
                                 'change_biomaterial_characteristics',
                                 'compound_based_treatment',
                                 'decontaminate',
                                 'dissect',
                                 'element_design_protocol',
                                 'feature_extraction',
                                 'fractionate',
                                 'genetic_modification',
                                 'grow',
                                 'harvest',
                                 'histological_slide_preparation',
                                 'hybridization',
                                 'image_acquisition',
                                 'incubate',
                                 'infect',
                                 'irradiate',
                                 'labeling',
                                 'linear_amplification',
                                 'nucleic_acid_extraction',
                                 'pool',
                                 'preservation',
                                 'purify',
                                 'reverse_transcription',
                                 'sacrifice',
                                 'specified_biomaterial_action',
                                 'split',
                                 'starvation',
                                 'store',
                                 'transfect',
                                 'unknown_protocol_type',
                                 'wash',
                                ];

#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();

  $self->_setExperimenatProtocolTypes;
 # $logger->infor("****Dump the config****", sub { Dumper($experimenatProtocolTypes ) });
  my $protocols = $docRoot->getProtocolVOs();

  unless($protocols) {
    RAD::MR_T::MageImport::ValidatorException->new("No Protocols Were Found")->throw();
  }

  foreach my $protocol (@$protocols) {
    my $name = $protocol->getName();
    my $type = $protocol->getProtocolType();
    my $typeValue = $type->getValue();

    unless($self->_isMgedTerm($typeValue, $name)) {
      $logger->warn("ProtocolType [$typeValue] for Protocol [$name] is not supported by MGED");
      RAD::MR_T::MageImport::ValidatorException->new("ProtocolType [$typeValue] for Protocol [$name] is not supported by MGED")->throw();
    }
  }

  return 1;
}

#--------------------------------------------------------------------------------

sub _isMgedTerm {
  my ($self, $value, $name) = @_;

  my $logger = $self->getLogger();


  foreach my $mged (@$experimenatProtocolTypes) {
    if($mged eq $value) {
      $logger->warn("Type [$mged] is allowed BUT highly discouraged for Protocol [$name]") if($value eq 'specified_biomaterial_action');
      return 1;
    }
  }
  return 0;
}

sub _setExperimenatProtocolTypes {
 my $url = "http://hera.pcbi.upenn.edu/rad-dev/Querier/php/moTermQuery.php?category=ExperimentalProtocolType";
 my $resultXML = get($url);

 my $root = XMLin($resultXML);

 $experimenatProtocolTypes = $root->{results}->{term} if $root->{results}->{term};

}


1;
