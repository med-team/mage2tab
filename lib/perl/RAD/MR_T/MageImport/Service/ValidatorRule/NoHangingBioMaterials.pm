package RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials;

use strict 'vars';

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

my %distinctBioMaterialNames;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::NoHangingBioMaterials->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

A "Hanging BioMaterial" is one which is never the input to a Treatment and is not a LabeledExtract.  
This Class checks that all biomaterials which are not LabeledExtract's are the input to at least one
Treatment.  A check is also made comparing Distinct BioMaterials used in Treatments to the 
total number of DocRoot BioMaterialVOs (This checks for a "Hanging LabeledExtract" or an entirely unused
treatment Series)

=cut

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();
  %distinctBioMaterialNames = ();

  my $bioMaterials = $docRoot->getBioMaterialVOs();
  my $treatments = $docRoot->getTreatmentVOs();

  unless($treatments) {
    RAD::MR_T::MageImport::ValidatorException->new("No Treatments Found for Study")->throw();
  }

  unless($bioMaterials) {
    RAD::MR_T::MageImport::ValidatorException->new("No BioMaterials Found for Study")->throw();
  }

  foreach my $bm (@$bioMaterials) {
    if($bm->getSubclassView() =~ /Source/i) {
      $self->checkTreatmentsAsTree($treatments, $bm);
    }
  }

  my $numberBioMaterials = scalar(@$bioMaterials);
  my $numberTreatmentBioMaterials = scalar(keys %distinctBioMaterialNames);

  unless($numberBioMaterials == $numberTreatmentBioMaterials) {
    $logger->warn("Found [$numberTreatmentBioMaterials] BioMaterials linked to Treatments and [$numberBioMaterials] total");
    RAD::MR_T::MageImport::ValidatorException->new("Found [$numberTreatmentBioMaterials] BioMaterials linked to Treatments and [$numberBioMaterials] total")->throw();
  }

  return 1;
}

#--------------------------------------------------------------------------------

sub checkTreatmentsAsTree {
  my ($self, $treatments, $bm) = @_;
  my $logger = $self->getLogger();
  my $name = $bm->getName();
  $distinctBioMaterialNames{$name} = 1;

  my $outputBioMaterials = $self->getAllOutputBioMaterials($treatments, $bm);

  if(scalar(@$outputBioMaterials) == 0 && $bm->getSubclassView() ne 'LabeledExtract') {
    $logger->warn("BioMaterial [$name] has no Treatments (Hanging) and is NOT a LabeledExtract");
    RAD::MR_T::MageImport::ValidatorException->new("BioMaterial [$name] has no Treatments (Hanging) and is NOT a LabeledExtract")->throw();
  }

  foreach my $out (@$outputBioMaterials) {
    $self->checkTreatmentsAsTree($treatments, $out);
  }
}

#--------------------------------------------------------------------------------

sub getAllOutputBioMaterials {
  my ($self, $treatments, $bm) = @_;

  my @outputBioMaterials;

  foreach my $treatment (@$treatments) {
    my @inputBioMaterials = map { $_->getBioMaterial() } @{$treatment->getInputBMMs()};

    foreach my $input (@inputBioMaterials) {
      if($input->getName() eq $bm->getName()) {
        push(@outputBioMaterials, $treatment->getOutputBM());
      }
    }
  }
  return \@outputBioMaterials;
}

1;
