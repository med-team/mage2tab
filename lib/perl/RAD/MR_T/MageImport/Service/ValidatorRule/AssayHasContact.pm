package RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact;

use strict 'vars';

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

Requires each Assay in the DocRoot to have an Operator set.  

This is Required by the Gus Schema

=cut

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();
  my $assays = $docRoot->getAssayVOs();


  unless($assays) {
    RAD::MR_T::MageImport::ValidatorException->new("No Assays Were Found")->throw();
  }

  foreach my $assay (@$assays) {
    my $name = $assay->getName();

    unless($assay->getOperator()) {
      $self->appendResportStr("No Operator Found for Assay [$name]\n");
      $logger->warn("No Operator Found for Assay [$name]");
      RAD::MR_T::MageImport::ValidatorException->new("No Operator Found for Assay [$name]")->throw();
    }
  }

  return 1;
}


1;
