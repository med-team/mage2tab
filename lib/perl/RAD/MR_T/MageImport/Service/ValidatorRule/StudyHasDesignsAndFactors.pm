package RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasDesignsAndFactors;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasDesignsAndFactors

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasDesignsAndFactors->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

The Study must have designs and factors

=cut

#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();

  my $studyDesigns = $docRoot->getStudyVO()->getDesigns;

  unless($studyDesigns) {
    $logger->warn("No study designs Were Found");
    RAD::MR_T::MageImport::ValidatorException->new("No study designs Were Found")->throw();
  }

  foreach my $studyDesign (@$studyDesigns) {
    my $types = $studyDesign->getFactors();
    unless($types){
      $logger->warn("No study factors were found");
      RAD::MR_T::MageImport::ValidatorException->new("No study factors were found")->throw();
    }
  }

  return 1;
}



1;
