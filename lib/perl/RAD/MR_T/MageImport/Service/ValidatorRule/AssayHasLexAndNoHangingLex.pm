package RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex;

use strict 'vars';

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasLexAndNoHangingLex->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

Require that each AssayVO has at least one LabeledExtract set.  Will also check for Hanging LabeledExtracts by 
comparing Total LabeledExtracts and distinct LabeledExtracts linked to AssayVOs

=cut

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();
  my $assays = $docRoot->getAssayVOs();
  my $bioMaterials = $docRoot->getBioMaterialVOs();

  unless($assays) {
    RAD::MR_T::MageImport::ValidatorException->new("No Assays Found for Study")->throw();
  }

  unless($bioMaterials) {
    RAD::MR_T::MageImport::ValidatorException->new("No BioMaterials Found for Study")->throw();
  }

  my $numberLex;
  foreach my $bm (@$bioMaterials) {
    next unless($bm->getSubclassView eq 'LabeledExtract');
    $numberLex++;
  }

  my %lexNames;
  foreach my $assay (@$assays) {
    my $lexs = $assay->getLabeledExtracts();
    my $assayName = $assay->getName();

    unless($lexs) {
      $self->appendResportStr("No LabeledExtract Assigned to Assay [$assayName]");
      $logger->warn("No LabeledExtract Assigned to Assay [$assayName]");
      RAD::MR_T::MageImport::ValidatorException->new("No LabeledExtract Assigned to Assay [$assayName]")->throw();
    }

    foreach my $lex (@$lexs) {
      my $name = $lex->getName();
      $lexNames{$name} = 1;
    }
  }

  my $distinctLexNames = scalar(keys %lexNames);

  unless($distinctLexNames == $numberLex) {
    $logger->warn("Found [$numberLex] LabeledExtracts but [$distinctLexNames] were assigned to an Assay");
    $self->appendResportStr("Found [$numberLex] LabeledExtracts but [$distinctLexNames] were assigned to an Assay");
    RAD::MR_T::MageImport::ValidatorException->new("Found [$numberLex] LabeledExtracts but [$distinctLexNames] were assigned to an Assay")->throw();
  }

  return 1;
}


1;
