package RAD::MR_T::MageImport::Service::ValidatorRule::CheckDataFiles;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::CheckDataFiles

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::CheckDataFiles->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

The number of data rows excluding header rows should be equal to the number of probe sets as in db

=cut
#-------------------------------------------------------------------------------

my $gusProtocol = {
		   name=>"",
		   param=>[
			   {name=>[]}
			  ],

};


#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();

  my $assays = $docRoot->getAssayVOs();
  RAD::MR_T::MageImport::ValidatorException->new("No Assays Were Found")->throw() unless $assays;

  foreach my $assay (@$assays){
    my $array;
    $logger->warn("\narray source id: ", $assay->getArraySourceId);
    if($array = $self->_isGusArray($assay->getArraySourceId)){
      $logger->warn("Number of elements:", $array->{number_of_elements}, "\tarray name: ", $array->{name});
    }

    my $acquisitions = $assay->getAcquisitions();
    next unless $acquisitions;

    foreach my $acquisition (@$acquisitions){
      my $quantifications = $acquisition->getQuantifications();
      next unless $quantifications;
      foreach my $quantification (@$quantifications){
	$logger->warn("array data file",$quantification->getUri, "\tprotocol: ", $quantification->getProtocolName);

	my $file=$quantification->getUri;
	if(my $number = $self->_fileCounter($file)){
	  $logger->warn("number of line in file", $number);

	  if($quantification->getProtocolName =~ /Probe Cell Analysis/i){
	    $self->_checkCELFile($file, $array->{name});
	  }
	  if($quantification->getProtocolName =~ /GenePix/i){
	  # check gene pix file
	  }
	}

	my $processes = $quantification->getProcesses();
	next unless $processes;
	foreach my $process (@$processes){
	  $logger->warn("derived data file",$process->getUri, "\tProtocol: ", $process->getProtocolName);
	  my $file=$process->getUri;
	  if(my $number = $self->_fileCounter($file)){$logger->warn("number of line in file", $number);}
	}
      }
    }
  }

  return 1;
}

#-------------------------------------------------
sub _checkCELFile{
  my ($self, $file, $arrayName) = @_;
  my $logger = $self->getLogger();

  my @nameStr = split(' ', $arrayName);

  open(FILE, $file);

  while(<FILE>) {
   chomp;
   if($_ =~ /DatHeader/){ 

     if($_ =~ /$nameStr[1]/){$logger->warn("Affy Chip Name matches inside CEL file: ", $nameStr[1]);}
     else{$logger->warn("Affy Chip Name doesn't match inside CEL file: ", $nameStr[1]);}

     last;
   }
 }
 close(FILE);

#  print scalar(@fileLines);

#  $logger->warn($fileLines[scalar(@fileLines)-2]);

}


#------------------------------------------------
sub _fileCounter{
  my ($self, $file) = @_;
  my $logger = $self->getLogger();

  my $lines = 0;
  my $buffer;
  open(FILE, $file) or $logger->warn("Can't open `$file': $!");
  while (sysread FILE, $buffer, 4096) {
    $lines += ($buffer =~ tr/\n//);
  }
  close FILE;

  return $lines;

#  my $counter =0 ;
#  open(FILE, "< $file") or {$logger->warn("cann't open $file")};
#  $counter++ while <FILE>;
#  return $counter;
}
#--------------------------------------------------------------------------------

sub _isGusArray {
  my ($self, $name) = @_;

  my $url = "http://hera.pcbi.upenn.edu/rad-dev/Querier/php/radArrayQuery.php?source_id=$name";
  my $resultXML = get($url);
  my $root = XMLin($resultXML);

  return $root->{results}->{array};
}


1;
