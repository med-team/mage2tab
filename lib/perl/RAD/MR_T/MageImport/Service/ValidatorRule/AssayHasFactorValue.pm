package RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasFactorValue;

use strict 'vars';

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::AssayHasContact->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

Requires each Assay in the DocRoot to have an Operator set.  

This is Required by the Gus Schema

=cut

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();
  my $assays = $docRoot->getAssayVOs();
  my $passed = 1;

  unless($assays) {
    $passed = 0;
    $logger->warn("No Assays Were Found");
  }

  foreach my $assay (@$assays) {
    my $name = $assay->getName();
    my $acqs = $assay->getAcquisitions;

    unless ($acqs){
      $logger->warn("No Acquisitions Found for Assay [$name]"); 
      $passed = 0;
    }

    foreach my $acq (@$acqs){
      unless ($acq->getFactorValues){
	$passed = 0;
	$logger->warn("No Factorvalues Found for Assay [$name]"); 
      }
    }
  }


  RAD::MR_T::MageImport::ValidatorException->new("AssayHasFactorValue validation failed, please see log file for details")->throw() unless $passed;
  return 1;
}


1;
