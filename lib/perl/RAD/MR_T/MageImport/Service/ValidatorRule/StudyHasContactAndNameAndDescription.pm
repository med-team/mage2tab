package RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasContactAndNameAndDescription;

use strict 'vars';

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasContactAndNameAndDescription

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyHasContactAndNameAndDescription->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

Require that the StudyVO has a Name, Description, and MainContact.  

These are required by the Gus Schema.

=cut

sub mycheck {
  my ($self, $docRoot) = @_;

  my $study =  $docRoot->getStudyVO();

  unless($study) {
    RAD::MR_T::MageImport::ValidatorException->new("No General Study Information Found")->throw();
  }

  my $studyName = $study->getName();
  my $studyDesc = $study->getDescription();
  my $studyContact = $study->getContact();

  unless($studyName) {
    RAD::MR_T::MageImport::ValidatorException->new("Study NAME is required but not Found")->throw();
  }

  unless($studyDesc) {
    RAD::MR_T::MageImport::ValidatorException->new("Study Description is required but not Found for [$studyName]")->throw();
  }

  unless($studyContact) {
    RAD::MR_T::MageImport::ValidatorException->new("Main Contact for Study is required but WAS NOT SET for [$studyName]")->throw();
  }


  return 1;
}


1;
