package RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyNameNotInGus->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

If the protocols in IDF are existing GUS protocol, the parameters should also exist in GUS.

=cut
#-------------------------------------------------------------------------------

my $gusStudy = {
		name=>"",
		id=>"",
		desc=>"",

};


#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;

  my $study = $docRoot->getStudyVO();

  my $logger = $self->getLogger();

  unless($study) {
    RAD::MR_T::MageImport::ValidatorException->new("No Study Were Found")->throw();
  }

  if(my $gusStudy = $self->_isGusStudy($study->getName)) {
    $logger->warn("Study: ",$study->getName, " name already exist in GUS DB");
    RAD::MR_T::MageImport::ValidatorException->new("study name were already exist in GUS, please see log file for details")->throw();
  }

  return 1;
}

#--------------------------------------------------------------------------------

sub _isGusStudy {
  my ($self, $name) = @_;

  my $url = "http://hera.pcbi.upenn.edu/rad-dev/Querier/php/radStudyQuery.php?name=$name";
  my $resultXML = get($url);
  my $root = XMLin($resultXML);

  return $root->{results}->{study};
}


1;
