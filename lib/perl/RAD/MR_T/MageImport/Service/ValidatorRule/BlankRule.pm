package RAD::MR_T::MageImport::Service::ValidatorRule::BlankRule;

# this is blank rule impl

use strict 'vars';
use Carp;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;

use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

sub check {
 my ($self, $docRoot) = @_;
 $self->getValidator->check($docRoot);
 $self->mycheck($docRoot);
 return ref($self);
}

sub mycheck {
  my ($self, $docRoot) = @_;
  return 1;
}
1;
