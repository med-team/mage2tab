package RAD::MR_T::MageImport::Service::ValidatorRule::CheckExistingGusProtocols;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::CheckExistingGusProtocols

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::CheckExistingGusProtocols->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

If the protocols in IDF are existing GUS protocol, the parameters should also exist in GUS.

=cut
#-------------------------------------------------------------------------------

my $gusProtocol = {
		   name=>"",
		   param=>[
			   {name=>[]}
			  ],

};


#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;

  my $protocols = $docRoot->getProtocolVOs();

  my $logger = $self->getLogger();

  unless($protocols) {
    RAD::MR_T::MageImport::ValidatorException->new("No Protocols Were Found")->throw();
  }

  my $failed = 0;

  PROTOCOL: foreach my $protocol (@$protocols) {
    my $name = $protocol->getName();

    if(my $gusProtocol = $self->_isGusProtocol($name)) {
      my $params = $protocol->getParams;

      next PROTOCOL unless $params;

      PARAM: foreach my $param (@$params){
	if( !$gusProtocol->{param}->{name}){
	  $logger->warn("Protocol: ".$protocol->getName." parameter: ".$param->getName." doesn't exist in GUS protocol, ".$gusProtocol->{name});
	  $failed = 1;
	  next PARAM;
	}

	foreach my $param (@{$gusProtocol->{param}->{name}}){
	  if($param eq $param->getName){next PARAM;}
	}

	$failed = 1;
	$logger->warn("Protocol: ".$protocol->getName. " parameter: ". $param->getName. " doesn't exist in GUS protocol, ".$gusProtocol->{name});
      }
    }
  }

  RAD::MR_T::MageImport::ValidatorException->new("Protocol parameters were not found in GUS, please see log file for details")->throw() if $failed;

  return 1;
}

#--------------------------------------------------------------------------------

sub _isGusProtocol {
  my ($self, $name) = @_;

  my $url = "http://hera.pcbi.upenn.edu/rad-dev/Querier/php/radProtocolQuery.php?name=$name";
  my $resultXML = get($url);
  my $root = XMLin($resultXML);

  return $root->{results}->{protocol};
}


1;
