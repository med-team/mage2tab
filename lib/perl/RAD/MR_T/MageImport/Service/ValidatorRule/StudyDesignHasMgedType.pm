package RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType;

use strict 'vars';
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;

use RAD::MR_T::MageImport::VO::DocRoot;
use RAD::MR_T::MageImport::Service::AbstractValidatorRule;


use vars qw / @ISA / ;
@ISA = qw / RAD::MR_T::MageImport::Service::AbstractValidatorRule/;

=head1 NAME

RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType

=head1 SYNOPSIS

 my $validator = RAD::MR_T::MageImport::Service::Validator->new;
 my $decoValidator = RAD::MR_T::MageImport::Service::ValidatorRule::StudyDesignHasMgedType->new($validator);

 $decoValidator->check($docRoot);

=head1 DESCRIPTION

The StudyDesignHasMgedType should be coming from the MGED Ontology

This is Required by the Gus Schema

=cut

#--------------------------------------------------------------------------------

# How else could we get these???  Ideally we don't want to connect to 
# our database instance.... Another option:  there is Existing Java code 
# in GUS/Supported to parse an MGED Owl file.  The terms listed below 
# are very stable and that may not be necessary
my $studyDesignTypes = [
          'sex_design',
          'organism_part_comparison_design',
          'cell_cycle_design',
          'species_design',
          'development_or_differentiation_design',
          'strain_or_line_design',
          'cell_type_comparison_design',
          'is_expressed_design',
          'unknown_experiment_design_type',
          'organism_status_design',
          'individual_genetic_characteristics_design',
          'physiological_process_design',
          'cell_component_comparison_design',
          'cellular_process_design',
          'imprinting_design',
          'innate_behavior_design',
          'family_history_design',
          'clinical_history_design',
          'unknown_experiment_design_type',
          'disease_state_design',
          'genotyping_design',
          'secreted_protein_identification_design',
          'binding_site_identification_design',
          'RNA_stability_design',
          'translational_bias_design',
          'co-expression_design',
          'operon_identification_design',
          'transcript_identification_design',
          'comparative_genome_hybridization_design',
          'unknown_experiment_design_type',
          'tiling_path_design',
          'replicate_design',
          'quality_control_testing_design',
          'normalization_testing_design',
          'time_series_design',
          'software_variation_design',
          'dye_swap_design',
          'hardware_variation_design',
          'self_vs_self_design',
          'loop_design',
          'reference_design',
          'all_pairs',
          'operator_variation_design',
          'optimization_design',
          'ex_vivo_design',
          'unknown_experiment_design_type',
          'array_platform_variation_design',
          'in_vivo_design',
          'in_vitro_design',
          'dose_response_design',
          'stimulus_or_stress_design',
          'pathogenicity_design',
          'injury_design',
          'disease_state_design',
          'compound_treatment_design',
          'growth_condition_design',
          'genetic_modification_design',
          'cellular_modification_design',
          'stimulated_design_type',
          'unknown_experiment_design_type',
          'non-targeted_transgenic_variation_design',
          'comparative_genome_hybridization_design',
          'binding_site_identification_design',
          'transcript_identification_design',
          'cellular_modification_design'
        ];

#--------------------------------------------------------------------------------

sub mycheck {
  my ($self, $docRoot) = @_;
  my $logger = $self->getLogger();

  $self->_setStudyDesignTypes;

  my $studyDesigns = $docRoot->getStudyVO()->getDesigns;

  unless($studyDesigns) {
    $logger->warn("No study designs Were Found");
    RAD::MR_T::MageImport::ValidatorException->new("No study designs Were Found")->throw();
  }

  foreach my $studyDesign (@$studyDesigns) {
    my $name = $studyDesign->getName();
    my $types = $studyDesign->getTypes();
    next unless $types;
    foreach my $type (@$types){
      my $typeValue = $type->getValue();
      unless($self->_isMgedTerm($typeValue)) {
	$logger->warn("DesignType [$typeValue] for Design [$name] is not supported by MGED");
	RAD::MR_T::MageImport::ValidatorException->new("DesignType [$typeValue] for Design [$name] is not supported by MGED")->throw();
      }
    }
  }

  return 1;
}

#--------------------------------------------------------------------------------

sub _isMgedTerm {
  my ($self, $value) = @_;

  my $logger = $self->getLogger();
  foreach my $mged (@$studyDesignTypes) {
    if($mged eq $value) {
      return 1;
    }
  }
  return 0;
}

sub _setStudyDesignTypes {
 my $url = "http://hera.pcbi.upenn.edu/rad-dev/Querier/php/moTermQuery.php?category=ExperimentDesignType";
 my $resultXML = get($url);

 my $root = XMLin($resultXML);

 $studyDesignTypes = $root->{results}->{term} if $root->{results}->{term};

}


1;
