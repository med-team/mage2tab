package RAD::MR_T::MageImport::ServiceFactory;

use strict;
use Error qw(:try);
use Data::Dumper;

use RAD::MR_T::MageImport::MageImportError;


=head1

=over

=item new construct

side effect: createDefaultDatabase sounds tricky here, but 
             there is a case that people can run the importer outside of ga 
             also we try to refactor the DbiDatabase creations from the test case classes

=cut

sub new {
  my $class = shift;
  my $arg = shift;
  my $self = bless {_config=>$arg}, $class;

  if($self->{_config}->{nolog}){
    $self->setLogger(_Logger->new());
  }
  else{
    my $sLogger;
    eval "{use Log::Log4perl qw(get_logger :levels)}";

    if($@){
      $sLogger = _Logger->new();
    }
    else{
      Log::Log4perl->init_once("$ENV{GUS_HOME}/config/log4perl.config");
      $sLogger = get_logger("RAD::MR_T::MageImport::Service");
    }

    $self->setLogger($sLogger);
  }

  $self->_createAllServices($self->{_config});

  if(my $container = $self->{_config}->{container}){
    $self->_createDefaultDatabase($container->{property}->{value}) if  $container->{id} eq 'database' && $container->{property};
  }

  $self->_createLoggers();
  return $self;
}

=head1 createAllSerivices

 this will read in config and create all services
 side effect: what if people only have one service the config hash will be totally different
              assume there are always minimum two services defined

=cut

sub _createAllServices {
  my ($self, $config) = @_;
  my $services = $self->{_config}->{service};

  foreach my $service ( keys %$services){
    my $creator = "_create".ucfirst($service);

    $self->{$service} = $self->$creator();
  }
}

=item _createDefaultDatabase($gusconfig)

to-dos: 1)should we move this function to utility class
        2)$ENV{GUS_HOME}/config/gus.config cann't pass through XML config

=cut

sub _createDefaultDatabase {
  my ($self, $gusconfigFile) = @_;

  require GUS::ObjRelP::DbiDatabase;
  require GUS::Supported::GusConfig;

  $gusconfigFile = "$ENV{GUS_HOME}/config/gus.config";
# unless $gusconfigFile;

  my $config = GUS::Supported::GusConfig->new($gusconfigFile);
  my $login       = $config->getDatabaseLogin();
  my $password    = $config->getDatabasePassword();
  my $core        = $config->getCoreSchemaName();
  my $dbiDsn      = $config->getDbiDsn();
  my $oraDfltRbs  = $config->getOracleDefaultRollbackSegment();

  my $database;
  unless($database = GUS::ObjRelP::DbiDatabase->getDefaultDatabase()) {
    $database = GUS::ObjRelP::DbiDatabase->new($dbiDsn, $login, $password,
                                                  0,0,1,$core, $oraDfltRbs);
  }

  $self->{_dbi_database} = $database;
}

sub getServiceByName{
  my ($self, $name) = @_;
  return $self->{$name};
}

sub _createReader {
  my $self = shift;

  my $readerConfig = $self->{_config}->{service}->{reader};

  my $property = $readerConfig->{property};
  my $args = $self->getArgsFromPropertyHash($property);

  #Temp------------
  # Remove these lines and substitute $args into the new() below...
  my @keys = keys(%$args);
  my $key = $keys[0];
  my $magefile = $args->{$key};
  #Temp------------

  my $readerClass = $readerConfig->{class};

  eval "require $readerClass";

  my $reader = eval {
    $readerClass->new($magefile);
  };

  RAD::MR_T::MageImport::MageImportReaderNotCreatedError->new(ref($self)."->createReader: ".$readerClass. "\n".$@)->throw() if $@;

  return $reader;
}

sub _createTester {
  my $self = shift;

  my $testerConfig = $self->{_config}->{service}->{tester};
  my $property = $testerConfig->{property};
  my $args = $self->getArgsFromPropertyHash($property);

  #Temp------------
  # Remove these lines and substitute $args into the new() below...
  my @keys = keys(%$args);
  my $key = $keys[0];
  my $file = $args->{$key};
  #Temp------------

  my $testerClass = $testerConfig->{class};
  eval "require $testerClass"; 
  my $tester = eval {
    $testerClass->new($file);
  };

  RAD::MR_T::MageImport::ServiceFactoryException::TesterNotCreatedError->new(ref($self)."->createTester: $testerClass")->throw() if $@;

  return $tester;
}


sub _createReporter {
  my $self = shift;

  my $reporterConfig = $self->{_config}->{service}->{reporter};
  my $property = $reporterConfig->{property};
  my $args = $self->getArgsFromPropertyHash($property);

  #Temp------------
  # Remove these lines and substitute $args into the new() below...
  my @keys = keys(%$args);
  my $key = $keys[0];
  my $file = $args->{$key};
  #Temp------------

  my $reporterClass = $reporterConfig->{class};
  eval "require $reporterClass"; 

  my $reporter = eval {
    $reporterClass->new($file); 
  };

  RAD::MR_T::MageImport::ServiceFactoryException::ReporterNotCreatedError->new(ref($self)."->createReporter: $reporterClass")->throw() if $@;

  return $reporter;
}


sub _createTranslator {
  my $self = shift;

  my $tConfig = $self->{_config}->{service}->{translator};
  my $property = $tConfig->{property};
  my $args = $self->getArgsFromPropertyHash($property);

  my $tClass = $tConfig->{class};

  eval "require $tClass";

  my $translator = eval { 
    $tClass->new($args)
  };

  RAD::MR_T::MageImport::ServiceFactoryException->new(ref($self)."->createTransltor: $tClass\n" . $@)->throw() if $@;

  return $translator;
}


sub _createValidator {
  my $self = shift;

  my $validatorConfig = $self->{_config}->{service}->{validator};
  my $validatorBaseClass = $validatorConfig->{baseClass};
  my $require_p = "{require $validatorBaseClass; $validatorBaseClass->new }";
  my $validator = eval $require_p;
  RAD::MR_T::MageImport::MageImportValidatorNotCreatedError->new(ref($self)."->createValidator: ".$require_p)->throw() if $@;

# a temporary solution for now
  $validator->setLogger($self->getLogger);

  if($validatorConfig->{decorProperties}){
    my $property = $validatorConfig->{decorProperties};
    my $args = $self->getArgsFromPropertyHash($property);

    #Temp------------
    # Remove these lines and substitute $args into the new() below...
    my @keys = keys(%$args);
    my $key = $keys[0];
    my $rulesStr = $args->{$key};
    #Temp------------

    my @rules = split(',', $rulesStr);
    if(@rules){
      foreach my $rule (@rules){
	$validator = $self->_decoValidator($rule, $validator);
	$validator->setLogger($self->getLogger);
      }
    }
  }
  return $validator;
}

sub _decoValidator {
  my ($self, $rule, $validator) = @_;

  my $ruleClass = "RAD::MR_T::MageImport::Service::ValidatorRule::$rule";

  $ruleClass =~ s/\.pm$//;
 eval "require $ruleClass";

  my $decoValidator = $ruleClass->new($validator);

#  my $decoValidator= eval "require $ruleClass; $ruleClass->new($validator)";

  RAD::MR_T::MageImport::MageImportValidatorNotCreatedError->new(ref($self)."->decoValidator: ".$ruleClass.$@)->throw() if $@;
  return $decoValidator;
}


sub _createProcessor {
  my $self = shift;

  my $processorConfig = $self->{_config}->{service}->{processor};
  my $processorBaseClass = $processorConfig->{baseClass};
  my $require_p = "{require $processorBaseClass; $processorBaseClass->new }";
  my $processor = eval $require_p;
  RAD::MR_T::MageImport::ServiceFactoryException::MageImportProcessorNotCreatedError->new(ref($self)."->createProcessor: ".$require_p)->throw() if $@;

# a temporary solution for now this code stinks!!!
  $processor->setLogger ($self->getLogger);

  if($processorConfig->{decorProperties}){
    my $property = $processorConfig->{decorProperties};
    my $decoPropArgs = $self->getArgsFromPropertyHash($property);

    #Temp------------
    my @keys = keys(%$decoPropArgs);
    my $key = $keys[0];
    my $rulesStr = $decoPropArgs->{$key};
    #Temp------------

    my @rules = split(',', $rulesStr);
    if(@rules){
      foreach my $rule (@rules){
	my $ruleConstructArg;

        my $ruleConstructArg;
	if(my $decoProperty = $self->{_config}->{serviceDeco}->{$rule}->{property}){
          my $args = $self->getArgsFromPropertyHash($decoProperty);

          #Temp------------
          # Remove these lines and substitute $args into the new() below...
          my @keys = keys(%$args);
          my $key = $keys[0];
          $ruleConstructArg = $args->{$key};
          #Temp------------

	}

	$processor = $self->_decoProcessor($rule, $processor, $ruleConstructArg);
      }
    }
  }
  return $processor;
}

sub _decoProcessor {
  my ($self, $rule, $processor, $ruleConArg) = @_;

  my $ruleClass = "RAD::MR_T::MageImport::Service::ProcessModule::$rule";

  #print "RULE_CLASS=$ruleClass\n";

  my $require_p = "{require $ruleClass;}";
  eval $require_p;

  if($@) {
    die $@;
    
  }

  #print "PAST EVAL\n";
  #print "Processor=".$processor."\n";
  #print "ruleconarg=".$ruleConArg."\n";

  my $decoProcessor = $ruleClass->new($processor, $ruleConArg);

#  my $decoProcessor= eval "require $ruleClass; $ruleClass->new($processor)";

  RAD::MR_T::MageImport::ServiceFactoryException::MageImportProcessorNotCreatedError->new(ref($self)."->decoProcessor: ".$require_p.$@)->throw() if $@;
  return $decoProcessor;
}


=item createLogger

to-dos:  this is not the best place for this method, should refactor it to 
         a ConfigManager class

side-effect: assume log4perl.conf is in the current dir

=cut

sub _createLoggers {
  my $self= shift;

  my $sLogger = $self->getLogger;

  foreach my $service ( keys %{$self->{_config}->{service}}){
    $self->{$service}->setLogger($sLogger);
  }
}

sub getArgsFromPropertyHash {
  my ($self, $property) = @_;

  my $args = {};
  foreach(keys %$property) {
    $args->{$_} = $property->{$_}->{value};
  }
  return $args;
}

sub setLogger {$_[0]->{_logger} = $_[1]}

sub getLogger {$_[0]->{_logger}}

package _Logger;

sub new {my $self = shift;}
sub debug {my $self = shift;}
sub fatal {my $self = shift;}
sub info {my $self = shift;}
sub warn {my $self = shift;  print "@_\n";}


1;
