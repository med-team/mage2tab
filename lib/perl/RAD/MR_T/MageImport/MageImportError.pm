package RAD::MR_T::MageImport::MageImportError;
use base qw(Error);

#################################################
# $Revision: 1 $ $Date: 2005-10-07 13:42:38 -0400 (Fri, 07 Oct 2005) $ $Author: jbrestel $
#################################################

use overload ('""' => 'stringify');

=head1 RAD::MR_T::MageImport::MageImportError

this is MageImport exception package

=cut

=item new() construct

to-dos: need to generalize the contruct, some of more specific argument should go to sublasses

=cut


sub new {
  my ($self, $m, $expected, $actual) = @_;

  my $name = $self;
  $name =~ s/^.+:://;

  my $text = "**** $name:  $m\n\n";

  if($expected) {
     $text = $text . "(Expected=$expected,Found=$actual)\n\n";
   }

  my @args = ();

  local $Error::Depth = $Error::Depth + 1;
  local $Error::Debug = 1;  # Enables storing of stacktrace

  $self->SUPER::new(-text => $text, @args);
}
1;

package RAD::MR_T::MageImport::ReaderException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

package RAD::MR_T::MageImport::ReaderException::FileError;
use base qw(RAD::MR_T::MageImport::ReaderException);
1;

package RAD::MR_T::MageImport::ReporterException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

package RAD::MR_T::MageImport::ReporterException::SqlError;
use base qw(RAD::MR_T::MageImport::ReporterException);
1;
package RAD::MR_T::MageImport::ReporterException::DefaultDbNotFound;
use base qw(RAD::MR_T::MageImport::ReporterException);
1;
package RAD::MR_T::MageImport::ReporterException::DefaultAlgoInvoIdNotFound;
use base qw(RAD::MR_T::MageImport::ReporterException);
1;

package RAD::MR_T::MageImport::TesterException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

package RAD::MR_T::MageImport::TesterException::SqlError;
use base qw(RAD::MR_T::MageImport::TesterException);
1;
package RAD::MR_T::MageImport::TesterException::DefaultDbNotFound;
use base qw(RAD::MR_T::MageImport::TesterException);
1;
package RAD::MR_T::MageImport::TesterException::DefaultAlgoInvoIdNotFound;
use base qw(RAD::MR_T::MageImport::TesterException);
1;
package RAD::MR_T::MageImport::VOException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;
package RAD::MR_T::MageImport::VOException::ObjectTypeError;
use base qw(RAD::MR_T::MageImport::VOException);
1;
package RAD::MR_T::MageImport::VOException::ArgumentError;
use base qw(RAD::MR_T::MageImport::VOException);
1;


package RAD::MR_T::MageImport::ObjectMapperException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;
package RAD::MR_T::MageImport::ObjectMapperException::ImproperVoObject;
use base qw(RAD::MR_T::MageImport::ObjectMapperException);
1;
package RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError;
use base qw(RAD::MR_T::MageImport::ObjectMapperException);
1;
package RAD::MR_T::MageImport::ObjectMapperException::NonUniqueNameError;
use base qw(RAD::MR_T::MageImport::ObjectMapperException);
1;

package RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError::MultipleRowsReturned;
use base qw(RAD::MR_T::MageImport::ObjectMapperException::RetrieveFromDbError);
1;


package RAD::MR_T::MageImport::ServiceFactoryException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;
package RAD::MR_T::MageImport::ServiceFactoryException::ReaderNotCreatedError;
use base qw(RAD::MR_T::MageImport::ServiceFactoryException);
1;
package RAD::MR_T::MageImport::ServiceFactoryException::ValidatorNotCreatedError;
use base qw(RAD::MR_T::MageImport::ServiceFactoryException);
1;
package RAD::MR_T::MageImport::ServiceFactoryException::ProcessorNotCreatedError;
use base qw(RAD::MR_T::MageImport::ServiceFactoryException);
1;
package RAD::MR_T::MageImport::ServiceFactoryException::TesterNotCreatedError;
use base qw(RAD::MR_T::MageImport::ServiceFactoryException);
1;
package RAD::MR_T::MageImport::ServiceFactoryException::ReporterNotCreatedError;
use base qw(RAD::MR_T::MageImport::ServiceFactoryException);
1;


package RAD::MR_T::MageImport::ValidatorException;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;
package RAD::MR_T::MageImport::ValidatorException::UnNamedVOError;
use base qw(RAD::MR_T::MageImport::ValidatorException);
1;
package RAD::MR_T::MageImport::ValidatorException::VONameNotUniqueError;
use base qw(RAD::MR_T::MageImport::ValidatorException);
1;

=item RAD::MR_T::MageImport::MageImportObjectTypeError

to be deprecated, replaced by RAD::MR_T::MageImport::VOException::ObjectTypeError

=cut

package RAD::MR_T::MageImport::MageImportObjectTypeError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportArgumentError

to be deprecated, replaced by RAD::MR_T::MageImport::VOException::ArgumentError

=cut

package RAD::MR_T::MageImport::MageImportArgumentError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportOENotFoundError

to be deprecated, replaced by RAD::MR_T::MageImport::ObjectMapperException::OENotFoundError

=cut

package RAD::MR_T::MageImport::MageImportOENotFoundError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportExtDbNotFoundError

to be deprecated, replaced by RAD::MR_T::MageImport::ObjectMapperException::ExtDbNotFoundError

=cut

package RAD::MR_T::MageImport::MageImportExtDbNotFoundError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportReaderNotCreatedError

to be deprecated, replaced by RAD::MR_T::MageImport::ServiceFactoryException::ReaderNotCreatedError

=cut

package RAD::MR_T::MageImport::MageImportReaderNotCreatedError;
use base qw(RAD::MR_T::MageImport::MageImportError);

=item RAD::MR_T::MageImport::MageImportValidatorNotCreatedError

to be deprecated, replaced by RAD::MR_T::MageImport::ServiceFactoryException::ValidatorNotCreatedError

=cut

package RAD::MR_T::MageImport::MageImportValidatorNotCreatedError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportUnNamedVOError

to be deprecated, replaced by RAD::MR_T::MageImport::ValidatorException::UnNamedVOError

=cut

package RAD::MR_T::MageImport::MageImportUnNamedVOError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;

=item RAD::MR_T::MageImport::MageImportVONameNotUniqueError

to be deprecated, replaced by RAD::MR_T::MageImport::ValidatorException::VONameNotUniqueError

=cut

package RAD::MR_T::MageImport::MageImportVONameNotUniqueError;
use base qw(RAD::MR_T::MageImport::MageImportError);
1;
