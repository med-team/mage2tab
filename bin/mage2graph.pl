#!/usr/bin/perl
use strict;
use FileHandle;
use Getopt::Long;
use Error qw(:try);

use RAD::MR_T::MageImport::ServiceFactory;

my($help, $magedoc);

&GetOptions("help!" => \$help,
            "usage!" => \$help,
            "h!" => \$help,
            "magedoc=s" => \$magedoc,
           );

&usage() if ($help);
unless ($magedoc ) {

  print STDERR "REQUIRED: --magedoc=$magedoc (the file must have extension in one of them: .idf for MAGE-Tab (if MAGE-Tab is seprated into idf and sdrf files),  .tab|.xls for MAGE-Tab (if MAGE-Tab is a single file) or .xml for MAGE-ML).\n";
  &usage() ;
}

unless ($magedoc =~ /.*\.idf$/ || $magedoc =~ /.*\.tab$/ || $magedoc =~ /\.xls$/ || $magedoc =~ /.*\.xml$/){
  print STDERR "Wrong file extension, the file must have extension in one of them: .idf for MAGE-Tab (if MAGE-Tab is seprated into idf and sdrf files),  .tab|.xls for MAGE-Tab (if MAGE-Tab is a single file) or .xml for MAGE-ML.\n";

  &usage() ;
}


try {
  my $config = {
		'serviceDeco' => {
				  'MergeTreatmentSeriesForSpeciedBiomatAction' => {
										   'baseClass' => 'RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeries',
										   'property' => {'termStr' => {'value' => 'purify'} }
										  }
				 },
		'service' => {
			      'reader' => {'class' => 'RAD::MR_T::MageImport::Service::Reader::MageTabReader'},
			      'processor' => {
					      'decorProperties' => {
								    'modules' => {
										  'value' => 'NamingBioSource,MergeTreatmentSeriesForSpeciedBiomatAction'
										 }
								   },
					      'baseClass' => 'RAD::MR_T::MageImport::Service::Processor'
					     },
			      'translator' => {
					       'class' => 'RAD::MR_T::MageImport::Service::Translator::VoToDot'
					      }
			     },
		'nolog' =>1
	       };

  if ($magedoc =~ /.*\.tab$/ || $magedoc =~ /.*\.idf$/ || $magedoc =~ /\.xls/){
    $config->{service}->{reader}->{class} = 'RAD::MR_T::MageImport::Service::Reader::MageTabReader';
    print STDERR "Using MAGE-Tab reader\n";
  }

  if ($magedoc =~ /.*\.xml$/){
    $config->{service}->{reader}->{class} = 'RAD::MR_T::MageImport::Service::Reader::MagemlReader';
    print STDERR "Using MAGE-ML reader\n";
  }

  my $serviceFactory = RAD::MR_T::MageImport::ServiceFactory->new($config);
  my $reader =  $serviceFactory->getServiceByName('reader');

  $reader->setFile($magedoc);
  my $docRoot = $reader->parse();

  if(my $processor = $serviceFactory->getServiceByName('processor')) {
    $processor->process($docRoot);
  }

  my $translator = $serviceFactory->getServiceByName('translator');

  my $f = $magedoc.".dot";
  open(STDOUT, ">$f") || die "Can't redirect stdout";
  $translator->mapAll($docRoot);
  close(STDOUT);
  print STDERR "Done. Output to file $f\n";

} catch Error with {
  my $e = shift;
  print $e->stacktrace();
  $e->throw();
};

sub usage {
  print STDERR "USAGE:\nmage2graph.pl  --magedoc=$magedoc (the file must have extension in either .tab for MAGE-Tab or .xml for MAGE-ML).\n";
  exit(0);
}

