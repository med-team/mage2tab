#!/usr/bin/perl
use strict;
use FileHandle;
use Getopt::Long;
use Error qw(:try);

use RAD::MR_T::MageImport::ServiceFactory;

my($help, $magedoc);

&GetOptions("help!" => \$help,
            "usage!" => \$help,
            "h!" => \$help,
            "magedoc=s" => \$magedoc,
           );

&usage() if ($help);
unless ($magedoc ) {
  print STDERR "REQUIRED: --magedoc=$magedoc (the file must have extension in either .tab for MAGE-Tab or .xml for MAGE-ML).\n";
  &usage() ;
}

unless ($magedoc =~ /.*\.tab$/ || $magedoc =~ /.*\.xml$/){
  print STDERR "Wrong file extension, the file must have extension in either .tab for MAGE-Tab or .xml for MAGE-ML.\n";
  &usage() ;
}


try {
  my $config = {
		'serviceDeco' => {
				  'MergeTreatmentSeriesForSpeciedBiomatAction' => {
										   'baseClass' => 'RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeries',
										   'property' => {'termStr' => {'value' => 'purify'} }
										  }
				 },
		'service' => {
			      'reader' => {'class' => 'RAD::MR_T::MageImport::Service::Reader::MagemlReader'},
			      'validator' => {
					      'decorProperties' => {
								    'rules' => {
										  'value' => 'StudyNameNotInGus,StudyHasContactAndNameAndDescription,StudyHasDesignsAndFactors,StudyDesignHasMgedType,CheckExistingGusProtocols,ProtocolHasMgedType,AssayHasContact,AssayHasLexAndNoHangingLex,NoHangingBioMaterials,AssayHasFactorValue,CheckDataFiles'
										 }
								   },
					      'baseClass' => 'RAD::MR_T::MageImport::Service::Validator'
					     },
			     },
		'nolog' =>1
	       };

  if ($magedoc =~ /.*\.tab$/){
    $config->{service}->{reader}->{class} = 'RAD::MR_T::MageImport::Service::Reader::MageTabReader';
    print STDERR "Using MAGE-Tab reader\n";
  }

  if ($magedoc =~ /.*\.xml$/){
    $config->{service}->{reader}->{class} = 'RAD::MR_T::MageImport::Service::Reader::MagemlReader';
    print STDERR "Using MAGE-ML reader\n";
  }

  my $serviceFactory = RAD::MR_T::MageImport::ServiceFactory->new($config);
  my $reader =  $serviceFactory->getServiceByName('reader');

  $reader->setFile($magedoc);
  my $docRoot = $reader->parse();

  my $f = $magedoc."-validation.txt";
  open(STDOUT, ">$f") || die "Can't redirect stdout";
  if(my $processor = $serviceFactory->getServiceByName('validator')) {
    try{
      $processor->check($docRoot);
    }
      catch Error with {
	my $e = shift;
	print "Error:", $e->text(), "\n";
	$processor->setPassed(0);
      };
    print STDERR "Validations Failed\n" unless $processor->isPassed;
  }
  close(STDOUT);


  print STDERR "Done. Output to file $f\n";

} catch Error with {
  my $e = shift;
  print $e->stacktrace();
  $e->throw();
};

sub usage {
  print STDERR "USAGE:\nmage-checker.pl  --magedoc=$magedoc (the file must have extension in either .tab for MAGE-Tab or .xml for MAGE-ML).\n";
  exit(0);
}

