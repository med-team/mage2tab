#!/usr/bin/perl
use strict;
use FileHandle;
use Getopt::Long;
use Error qw(:try);

use RAD::MR_T::MageImport::ServiceFactory;

my($help, $mageml);

&GetOptions("help!" => \$help,
            "usage!" => \$help,
            "h!" => \$help,
            "mageml=s" => \$mageml,
           );

&usage() if ($help);
unless ($mageml ) {
  print STDERR "REQUIRED: --mageml=$mageml.\n";
  &usage() ;
}


try {
  my $config = {
		'serviceDeco' => {
				  'MergeTreatmentSeriesForSpeciedBiomatAction' => {
										   'baseClass' => 'RAD::MR_T::MageImport::Service::ProcessModule::MergeTreatmentSeries',
										   'property' => {'termStr' => {'value' => 'purify'} }
										  }
				 },
		'service' => {
			      'reader' => {'class' => 'RAD::MR_T::MageImport::Service::Reader::MagemlReader'},
			      'processor' => {
					      'decorProperties' => {
								    'modules' => {
										  'value' => 'NamingBioSource,MergeTreatmentSeriesForSpeciedBiomatAction'
										 }
								   },
					      'baseClass' => 'RAD::MR_T::MageImport::Service::Processor'
					     },
			      'translator' => {
					       'class' => 'RAD::MR_T::MageImport::Service::Translator::VoToMageTab'
					      }
			     },
		'nolog' =>1
	       };

  if ($mageml =~ /.*\.tab$/ || $mageml =~ /.*\.idf$/ || $mageml =~ /\.xls/){
    $config->{service}->{reader}->{class} = 'RAD::MR_T::MageImport::Service::Reader::MageTabReader';
    print STDERR "Using MAGE-Tab reader\n";
  }

  my $serviceFactory = RAD::MR_T::MageImport::ServiceFactory->new($config);

  my $reader =  $serviceFactory->getServiceByName('reader');

  $reader->setFile($mageml);
  my $docRoot = $reader->parse();

  if(my $processor = $serviceFactory->getServiceByName('processor')) {
    $processor->process($docRoot);
  }

  my $translator = $serviceFactory->getServiceByName('translator');

  my $f = $mageml.".idf";
  open(STDOUT, ">$f") || die "Can't redirect stdout";
  print STDOUT "SDRF File\t$mageml".".sdrf\n";
 # $translator->mapAll($docRoot);
  $translator->writeIDF($docRoot);
  close(STDOUT);

  $f = $mageml.".sdrf";
  open(STDOUT, ">$f") || die "Can't redirect stdout";
  $translator->writeSDRF($docRoot);
  close(STDOUT);

  print STDERR "Done. Output to file $mageml.idf and $mageml.sdrf\n";

} catch Error with {
  my $e = shift;
  print $e->stacktrace();
  $e->throw();
};

sub usage {
  print STDERR "USAGE:\nmage2tab.pl  --mageml='MAGE-ML file to be converted'\n";
  exit(0);
}

